﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using ConquerEmulator.Client;
using ConquerEmulator.Events;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.DMap;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Mob;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Database
{
    internal class MySqlDatabase
    {
        public static IniFile Config;

        #region Warehouses
        public static void LoadWarehouses(GameClient Client)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var Command = new MySqlCommand("SELECT * FROM warehouses WHERE Name = @name LIMIT 1", m_Conn);
                Command.Parameters.AddWithValue("@name", Client.Hero.Name);

                using (var rdr = Command.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        Client.Warehouse.Password = rdr["Password"].ToString();
                        Client.Warehouse.Money = int.Parse(rdr["Money"].ToString());
                        Client.Warehouse.Warehouses[(uint)WhIDs.TwinCity] = ConquerItem.StringToDictionary(rdr["Twin"].ToString());
                        Client.Warehouse.Warehouses[(uint)WhIDs.Phoenix] = ConquerItem.StringToDictionary(rdr["Phoenix"].ToString());
                        Client.Warehouse.Warehouses[(uint)WhIDs.Ape] = ConquerItem.StringToDictionary(rdr["Ape"].ToString());
                        Client.Warehouse.Warehouses[(uint)WhIDs.Desert] = ConquerItem.StringToDictionary(rdr["Desert"].ToString());
                        Client.Warehouse.Warehouses[(uint)WhIDs.Bird] = ConquerItem.StringToDictionary(rdr["Bird"].ToString());
                        Client.Warehouse.Warehouses[(uint)WhIDs.Market] = ConquerItem.StringToDictionary(rdr["Market"].ToString());
                        Client.Warehouse.Warehouses[(uint)WhIDs.Mobile] = ConquerItem.StringToDictionary(rdr["Mobile"].ToString());
                    }
                }
            }
        }
        #endregion
        #region Npc
        public static void LoadNpcs()
        {
            try
            {
                ushort Count = 0;
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var Command = new MySqlCommand("SELECT * FROM cq_npc", m_Conn);
                    using (var rdr = Command.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            INpc Npc = new NpcSpawnPacket();
                            Npc.UID = uint.Parse(rdr["id"].ToString());
                            Npc.X = ushort.Parse(rdr["cellx"].ToString());
                            Npc.Y = ushort.Parse(rdr["celly"].ToString());
                            Npc.Type = uint.Parse(rdr["lookface"].ToString());
                            Npc.Interaction = ushort.Parse(rdr["type"].ToString());
                            Npc.StatusFlag = uint.Parse(rdr["sort"].ToString());
                            Npc.MapID = new Map(ushort.Parse(rdr["mapid"].ToString()));
                            Npc.Avatar = ushort.Parse(rdr["avatar"].ToString());

                            GetNpcDictionary:
                            Dictionary<uint, INpc> npcs;
                            if (Kernel.Npcs.TryGetValue(Npc.MapID.ID, out npcs))
                                npcs.Add(Npc.UID, Npc);
                            else
                            {
                                npcs = new Dictionary<uint, INpc>();
                                Kernel.Npcs.Add(Npc.MapID.ID, npcs);
                                goto GetNpcDictionary;
                            }
                            Count++;
                        }
                    }
                }
                Console.WriteLine("Loaded {0} Npcs.", Count);
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }
        #endregion
        #region GuildMonsters
        public static void LoadGuildMonsters()
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var Command = new MySqlCommand("SELECT * FROM guildmobs", m_Conn);
                    using (var rdr = Command.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var Monster = new SOBMonster((EntityFlag)byte.Parse(rdr["Type"].ToString()));

                            Monster.UID = uint.Parse(rdr["UID"].ToString());
                            Monster.MaxHitpoints = int.Parse(rdr["Hitpoints"].ToString());
                            Monster.Hitpoints = Monster.MaxHitpoints;
                            Monster.MapID = new Map(1038);
                            Monster.X = ushort.Parse(rdr["X"].ToString());
                            Monster.Y = ushort.Parse(rdr["Y"].ToString());
                            Monster.Mesh = uint.Parse(rdr["Mesh"].ToString());
                            Monster.Facing = (ConquerAngle)byte.Parse(rdr["Facing"].ToString());
                            GuildWar.GuildMobs.TryAdd(Monster.UID, Monster);
                        }
                    }
                }
                Console.WriteLine("Loaded " + GuildWar.GuildMobs.Count + " guild monsters.");
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }
        #endregion
        #region Connection
        public static string myAcc, myPass, myIP, myDB, ServerIP, ServerVersion;
        public static ushort myPort, GPort;
        public static bool isPVP;

        public static string ConnectionString;

        public static bool CreateConnection()
        {
            ConnectionString = "Server='" + myIP + "';Port='" + myPort + "';Database='" + myDB + "';Username='" + myAcc + "';Password='" + myPass + "';Pooling=true; Max Pool Size = 100; Min Pool Size = 25; SslMode=none";

            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();
                    m_Conn.Close();
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        #endregion
        #region Associates
        public static void AddEnemy(GameClient Client, uint UID, string Name)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var cmd = new MySqlCommand("INSERT INTO `enemies` (ownername, enemyuid, enemyname) VALUES (@name, @enemyUID, @enemyname)", m_Conn);
                cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                cmd.Parameters.AddWithValue("@enemyUID", UID.ToString());
                cmd.Parameters.AddWithValue("@enemyname", Name);
                cmd.ExecuteNonQuery();
            }
        }
        public static void AddFriend(GameClient Client, uint UID, string Name)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var cmd = new MySqlCommand("INSERT INTO `friends` (ownername, frienduid, friendname) VALUES (@name, @friendUID, @friendname)", m_Conn);
                    cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                    cmd.Parameters.AddWithValue("@friendUID", UID.ToString());
                    cmd.Parameters.AddWithValue("@friendname", Name);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }
        public static void DeleteEnemey(uint UID, string Name)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var cmd = new MySqlCommand("DELETE FROM `enemies` WHERE `ownername` = @name AND enemyuid = @enemyUID", m_Conn);
                    cmd.Parameters.AddWithValue("@name", Name);
                    cmd.Parameters.AddWithValue("@enemyUID", UID.ToString());
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }
        public static void DeleteFriend(uint UID, string Name)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var cmd = new MySqlCommand("DELETE FROM `friends` WHERE `ownername` = @name AND frienduid = @friendUID", m_Conn);
                    cmd.Parameters.AddWithValue("@name", Name);
                    cmd.Parameters.AddWithValue("@friendUID", UID.ToString());
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }
        public static void LoadFriends(GameClient Client)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var Command = new MySqlCommand("SELECT * FROM friends WHERE ownername = @name", m_Conn);
                Command.Parameters.AddWithValue("@name", Client.Hero.Name);

                using (var rdr = Command.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        IAssociate associate = AssociatePacket.Create();
                        associate.UID = uint.Parse(rdr["frienduid"].ToString());
                        associate.Name = rdr["friendname"].ToString();
                        Client.Friends.TryAdd(associate.UID, associate);
                    }
                    rdr.Close();
                }
            }
        }

        public static void LoadEnemies(GameClient Client)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var Command = new MySqlCommand("SELECT * FROM enemies WHERE ownername = @name", m_Conn);
                Command.Parameters.AddWithValue("@name", Client.Hero.Name);

                using (var rdr = Command.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        IAssociate associate = AssociatePacket.Create();
                        associate.UID = uint.Parse(rdr["enemyuid"].ToString());
                        associate.Name = rdr["enemyname"].ToString();
                        Client.Enemies.TryAdd(associate.UID, associate);
                    }
                }
            }
        }
        #endregion
        #region Account / Character
        public static bool Authenticate(GameClient Client, uint UID, uint PasswordCheckSum)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var Command = new MySqlCommand("SELECT * FROM accounts WHERE Id = @ID", m_Conn);
                Command.Parameters.AddWithValue("@ID", UID);
                using (var rdr = Command.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        Client.Account = rdr["Name"].ToString();
                        Client.Password = rdr["Password"].ToString();

                        if ((uint)NativeExtended.GetHashCode32(Client.Password) != PasswordCheckSum)
                            return false;

                        Client.LoginAction = (LoginAction)Convert.ToInt16(rdr["LoginAction"]);
                        if (Client.LoginAction == LoginAction.Banned)
                            return false;

                        Client.Flag = (PlayerFlag)Convert.ToInt16(rdr["Flag"]);
                        rdr.Close();
                    }
                }
            }
            return true;
        }

        public static bool CharacterExists(GameClient Client, string EntityName)
        {
            bool Exist;
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var Command = new MySqlCommand("SELECT * FROM `characters` WHERE `Name` = @name LIMIT 1", m_Conn);
                Command.Parameters.AddWithValue("@name", EntityName);

                using (var rdr = Command.ExecuteReader())
                {
                    Exist = rdr.Read();
                }
            }
            return Exist;
        }

        public static void BanCharacter(GameClient client)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var cmd = new MySqlCommand("UPDATE `accounts` SET `LoginAction` = @loginAction WHERE `Name` = @account LIMIT 1", m_Conn);
                cmd.Parameters.AddWithValue("@loginAction", LoginAction.Banned);
                cmd.Parameters.AddWithValue("@account", client.Account);
                cmd.ExecuteNonQuery();
            }
        }

        public static bool CreateCharacter(string Account, string Name, byte Profession, ushort Sex, byte Avatar)
        {
            var Job = "";
            var Stats = new byte[4];
            Stats[0] = (byte)FlatDatabase.StatPoints.ReadInt32(Job, "Strength[1]", 1);
            Stats[1] = (byte)FlatDatabase.StatPoints.ReadInt32(Job, "Agility[1]", 1);
            Stats[2] = (byte)FlatDatabase.StatPoints.ReadInt32(Job, "Vitality[1]", 1);
            Stats[3] = (byte)FlatDatabase.StatPoints.ReadInt32(Job, "Spirit[1]", 1);
            var Hp = (uint)(Stats[2] * 24 + Stats[0] * 3 + Stats[1] * 3 + Stats[3] * 3);
            var Mana = (uint)(Stats[3] * 5);

            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();
                #region Account
                var cmd = new MySqlCommand("UPDATE `accounts` SET `LoginAction` = @loginAction, `Character` = @name WHERE `Name` = @account", m_Conn);
                cmd.Parameters.AddWithValue("@loginAction", 1);
                cmd.Parameters.AddWithValue("@name", Name);
                cmd.Parameters.AddWithValue("@account", Account);
                cmd.ExecuteNonQuery();
                #endregion
                #region Character
                cmd = new MySqlCommand("INSERT INTO `characters` (Account, Name, MapId, X, Y, Sex,"
                    + " Avatar, Strength, Vitality, Agility, Spirit, Hp, Mana,"
                    + " HairStyle, Profession, Money)"
                    + " Values (@account, @name, @map, @x, @y, @sex, @avatar," + " @strength, @vitality, @agility, @spirit,"
                    + " @HP, @mana, @hair, @profession, @money)", m_Conn);

                cmd.Parameters.AddWithValue("@account", Account);
                cmd.Parameters.AddWithValue("@name", Name);
                cmd.Parameters.AddWithValue("@map", 1010);
                cmd.Parameters.AddWithValue("@x", 61);
                cmd.Parameters.AddWithValue("@y", 109);
                cmd.Parameters.AddWithValue("@sex", Sex);
                cmd.Parameters.AddWithValue("@avatar", Avatar);
                cmd.Parameters.AddWithValue("@strength", Stats[0]);
                cmd.Parameters.AddWithValue("@vitality", Stats[2]);
                cmd.Parameters.AddWithValue("@agility", Stats[1]);
                cmd.Parameters.AddWithValue("@spirit", Stats[3]);
                cmd.Parameters.AddWithValue("@HP", Hp);
                cmd.Parameters.AddWithValue("@mana", Mana);
                cmd.Parameters.AddWithValue("@hair", 410);
                cmd.Parameters.AddWithValue("@profession", Profession);
                cmd.Parameters.AddWithValue("@money", 100);
                cmd.ExecuteNonQuery();
                #endregion
                #region Warehouses
                cmd = new MySqlCommand("INSERT INTO `warehouses` (Name, Money, Twin, Phoenix, Ape, Desert, Bird, Stone, Market, Mobile)" + " VALUES (@name, 0, 0, 0, 0, 0, 0, 0, 0, 0)", m_Conn);

                cmd.Parameters.AddWithValue("@name", Name);
                cmd.ExecuteNonQuery();
                #endregion
            }
            #region Starter Items
            AddItem(Name, 132303);
            for (var i = 0; i < 3; i++)
                AddItem(Name, 1000000);

            switch (Profession)
            {
                case 10:
                case 20: AddItem(Name, 410301); break;
                case 40: AddItem(Name, 500301); break;
                case 100: AddItem(Name, 421301); break;
            }
            #endregion
            return true;
        }

        public static void LoadCharacter(GameClient Result)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var Command = new MySqlCommand("SELECT I.*, IFNULL(J.ID, 0) as SpouseID, IFNULL(J.House, 0) as SpouseHouse FROM characters as I Left Join characters J On J.`name` = I.Spouse WHERE I.Account = @account LIMIT 1", m_Conn);
                    Command.Parameters.AddWithValue("@account", Result.Account);
                    #region Load Character
                    using (var rdr = Command.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            Result.Hero = new Entity(EntityFlag.Player, Result);
                            Result.Hero.UID = uint.Parse(rdr["Id"].ToString());
                            Result.Hero.Name = rdr["Name"].ToString();

                            ConquerDMap newDMap;
                            ConquerDMap.Maps.TryGetValue(ushort.Parse(rdr["MapID"].ToString()), out newDMap);
                            if (newDMap != null)
                            {
                                Result.Hero.MapID = new Map(newDMap.MapID, newDMap.MapID.MapDoc);
                                Result.Hero.X = ushort.Parse(rdr["X"].ToString());
                                Result.Hero.Y = ushort.Parse(rdr["Y"].ToString());
                            }
                            else
                            {
                                Result.Hero.MapID = new Map(1002);
                                Result.Hero.X = 400;
                                Result.Hero.Y = 400;
                            }

                            Result.Hero.Level = byte.Parse(rdr["Level"].ToString());
                            Result.Hero.Sex = ushort.Parse(rdr["Sex"].ToString());
                            Result.Hero.Avatar = byte.Parse(rdr["Avatar"].ToString());
                            Result.Hero.Hitpoints = int.Parse(rdr["Hp"].ToString());
                            Result.Mana = ushort.Parse(rdr["Mana"].ToString());
                            Result.Silvers = uint.Parse(rdr["Money"].ToString());
                            Result.ConquerPoints = uint.Parse(rdr["ConquerPts"].ToString());
                            Result.PrizeNPC = int.Parse(rdr["Prize"].ToString());
                            Result.Hero.Hairstyle = ushort.Parse(rdr["Hairstyle"].ToString());
                            Result.Spouse = rdr["Spouse"].ToString();
                            Result.SpouseUID = uint.Parse(rdr["SpouseID"].ToString());
                            Result.Job = byte.Parse(rdr["Profession"].ToString());
                            Result.Hero.Reborn = ushort.Parse(rdr["Reborn"].ToString());
                            Result.VirtuePoints = uint.Parse(rdr["VirtuePoints"].ToString());
                            Result.HouseLevel = int.Parse(rdr["House"].ToString());
                            Result.SpouseHouseLevel = int.Parse(rdr["SpouseHouse"].ToString());

                            if (Result.Hero.Reborn > 0)
                            {
                                Result.Strength = ushort.Parse(rdr["Strength"].ToString());
                                Result.Vitality = ushort.Parse(rdr["Vitality"].ToString());
                                Result.Agility = ushort.Parse(rdr["Agility"].ToString());
                                Result.Spirit = ushort.Parse(rdr["Spirit"].ToString());
                                Result.StatPoints = ushort.Parse(rdr["StatPoints"].ToString());
                            }
                            else
                                FlatDatabase.GetLevelStats(Result);

                            Result.Hero.GuildID = ushort.Parse(rdr["GuildID"].ToString());
                            Result.GuildDonation = uint.Parse(rdr["GuildDonation"].ToString());
                            Result.Hero.GuildRank = (GuildPosition)byte.Parse(rdr["GuildRank"].ToString());
                            Result.Guild = Kernel.FGuild(Result.Hero.GuildID);
                            Result.HeadKillCounter = uint.Parse(rdr["HeadKillCount"].ToString());
                            Result.LottoTries = byte.Parse(rdr["LottoTries"].ToString());
                            Result.Merchant = (MerchantTypes)byte.Parse(rdr["Merchant"].ToString());
                            Result.Hero.Stamps.HeavenBless = DateTime.Parse(rdr["HeavenBlessDT"].ToString());
                            Result.OfflineTrainingTime = ulong.Parse(rdr["OfflineTGTime"].ToString());
                            Result.Hero.Stamps.OfflineTG = DateTime.Parse(rdr["OfflineTGStartDT"].ToString());
                            Result.MonsterHunter = bool.Parse(rdr["ActiveQuest"].ToString());
                            Result.MHKillCount = ushort.Parse(rdr["QuestKillCount"].ToString());
                            Result.MHAttempts = ushort.Parse(rdr["QuestMHTries"].ToString());
                            Result.Hero.Stamps.MonsterHunter = DateTime.Parse(rdr["QuestMHDTG"].ToString());
                            Result.Hero.Stamps.ExpPotion = DateTime.Parse(rdr["XpPotLasts"].ToString());
                            rdr.Close();
                        }
                    }
                    #endregion
                    LoadItems(Result);
                    LoadProficiences(Result);
                    LoadSkills(Result);
                    LoadWarehouses(Result);
                    LoadFriends(Result);
                    LoadEnemies(Result);
                    GetMetBankCount(Result);
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
                Result.Socket.Disconnect();
            }
        }

        #region Nobility
        public static void UpdateNobility(NobilityInfo Info)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var UpdateQuery = "UPDATE DONATION = @Donation";
                    var InsertQuery = "INSERT INTO `nobility` (UID, MODEL, PLAYER_NAME, DONATION) VALUES (@UID, @pModel, @pName, @Donation)" + " ON DUPLICATE KEY " + UpdateQuery;

                    var cmd = new MySqlCommand("", m_Conn);
                    cmd.CommandText = InsertQuery;
                    cmd.Parameters.AddWithValue("@UID", Info.UID);
                    cmd.Parameters.AddWithValue("@pModel", Info.Model);
                    cmd.Parameters.AddWithValue("@pName", Info.playerName);
                    cmd.Parameters.AddWithValue("@Donation", Info.Donation);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }
        public static NobilityInfo LoadNobilityInfo(GameClient Client)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var Command = new MySqlCommand("SELECT * FROM nobility WHERE `PLAYER_NAME` = @name LIMIT 1", m_Conn);
                Command.Parameters.AddWithValue("@name", Client.Hero.Name);
                using (var rdr = Command.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        NobilityInfo Info = new NobilityInfo();
                        Info.UID = uint.Parse(rdr["UID"].ToString());
                        Info.Model = ushort.Parse(rdr["MODEL"].ToString());
                        Info.playerName = rdr["PLAYER_NAME"].ToString();
                        Info.Donation = uint.Parse(rdr["DONATION"].ToString());

                        return Info;
                    }
                    rdr.Close();
                }
            }
            return null;
        }
        public static uint GetNobilityRank(long Donation)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var Command = new MySqlCommand("SELECT COUNT(*) AS `rank` FROM nobility WHERE DONATION >= (SELECT DONATION FROM nobility WHERE `DONATION` = @donation)", m_Conn);
                Command.Parameters.AddWithValue("@donation", Donation);
                return Convert.ToUInt32(Command.ExecuteScalar());
            }
        }
        public static IList<NobilityInfo> GetNobilityPages()
        {
            var nobility = new List<NobilityInfo>();
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var Command = new MySqlCommand("SELECT * FROM nobility ORDER BY DONATION DESC", m_Conn);
                using (var rdr = Command.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        NobilityInfo Info = new NobilityInfo();
                        Info.UID = uint.Parse(rdr["UID"].ToString());
                        Info.Model = ushort.Parse(rdr["Model"].ToString());
                        Info.playerName = rdr["PLAYER_NAME"].ToString();
                        Info.Donation = uint.Parse(rdr["DONATION"].ToString());
                        nobility.Add(Info);
                    }
                    rdr.Close();
                }
            }
            return nobility;
        }
        #endregion

        public static void SaveCharacter(GameClient Character)
        {
            try
            {
                var Entity = Character.Hero;
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var cmd = new MySqlCommand("UPDATE characters SET MapID = @mapID, X = @posX, Y = @posY,"
                        + " Level = @level, PkPoints = @pkPoints, Experience = @exp, Sex = @sex, Avatar = @avatar,"
                        + " Strength = @strength, Vitality = @vitality, Agility = @agility, Spirit = @spirit,"
                        + " StatPoints = @statPoints, Hp = @HP, Mana = @mana, Money = @silvers, HairStyle = @hair, "
                        + " Profession = @profession, Reborn = @reborn, ConquerPts = @conquerPts,"
                        + " VirtuePoints = @virtuePoints, GuildID = @guildID, GuildDonation = @guildDonation,"
                        + " GuildRank = @guildRank, headkillCount = @headkillCount, LottoTries = @lottoTries,"
                        + " Merchant = @merchant, HeavenBlessDT = @heavenblessDT, OfflineTGTime=@offlinetg, "
                        + " OfflineTGStartDT=@offlinetgstart, ActiveQuest=@activequest, QuestKillCount=@questkillcount,"
                        + " QuestMHDTG=@questmhdtg, QuestMHTries=@questmhtries, XpPotLasts = @xpPotLasts"
                        + " WHERE Name = @name", m_Conn);

                    cmd.Parameters.AddWithValue("@mapID", Entity.MapID);
                    cmd.Parameters.AddWithValue("@posX", Entity.X);
                    cmd.Parameters.AddWithValue("@posY", Entity.Y);
                    cmd.Parameters.AddWithValue("@level", Entity.Level);
                    cmd.Parameters.AddWithValue("@pkPoints", Entity.PkPoints);
                    cmd.Parameters.AddWithValue("@exp", Character.Experience);
                    cmd.Parameters.AddWithValue("@sex", Entity.Sex);
                    cmd.Parameters.AddWithValue("@avatar", Entity.Avatar);
                    cmd.Parameters.AddWithValue("@strength", Character.Strength);
                    cmd.Parameters.AddWithValue("@vitality", Character.Vitality);
                    cmd.Parameters.AddWithValue("@agility", Character.Agility);
                    cmd.Parameters.AddWithValue("@spirit", Character.Spirit);
                    cmd.Parameters.AddWithValue("@statpoints", Character.StatPoints);
                    cmd.Parameters.AddWithValue("@HP", Entity.Hitpoints);
                    cmd.Parameters.AddWithValue("@mana", Character.Mana);
                    cmd.Parameters.AddWithValue("@silvers", Character.Silvers);
                    cmd.Parameters.AddWithValue("@hair", Entity.Hairstyle);
                    cmd.Parameters.AddWithValue("@profession", Character.Job);
                    cmd.Parameters.AddWithValue("@reborn", Entity.Reborn);
                    cmd.Parameters.AddWithValue("@conquerPts", Character.ConquerPoints);
                    cmd.Parameters.AddWithValue("@virtuePoints", Character.VirtuePoints);
                    cmd.Parameters.AddWithValue("@guildID", Entity.GuildID);
                    cmd.Parameters.AddWithValue("@guildDonation", Character.GuildDonation);
                    cmd.Parameters.AddWithValue("@guildRank", Entity.GuildRank);
                    cmd.Parameters.AddWithValue("@headkillCount", Character.KoInfo.KillCount);
                    cmd.Parameters.AddWithValue("@lottoTries", Character.LottoTries);
                    cmd.Parameters.AddWithValue("@merchant", Character.Merchant);
                    cmd.Parameters.AddWithValue("@heavenblessDT", Character.Hero.Stamps.HeavenBless);
                    cmd.Parameters.AddWithValue("@name", Entity.Name);
                    cmd.Parameters.AddWithValue("@offlinetg", Character.OfflineTrainingTime);
                    cmd.Parameters.AddWithValue("@offlinetgstart", Character.Hero.Stamps.OfflineTG);
                    cmd.Parameters.AddWithValue("@activequest", Character.MonsterHunter);
                    cmd.Parameters.AddWithValue("@questkillcount", Character.MHKillCount);
                    cmd.Parameters.AddWithValue("@questmhdtg", Character.Hero.Stamps.MonsterHunter);
                    cmd.Parameters.AddWithValue("@questmhtries", Character.MHAttempts);
                    cmd.Parameters.AddWithValue("@xpPotLasts", Entity.Stamps.ExpPotion);
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "UPDATE warehouses SET Password = @password, Money = @money,"
                        + " Twin = @twin, Phoenix = @phoenix, Ape = @ape, Desert = @desert, Bird = @Bird,"
                        + " Market = @market, Mobile = @mobile WHERE Name = @warehouseName";

                    cmd.Parameters.AddWithValue("@password", Character.Warehouse.Password);
                    cmd.Parameters.AddWithValue("@money", Character.Warehouse.Money);
                    cmd.Parameters.AddWithValue("@twin", ConquerItem.DictionaryToString(Character.Warehouse.Warehouses[(uint)WhIDs.TwinCity]));
                    cmd.Parameters.AddWithValue("@phoenix", ConquerItem.DictionaryToString(Character.Warehouse.Warehouses[(uint)WhIDs.Phoenix]));
                    cmd.Parameters.AddWithValue("@ape", ConquerItem.DictionaryToString(Character.Warehouse.Warehouses[(uint)WhIDs.Ape]));
                    cmd.Parameters.AddWithValue("@desert", ConquerItem.DictionaryToString(Character.Warehouse.Warehouses[(uint)WhIDs.Desert]));
                    cmd.Parameters.AddWithValue("@bird", ConquerItem.DictionaryToString(Character.Warehouse.Warehouses[(uint)WhIDs.Bird]));
                    cmd.Parameters.AddWithValue("@market", ConquerItem.DictionaryToString(Character.Warehouse.Warehouses[(uint)WhIDs.Market]));
                    cmd.Parameters.AddWithValue("@mobile", ConquerItem.DictionaryToString(Character.Warehouse.Warehouses[(uint)WhIDs.Mobile]));
                    cmd.Parameters.AddWithValue("@warehouseName", Entity.Name);
                    cmd.ExecuteNonQuery();
                }
                SaveItems(Character);
                UpdateProficiency(Character);
                SaveSkills(Character);
                SaveMetBank(Character);
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }
        #region PROFICIENCY SAVE/LOAD
        public static void UpdateProficiency(GameClient Client)
        {
            foreach (var Prof in Client.Proficiencies.Values)
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var UpdateQuery = "UPDATE Level = @level, Experience = @exp";
                    var InsertQuery = "INSERT INTO `proficiencies` (Name, ProficiencyID, Level, Experience) VALUES (@name, @profID, @level, @exp)"
                        + " ON DUPLICATE KEY " + UpdateQuery;

                    var cmd = new MySqlCommand("", m_Conn);
                    cmd.CommandText = InsertQuery;
                    cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                    cmd.Parameters.AddWithValue("@profID", Prof.ID);
                    cmd.Parameters.AddWithValue("@level", Prof.Level);
                    cmd.Parameters.AddWithValue("@exp", Prof.Experience);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void LoadProficiences(GameClient Client)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var cmd = new MySqlCommand("SELECT * FROM `proficiencies` WHERE `Name` = @name", m_Conn);
                cmd.Parameters.AddWithValue("@name", Client.Hero.Name);

                using (var RDR = cmd.ExecuteReader())
                {
                    while (RDR.Read())
                    {
                        var Prof = new ConquerSpell();
                        Prof.ID = ushort.Parse(RDR["ProficiencyID"].ToString());
                        Prof.Level = byte.Parse(RDR["Level"].ToString());
                        Prof.Experience = int.Parse(RDR["Experience"].ToString());

                        Client.Proficiencies.TryAdd(Prof.ID, Prof);
                    }
                }
            }
        }
        #endregion
        #region SKILL/SPELL SAVE/LOAD
        public static void SaveSkills(GameClient Client)
        {
            foreach (var Skill in Client.Spells.Values)
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var UpdateQuery = "UPDATE `Level` = @level, `Experience` = @exp";
                    var InsertQuery = "INSERT INTO `skills` (Name, SkillID, Level, Experience) VALUES (@name, @skillID, @level, @exp)"
                        + " ON DUPLICATE KEY " + UpdateQuery;

                    var cmd = new MySqlCommand("", m_Conn);
                    cmd.CommandText = InsertQuery;
                    cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                    cmd.Parameters.AddWithValue("@skillID", Skill.ID);
                    cmd.Parameters.AddWithValue("@level", Skill.Level);
                    cmd.Parameters.AddWithValue("@exp", Skill.Experience);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void LoadSkills(GameClient Client)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var cmd = new MySqlCommand("SELECT * FROM `skills` WHERE `Name` = @name", m_Conn);
                cmd.Parameters.AddWithValue("@name", Client.Hero.Name);

                using (var RDR = cmd.ExecuteReader())
                {
                    while (RDR.Read())
                    {
                        var Spell = new ConquerSpell();
                        Spell.ID = ushort.Parse(RDR["SkillID"].ToString());
                        Spell.Level = byte.Parse(RDR["Level"].ToString());
                        Spell.Experience = int.Parse(RDR["Experience"].ToString());

                        Client.Spells.TryAdd(Spell.ID, Spell);
                    }
                }
            }
        }
        #endregion
        #region Inventory/Equipment Save/Load
        #region Item Save
        public static void SaveItems(GameClient Client)
        {
            foreach (var item in Client.Inventory)
                UpdateItem(Client.Hero.Name, item);
            foreach (var item in Client.Equipment.Values)
                UpdateItem(Client.Hero.Name, item);
        }

        private static void AddItem(string Name, uint ItemID)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var cmd = new MySqlCommand("INSERT INTO `items` (Name, ItemUID, Position, ItemID) VALUES (@name, @itemUID, @position, @itemID)", m_Conn);
                    cmd.Parameters.AddWithValue("@name", Name);
                    cmd.Parameters.AddWithValue("@itemUID", ConquerItem.NextItemUID);
                    cmd.Parameters.AddWithValue("@position", 0);
                    cmd.Parameters.AddWithValue("@itemID", ItemID);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
            }
        }

        public static void UpdateItem(string Name, IConquerItem Item)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var UpdateQuery = "UPDATE `Position` = @position, `Plus` = @plus, `Bless` = @bless," + " `Enchant` = @enchant, `SocketOne` = @sockone, `SocketTwo` = @socktwo, `Dura` = @dura, `MDura` = @mdura, `Color` = @color, `Locked` = @locked";
                    var InsertQuery = "INSERT INTO `items`(Name, ItemUID, Position, ItemID, Plus, Bless, Enchant, SocketOne, SocketTwo, Dura, MDura, Color, Locked)" + " VALUES(@name, @itemUID, @position, @itemID, @plus, @bless, @enchant, @sockone, @socktwo, @dura, @mdura, @color, @locked)" + " ON DUPLICATE KEY " + UpdateQuery;

                    var cmd = new MySqlCommand("", m_Conn);
                    cmd.CommandText = InsertQuery;
                    cmd.Parameters.AddWithValue("@name", Name);
                    cmd.Parameters.AddWithValue("@itemUID", Item.UID);
                    cmd.Parameters.AddWithValue("@position", Item.Position);
                    cmd.Parameters.AddWithValue("@itemID", Item.ID);
                    cmd.Parameters.AddWithValue("@plus", Item.Plus);
                    cmd.Parameters.AddWithValue("@bless", Item.Bless);
                    cmd.Parameters.AddWithValue("@enchant", Item.Enchant);
                    cmd.Parameters.AddWithValue("@sockone", Item.SocketOne);
                    cmd.Parameters.AddWithValue("@socktwo", Item.SocketTwo);
                    cmd.Parameters.AddWithValue("@dura", Item.Durability);
                    cmd.Parameters.AddWithValue("@mdura", Item.MaxDurability);
                    cmd.Parameters.AddWithValue("@color", Item.Color);
                    cmd.Parameters.AddWithValue("@locked", Item.Locked);
                    cmd.ExecuteNonQuery();
                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void DeleteItem(uint ItemUID)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var cmd = new MySqlCommand("DELETE FROM `items` WHERE `ItemUID` = @itemUID", m_Conn);
                    cmd.Parameters.AddWithValue("@itemUID", ItemUID);
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
            }
        }
        #endregion
        public static void LoadItems(GameClient Client)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var cmd = new MySqlCommand("SELECT * FROM `items` WHERE `Name` = @name", m_Conn);
                cmd.Parameters.AddWithValue("@name", Client.Hero.Name);

                using (var RDR = cmd.ExecuteReader())
                {
                    while (RDR.Read())
                    {
                        var Item = new ConquerItem(true);
                        Item.UID = uint.Parse(RDR["ItemUID"].ToString());
                        Item.Position = (ItemPosition)ushort.Parse(RDR["Position"].ToString());
                        Item.ID = uint.Parse(RDR["ItemID"].ToString());
                        Item.Plus = byte.Parse(RDR["Plus"].ToString());
                        Item.Bless = byte.Parse(RDR["Bless"].ToString());
                        Item.Enchant = byte.Parse(RDR["Enchant"].ToString());
                        Item.SocketOne = byte.Parse(RDR["SocketOne"].ToString());
                        Item.SocketTwo = byte.Parse(RDR["SocketTwo"].ToString());
                        Item.Durability = ushort.Parse(RDR["Dura"].ToString());
                        Item.MaxDurability = ushort.Parse(RDR["MDura"].ToString());
                        Item.Color = byte.Parse(RDR["Color"].ToString());
                        Item.Locked = byte.Parse(RDR["Locked"].ToString());

                        if (Item.Position == 0)
                            Client.AddInventory(Item);
                        else if (Item.Position >= ConquerItem.FirstSlot && Item.Position <= ConquerItem.LastSlot)
                            Client.Equipment.TryAdd((ushort)Item.Position, Item);
                    }
                    RDR.Close();
                }
            }
        }
        #endregion
        #region Spouse Save/Remove
        public static void SaveSpouse(GameClient Client, GameClient Spouse)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var cmd = new MySqlCommand("", m_Conn);
                cmd.CommandText = "UPDATE characters SET Spouse = @spouse WHERE Name = @name";
                cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                cmd.Parameters.AddWithValue("@spouse", Spouse.Hero.Name);
                cmd.ExecuteNonQuery();

                cmd = new MySqlCommand("", m_Conn);
                cmd.CommandText = "UPDATE characters SET Spouse = @spouse WHERE Name = @name";
                cmd.Parameters.AddWithValue("@spouse", Spouse.Hero.Name);
                cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                cmd.ExecuteNonQuery();
            }
        }

        public static void RemoveSpouse(GameClient Client)
        {
            var SpouseName = Client.Spouse;
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var cmd = new MySqlCommand("", m_Conn);
                cmd.CommandText = "UPDATE characters SET Spouse = @spouse WHERE Name = @name";
                cmd.Parameters.AddWithValue("@spouse", "");
                cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                cmd.ExecuteNonQuery();

                cmd = new MySqlCommand("", m_Conn);
                cmd.CommandText = "UPDATE characters SET Spouse = @spouse WHERE Name = @name";
                cmd.Parameters.AddWithValue("@spouse", "");
                cmd.Parameters.AddWithValue("@name", SpouseName);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        public static void SavePrize(GameClient Client)
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var cmd = new MySqlCommand("", m_Conn);
                cmd.CommandText = "UPDATE characters SET Prize = @prize WHERE Name = @name";

                cmd.Parameters.AddWithValue("@prize", 0);
                cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteCharacter(GameClient Client)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var cmd = new MySqlCommand("UPDATE `accounts` SET `LoginAction` = @loginaction, `Character` = @character WHERE `Name` = @name", m_Conn);
                    cmd.Parameters.AddWithValue("@loginaction", 2);
                    cmd.Parameters.AddWithValue("@character", "");
                    cmd.Parameters.AddWithValue("@name", Client.Account);
                    cmd.ExecuteNonQuery();

                    cmd = new MySqlCommand("DELETE FROM `characters` WHERE `Name`= @name", m_Conn);
                    cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                    cmd.ExecuteNonQuery();

                    cmd = new MySqlCommand("DELETE FROM `warehouses` WHERE `Name`= @name", m_Conn);
                    cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                    cmd.ExecuteNonQuery();

                    cmd = new MySqlCommand("DELETE FROM `items` WHERE `Name` = @name", m_Conn);
                    cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                    cmd.ExecuteNonQuery();

                    cmd = new MySqlCommand("DELETE FROM `skills` WHERE `Name` = @name", m_Conn);
                    cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                    cmd.ExecuteNonQuery();

                    cmd = new MySqlCommand("DELETE FROM `proficiencies` WHERE `Name` = @name", m_Conn);
                    cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                    cmd.ExecuteNonQuery();

                    cmd = new MySqlCommand("DELETE FROM `friends` WHERE `ownername` = @name  OR WHERE `friendname = @name", m_Conn);
                    cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                    cmd.ExecuteNonQuery();

                    cmd = new MySqlCommand("DELETE FROM `enemies` WHERE `ownername` = @name  OR WHERE `enemyname = @name", m_Conn);
                    cmd.Parameters.AddWithValue("@name", Client.Hero.Name);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }
        #endregion
        #region Item
        public static int GetLastItemId()
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var Command = new MySqlCommand("SELECT MAX(ItemUID) from items", m_Conn);
                    return (int)Command.ExecuteScalar();
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
                return 0;
            }
        }

        public static uint GetItemId(string Name, bool ReplaceQuality, byte Quality)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var Command = new MySqlCommand("SELECT * FROM itemtype WHERE ItemName = @name", m_Conn);
                    Command.Parameters.AddWithValue("@name", Name);

                    using (var rdr = Command.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            var Id = uint.Parse(rdr["ItemID"].ToString());
                            if (!ReplaceQuality)
                                return Id;

                            var end = (byte)(Id % 10);
                            Id -= end;
                            Id += Quality;
                            return Id;
                        }
                    }
                }
                return 0;
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
                // Database.WriteLine("Failed To Get ItemId!");
                return 0;
            }
        }

        public static string LoadItemString(uint ItemId, string Type)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var Command = new MySqlCommand("SELECT * FROM itemtype WHERE ItemID = @itemID", m_Conn);
                    Command.Parameters.AddWithValue("@itemID", ItemId);

                    using (var rdr = Command.ExecuteReader())
                    {
                        if (rdr.Read())
                            return rdr[Type].ToString();
                    }
                }
                return "0";
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
                //Database.WriteLine("Failed To ReadItem {0}!", ItemId);
                return "0";
            }
        }

        public static bool FindItem(uint ItemId)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var Command = new MySqlCommand("SELECT * FROM itemtype WHERE ItemID = @itemID", m_Conn);
                    Command.Parameters.AddWithValue("@itemID", ItemId);

                    using (var rdr = Command.ExecuteReader())
                    {
                        if (rdr.Read())
                            return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        #endregion
        #region Monster
        public static void LoadMonsterSpawns()
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var Command = new MySqlCommand("SELECT * FROM cq_generator", m_Conn);
                using (var rdr = Command.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        var Spawn = new MonsterSpawn();
                        Spawn.Type = uint.Parse(rdr["npctype"].ToString());
                        Spawn.XStart = ushort.Parse(rdr["bound_x"].ToString());
                        Spawn.YStart = ushort.Parse(rdr["bound_y"].ToString());
                        Spawn.UniqueID = uint.Parse(rdr["id"].ToString());
                        Spawn.MapID = ushort.Parse(rdr["mapid"].ToString());
                        Spawn.XStop = (ushort)(Spawn.XStart + ushort.Parse(rdr["bound_cx"].ToString()));
                        Spawn.YStop = (ushort)(Spawn.YStart + ushort.Parse(rdr["bound_cy"].ToString()));
                        Spawn.NumberToSpawn = ushort.Parse(rdr["max_per_gen"].ToString());
                        Spawn.RestSecs = ushort.Parse(rdr["rest_secs"].ToString());

                        if (!isPVP || (Spawn.Type == 900))
                            Kernel.MonsterSpawns.Add(Spawn.UniqueID, Spawn);
                    }
                }
            }
            Console.WriteLine("Loaded {0} monster spawns.", Kernel.MonsterSpawns.Count);
        }

        public static void LoadMonsters()
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var Command = new MySqlCommand("SELECT * FROM cq_monstertype", m_Conn);
                    using (var rdr = Command.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var Monster = new MonsterStatus();
                            Monster.Id = uint.Parse(rdr["id"].ToString());
                            Monster.Name = rdr["name"].ToString();
                            Monster.Mesh = uint.Parse(rdr["lookface"].ToString());
                            Monster.Level = byte.Parse(rdr["level"].ToString());
                            Monster.Hitpoints = int.Parse(rdr["life"].ToString());
                            Monster.MinAttack = uint.Parse(rdr["attack_min"].ToString());
                            Monster.MaxAttack = uint.Parse(rdr["attack_max"].ToString());
                            Monster.MagicDefence = uint.Parse(rdr["magic_def"].ToString());
                            Monster.Defence = uint.Parse(rdr["defence"].ToString());
                            Monster.AttackRange = uint.Parse(rdr["attack_range"].ToString());
                            Monster.ViewDistance = uint.Parse(rdr["view_range"].ToString());
                            Monster.Drops.DropMoney = int.Parse(rdr["drop_money"].ToString());
                            Monster.Drops.DropBoots = uint.Parse(rdr["drop_shoes"].ToString());
                            Monster.Drops.DropArmet = uint.Parse(rdr["drop_armet"].ToString());
                            Monster.Drops.DropArmor = uint.Parse(rdr["drop_armor"].ToString());
                            Monster.Drops.DropNecklace = uint.Parse(rdr["drop_necklace"].ToString());
                            Monster.Drops.DropRing = uint.Parse(rdr["drop_ring"].ToString());
                            Monster.Drops.DropShield = uint.Parse(rdr["drop_shield"].ToString());
                            Monster.Drops.DropWeapon = uint.Parse(rdr["drop_weapon"].ToString());
                            Monster.Drops.DropHP = uint.Parse(rdr["drop_hp"].ToString());
                            Monster.Drops.DropMP = uint.Parse(rdr["drop_mp"].ToString());
                            Monster.Speed = uint.Parse(rdr["attack_speed"].ToString());
                            Monster.Dodge = uint.Parse(rdr["dodge"].ToString());
                            Monster.MagicType = uint.Parse(rdr["magic_type"].ToString());
                            Monster.Action = int.Parse(rdr["action"].ToString());
                            Monster.CreateItemGenerator();

                            FlatDatabase.LoadSpecial(Monster);
                            Kernel.MonsterStats.Add(Monster.Id, Monster);
                        }
                    }
                }
                Console.WriteLine("Loaded " + Kernel.MonsterStats.Count + " monster stats.");
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }
        #endregion
        #region Guilds
        public static void LoadGuilds()
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var Command = new MySqlCommand("SELECT * FROM guilds", m_Conn);
                    using (var rdr = Command.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var CGuild = new ConquerGuild();
                            CGuild.Name = rdr["Name"].ToString();
                            CGuild.Leader = rdr["Leader"].ToString();
                            CGuild.Bulletin = rdr["Bulletin"].ToString();
                            CGuild.UID = ushort.Parse(rdr["Id"].ToString());
                            CGuild.Fund = uint.Parse(rdr["Fund"].ToString());
                            CGuild.WarWins = uint.Parse(rdr["WarWins"].ToString());
                            CGuild.HoldingPole = sbyte.Parse(rdr["HoldingPole"].ToString());
                            CGuild.HasPrize = sbyte.Parse(rdr["HasPrize"].ToString());
                            CGuild.DeputyCount = sbyte.Parse(rdr["DeputyCount"].ToString());
                            ConquerGuild.Create(CGuild, rdr["Members"].ToString(), rdr["Allies"].ToString(), rdr["Enemies"].ToString());

                            if (CGuild.UID > ConquerGuild.LastGuildUID)
                                ConquerGuild.LastGuildUID = CGuild.UID;
                            if (!Kernel.Guilds.ContainsKey(CGuild.UID))
                                Kernel.Guilds.TryAdd(CGuild.UID, CGuild);
                        }
                    }
                }
                Console.WriteLine("Loaded {0} Guilds.", Kernel.Guilds.Count);
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }

        public static void CreateGuild(ConquerGuild Guild)
        {
            try
            {
                var Members = "";
                foreach (var member in Guild.Members)
                    Members += "-" + member.Value + "-";
                Members = Members.Replace("--", "-");
                Members = Members.Trim('-');
                var Allies = "";
                foreach (var Allie in Guild.Allies)
                    Allies += "-" + Allie + "-";
                Allies = Allies.Replace("--", "-");
                Allies = Allies.Trim('-');
                var Enemies = "";
                foreach (var Enemy in Guild.Enemies)
                    Enemies += "-" + Enemy + "-";
                Enemies = Enemies.Replace("--", "-");
                Enemies = Enemies.Trim('-');

                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var cmd = new MySqlCommand("INSERT INTO `guilds` (Id, Name, Leader," + " Bulletin, Fund, WarWins, HoldingPole, HasPrize, Members, Allies," + " Enemies) VALUES (@id, @name, @leader, @bulletin, @fund, @warWins," + " @holdingPole, @hasPrize, @members, @allies, @enemies)", m_Conn);

                    cmd.Parameters.AddWithValue("@id", Guild.UID);
                    cmd.Parameters.AddWithValue("@name", Guild.Name);
                    cmd.Parameters.AddWithValue("@leader", Guild.Leader);
                    cmd.Parameters.AddWithValue("@bulletin", Guild.Bulletin);
                    cmd.Parameters.AddWithValue("@fund", Guild.Fund);
                    cmd.Parameters.AddWithValue("@warWins", Guild.WarWins);
                    cmd.Parameters.AddWithValue("@holdingPole", Guild.HoldingPole);
                    cmd.Parameters.AddWithValue("@hasPrize", Guild.HasPrize);
                    cmd.Parameters.AddWithValue("@members", Members);
                    cmd.Parameters.AddWithValue("@allies", Allies);
                    cmd.Parameters.AddWithValue("@enemies", Enemies);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }

        public static void SaveGuilds()
        {
            try
            {
                foreach (var Guild in Kernel.Guilds.Values)
                    SaveGuild(Guild);
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }

        public static void SaveGuild(ConquerGuild Guild)
        {
            try
            {
                var Members = "";
                foreach (var Member in Guild.Members.Values)
                    Members += "-" + Member + "-";
                Members = Members.Replace("--", "-");
                Members = Members.Trim('-');
                var Allies = "";
                foreach (var Allie in Guild.Allies)
                    Allies += "-" + Allie + "-";
                Allies = Allies.Replace("--", "-");
                Allies = Allies.Trim('-');
                var Enemies = "";
                foreach (var Enemy in Guild.Enemies)
                    Enemies += "-" + Enemy + "-";
                Enemies = Enemies.Replace("--", "-");
                Enemies = Enemies.Trim('-');

                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var cmd = new MySqlCommand("", m_Conn);
                    cmd.CommandText = "UPDATE guilds SET Name = @name, Leader = @leader," + " Bulletin = @bulletin, Fund = @fund, " +
                                      "WarWins = @warWins, HoldingPole = @holdingPole," + " HasPrize = @hasPrize, Members = @members, " +
                                      "Allies = @allies, Enemies = @enemies, Deputycount = @deputyCount" + " WHERE Id = @id";

                    cmd.Parameters.AddWithValue("@name", Guild.Name);
                    cmd.Parameters.AddWithValue("@leader", Guild.Leader);
                    cmd.Parameters.AddWithValue("@bulletin", Guild.Bulletin);
                    cmd.Parameters.AddWithValue("@fund", Guild.Fund);
                    cmd.Parameters.AddWithValue("@warWins", Guild.WarWins);
                    cmd.Parameters.AddWithValue("@holdingPole", Guild.HoldingPole);
                    cmd.Parameters.AddWithValue("@hasPrize", Guild.HasPrize);
                    cmd.Parameters.AddWithValue("@members", Members);
                    cmd.Parameters.AddWithValue("@allies", Allies);
                    cmd.Parameters.AddWithValue("@enemies", Enemies);
                    cmd.Parameters.AddWithValue("@deputyCount", Guild.DeputyCount);
                    cmd.Parameters.AddWithValue("@id", Guild.UID);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }

        public static void DeleteGuild(ConquerGuild CGuild)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var cmd = new MySqlCommand("DELETE FROM `guilds` WHERE `Id` = @guildUID", m_Conn);
                    cmd.Parameters.AddWithValue("@guildUID", CGuild.UID);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }
        #endregion
        #region Misc
        public static void LoadKo()
        {
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var count = 0;
                var Command = new MySqlCommand("SELECT * FROM characters WHERE headkillCount > 0 ORDER BY headkillCount DESC LIMIT 100", m_Conn);
                using (var rdr = Command.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        var info = new KoInfo
                        {
                            Name = rdr.GetString("Name"),
                            KillCount = rdr.GetUInt32("headkillCount"),
                            Position = count++
                        };
                        Kernel.KoTop100.Add(info);
                    }
                }
            }
            Console.WriteLine("KoBoards loaded.");
        }
        public static IList<Tuple<uint, uint>> LoadMaps()
        {
            var maps = new List<Tuple<uint, uint>>();
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var Command = new MySqlCommand("SELECT * FROM cq_map", m_Conn);
                    using (var rdr = Command.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var mapId = rdr.GetUInt32("id");
                            var mapDoc = rdr.GetUInt32("mapdoc");
                            if (mapDoc == 0)
                                mapDoc = mapId;
                            maps.Add(new Tuple<uint, uint>(mapId, mapDoc));
                        }
                    }
                }
            }
            catch
            {
            }
            return maps;
        }

        public static void LoadConfig()
        {
            myAcc = Config.ReadString("Database", "Account", "root");
            myPass = Config.ReadString("Database", "Password", "7CX3o28V3G9499D");
            myIP = Config.ReadString("Database", "mysqlIP", "localhost");
            myDB = Config.ReadString("Database", "dbName", "conquer");
            myPort = Config.ReadUInt16("Database", "Port", 3306);
            ServerIP = Config.ReadString("Server", "IP", "107.172.221.147");
            GPort = Config.ReadUInt16("Server", "Game Port", 5816);
            ServerVersion = Config.ReadString("Version", "Version", "ERROR");
            isPVP = Config.ReadBool("Server", "PVP", false);
        }
        #endregion
        #region Met Storage 
        public static void GetMetBankCount(GameClient Client)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var cmd = new MySqlCommand("Select * FROM ma_store WHERE CHARACTER_ID = @CharUID", m_Conn);
                    cmd.Parameters.AddWithValue("@charUID", Client.Hero.UID);
                    using (var rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            Client.DragonballCount = int.Parse(rdr["dbs"].ToString());
                            Client.MeteorCount = int.Parse(rdr["mets"].ToString());
                            rdr.Close();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }

        public static void SaveMetBank(GameClient Client)
        {
            try
            {
                using (var m_Conn = new MySqlConnection(ConnectionString))
                {
                    m_Conn.Open();

                    var UpdateQuery = "UPDATE `mets` = @metscrolls, `dbs` = @dbscrolls";
                    var InsertQuery = "INSERT INTO `ma_store` (character_id, mets, dbs) VALUES(@charUID, @metscrolls, @dbscrolls)" + " ON DUPLICATE KEY " + UpdateQuery;

                    var cmd = new MySqlCommand("", m_Conn);
                    cmd.CommandText = InsertQuery;
                    cmd.Parameters.AddWithValue("@charUID", Client.Hero.UID);
                    cmd.Parameters.AddWithValue("@metscrolls", Client.MeteorCount);
                    cmd.Parameters.AddWithValue("@dbscrolls", Client.DragonballCount);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }
        #endregion
    }
}