﻿using System;
using System.IO;
using System.Threading;

namespace ConquerEmulator.Database
{
	internal class LogHandler
	{
	    private static readonly object WriteSync = new object();

	    public static void WriteLine(Exception Exc)
		{
			try
			{
				lock (WriteSync)
				{
					var errorsDirectory = FlatDatabase.Location + "\\Errors\\";
					if (!Directory.Exists(errorsDirectory))
						Directory.CreateDirectory(errorsDirectory);

					var errorLog = errorsDirectory + $"Errors({DateTime.Now.Month}.{DateTime.Now.Day}).txt";
					using (var writer = new StreamWriter(errorLog, true))
					{
						writer.WriteLine("[{0}] \r\n{1}\r\n", DateTime.Now, Exc);
					}
				}
				Console.WriteLine(Exc.ToString());
			}
			catch
			{
			}
		}

	    public static void WriteLine(UnhandledExceptionEventArgs exc)
		{
			WriteLine((Exception)exc.ExceptionObject);
		}

	    public static void WriteLine(ThreadExceptionEventArgs exc)
		{
			WriteLine(exc.Exception);
		}
	}
}