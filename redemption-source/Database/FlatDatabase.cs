﻿using System;
using System.IO;
using System.Linq;
using ConquerEmulator.Attacking;
using ConquerEmulator.Client;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Mob;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Database
{
	internal class FlatDatabase
	{
		#region Revivers
		public static void LoadRevivers()
		{
			Kernel.Revivers.Add(1, new ReviveMonster(1, 1005, 38, 68));
			Kernel.Revivers.Add(2, new ReviveMonster(1, 1004, 33, 32));
			Kernel.Revivers.Add(3, new ReviveMonster(1, 1004, 69, 33));
			Kernel.Revivers.Add(4, new ReviveMonster(1, 1004, 70, 70));
			Kernel.Revivers.Add(5, new ReviveMonster(1, 1004, 33, 70));
			Kernel.Revivers.Add(6, new ReviveMonster(1, 6000, 49, 108));
			Kernel.Revivers.Add(7, new ReviveMonster(1, 6000, 18, 81));
			Kernel.Revivers.Add(8, new ReviveMonster(1, 6000, 39, 68));
			Kernel.Revivers.Add(9, new ReviveMonster(1, 1003, 32, 56));
			Kernel.Revivers.Add(10, new ReviveMonster(1, 1005, 69, 32));
		}
		#endregion
		#region Skill
		public static MAttackData GetMAttackData(ConquerSpell Spell)
		{
			var lpData = new MAttackData();
            IniFile rdr = new IniFile(Location + "\\Spells\\" + Spell.ID + "[" + Spell.Level + "].ini");
            lpData.SpellID = rdr.ReadUInt16("SpellInformation", "ID", 0);
            lpData.SpellLevel = rdr.ReadByte("SpellInformation", "Level", 0);
            lpData.Stamina = rdr.ReadSByte("SpellInformation", "Stamina", 0);
            lpData.Range = rdr.ReadSByte("SpellInformation", "Range", 0);
            lpData.Distance = rdr.ReadSByte("SpellInformation", "Distance", 0);
            lpData.Mana = rdr.ReadUInt16("SpellInformation", "Mana", 0);
            lpData.BaseDamage = rdr.ReadInt32("SpellInformation", "MDamage", 0);
            lpData.SuccessRate = rdr.ReadByte("SpellInformation", "SucessRate", 0);
            lpData.Aggressive = (rdr.ReadByte("SpellInformation", "Aggressive", 0) == 1);
            lpData.Weapon = rdr.ReadUInt16("SpellInformation", "Weapon", 0);
            lpData.TargetType = (MAttackTargetType)rdr.ReadByte("SpellInformation", "TargetType", 0);
            lpData.Sort = (MagicSort)rdr.ReadByte("SpellInformation", "Unknown0", (byte)MagicSort.ATTACK);
            lpData.MultipleTargets = (rdr.ReadByte("SpellInformation", "MultipleTargets", 0) == 1);
            lpData.IsXPSkill = (rdr.ReadByte("SpellInformation", "IsXPSkill", 0) == 1);
            lpData.SecondsTimer = rdr.ReadUInt16("SpellInformation", "Timer", 0);
            lpData.NextSpellID = rdr.ReadUInt16("SpellInformation", "Delay2", 0);
            lpData.Experience = rdr.ReadUInt32("SpellInformation", "Experience", 0);
            lpData.GroundAttack = (rdr.ReadByte("SpellInformation", "Ground", 0) == 1);
		    return lpData;
		}
		#endregion
		#region Shops
		public static bool ShopItemExists(uint ShopUID, uint ItemID)
		{
			var Header = ShopUID.ToString();
			var ItemAmount = (sbyte)Shops.ReadByte(Header, "ItemAmount", 0);
			for (sbyte i = 0; i < ItemAmount; i++)
				if (Shops.ReadUInt32(Header, "Item" + i, 0) == ItemID)
					return true;
			return false;
		}
		#endregion
		#region Lottery
		public static void LoadLotteryItems()
		{
			if (File.Exists(Location + "Lotto.txt"))
			{
				foreach (var Line in File.ReadAllLines(Location + "Lotto.txt"))
				{
					var Info = Line.Split(',');

					IConquerItem prize = new ConquerItem(true);
					prize.ID = uint.Parse(Info[0]);
					prize.Plus = byte.Parse(Info[1]);
					prize.SocketOne = byte.Parse(Info[2]);
					prize.SocketOne = byte.Parse(Info[3]);

					if (MySqlDatabase.FindItem(prize.ID))
						Kernel.LotteryItems.Add(prize);
				}
			}
		}
		#endregion
		#region Training Ground Mobs
		public static void LoadTgMonsters()
		{
			uint num = 900000;
			foreach (var folder in Directory.GetDirectories(Location + "\\TG\\"))
			{
				foreach (var file in Directory.GetFiles(folder))
				{
					var ini = new IniFile(file);
					var tg = new SOBMonster(EntityFlag.TrainingGround);
					tg.Defence = ini.ReadUInt16("SpawnInformation", "Defence", 0);
					tg.MapID = new Map(1039);
					tg.X = ini.ReadUInt16("SpawnInformation", "SpawnX", 0);
					tg.Y = ini.ReadUInt16("SpawnInformation", "SpawnY", 0);
					tg.Mesh = (uint)(ini.ReadUInt32("SpawnInformation", "Mesh", 0) + (ini.ReadInt16("SpawnInformation", "Type", 0) << 16));
					tg.MaxHitpoints = ini.ReadInt32("SpawnInformation", "MaxHealth", 0);
					tg.Hitpoints = tg.MaxHitpoints;
					tg.Name = ini.ReadString("SpawnInformation", "Name", string.Empty);
					tg.Level = ini.ReadUInt16("SpawnInformation", "Level", 0);
					tg.UID = num--;
					Kernel.TrainingMobs.Add(tg.UID, tg);
				}
			}
		}
		#endregion
		#region Database Variables
		public static string Location = Directory.GetCurrentDirectory() + "\\Database\\";

		public static IniFile SpellExp;
		public static IniFile ProfExp;
		public static IniFile LevExp;
		public static IniFile Shops;
		public static IniFile Pets;
		public static IniFile Transform;
		public static IniFile RevivePoints;
		public static IniFile SpecialDrops;
        public static IniFile DropRates;
        public static IniFile StatPoints;
        #endregion
        #region Experience
        public static uint LoadSpellExp(uint SpellId, byte level)
		{
			return SpellExp.ReadUInt32(SpellId.ToString(), level.ToString(), 9999999);
		}

		public static uint LoadProfExp(byte level)
		{
			return ProfExp.ReadUInt32("Proficiency", level.ToString(), 2100000000);
		}

		public static uint LoadLevelExp(byte level)
		{
			return LevExp.ReadUInt32("Level", level.ToString(), 1073741759);
		}
        public static uint LoadUpLevelTime(byte level)
        {
            return LevExp.ReadUInt32("UpLevelTime", level.ToString(), 25527);
        }
        #endregion
        #region Item Stats
        public static void LoadItemStats(GameClient Client, IConquerItem Item)
		{
			var standerd = new StanderdItemStats(Item.ID);

			if (Client.Hero.InTransformation)
			{
				Client.Transform.Defence += standerd.PhysicalDefence;
				Client.Transform.MDefence += standerd.MDefence;
				Client.Transform.Dodge += standerd.Dodge;
			}
			else
			{
				Client.Hero.Defence += standerd.PhysicalDefence;
				Client.Hero.MDefence += standerd.MDefence;
				Client.Hero.Dodge += standerd.Dodge;
			}

			if (Item.Position == ItemPosition.Right)
				Client.Hero.Speed += (ushort)standerd.Frequency;

			Client.BaseMagicAttack += standerd.MAttack;
			Client.ItemHP += standerd.HP;
			Client.ItemMP += standerd.MP;

			if (Item.Position == ItemPosition.Right)
				Client.AttackRange += standerd.AttackRange;
			if (Item.Position == ItemPosition.Left)
			{
				Client.BaseMinAttack += (uint)(standerd.MinAttack * 0.5F);
				Client.BaseMaxAttack += (uint)(standerd.MaxAttack * 0.5F);
			}
			else
			{
				Client.BaseMinAttack += standerd.MinAttack;
				Client.BaseMaxAttack += standerd.MaxAttack;
			}

			if (Item.Plus != 0)
			{
				var plus = new PlusItemStats(Item.ID, Item.Plus);
				Client.BaseMinAttack += plus.MinAttack;
				Client.BaseMaxAttack += plus.MaxAttack;
				Client.BaseMagicAttack += plus.MAttack;
				if (Client.Hero.InTransformation)
				{
					Client.Transform.Defence += plus.PhysicalDefence;
					Client.Transform.Dodge += plus.Dodge;
				}
				else
				{
					Client.Hero.Defence += plus.PhysicalDefence;
					Client.Hero.Dodge += plus.Dodge;
				}
				Client.Hero.PlusMDefence += plus.PlusMDefence;
				Client.ItemHP += plus.HP;
			}
			if ((Item.Position != ItemPosition.Garment) && (Item.Position != ItemPosition.Bottle))
			{
				Client.ItemHP += Item.Enchant;
				Client.BlessPercent += (sbyte)Item.Bless;
                GemAlgorithm(Client, Item.SocketOne, Item.SocketTwo, true);
            }
		}

		public static void UnloadItemStats(GameClient Client, IConquerItem Item)
		{
			var standerd = new StanderdItemStats(Item.ID);

			if (Client.Hero.InTransformation)
			{
				Client.Transform.Defence -= standerd.PhysicalDefence;
				Client.Transform.MDefence -= standerd.MDefence;
				Client.Transform.Dodge -= standerd.Dodge;
			}
			else
			{
				Client.Hero.Defence -= standerd.PhysicalDefence;
				Client.Hero.MDefence -= standerd.MDefence;
				Client.Hero.Dodge -= standerd.Dodge;
			}

			if (Item.Position == ItemPosition.Right)
				Client.Hero.Speed -= (ushort)standerd.Frequency;

			Client.BaseMagicAttack -= standerd.MAttack;
			Client.ItemHP -= standerd.HP;
			Client.ItemMP -= standerd.MP;

			if (Item.Position == ItemPosition.Right)
				Client.AttackRange = 0;
			if (Item.Position == ItemPosition.Left)
			{
				Client.BaseMinAttack -= (uint)(standerd.MinAttack * 0.5F);
				Client.BaseMaxAttack -= (uint)(standerd.MaxAttack * 0.5F);
			}
			else
			{
				Client.BaseMinAttack -= standerd.MinAttack;
				Client.BaseMaxAttack -= standerd.MaxAttack;
			}

			if (Item.Plus != 0)
			{
				var plus = new PlusItemStats(Item.ID, Item.Plus);
				Client.BaseMinAttack -= plus.MinAttack;
				Client.BaseMaxAttack -= plus.MaxAttack;
				Client.BaseMagicAttack -= plus.MAttack;

				if (Client.Hero.InTransformation)
				{
					Client.Hero.Defence -= plus.PhysicalDefence;
					Client.Hero.Dodge -= plus.Dodge;
				}
				else
				{
					Client.Hero.Defence -= plus.PhysicalDefence;
					Client.Hero.Dodge -= plus.Dodge;
				}
				Client.Hero.PlusMDefence -= plus.PlusMDefence;
				Client.ItemHP -= plus.HP;
			}
			if ((Item.Position != ItemPosition.Garment) && (Item.Position != ItemPosition.Bottle))
			{
				Client.ItemHP -= Item.Enchant;
				Client.BlessPercent -= (sbyte)Item.Bless;
                GemAlgorithm(Client, Item.SocketOne, Item.SocketTwo, false);
            }
		}

        private static void GemAlgorithm(GameClient Client, byte SocketOne, byte SocketTwo, bool Add)
        {
            byte Gem = SocketOne;
            bool FirstGem = true;

            AnalyzeGem:
            var GemID = (byte)(Gem / 10);
            if (GemID <= 7)
            {
                int GemQ = (short)Math.Min(Gem - (GemID * 10), 3);
                switch (GemID)
                {
                    case GemsConst.RainbowGem:
                        {
                            if (GemQ == 3)
                                GemQ = 25;
                            else
                                GemQ = ((GemQ + 1) * 5);
                            break;
                        }
                    case GemsConst.MoonGem:
                        {
                            if (GemQ == 3)
                                GemQ = 50;
                            else
                                GemQ *= 15;
                            break;
                        }
                    case GemsConst.TortoiseGem:
                        {
                            GemQ *= 2;
                            break;
                        }
                    case GemsConst.VioletGem:
                        {

                            if (GemQ == 0)
                                GemQ = 30;
                            else
                                GemQ *= 50;
                            break;
                        }
                    case GemsConst.ThunderGem:
                    case GemsConst.GloryGem:
                        {
                            // these large numbers are due to gemq*0.01
                            if (GemQ == 0)
                                GemQ = 10000;
                            else if (GemQ == 1)
                                GemQ = 30000;
                            else if (GemQ == 2)
                                GemQ = 50000;
                            break;
                        }
                    default: GemQ *= 5; break;
                }

                if (Add)
                    Client.Gems[GemID] += ((double)GemQ * 0.01);
                else
                    Client.Gems[GemID] -= ((double)GemQ * 0.01);
            }
            if (FirstGem)
            {
                Gem = SocketTwo;
                FirstGem = false;
                goto AnalyzeGem;
            }
        }
        #endregion
        #region Misc 
        public static void GetLevelStats(GameClient Client)
        {
            var Level = Client.Hero.Level;
            var Job = "";
            if ((Client.Job >= 10) && (Client.Job <= 15))
                Job = "Trojan";
            if ((Client.Job >= 20) && (Client.Job <= 25))
                Job = "Warrior";
            if ((Client.Job >= 40) && (Client.Job <= 45))
                Job = "Archer";
            if ((Client.Job >= 100) && (Client.Job <= 145))
                Job = "Taoist";

            if (Level > 120)
                Level = 120;
            Client.Strength = (ushort)StatPoints.ReadUInt32(Job, "Strength[" + Level + "]", 1);
            Client.Agility = (ushort)StatPoints.ReadUInt32(Job, "Agility[" + Level + "]", 1);
            Client.Vitality = (ushort)StatPoints.ReadUInt32(Job, "Vitality[" + Level + "]", 1);
            Client.Spirit = (ushort)StatPoints.ReadUInt32(Job, "Spirit[" + Level + "]", 1);
        }

        public static void LoadRates()
        {
            // Flat Rates
            DataStructures.ExperienceRate = DropRates.ReadByte("Rates", "Experience", 0);

            // Mining
            DataStructures.IronOre = DropRates.ReadInt32("Mining", "Iron", 0);
            DataStructures.CopperOre = DropRates.ReadInt32("Mining", "Copper", 0);
            DataStructures.EuxeniteOre = DropRates.ReadInt32("Mining", "Euxenite", 0);
            DataStructures.SilverOre = DropRates.ReadInt32("Mining", "Silver", 0);
            DataStructures.GoldOre = DropRates.ReadInt32("Mining", "Gold", 0);
            DataStructures.NormalGem = DropRates.ReadInt32("Mining", "NormalGem", 0);
            DataStructures.RefinedGem = DropRates.ReadInt32("Mining", "RefinedGem", 0);
            DataStructures.SuperGem = DropRates.ReadInt32("Mining", "SuperGem", 0);

            // Monster Drops
            DataStructures.Meteor = DropRates.ReadDouble("MonsterDrops", "Meteor", 0);
            DataStructures.DragonBall = DropRates.ReadDouble("MonsterDrops", "DragonBall", 0);
            DataStructures.PlusOne = DropRates.ReadDouble("MonsterDrops", "PlusOne", 0);

            // Socket Chances
            DataStructures.MetSock1 = DropRates.ReadDouble("Socketing", "MetSock1", 0);
            DataStructures.MetSock2 = DropRates.ReadDouble("Socketing", "MetSock2", 0);
            DataStructures.DBSock1 = DropRates.ReadDouble("Socketing", "DBSock1", 0);
            DataStructures.DBSock2 = DropRates.ReadDouble("Socketing", "DBSock2", 0);
        }

        public static void LoadSpecial(MonsterStatus Monster)
		{
			var Header = Monster.Id.ToString();
			Monster.Drops.DropSpecials = new SpecialItemWatcher[SpecialDrops.ReadInt32(Header, "Count", 0)];
			for (int i = 0; i < Monster.Drops.DropSpecials.Length; i++)
			{
			    var Data = SpecialDrops.ReadString(Header, i.ToString(), "", 32).Split(',');
			    if (Data.Length == 2)
			        Monster.Drops.DropSpecials[i] = new SpecialItemWatcher(uint.Parse(Data[0]), double.Parse(Data[1]));
			    else
			        Monster.Drops.DropSpecials[i] = new SpecialItemWatcher(uint.Parse(Data[0]), double.Parse(Data[1]), byte.Parse(Data[2]));
			}
		}

		public static bool LoadPortal(GameClient Client, ushort X, ushort Y, out uint destMapId, out ushort destX, out ushort destY)
		{
		    destMapId = destX = destY = 0;
			var Contents = File.ReadAllLines(Location + "Portals.txt");
			if (Kernel.GetDistance(X, Y, Client.Hero.X, Client.Hero.Y) <= 5)
			{
				foreach (var Content in Contents)
				{
					var HArray = Content.Split(' ');
					if ((HArray[0] == Client.Hero.MapID.ToString()) && (Kernel.GetDistance(X, Y, ushort.Parse(HArray[1]), ushort.Parse(HArray[2])) <= 3))
					{
						destMapId = new Map(Convert.ToUInt16(HArray[3]));
						destX = Convert.ToUInt16(HArray[4]);
						destY = Convert.ToUInt16(HArray[5]);
						return true;
					}
				}
			}
			return false;
		}

		public static bool IsValidJob(int JobID)
		{
			int[] AvailableJobs = { 10, 20, 40, 100 };
			return AvailableJobs.Contains(JobID);
		}

		public static bool IsCorrectProf(ushort ProfID)
		{
			ushort[] AvailableProfs = { 000, 410, 420, 421, 430, 440, 450, 460, 480, 481, 490, 510, 530, 540, 550, 560, 562, 580, 561, 500, 900, 422 };
			return AvailableProfs.Contains(ProfID);
		}

		public static bool IsCorrectSpell(ushort SpellID, byte Level)
		{
            IniFile rdr = new IniFile(Location + "\\Spells\\" + SpellID + "[" + Level + "].ini");
            return rdr.ReadUInt16("SpellInformation", "ID", 0) != 0;
		}

		public static void SaveToFile(string IP)
		{
			var Split = IP.Split(' ');
			File.WriteAllLines(Location + "BannedIPs.txt", Split);
		}

		public static void LoadBanned()
		{
			if (File.Exists(Location + "BannedIPs.txt"))
			{
				foreach (var IpAdder in File.ReadAllLines(Location + "BannedIPs.txt"))
					Kernel.BannedIPs.Add(IpAdder);
				Console.WriteLine("Loaded {0} Banned IPs.", Kernel.BannedIPs.Count);
			}
		}

		public static bool ValidName(string CharacterName, bool SquareBrackets)
		{
			const byte Dash = 45; // "-"
			const byte Underscore = 95; // "_"
			const byte NumStart = 48; // "0"
			const byte NumEnd = 57; // "9"
			const byte UpperStart = 65; // "A"
			const byte UpperEnd = 90; // "Z"
			const byte LowerStart = 97; // "a"
			const byte LowerEnd = 122; // "z"
			const byte LeftSquare = (byte)'[';
			const byte RightSquare = (byte)']';
			const byte VerticleLine = (byte)'|';

			for (byte i = 0; i < CharacterName.Length; i++)
			{
				var c = (byte)CharacterName[i];
				if ((c == Dash) || (c == Underscore) || (c == VerticleLine))
					continue;
				if ((c >= NumStart) && (c <= NumEnd))
					continue;
				if ((c >= UpperStart) && (c <= UpperEnd))
					continue;
				if ((c >= LowerStart) && (c <= LowerEnd))
					continue;
                if (SquareBrackets)
                {
                    if (c == LeftSquare)
                        continue;
                    else if (c == RightSquare)
                        continue;
                }
				return false;
			}
			return true;
		}

		public static bool ValidWhPass(string Name)
		{
			if (Name.IndexOfAny(new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' }) > -1)
				return true;
			return false;
		}
		#endregion
	}
}