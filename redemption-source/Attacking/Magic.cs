﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Events;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Mob;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Attacking
{
    public enum MagicSort
    {
        ATTACK = 1,
        RECRUIT = 2,			// support auto active.
        CROSS = 3,
        FAN = 4,			// support auto active(random).
        BOMB = 5,
        ATTACHSTATUS = 6,
        DETACHSTATUS = 7,
        SQUARE = 8,
        JUMPATTACK = 9,			// move, a-lock
        RANDOMTRANS = 10,			// move, a-lockf
        DISPATCHXP = 11,
        COLLIDE = 12,			// move, a-lock & b-synchro
        SERIALCUT = 13,			// auto active only.
        LINE = 14,			// support auto active(random).
        ATKRANGE = 15,			// auto active only, forever active.
        ATKSTATUS = 16,			// support auto active, random active.
        CALLTEAMMEMBER = 17,
        RECORDTRANSSPELL = 18,
        TRANSFORM = 19,
        ADDMANA = 20,			// support self target only.
        LAYTRAP = 21,
        DANCE = 22,			// ÌøÎè(only use for client)
        CALLPET = 23,			// ÕÙ»½ÊÞ
        VAMPIRE = 24,			// ÎüÑª£¬power is percent award. use for call pet
        INSTEAD = 25,			// ÌæÉí. use for call pet
        DECLIFE = 26,		
        GROUNDSTING = 27,			
        REBORN = 28,		
        TEAM_MAGIC = 29,			
        BOMB_LOCKALL = 30,			
        SORB_SOUL = 31,		
        STEAL = 32,			
        LINE_PENETRABLE = 33,			
        BLAST_THUNDER = 34,			
        MULTI_ATTACHSTATUS = 35,			
        MULTI_DETACHSTATUS = 36,			
        MULTI_CURE = 37,			
        STEAL_MONEY = 38,		
        KO = 39,			
        ESCAPE = 40				
    }

    public enum MAttackTargetType : byte
    {
        WeaponSkill = 8,
        Physical = 4,
        BombMagic = 2,
        MagicHeal = 1,
        Magic = 0
    }

    public struct MAttackData
    {
        public ushort SpellID;
        public ushort Mana;
        public ushort Weapon;
        public ushort SpellLevel;
        public sbyte Stamina;
        public sbyte Range;
        public sbyte Distance;
        public byte SuccessRate;
        public MAttackTargetType TargetType;
        public MagicSort Sort;
        public int BaseDamage;
        public bool Aggressive;
        public bool MultipleTargets;
        public bool IsXPSkill;
        public ushort SecondsTimer;
        public ushort NextSpellID;
        public uint Experience;
        public bool GroundAttack;

        public double BaseDamagePercent => Math.Max(BaseDamage - 30000, 0) * 0.01;

        public string GetName()
        {
            var rdr = new IniFile(FlatDatabase.Location + "\\Spells\\" + SpellID + "[" + SpellLevel + "].ini");
            return rdr.ReadString("SpellInformation", "Name", "", 32);
        }
    }

    public unsafe partial class AttackProcessor
    {
        private static bool InTournament(GameClient Attacker, IBaseEntity Opponent, out int Damage)
        {
            Damage = 0;

            if (!Attacker.InTournament)
                return false;

            if (TeamDeathMatch.Started)
            {
                Damage = 1;
                TeamDeathMatch.IncrementScore(Attacker);
            }

            if (Dueling.Started)
            {
                Dueling.IncrementScore(Attacker);
            }

            if (!Dueling.Started && TournamentBase.Started)
            {
                if (TournamentBase.TournamentType == "PassTheBomb")
                {
                    if ((Attacker.Hero.StatusFlag & StatusFlag.Curse) == StatusFlag.Curse)
                    {
                        var StatPacket = StatTypePacket.Create();
                        StatPacket.ID = StatIDs.RaiseFlag;
                        Attacker.Hero.StatusFlag &= ~StatusFlag.Curse;
                        StatPacket.BigValue = (ulong)Attacker.Hero.StatusFlag;
                        StatPacket.UID = Attacker.Hero.UID;
                        Attacker.SendScreen(StatPacket, true);

                        Attacker.HasBomb = false;
                        PassTheBomb.BombPass((GameClient)Opponent.Owner);
                    }
                }
                else // FFA and MostKills
                {
                    Damage = 1;
                    TournamentBase.IncrementScore(Attacker);
                }
            }
            return true;
        }


        public static bool MagicalAttack(IBaseEntity Attacker, IBaseEntity Opponent, ushort SpellID, ushort X, ushort Y)
        {
            try
            {
                var Spell = new ConquerSpell();
                GameClient AttackerClient = null;

                if (Attacker.EntityFlag != EntityFlag.Player)
                    Spell.ID = SpellID;
                else
                {
                    AttackerClient = Attacker.Owner as GameClient;
                    if (!AttackerClient.Spells.ContainsKey(SpellID))
                        return false;
                    Spell = AttackerClient.Spells[SpellID];
                }

                var MData = FlatDatabase.GetMAttackData(Spell);
                if (MData.Aggressive)
                {
                    if (AttackerClient?.Pet != null && Opponent != Attacker)
                        AttackerClient.PetTarget = Opponent;
                }

                var Targets = new Dictionary<IBaseEntity, Damage>();
                var MAttackCheck = MAttackChecks(Attacker, Opponent, &MData);
                if (MAttackCheck == 0)
                {
                    ProcessBySort(Attacker, AttackerClient, Opponent, &MData, X, Y, MData.Sort, ref Targets);
                }
                if (Targets != null)
                {
                    Packets.ShowAttack(Attacker as Entity, Targets, Spell, X, Y);
                    FinalizeAttack(Attacker as Entity, Targets, MData);
                    return true;
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
                return false;
            }
            return false;
        }

        private static bool ProcessBySort(IBaseEntity Attacker, GameClient AttackerClient, IBaseEntity Opponent, MAttackData* Data, ushort X, ushort Y, MagicSort Sort, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            var Success = false;
            switch (Sort)
            {
                case MagicSort.ATTACK: Success = ProcessStandard(Attacker, AttackerClient, Opponent, Data, ref Targets); break;
                case MagicSort.RECRUIT: Success = ProcessRecruit(AttackerClient, Opponent, Data, ref Targets); break;
                case MagicSort.FAN: Success = ProcessFan(AttackerClient, Opponent, Data, ref Targets, X, Y); break;
                case MagicSort.BOMB: Success = ProcessBomb(AttackerClient, Opponent, Data, ref Targets); break;
                case MagicSort.ATTACHSTATUS: Success = ProcessAttach(AttackerClient, Opponent, Data, ref Targets); break;
                case MagicSort.DETACHSTATUS: Success = ProcessDetach(AttackerClient, Opponent, Data, ref Targets); break;
                case MagicSort.TRANSFORM: ProcessTransform(AttackerClient, Data, ref Targets); break;
                case MagicSort.ADDMANA: Success = ProcessAddMana(AttackerClient, Data, ref Targets); break;
                case MagicSort.DECLIFE: Success = ProcessDecLife(AttackerClient, Opponent, Data, ref Targets); break;
                case MagicSort.ATKSTATUS: ProcessAtkStatus(AttackerClient, Opponent, Data, ref Targets); break;
                case MagicSort.LINE:
                case MagicSort.LINE_PENETRABLE: Success = ProcessLineAttack(AttackerClient, ref Targets, Data, ref X, ref Y); break;
                case MagicSort.CALLPET: ProcessCallPet(AttackerClient, Data, ref Targets); break;
            }
            return Success;
        }

        private static bool ProcessFan(GameClient AttackerClient, IBaseEntity Opponent, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets, ushort X, ushort Y)
        {
            if (AttackerClient != null)
            {
                var Success = false;
                //const short DefaultFanWidth = 120; // degrees
                var Base = RandomGenerator.Generator.Next((int)AttackerClient.Hero.MinAttack, (int)AttackerClient.Hero.MaxAttack);
                if (Data->Weapon == 500) // Bow
                {
                    var Quivery = AttackerClient.Equipment.Values.FirstOrDefault(x => ConquerItem.IsItemType(x.ID, ItemType.Arrow));
                    if (Quivery?.Arrows >= 3)
                    {
                        if (AttackerClient.Hero.MapID != 1039)
                        {
                            //Quivery.Arrows -= 3;
                            if (Quivery.Arrows <= 0)
                                AttackerClient.Unequip(AttackerClient, (ushort)ItemPosition.Left);
                            else
                            {
                                var Update = new ItemUsagePacket(Quivery.UID, Quivery.Durability, ItemUsageID.UpdateDurability);
                                AttackerClient.Send(Update, Update.Size);
                            }
                        }

                        Success = true;
                        Targets = FieldOfViewMAttack<double, short>(AttackerClient, Kernel.GetAngle(AttackerClient.Hero.X, AttackerClient.Hero.Y, X, Y),
                            ArcherDamage, Data->BaseDamagePercent, Data->Range, Base);
                    }
                }
                else
                {
                    Success = true;
                    Targets = FieldOfViewMAttack<double, short>(AttackerClient, Kernel.GetAngle(AttackerClient.Hero.X, AttackerClient.Hero.Y, X, Y),
                        PhysicalDamage, Data->BaseDamagePercent, Data->Range, Base);
                }
                return Success;
            }
            return false;
        }

        private static bool ProcessBomb(GameClient AttackerClient, IBaseEntity Opponent, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            if (AttackerClient != null)
            {
                var Success = false;
                if (Data->TargetType == MAttackTargetType.BombMagic || Data->TargetType == MAttackTargetType.Magic)
                {
                    Success = true;
                    Targets = FieldOfViewMAttack<object, object>(AttackerClient, 0,
                        MagicDamage, null, Data->Range, Data->BaseDamage);
                }
                else if (Data->TargetType == MAttackTargetType.Physical ||
                         Data->TargetType == MAttackTargetType.WeaponSkill)
                {
                    Success = true;
                    var Base = RandomGenerator.Generator.Next((int)AttackerClient.Hero.MinAttack, (int)AttackerClient.Hero.MaxAttack);
                    Targets = FieldOfViewMAttack<double, object>(AttackerClient, 0,
                        PhysicalDamage, Data->BaseDamagePercent, Data->Range, Base);
                }
                return Success;
            }
            return false;
        }

        private static Dictionary<IBaseEntity, Damage> FieldOfViewMAttack<TDamage, TValid>(GameClient Attacker, short Angle,
          DamageCalculationCallback<TDamage> DamageCallback, TDamage DamageCallbackParameter, sbyte Range, int BaseDamage)
        {
            var Result = new Dictionary<IBaseEntity, Damage>();
            IBaseEntity Opponent;
            Damage damage;
            foreach (var obj in Attacker.Screen.Objects)
            {
                if (obj.MapObjType == MapObjectType.Monster || obj.MapObjType == MapObjectType.Player ||
                    obj.MapObjType == MapObjectType.SOB)
                {
                    Opponent = obj as IBaseEntity;
                    if (SafeAttack(Attacker.Hero, Opponent, Range))
                    {
                        if (Angle != 0)
                        {
                            if (Math.Abs(Angle - Kernel.GetAngle(Attacker.Hero.X, Attacker.Hero.Y, Opponent.X, Opponent.Y)) > 120)
                                continue;
                        }
                        damage.Show = DamageCallback(BaseDamage, Attacker.Hero, Opponent, DamageCallbackParameter);
                        damage.Experience = Math.Min(Opponent.Hitpoints, damage.Show);
                        Opponent.Hitpoints -= Math.Min(damage.Show, Opponent.Hitpoints); Math.Min(damage.Show, Opponent.Hitpoints);
                        Result.Add(Opponent, damage);
                    }
                }
            }
            return Result;
        }

        private static bool ProcessAttach(GameClient AttackerClient, IBaseEntity Opponent, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            /* Sadly, I have no idea how this one works. I've compared it all I can and have drawn a blank :( */
            switch (Data->SpellID)
            {
                case 1025: Superman(AttackerClient, Data, ref Targets); break;
                case 1020: XPShield(AttackerClient, Data, ref Targets); break;
                case 1110: Cyclone(AttackerClient, Data, ref Targets); break;
                case 1095: Stigma(AttackerClient, Opponent, Data, ref Targets); break;
                case 1075: Invisibility(Opponent, Data, ref Targets); break;
                case 8002:
                case 8003: Fly(AttackerClient, Data, ref Targets); break;
                default: return false;
            }
            return true;
        }

        private static bool ProcessDetach(GameClient AttackerClient, IBaseEntity Opponent, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            /* Sadly, I have no idea how this one works. I've compared it all I can and have drawn a blank :( */
            switch (Data->SpellID)
            {
                case 1050:
                case 1100: Pray(AttackerClient, Opponent, ref Targets); break;
                default: return true;
            }
            return false;
        }

        private static bool ProcessStandard(IBaseEntity Attacker, GameClient AttackerClient, IBaseEntity Opponent, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            if (SafeAttack(Attacker, Opponent, Data->Distance))
            {
                var damage = new Damage();
                var Magic = false;
                if (Data->Weapon == 0)
                {
                    damage.Show = CalculateMagicDmg(Data->BaseDamage, Attacker, Opponent, Data->SpellID);
                    Magic = true;
                }
                else if (AttackerClient != null) // && Data->Weapon != 0
                {
                    var left = AttackerClient.Equipment[(ushort)ItemPosition.Left];
                    var right = AttackerClient.Equipment[(ushort)ItemPosition.Right];
                    if (left == null || right == null)
                        return false;
                    if (ConquerItem.GetItemType(left.ID) != 601 && ConquerItem.GetItemType(right.ID) != 601)
                        return false;
                    damage.Show = CalculatePhysDmg(Attacker, Opponent, Data->BaseDamagePercent);
                }
                else
                {
                    return false;
                }
                damage.Experience = Math.Min(Opponent.Hitpoints, damage.Show);
                Opponent.Hitpoints -= Math.Min(damage.Show, Opponent.Hitpoints);

                Targets = new Dictionary<IBaseEntity, Damage>(1);
                Targets.Add(Opponent, damage);
                return Magic;
            }
            return false;
        }

        private static bool ProcessRecruit(GameClient AttackerClient, IBaseEntity Opponent, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            var damage = new Damage();
            if (Data->MultipleTargets)
            {
                Opponent = null;
                if (AttackerClient != null)
                {
                    if (AttackerClient.Team.Active)
                    {
                        Targets = new Dictionary<IBaseEntity, Damage>(AttackerClient.Team.Teammates.Length);
                        foreach (var TeamMember in AttackerClient.Team.Teammates)
                        {
                            Opponent = TeamMember.Hero;
                            if (SafeHeal(AttackerClient.Hero, Opponent, Data->Distance, true) == 0)
                            {
                                damage.Show = Data->BaseDamage;
                                damage.Experience = Math.Min(Opponent.MaxHitpoints - Opponent.Hitpoints, Data->BaseDamage);
                                Opponent.Hitpoints = Math.Min(Opponent.Hitpoints + Data->BaseDamage, Opponent.MaxHitpoints);
                                Targets.Add(Opponent, damage);
                            }
                        }
                        return true;
                    }
                    Opponent = AttackerClient.Hero;
                }
            }
            if (Opponent != null)
            {
                if (SafeHeal(AttackerClient.Hero, Opponent, Data->Distance, true) == 0)
                {
                    Targets = new Dictionary<IBaseEntity, Damage>();
                    damage.Show = Data->BaseDamage;
                    damage.Experience = Math.Min(Opponent.MaxHitpoints - Opponent.Hitpoints, Data->BaseDamage);
                    Opponent.Hitpoints = Math.Min(Opponent.Hitpoints + Data->BaseDamage, Opponent.MaxHitpoints);
                    Targets.Add(Opponent, damage);
                    return true;
                }
            }
            return false;
        }

        private static bool ProcessAddMana(GameClient Attacker, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            Attacker.Mana = (ushort)Math.Min(Attacker.MaxMana, Attacker.Mana + Data->BaseDamage);

            SendUpdate(Attacker, Attacker.Mana, StatIDs.Mana);
            Targets = new Dictionary<IBaseEntity, Damage>();
            Targets.Add(Attacker.Hero, new Damage(Data->BaseDamage, Data->BaseDamage));
            return true;
        }

        private static bool ProcessDecLife(GameClient Attacker, IBaseEntity Opponent, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            if (SafeAttack(Attacker.Hero, Opponent, Data->Distance))
            {
                var damage = new Damage();
                damage.Show = (int)(Opponent.Hitpoints * Data->BaseDamagePercent);
                damage.Experience = Math.Min(Opponent.Hitpoints, damage.Show);
                Opponent.Hitpoints -= Math.Min(damage.Show, Opponent.Hitpoints);

                Targets = new Dictionary<IBaseEntity, Damage>(1);
                Targets.Add(Opponent, damage);
                return true;
            }
            return false;
        }

        private static void ProcessAtkStatus(GameClient Attacker, IBaseEntity Opponent, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            if (Opponent != null)
            {
                var damage = new Damage();
                damage.Show = CalculatePhysDmg(Attacker.Hero, Opponent, Data->BaseDamagePercent);
                damage.Experience = Math.Min(damage.Show, Opponent.Hitpoints);
                Opponent.Hitpoints -= Math.Min(damage.Show, Opponent.Hitpoints);
                Targets.Add(Opponent, damage);
            }
        }

        private static bool ProcessLineAttack(GameClient Attacker, ref Dictionary<IBaseEntity, Damage> Targets, MAttackData* Data, ref ushort X, ref ushort Y)
        {
            var LOS = DDALineAlgorithm.Line(Attacker.Hero.X, Attacker.Hero.Y, X, Y, Data->Range);
            IBaseEntity Opponent;
            Targets = new Dictionary<IBaseEntity, Damage>();
            var damage = new Damage();
            foreach (var obj in Attacker.Screen.Objects)
            {
                if (obj != null)
                {
                    foreach (var coord in LOS)
                    {
                        if (coord.X == obj.X && coord.Y == obj.Y)
                        {
                            if (obj.MapObjType == MapObjectType.Monster || obj.MapObjType == MapObjectType.Player || obj.MapObjType == MapObjectType.SOB)
                            {
                                Opponent = obj as IBaseEntity;
                                if (!Targets.ContainsKey(Opponent))
                                {
                                    int Damage;
                                    if (InTournament(Attacker, Opponent, out Damage))
                                    {
                                        damage.Show = Damage;
                                        damage.Experience = Math.Min(damage.Show, Opponent.Hitpoints);
                                        Opponent.Hitpoints -= Math.Min(damage.Show, Opponent.Hitpoints);
                                        Targets.Add(Opponent, damage);
                                    }
                                    if (SafeAttack(Attacker.Hero, Opponent, Data->Distance))
                                    {
                                        damage.Show = CalculatePhysDmg(Attacker.Hero, Opponent, 1.00);
                                        damage.Experience = Math.Min(damage.Show, Opponent.Hitpoints);
                                        Opponent.Hitpoints -= Math.Min(damage.Show, Opponent.Hitpoints);
                                        Targets.Add(Opponent, damage);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }

        private static void ProcessCallPet(GameClient Attacker, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            try
            {
                var Name = string.Empty;
                switch (Data->SpellID)
                {
                    case 4000:
                        {
                            switch (Data->SpellLevel)
                            {
                                case 0: Name = "IronGuard"; break;
                                case 1: Name = "CopperGuard"; break;
                                case 2: Name = "SilverGuard"; break;
                                case 3: Name = "GoldGuard"; break;
                            }
                            break;
                        }
                    default: Name = Data->GetName(); break;
                }

                var Assignment = AssignPetPacket.Create();
                MonsterPet.GetAssignmentData(Attacker, Name, &Assignment);
                Attacker.Send(Assignment, Assignment.Size);

                Attacker.Pet = new MonsterPet(Attacker, Name, Assignment);
                Attacker.Pet.Attach();

                Targets = new Dictionary<IBaseEntity, Damage>();
                Targets.Add(Attacker.Hero, new Damage(0, 1));
            }
            catch (ArgumentException)
            {
                // pet not implemented/bad pet name
            }
        }

        private static void ProcessTransform(GameClient Attacker, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            if (!Attacker.Hero.InTransformation)
            {
                var Section = Data->SpellID + "-" + Data->SpellLevel;
                var Mesh = FlatDatabase.Transform.ReadUInt16(Section, "Mesh", 0);
                if (Mesh != 0)
                {
                    Attacker.Transform.Start(Mesh, Data->SpellID);
                    Attacker.Hero.Stamps.TransformationTime = DateTime.Now.AddSeconds(Data->SecondsTimer);
                    Attacker.Hero.OverlappingMesh = Mesh;
                    var HPModifier = (double)Attacker.Hero.Hitpoints / Attacker.Hero.MaxHitpoints;
                    Attacker.Hero.MaxHitpoints = FlatDatabase.Transform.ReadInt32(Section, "HP", 0);
                    Attacker.Hero.Hitpoints = Math.Max((int)(Attacker.Hero.MaxHitpoints * HPModifier), 1);
                    Attacker.Hero.MaxAttack = FlatDatabase.Transform.ReadUInt32(Section, "MaxAttack", 0);
                    Attacker.Hero.MinAttack = FlatDatabase.Transform.ReadUInt32(Section, "MinAttack", 0);
                    Attacker.Hero.Defence = FlatDatabase.Transform.ReadUInt16(Section, "Defence", 0);
                    Attacker.Hero.Dodge = FlatDatabase.Transform.ReadSByte(Section, "Dodge", 0);
                    Attacker.Hero.MDefence = FlatDatabase.Transform.ReadUInt16(Section, "MDefence", 0);
                    Attacker.Transform.SendUpdates();
                }
            }
            Targets = new Dictionary<IBaseEntity, Damage>();
            Targets.Add(Attacker.Hero, new Damage(0, 1));
        }

        public static bool WeaponSkill(GameClient Attacker, IBaseEntity Opponent, IConquerItem LeftHand, IConquerItem RightHand, Dictionary<IBaseEntity, Damage> Targets) 
        {
            ushort LeftID = 0, RightID = 0;
            if (LeftHand != null)
                LeftID = ConquerItem.GetItemType(LeftHand.ID);
            if (RightHand != null)
                RightID = ConquerItem.GetItemType(RightHand.ID);

            foreach (var KVP in Attacker.Spells)
            {
                var Spell = KVP.Value;
                var Data = FlatDatabase.GetMAttackData(Spell);

                if ((Data.TargetType != MAttackTargetType.WeaponSkill) || ((LeftID != Data.Weapon) && (RightID != Data.Weapon)))
                    continue;
                if (Data.SpellID == 0)
                    continue;

                if (Data.SuccessRate >= RandomGenerator.Generator.Next(100))
                    ProcessBySort(Attacker.Hero, Attacker, Opponent, &Data, Opponent.X, Opponent.Y, Data.Sort, ref Targets);
            }
            return false;
        }

        public static int MAttackChecks(IBaseEntity Attacker, IBaseEntity Opponent, MAttackData* MData)
        {
            if (MData->SpellID == 0)
                return -1;
            var Client = Attacker.Owner as GameClient;
            if (Client != null)
            {
                if (MData->SpellID == 0)
                    return -2;
                if (MData->Stamina > Client.Stamina)
                    return -3;
                if (MData->Mana > Client.Mana)
                    return -4;
                if (MData->SuccessRate < RandomGenerator.Generator.Next(1, 99))
                    return -5;
                if (Client.InTournament)
                {
                    if (MData->SpellID != 1045 && MData->SpellID != 1046)
                        return -6;
                }
                if (MData->IsXPSkill)
                {
                    if ((Client.Hero.StatusFlag & StatusFlag.XPList) != StatusFlag.XPList)
                        return -6;

                    Client.Hero.StatusFlag &= ~StatusFlag.XPList;
                    Client.Send(new StatTypePacket(Client.Hero.UID, (uint)Client.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize());
                }
                if (Client.Hero.MapID != 1039)
                {
                    Client.Stamina -= (byte)MData->Stamina;
                    SendUpdate(Client, Client.Stamina, StatIDs.Stamina);

                    Client.Mana -= MData->Mana;
                    SendUpdate(Client, Client.Mana, StatIDs.Mana);
                }
            }
            return 0;
        }

        public static void SendUpdate(GameClient Client, uint _Value, StatIDs ID)
        {
            var Stat = StatTypePacket.Create();
            Stat.Value = _Value;
            Stat.ID = ID;
            Stat.UID = Client.Hero.UID;
            Client.Send(Stat, Stat.Size);
        }
    }
}