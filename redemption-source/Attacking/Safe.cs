﻿using System.Drawing;
using System.Linq;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Events;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Mob;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Attacking
{
    public partial class AttackProcessor
    {
        public static bool SafeAttack(IBaseEntity Attacker, IBaseEntity Opponent, sbyte AtkRange)
        {
            if ((Attacker == null) || (Opponent == null))
                return false;
            if (Attacker.MinAttack > Attacker.MaxAttack)
                return false;
            if (Opponent.MapID != Attacker.MapID)
                return false;
            if (Attacker.UID == Opponent.UID)
                return false;
            if (Attacker.Dead || Opponent.Dead)
                return false;
            if (Kernel.GetDistance(Attacker, Opponent) > AtkRange)
                return false;
            if (Opponent.EntityFlag == EntityFlag.Reviver)
                return false;
            if ((Opponent.StatusFlag & StatusFlag.Fly) == StatusFlag.Fly)
            {
                if (Attacker.EntityFlag == EntityFlag.Monster)
                    return false;
            }

            GameClient Target = null;
            if (Opponent.EntityFlag == EntityFlag.Player)
            {
                Target = Opponent.Owner as GameClient;
                if (Target == null)
                    return false;
                if (!Target.LoggedIn)
                    return false;
                if (!Stamps.Check(Target.Hero.Stamps.Protection))
                    return false;
            }
            if (Attacker.EntityFlag == EntityFlag.Player)
            {
                var Client = Attacker.Owner as GameClient;
                #region SOB
                if (Opponent.EntityFlag == EntityFlag.Pole)
                {
                    if (!GuildWar.Active)
                        return false;
                    if (Client != null && Client.Hero.GuildID == 0)
                        return false;
                }
                if ((Opponent.EntityFlag == EntityFlag.Pole) || (Opponent.EntityFlag == EntityFlag.Gate))
                {
                    Client = Attacker.Owner as GameClient;
                    if ((Client != null) && (Client.Guild.Name == GuildWar.PoleHolder))
                        return false;
                }
                #endregion
                if (Opponent.EntityFlag == EntityFlag.Monster)
                {
                    if ((Client != null) && ((Monster)Opponent.Owner).IsGuard && (Client.PkMode != PkMode.Kill))
                        return false;
                }
                if (Client != null && Target != null)
                {
                    #region Tournament Checks
                    if (Client.InTournament)
                    {
                        if (!Client.PkAllowed)
                        {
                            Client.Speak(Color.Red, ChatType.TopLeft, "You cannot PK yet.");
                            return false;
                        }
                        if (TournamentBase.TournamentType == "PassTheBomb")
                        {
                            if (TournamentBase.Started && TournamentBase.Entries.Contains(Client))
                            {
                                if (!Client.HasBomb)
                                    return false;
                            }
                        }

                        var success = TeamDeathMatch.Teams.ContainsKey(Client) && TeamDeathMatch.Teams.ContainsKey(Target);
                        if (success)
                        {
                            if (Client.Equipment[(ushort)ItemPosition.Garment].ID == Target.Equipment[(ushort)ItemPosition.Garment].ID)
                                return false;
                        }
                    }
                    #endregion
                    if (Client.PkMode != PkMode.Kill)
                    {
                        if (Client.PkMode == PkMode.Peace)
                            return false;
                        if (Client.PkMode == PkMode.Capture)
                        {
                            if (((Opponent.StatusFlag & StatusFlag.FlashingName) != StatusFlag.FlashingName) && ((Opponent.StatusFlag & StatusFlag.BlackName) != StatusFlag.BlackName))
                                return false;
                        }
                        #region Team-Mode Pk
                        if (Client.PkMode == PkMode.Team)
                        {
                            if (Client.Team.Active)
                            {
                                if (Client.Team.Teammates.Contains(Target))
                                    return false;
                            }
                            if ((Client.Guild != null) && (Target.Guild != null))
                            {
                                if (Client.Guild.UID == Target.Guild.UID)
                                    return false;
                                if (Client.Guild.Allies.Contains(Target.Guild.Name))
                                    return false;
                            }
                            if (Client.Friends != null)
                            {
                                if (Client.Friends.ContainsKey(Opponent.UID))
                                    return false;
                            }
                            if ((Client.Spouse != "") || (Client.Spouse != "None"))
                            {
                                if (Client.Spouse == Target.Spouse)
                                    return false;
                            }
                        }
                        #endregion
                    }
                    var Settings = new MapSettings(Client.Hero.MapID.ID);
                    if (!Settings.Status.PKing)
                    {
                        Client.Speak(Color.Red, ChatType.TopLeft, "You cannot PK on this map.");
                        return false;
                    }
                }
            }
            return true;
        }

        public static byte SafeHeal(Entity Attacker, IBaseEntity Friend, sbyte Range, bool chkDead)
        {
            if ((Attacker == null) || (Friend == null))
                return 2;
            if (Attacker.MapID.ID != Friend.MapID.ID)
                return 4;
            if (Kernel.GetDistance(Attacker.X, Attacker.Y, Friend.X, Friend.Y) > Range)
                return 5;
            if (Attacker.Dead)
                return 6;
            if (Friend.Dead && chkDead)
                return 7;
            return 0;
        }
    }
}
