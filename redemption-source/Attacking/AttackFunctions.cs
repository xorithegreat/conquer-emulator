﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using RedemptionCO.Client;
using RedemptionCO.Database;
using RedemptionCO.Events;
using RedemptionCO.Item;
using RedemptionCO.Main;
using RedemptionCO.Main.Interfaces;
using RedemptionCO.Mob;
using RedemptionCO.MultiThreading;
using RedemptionCO.Network.Packets.Structures;

namespace RedemptionCO.Attacking
{
    public unsafe partial class AttackFunctions
    {
        public static bool SafePhysicalAttack(Entity Attacker, IBaseEntity Opponent, sbyte AtkRange)
        {
            if ((Attacker == null) || (Opponent == null))
                return false;
            if (Attacker.MinAttack > Attacker.MaxAttack)
                return false;
            if (Opponent.MapID != Attacker.MapID)
                return false;
            if (Attacker.UID == Opponent.UID)
                return false;
            if (Attacker.Dead || Opponent.Dead)
                return false;
            if (Kernel.GetDistance(Attacker, Opponent) > AtkRange)
                return false;
            if (Opponent.EntityFlag == EntityFlag.Reviver)
                return false;
            if ((Opponent.StatusFlag & StatusFlag.Fly) == StatusFlag.Fly)
                if (Attacker.EntityFlag == EntityFlag.Monster)
                    return false;

            GameClient Target = null;
            if (Opponent.EntityFlag == EntityFlag.Player)
            {
                Target = Opponent.Owner as GameClient;
                if (Target == null)
                    return false;
                if (!Target.LoggedIn)
                    return false;
                if (!Stamps.Check(Target.Hero.Stamps.Protection))
                    return false;
            }
            if (Attacker.EntityFlag == EntityFlag.Player)
            {
                var Client = Attacker.Owner as GameClient;
                #region SOB
                if (Opponent.EntityFlag == EntityFlag.Pole)
                {
                    if (!GuildWar.Active)
                        return false;
                    if (Attacker.GuildID == 0)
                        return false;
                }
                if ((Opponent.EntityFlag == EntityFlag.Pole) || (Opponent.EntityFlag == EntityFlag.Gate))
                {
                    Client = Attacker.Owner as GameClient;
                    if ((Client != null) && (Client.Guild.Name == GuildWar.PoleHolder))
                        return false;
                }
                #endregion
                if (Opponent.EntityFlag == EntityFlag.Monster)
                {
                    if ((Client != null) && ((Monster)Opponent.Owner).IsGuard && (Client.PkMode != PkMode.Kill))
                        return false;
                }
                if (Client != null && Target != null)
                {
                    #region Tournament Checks
                    if (Client.inTournament)
                    {
                        if (!Client.PkAllowed)
                        {
                            Client.Speak(Color.Red, ChatType.TopLeft, "You cannot PK yet.");
                            return false;
                        }
                        if (TournamentBase.TournamentType == "PassTheBomb")
                        {
                            if (TournamentBase.Started && TournamentBase.Entries.Contains(Client))
                            {
                                if (!Client.HasBomb)
                                    return false;
                            }
                        }

                        bool success = TeamDeathMatch.Teams.ContainsKey(Client) && TeamDeathMatch.Teams.ContainsKey(Target);
                        if (success)
                        {
                            if (Client.Equipment[(ushort)ItemPosition.Garment].ID == Target.Equipment[(ushort)ItemPosition.Garment].ID)
                                return false;
                        }
                    }
                    #endregion
                    if (Client.PkMode != PkMode.Kill)
                    {
                        if (Client.PkMode == PkMode.Peace)
                            return false;
                        if (Client.PkMode == PkMode.Capture)
                        {
                            if (((Opponent.StatusFlag & StatusFlag.FlashingName) != StatusFlag.FlashingName) && ((Opponent.StatusFlag & StatusFlag.BlackName) != StatusFlag.BlackName))
                                return false;
                        }
                        #region Team-Mode Pk
                        if (Client.PkMode == PkMode.Team)
                        {
                            if (Client.Team.Active)
                                if (Client.Team.Teammates.Contains(Target))
                                    return false;
                            if ((Client.Guild != null) && (Target.Guild != null))
                            {
                                if (Client.Guild.UID == Target.Guild.UID)
                                    return false;
                                if (Client.Guild.Allies.Contains(Target.Guild.Name))
                                    return false;
                            }
                            if (Client.Friends != null)
                                if (Client.Friends.ContainsKey(Opponent.UID))
                                    return false;
                            if ((Client.Spouse != "") || (Client.Spouse != "None"))
                                if (Client.Spouse == Target.Spouse)
                                    return false;
                        }
                        #endregion
                    }
                    var Settings = new MapSettings(Client.Hero.MapID.ID);
                    if (!Settings.Status.PKing)
                    {
                        Client.Speak(Color.Red, ChatType.TopLeft, "You cannot PK on this map.");
                        return false;
                    }
                }
            }
            return true;
        }

        public static byte SafeHeal(Entity Attacker, IBaseEntity Friend, sbyte Range, bool chkDead)
        {
            if ((Attacker == null) || (Friend == null))
                return 2;
            if (Attacker.MapID.ID != Friend.MapID.ID)
                return 4;
            if (Kernel.GetDistance(Attacker.X, Attacker.Y, Friend.X, Friend.Y) > Range)
                return 5;
            if (Attacker.Dead)
                return 6;
            if (Friend.Dead && chkDead)
                return 7;
            return 0;
        }

        private static void AdjustDurability(GameClient Client, IBaseEntity Opponent)
        {
            #region Attacker Durability Loss
            for (ushort pos = 4; pos < 7; pos++)
            {
                if (!Client.Equipment.ContainsKey(pos))
                    continue;

                var item = Client.Equipment[pos];
                if (item.Durability == 0)
                    continue;
                item.Durability--;
                item.Send(Client);
                break;
            }
            #endregion
            #region Defender Durability Loss
            if (Opponent.EntityFlag == EntityFlag.Player)
                for (ushort pos = 0; pos < 9; pos++)
                {
                    if ((pos >= 4) && (pos <= 7))
                        continue;
                    if (!Client.Equipment.ContainsKey(pos))
                        continue;

                    var item = Client.Equipment[pos];
                    if (item.Durability == 0)
                        continue;
                    item.Durability--;
                    item.Send(Client);
                    break;
                }
            #endregion
        }

        public static void AppendToOpponent(Entity Attacker, IBaseEntity Opponent, int Damage = 0, ushort SpellID = 0)
        {
            GameClient Client = null;
            GameClient Attacked = null;

            if (Attacker.EntityFlag == EntityFlag.Player)
                Client = Attacker.Owner as GameClient;
            if (Opponent.EntityFlag == EntityFlag.Player)
                Attacked = Opponent.Owner as GameClient;

            if (DataStructures.ChanceSuccess(10.0))
            {
                if (Client != null)
                    AdjustDurability(Client, Opponent);
            }
            if (Opponent.Dead)
            {
                if (Attacker.EntityFlag == EntityFlag.Monster)
                    ((Monster)Attacker.Owner).Target = null;

                switch (Opponent.EntityFlag)
                {
                    case EntityFlag.Player:
                    case EntityFlag.Monster:
                    case EntityFlag.Pet:
                        ((Entity)Opponent).Die(Attacker);
                        break;
                }
            }
            else
            {
                Opponent.Hitpoints = Math.Min(Opponent.Hitpoints, Opponent.MaxHitpoints);
            }

            switch (Opponent.EntityFlag)
            {
                case EntityFlag.Player:
                    {
                        if (Client != null)
                        {
                            if (Damage == 0)
                                return;
                            if (Client.inTournament)
                                return;
                            if (Attacked.Hero.Flashing)
                                return;

                            var Settings = new MapSettings(Opponent.MapID);
                            if (Settings.Status.CanGainPKPoints)
                            {
                                ushort Gain = 10;
                                if (((Opponent.StatusFlag & StatusFlag.BlackName) != StatusFlag.BlackName) && ((Opponent.StatusFlag & StatusFlag.FlashingName) != StatusFlag.FlashingName))
                                {
                                    if (Opponent.Dead)
                                    {
                                        if ((Client.Guild != null) && (Attacked.Guild != null))
                                        {
                                            if (Attacked.Guild.Enemies.Contains(Client.Hero.Name))
                                                Gain = 3;
                                        }
                                        else if (Client.Enemies.ContainsKey(Opponent.UID))
                                            Gain = 5;
                                        else if ((Opponent.StatusFlag & StatusFlag.RedName) == StatusFlag.RedName)
                                            Gain = 5;
                                        Client.Hero.PkPoints += Gain;
                                    }
                                    AddFlash(Client);
                                    if (Attacked != Client)
                                        Attacked.AddEnemy(Client.Hero.UID, Client.Hero.Name);
                                }
                            }
                        }
                        Attacked.Send(new StatTypePacket(Opponent.UID, (uint)Opponent.Hitpoints, StatIDs.Hitpoints).Serialize());
                        break;
                    }
                case EntityFlag.Monster:
                    if (Attacker.EntityFlag == EntityFlag.Player)
                    {
                        if (((Monster)Opponent.Owner).IsGuard)
                            AddFlash(Client);
                        else
                        {
                            IncrementKillCounter(Client);
                            Experience.ProcessLeveling(Client, Opponent, (uint)Damage, SpellID);
                            if (Client.MonsterHunter)
                            {
                                if (((Monster)Opponent.Owner).Entity.Name == Client.MHMobName && ((Monster)Opponent.Owner).Entity.Dead)
                                {
                                    Client.MHKillCount++;
                                }
                            }
                        }
                    }
                    break;
                case EntityFlag.Pole:
                case EntityFlag.Gate:
                case EntityFlag.TrainingGround:
                    {
                        var sob = Opponent.Owner as SOBMonster;
                        if (sob == null)
                            return;

                        if (sob.OnAttack != null)
                        {
                            if (Attacker.EntityFlag == EntityFlag.Player)
                                sob.OnAttack?.Invoke(Attacker.Owner as GameClient, Damage, SpellID);
                        }
                        if (sob.Hitpoints <= 0)
                            sob.OnDie?.Invoke(Attacker.Owner as GameClient, 0, 0);
                        break;
                    }
            }
        }

        private static void AddFlash(GameClient client)
        {
            client.Hero.Flashing = true;
            client.Hero.Stamps.Flashing = DateTime.Now.AddMilliseconds(DataStructures.Flashing);

            foreach (var obj in client.Screen.Objects)
            {
                if (obj.MapObjType == MapObjectType.Monster)
                {
                    var mob = obj.Owner as Monster;
                    if ((mob != null) && mob.IsGuard)
                        mob.Target = client;
                }
            }
        }

        private static void IncrementKillCounter(GameClient Client)
        {
            if (Client.Hero.EntityFlag != EntityFlag.Player)
                return;

            var UpdateCounter = false;

            Client.XpSkillCounter++;
            if ((Client.Hero.StatusFlag & StatusFlag.Cyclone) == StatusFlag.Cyclone)
                if (UpdateCounter = Client.HeadKillCounter < 30)
                    Client.Hero.Stamps.CycloneFinish = Client.Hero.Stamps.CycloneFinish.AddSeconds(2);
            if ((Client.Hero.StatusFlag & StatusFlag.Superman) == StatusFlag.Superman)
                if (UpdateCounter = Client.HeadKillCounter < 30)
                    Client.Hero.Stamps.SupermanFinish = Client.Hero.Stamps.SupermanFinish.AddSeconds(2);
            if (UpdateCounter)
                Client.HeadKillCounter++;
        }

        public static int AppendMAttack(IBaseEntity Attacker, IBaseEntity Opponent, MAttackData* MData)
        {
            switch (Attacker.EntityFlag)
            {
                case EntityFlag.Monster:
                    {
                        if ((MData->BaseDamage > 0) && (MData->BaseDamage < 30000) && !MData->MultipleTargets && (Opponent != null))
                        {
                            var damage = CalculateMagicDamage(Attacker, Opponent, MData->SpellID);
                            Opponent.Hitpoints = Math.Max(0, Opponent.Hitpoints -= damage);
                            AppendToOpponent(Attacker as Entity, Opponent, damage, MData->SpellID);
                            return damage;
                        }
                        break;
                    }
                case EntityFlag.Player:
                    {
                        var Client = Attacker.Owner as GameClient;
                        if (Client == null)
                            return -1;
                        if (MData->SpellID == 0)
                            return -2;
                        if (MData->Stamina > Client.Stamina)
                            return -3;
                        if (MData->Mana > Client.Mana)
                            return -4;
                        if (MData->Sucess < RandomGenerator.Generator.Next(1, 99))
                            return -5;
                        if (Client.inTournament)
                        {
                            if (MData->SpellID != 1045 && MData->SpellID != 1046)
                                return -6;
                        }
                        if (MData->IsXPSpell)
                        {
                            if ((Client.Hero.StatusFlag & StatusFlag.XPList) != StatusFlag.XPList)
                                return -6;

                            Client.Hero.StatusFlag &= ~StatusFlag.XPList;
                            Client.Send(new StatTypePacket(Client.Hero.UID, (uint)Client.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize());
                        }
                        if (Client.Hero.MapID != 1039)
                        {
                            Client.Stamina -= MData->Stamina;
                            SendUpdate(Client, Client.Stamina, StatIDs.Stamina);

                            Client.Mana -= MData->Mana;
                            SendUpdate(Client, Client.Mana, StatIDs.Mana);
                        }
                        if ((MData->BaseDamage > 0) && (MData->BaseDamage < 30000) && !MData->MultipleTargets && (Opponent != null))
                        {
                            if (MData->Aggressive)
                            {
                                if (!SafePhysicalAttack(Client.Hero, Opponent, DataStructures.ViewDistance))
                                    return -7;

                                var damage = CalculateMagicDamage(Attacker, Opponent, MData->SpellID);
                                Opponent.Hitpoints = Math.Max(0, Opponent.Hitpoints -= damage);
                                AppendToOpponent(Client.Hero, Opponent, damage, MData->SpellID);
                                return damage;
                            }
                            if (SafeHeal(Client.Hero, Opponent as Entity, DataStructures.ViewDistance, true) == 0)
                            {
                                Opponent.Hitpoints = Math.Min(Opponent.Hitpoints + MData->BaseDamage, Opponent.MaxHitpoints);
                                AppendToOpponent(Client.Hero, Opponent, 0, MData->SpellID);
                            }
                            else
                                return 0;
                            return MData->BaseDamage;
                        }
                        break;
                    }
            }
            return 0;
        }

        public static void SendUpdate(GameClient Client, uint Value, StatIDs ID)
        {
            var Stat = StatTypePacket.Create();
            Stat.NewValue = Value;
            Stat.ID = ID;
            Stat.UID = Client.Hero.UID;
            Client.Send(Stat, Stat.Size);
        }

        public static Dictionary<uint, int> PhysHitTargetsInRange(GameClient Client, sbyte Range, PkMode Pk, ushort SpellID = 0)
        {
            var Result = new Dictionary<uint, int>();
            Entity Object = null;
            #region Players
            foreach (var MapObj in Client.Screen.Objects)
            {
                if (MapObj == null)
                    continue;
                if ((MapObj.MapObjType != MapObjectType.Player) && (MapObj.MapObjType != MapObjectType.Monster))
                    continue;
                if (MapObj.MapObjType == MapObjectType.Player)
                    Object = ((GameClient)MapObj.Owner).Hero;
                else if (MapObj.MapObjType == MapObjectType.Monster)
                    Object = ((Monster)MapObj.Owner).Entity;

                if (!Result.ContainsKey(MapObj.UID) && SafePhysicalAttack(Client.Hero, Object, Range))
                {
                    var Damage = SpellID == 0 ? CalcDamage(Client.Hero, Object) : CalculateMagicDamage(Client.Hero, Object, SpellID);
                    Result.Add(MapObj.UID, Damage);

                    if (Object != null)
                    {
                        Object.Hitpoints = Math.Max(Object.Hitpoints -= Damage, 0);
                        AppendToOpponent(Client.Hero, Object, Damage);
                    }
                }
            }
            #endregion
            return Result;
        }
    }
}