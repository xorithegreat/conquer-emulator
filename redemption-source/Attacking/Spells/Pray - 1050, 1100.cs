﻿using System.Collections.Generic;
using ConquerEmulator.Client;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;

namespace ConquerEmulator.Attacking
{
    public partial class AttackProcessor
    {
        public static void Pray(GameClient Attacker, IBaseEntity Opponent, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            if (Opponent != null)
            {
                if (Opponent.EntityFlag == EntityFlag.Player && Opponent.Dead)
                {
                    if (SafeHeal(Attacker.Hero, Opponent, DataStructures.ViewDistance, false) == 0)
                    {
                        var OpponentClient = Opponent.Owner as GameClient;
                        OpponentClient.Revive(OpponentClient, true);
                    }
                }
                Targets = new Dictionary<IBaseEntity, Damage>();
                Targets.Add(Opponent, new Damage(0, 0));
            }
        }
    }
}
