﻿using System;
using System.Collections.Generic;
using ConquerEmulator.Client;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Attacking
{
    public unsafe partial class AttackProcessor
    {
        public static void Meditation(GameClient Attacker, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            Attacker.Mana = (ushort)Math.Min(Attacker.MaxMana, Attacker.Mana + Data->BaseDamage);
            SendUpdate(Attacker, Attacker.Mana, StatIDs.Mana);

            Targets = new Dictionary<IBaseEntity, Damage>();
            Targets.Add(Attacker.Hero, new Damage(Data->BaseDamage, Data->BaseDamage));
        }
    }
}
