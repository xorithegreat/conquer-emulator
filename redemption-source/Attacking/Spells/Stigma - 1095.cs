﻿using System;
using System.Collections.Generic;
using ConquerEmulator.Client;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Attacking
{
    public unsafe partial class AttackProcessor
    {
        public const double StigmaPercent = 1.20;
        public static void Stigma(GameClient Attacker, IBaseEntity Opponent, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            if (Opponent != null)
            {
                if (Opponent.EntityFlag == EntityFlag.Player)
                {
                    var OpClient = (GameClient)Opponent.Owner;
                    OpClient.Hero.StatusFlag |= StatusFlag.Stigma;
                    OpClient.Hero.Stamps.StigmaTimeUp = DateTime.Now.AddSeconds(Data->SecondsTimer);
                    OpClient.SendScreen(new StatTypePacket(OpClient.Hero.UID, (uint)OpClient.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize(), true);
                }
                Targets = new Dictionary<IBaseEntity, Damage>();
                Targets.Add(Opponent, new Damage(0, 0));
            }
        }
    }
}
