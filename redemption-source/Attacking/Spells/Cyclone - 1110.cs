﻿using System;
using System.Collections.Generic;
using ConquerEmulator.Client;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Attacking
{
    public unsafe partial class AttackProcessor
    {
        public static void Cyclone(GameClient Attacker, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            Attacker.Hero.Stamps.CycloneFinish = DateTime.Now.AddSeconds(Data->SecondsTimer);
            if ((Attacker.Hero.StatusFlag & StatusFlag.Cyclone) != StatusFlag.Cyclone)
            {
                Attacker.HeadKillCounter = 0;
                Attacker.Hero.StatusFlag |= StatusFlag.Cyclone;
            }

            Attacker.SendScreen(new StatTypePacket(Attacker.Hero.UID, (uint)Attacker.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize(), true);
            Targets = new Dictionary<IBaseEntity, Damage>();
            Targets.Add(Attacker.Hero, new Damage(0, 0));
        }
    }
}
