﻿using System;
using System.Collections.Generic;
using ConquerEmulator.Client;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Attacking
{
    public unsafe partial class AttackProcessor
    {
        public static void Invisibility(IBaseEntity Opponent, MAttackData* Data, ref Dictionary<IBaseEntity, Damage> Targets)
        {
            var Attacked = Opponent.Owner as GameClient;
            if (Attacked != null)
            {
                Opponent.StatusFlag &= ~StatusFlag.Stigma;
                Opponent.StatusFlag |= StatusFlag.Invisibility;
                Attacked.Hero.Stamps.Invisibility = DateTime.Now.AddSeconds(Data->SecondsTimer);

                Attacked.SendScreen(new StatTypePacket(Opponent.UID, (uint)Opponent.StatusFlag, StatIDs.RaiseFlag).Serialize(), true);

                Targets = new Dictionary<IBaseEntity, Damage>();
                Targets.Add(Attacked.Hero, new Damage(0, 0));
            }
        }
    }
}
