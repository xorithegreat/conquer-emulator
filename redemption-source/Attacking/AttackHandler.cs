﻿using System;
using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Attacking
{
    internal unsafe class AttackHandler
    {
        public static void Handle(GameClient Client, byte* Packet)
        {
            if (Client.Hero.Dead)
                return;

            var TimeStamp = Native.timeGetTime();
            var Request = (RequestAttack*)Packet;
            var Opponent = Kernel.FindClient(Request->TargetUID);

            if ((Request->AttackType == AttackType.None) || Client.Hero.Dead)
                return;

            IMapObject Target;
            if (Request->AttackType == AttackType.Magic)
                Request->Decrypt(Client.SpellCrypto);
            if (Request->TargetUID == Client.Hero.UID)
                Target = Client.Hero;
            else
                Target = Client.Screen.FindObject(Request->TargetUID);

            #region BotCheck
            if (!Client.TimeFraud.Validate(TimeStamp, "InteractQuery"))
                return;
            #endregion

            Client.Hero.Action = ConquerAction.None;

            switch (Request->AttackType)
            {
                #region Magic
                case AttackType.Magic:
                    {
                        if (Client.Hero.InTransformation)
                            return;

                        if (Stamps.Check(Client.Hero.Stamps.Attack))
                        {
                            if (Request->SpellID == 1015)
                                Client.Hero.Stamps.Attack = DateTime.Now.AddMilliseconds(1250);
                            else
                                Client.Hero.Stamps.Attack = DateTime.Now.AddMilliseconds(500);

                            Client.Hero.Facing = Kernel.GetFacing(Kernel.GetAngle(Client.Hero.X, Client.Hero.Y, Request->TargetX, Request->TargetY));

                            var target = Target as IBaseEntity;
                            if (AttackProcessor.MagicalAttack(Client.Hero, target, Request->SpellID, Request->TargetX, Request->TargetY))
                            {
                                if (target == null && Client.Hero.MapID == 1039)
                                {
                                    Client.AutoAttaking = true;
                                    Client.AAMagic = PacketKernel.GetBytes(Request, Request->Size);
                                    Client.AaType = Request->AttackType;
                                    Client.AAX = Client.Hero.X;
                                    Client.AAY = Client.Hero.Y;
                                }
                            }
                        }
                        break;
                    }
                #endregion
                #region Archer + Physical
                case AttackType.Archer:
                case AttackType.Physical:
                    {
                        if (Request->AttackerUID == Client.Hero.UID)
                        {
                            if (Stamps.Check(Client.Hero.Stamps.Attack))
                            {
                                if ((Client.Hero.StatusFlag & StatusFlag.Cyclone) == StatusFlag.Cyclone)
                                    Client.Hero.Stamps.Attack = DateTime.Now.AddMilliseconds((double)Client.Hero.Speed / 10);
                                else
                                    Client.Hero.Stamps.Attack = DateTime.Now.AddMilliseconds(Client.Hero.Speed);

                                var target = Target as IBaseEntity;
                                if (AttackProcessor.PhysicalAttack(Client.Hero, target, Request->AttackType, false))
                                {
                                    if (target?.EntityFlag != EntityFlag.Player)
                                    {
                                        Client.AutoAttaking = true;
                                        Client.AAMagic = null;
                                        Client.AATarget = Target as IBaseEntity;
                                        Client.AaType = Request->AttackType;
                                        Client.AAX = Client.Hero.X;
                                        Client.AAY = Client.Hero.Y;
                                    }
                                }
                            }
                        }
                        break;
                    }
                #endregion
                #region Marriage
                #region Request
                case AttackType.RequestMariage:
                    {
                        if ((Opponent != null) && ((Opponent.Spouse == "None") || (Opponent.Spouse == "")))
                        {
                            if ((Client.Spouse != "None") || (Client.Spouse != ""))
                            {
                                Client.Speak(Color.Red, ChatType.Center, "You're already married!");
                                return;
                            }
                            if (Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, Opponent.Hero.X, Opponent.Hero.Y) > DataStructures.ViewDistance)
                            {
                                Client.Speak(Color.Turquoise, ChatType.Talk, "The target is to far away.");
                                return;
                            }
                            Request->TargetUID = Client.Hero.UID;
                            Opponent.Send(PacketKernel.GetBytes(Request, Request->Size));
                            Client.Speak(Color.Turquoise, ChatType.TopLeft, "Request has been sent to your love.");
                        }
                        break;
                    }
                #endregion
                #region Accept
                case AttackType.AcceptMariage:
                    {
                        if (Client.Hero.UID != Request->TargetUID)
                            return;

                        if ((Opponent.Spouse != null) && ((Opponent.Spouse == "None") || (Opponent.Spouse == "")) && ((Client.Spouse == "None") || (Client.Spouse == "")))
                        {
                            if (Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, Opponent.Hero.X, Opponent.Hero.Y) > DataStructures.ViewDistance)
                            {
                                Client.Speak(Color.Turquoise, ChatType.Talk, "The Target is to far away.");
                                return;
                            }
                            Client.Spouse = Opponent.Hero.Name;
                            Opponent.Spouse = Client.Hero.Name;
                            MySqlDatabase.SaveSpouse(Client, Opponent);
                            #region Spouse Name Sending
                            var SpousePacket = new StringPacket();
                            SpousePacket.ID = StringIds.Spouse;
                            SpousePacket.UID = Client.Hero.UID;
                            Client.Send(Packets.StringPacket(SpousePacket, Client.Spouse));

                            var SpousePacket2 = new StringPacket();
                            SpousePacket2.ID = StringIds.Spouse;
                            SpousePacket2.UID = Opponent.Hero.UID;
                            Opponent.Send(Packets.StringPacket(SpousePacket2, Opponent.Spouse));
                            #endregion
                            var IPacket = new ItemUsagePacket(Client.Hero.UID, 0, ItemUsageID.Fireworks);
                            Client.SendScreen(IPacket.Serialize(), true);

                            GameClient.SpeakToAll(Color.White, ChatType.Center, Client.Hero.Name + " and " + Client.Spouse + " have been united in holy matrimony!");
                        }
                        break;
                    }
                #endregion
                #endregion
                #region Merchant
                case AttackType.AcceptMerchant:
                    {
                        Client.Send(new StatTypePacket(Client.Hero.UID, (uint)MerchantTypes.Yes, StatIDs.Merchant).Serialize());
                        Client.Merchant = MerchantTypes.Yes;
                        Client.Send(*Request, Request->Size);
                        break;
                    }
                case AttackType.DeclineMerchant:
                    {
                        Client.Send(new StatTypePacket(Client.Hero.UID, (uint)MerchantTypes.No, StatIDs.Merchant).Serialize());
                        Client.Merchant = MerchantTypes.No;
                        Client.Send(*Request, Request->Size);
                        break;
                    }
                #endregion
                #region CloudSaint'sJar
                case AttackType.CloudSaintJar:
                    {
                        Request->AttackerUID = Client.Hero.UID;
                        Request->KillCount = Client.MHKillCount; //Current Amount of kills you have
                        Client.Send(PacketKernel.GetBytes(Request, Request->Size));
                        break;
                    }
                    #endregion
            }
        }
    }
}