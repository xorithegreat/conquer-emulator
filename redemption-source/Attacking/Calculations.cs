﻿using System;
using ConquerEmulator.Client;
using ConquerEmulator.Item;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Attacking
{
    public delegate int DamageCalculationCallback<T>(int Base, IBaseEntity Attacker, IBaseEntity Opponent, T Arg);

    public partial class AttackProcessor
    {
        public static DamageCalculationCallback<object> MagicDamage = CalculateMagicDmg;
        public static DamageCalculationCallback<double> PhysicalDamage = CalculatePhysDmg;
        public static DamageCalculationCallback<double> ArcherDamage = CalculateArchDmg;

        public static int CalculatePhysDmg(int Base, IBaseEntity Attacker, IBaseEntity Opponent, double Offset)
        {
            Base = (int)(Math.Min(Base, 100000) * Offset);
            if (Attacker.EntityFlag == EntityFlag.Monster)
                Base = (int)(Base * (1 + (GetLevelBonus(Attacker.Level, Opponent.Level) * 0.08)));
            if ((Attacker.StatusFlag & StatusFlag.Stigma) == StatusFlag.Stigma)
                Base = (int)(Base * StigmaPercent);
            if (Attacker.EntityFlag == EntityFlag.Player)
            {
                var Client = Attacker.Owner as GameClient;
                if (Client.Equipment.ContainsKey((ushort)ItemPosition.Left) || Client.Equipment.ContainsKey((ushort)ItemPosition.Right))
                    Base = (int)(Base * 1.5);
                if ((Client.Hero.StatusFlag & StatusFlag.Superman) == StatusFlag.Superman)
                {
                    if (Opponent.EntityFlag == EntityFlag.Player)
                        Base *= 3;
                    else
                        Base *= 10;
                }
            }
            if (Opponent.EntityFlag == EntityFlag.Player)
            {
                if ((Opponent.StatusFlag & StatusFlag.MagicShield) == StatusFlag.MagicShield)
                {
                    Base -= Opponent.Defence * 2;
                }
            }
            Base -= Opponent.Defence;
            Base = RemoveExcessDamage(Base, Attacker, Opponent);
            Base = Math.Max(Base, 1);

            return Base;
        }

        public static int CalculatePhysDmg(IBaseEntity Attacker, IBaseEntity Opponent, double Offset)
        {
            return CalculatePhysDmg(RandomGenerator.Generator.Next((int)Attacker.MinAttack, (int)Attacker.MaxAttack), Attacker, Opponent, Offset);
        }

        public static int RemoveExcessDamage(int CurrentDamage, IBaseEntity Attacker, IBaseEntity Opponent)
        {
            if (Opponent.EntityFlag != EntityFlag.Player)
                return CurrentDamage;
            var Client = Opponent.Owner as GameClient;
            if (Client.Hero.Reborn == 1)
                CurrentDamage = (int)Math.Round(CurrentDamage * 0.7);
            else if (Client.Hero.Reborn == 2)
                CurrentDamage = (int)Math.Round(CurrentDamage * 0.5);
            CurrentDamage = (int)Math.Round(CurrentDamage * (1.00 - (Client.BlessPercent * 0.01)));

            if (Client.Gems[GemsConst.TortoiseGem] > 0)
                CurrentDamage = (int)Math.Round(CurrentDamage * Math.Max(1.00 - Client.Gems[GemsConst.TortoiseGem], 0.50));

            return CurrentDamage;
        }

        public static int CalculateMagicDmg(int Base, IBaseEntity Attacker, IBaseEntity Opponent, object Omit)
        {
            Base += (int)Attacker.MagicAttack;
            Base = (int)((Base * 0.75) * (1 - (Opponent.MDefence * 0.01)) - Opponent.PlusMDefence);
            if (Opponent.EntityFlag == EntityFlag.Monster)
            {
                Base *= GetLevelBonus(Attacker.Level, Opponent.Level);
                Base = (int)(Base * 0.55);
            }
            Base = Math.Max(1, Base);
            return Base;
        }

        public static int CalculateArchDmg(int Base, IBaseEntity Attacker, IBaseEntity Opponent, double Factor)
        {
            if (Opponent.EntityFlag == EntityFlag.Monster)
            {
                if (Attacker.Level > Opponent.Level)
                    Base *= GetLevelBonus(Attacker.Level, Opponent.Level);
                Base = (int)(Base * (1.50 - Opponent.Dodge * 0.01));
            }
            else
            {
                var Dodge = (float)(1.00 - (Math.Min((int)Opponent.Dodge, 95) * 0.01));
                Base = (int)(Base * Dodge);
            }
            Base = RemoveExcessDamage(Base, Attacker, Opponent);
            Base = Math.Max((int)(Base * Factor), 1);
            return Base;
        }

        public static int GetLevelBonus(int AttackerLevel, int TargetLevel)
        {
            var num = AttackerLevel - TargetLevel;
            var bonus = 0;
            if (num >= 3)
            {
                num -= 3;
                bonus = 1 + (num / 5);
            }
            return bonus;
        }

        public static int CalculateExpBonus(ushort Level, ushort MonsterLevel, int Experience)
        {
            var leveldiff = (2 + Level - MonsterLevel);
            if (leveldiff < -5)
                Experience = (int)(Experience * 1.3);
            else if (leveldiff < -1)
                Experience = (int)(Experience * 1.2);
            else if (leveldiff == 4)
                Experience = (int)(Experience * 0.8);
            else if (leveldiff == 5)
                Experience = (int)(Experience * 0.3);
            else if (leveldiff > 5)
                Experience = (int)(Experience * 0.1);
            return Experience;
        }
    }
}