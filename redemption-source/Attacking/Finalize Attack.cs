﻿using System;
using System.Collections.Generic;
using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Mob;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;
using ConquerEmulator.Npc.Quests;

namespace ConquerEmulator.Attacking
{
    public partial class AttackProcessor
    {
        private static void AdjustDurability(GameClient Client, IBaseEntity Opponent)
        {
            #region Attacker Durability Loss
            for (ushort pos = 4; pos < 7; pos++)
            {
                if (!Client.Equipment.ContainsKey(pos))
                    continue;

                var item = Client.Equipment[pos];
                if (ConquerItem.IsItemType(item.UID, ItemType.Arrow))
                    continue;

                if (item.Durability > 0)
                {
                    item.Durability--;
                    item.Send(Client);
                }
                break;
            }
            #endregion
            #region Defender Durability Loss
            if (Opponent.EntityFlag == EntityFlag.Player)
            {
                for (ushort pos = 0; pos < 9; pos++)
                {
                    if ((pos >= 4) && (pos <= 7))
                        continue;
                    if (!Client.Equipment.ContainsKey(pos))
                        continue;

                    var item = Client.Equipment[pos];
                    if (item.Durability > 0)
                    {
                        item.Durability--;
                        item.Send(Client);
                    }
                    break;
                }
            }
            #endregion
        }

        public static void FinalizeAttack(Entity Attacker, Dictionary<IBaseEntity, Damage> Targets, MAttackData MData)
        {
            GameClient AttackerClient = null;
            GameClient OpponentClient = null;
            IBaseEntity Opponent;

            foreach (var DE in Targets)
            {
                Opponent = DE.Key;
                if (Attacker.EntityFlag == EntityFlag.Player)
                    AttackerClient = Attacker.Owner as GameClient;
                if (Opponent.EntityFlag == EntityFlag.Player)
                    OpponentClient = Opponent.Owner as GameClient;

                if (DataStructures.ChanceSuccess(10.0))
                {
                    if (AttackerClient != null)
                        AdjustDurability(AttackerClient, Opponent);
                }
                if (Opponent.Dead)
                {
                    switch (Opponent.EntityFlag)
                    {
                        case EntityFlag.Player:
                        case EntityFlag.Monster:
                        case EntityFlag.Pet:
                            ((Entity)Opponent).Die(Attacker);
                            break;
                    }
                }

                switch (Opponent.EntityFlag)
                {
                    case EntityFlag.Player:
                        {
                            if (AttackerClient != null)
                            {
                                if (DE.Value.Show == 0)
                                    return;
                                if (AttackerClient.InTournament)
                                    return;
                                if (OpponentClient.Hero.Flashing)
                                    return;
                                if (MData.SpellID != 0 && !MData.Aggressive)
                                    return;

                                var Settings = new MapSettings(Opponent.MapID);
                                if (Settings.Status.CanGainPKPoints)
                                {
                                    ushort Gain = 10;
                                    if (((Opponent.StatusFlag & StatusFlag.BlackName) != StatusFlag.BlackName) && ((Opponent.StatusFlag & StatusFlag.FlashingName) != StatusFlag.FlashingName))
                                    {
                                        if (Opponent.Dead)
                                        {
                                            if ((AttackerClient.Guild != null) && (OpponentClient.Guild != null))
                                            {
                                                if (OpponentClient.Guild.Enemies.Contains(AttackerClient.Hero.Name))
                                                    Gain = 3;
                                            }
                                            else if (AttackerClient.Enemies.ContainsKey(Opponent.UID))
                                                Gain = 5;
                                            else if ((Opponent.StatusFlag & StatusFlag.RedName) == StatusFlag.RedName)
                                                Gain = 5;
                                            AttackerClient.Hero.PkPoints += Gain;

                                            if (AttackerClient.Hero.PkPoints > 30000)
                                                AttackerClient.Hero.PkPoints = 30000;
                                        }
                                        AddFlash(AttackerClient);
                                        if (OpponentClient != AttackerClient)
                                            OpponentClient.AddEnemy(AttackerClient.Hero.UID, AttackerClient.Hero.Name);
                                    }
                                }
                            }
                            OpponentClient.Send(new StatTypePacket(Opponent.UID, (uint)Opponent.Hitpoints, StatIDs.Hitpoints).Serialize());
                            break;
                        }
                    case EntityFlag.Monster:
                        if (Attacker.EntityFlag == EntityFlag.Player)
                        {
                            var Monster = (Monster)Opponent.Owner;
                            if (Monster.IsGuard)
                                AddFlash(AttackerClient);
                            else
                            {
                                IncrementKillCounter(AttackerClient);
                                Experience.ProcessLeveling(AttackerClient, Opponent, (uint)DE.Value.Show, MData.SpellID);

                                if (!Monster.Entity.Dead)
                                    return;

                                #region MonsterHunter
                                if (AttackerClient.MonsterHunter)
                                {
                                    if (Monster.Type == AttackerClient.MonsterType)
                                        AttackerClient.MHKillCount++;
                                }
                                #endregion
                                #region DisCity
                                if (AttackerClient.Hero.MapID == 2022)
                                {
                                    if (Monster.Type == 402)
                                        AttackerClient.DisCityKills += 3;
                                    else AttackerClient.DisCityKills++;
                                }
                                if (AttackerClient.Hero.MapID == 2023)
                                {
                                    if (DisCity.RightFlank.ContainsKey(Attacker.UID))
                                    {
                                        if (DisCity.RightKills == 75 || DisCity.RightKills == 150)
                                        {
                                            GameClient.SpeakToMap(Color.Red, ChatType.Talk, $"The left flank has reached {DisCity.LeftKills} kills.", AttackerClient.Hero.MapID, AttackerClient.Hero.MapID.DynamicID);
                                            GameClient.SpeakToMap(Color.Red, ChatType.Talk, $"The right flank has reached {DisCity.RightKills} kills.", AttackerClient.Hero.MapID, AttackerClient.Hero.MapID.DynamicID);
                                        }
                                        DisCity.RightKills++;
                                    }
                                    else
                                    {
                                        if (DisCity.LeftKills == 75 || DisCity.LeftKills == 150)
                                        {
                                           GameClient.SpeakToMap(Color.Red, ChatType.Talk, $"The right flank has reached {DisCity.RightKills} kills.", AttackerClient.Hero.MapID, AttackerClient.Hero.MapID.DynamicID);
                                           GameClient.SpeakToMap(Color.Red, ChatType.Talk, $"The left has reached {DisCity.LeftKills} kills.", AttackerClient.Hero.MapID, AttackerClient.Hero.MapID.DynamicID);
                                        }
                                        DisCity.LeftKills++;
                                    }
                                }
                                if (DisCity.RightKills >= 200 || DisCity.LeftKills >= 200)
                                {
                                    var WinningFlank = DisCity.RightKills > 200 ? DisCity.RightFlank : DisCity.LeftFlank;
                                    foreach (var pClient in WinningFlank.Values)
                                    {
                                        pClient.Teleport(2024, 150, 284);
                                        pClient.Speak(Color.Red, ChatType.Talk, "Congratulations your flank advanced to the final stage!");
                                    }
                                    foreach (var pClient in Kernel.Clients)
                                    {
                                        if (pClient.Hero.MapID >= 4021 && pClient.Hero.MapID <= 4023)
                                        {
                                            pClient.Teleport(1020, 566, 564);
                                            pClient.Speak(Color.Red, ChatType.Talk, "There was somebody who reached stage 4 already. Better luck next time!");
                                        }
                                    }

                                    DisCity.LeftKills = 0;
                                    DisCity.RightKills = 0;
                                }
                                if (Monster.Type == 701)
                                {
                                    AttackerClient.Teleport(1020, 566, 564);
                                    GameClient.SpeakToAll(Color.Red, ChatType.Center, $"DisCity was won by {Attacker.Name}. Better luck next time");

                                    foreach (var pClient in Kernel.Clients)
                                    {
                                        if (pClient.Hero.MapID >= 4021 && pClient.Hero.MapID <= 4023)
                                            pClient.Teleport(1020, 566, 564);
                                    }
                                    DisCity.Clear();
                                }
                                if (Monster.Type == 3156)
                                {
                                    if (DisCity.Syrens > 0)
                                        DisCity.Syrens--;
                                    else
                                    {
                                        Kernel.SpawnMonster(5059, 147, 36, 2024);
                                        GameClient.SpeakToMap(Color.Yellow, ChatType.Center, "Ultimate Pluto has spawned hurry up to defeat him!", 2024, 0);
                                    }
                                }
                                #endregion

                                if ((Attacker.StatusFlag == StatusFlag.Cyclone) || (Attacker.StatusFlag == StatusFlag.Superman))
                                    AttackerClient.SendScreen(Packets.Attack(Attacker.UID, 0, 0, Monster.Entity.UID, (int)(65536 * AttackerClient.HeadKillCounter), AttackType.Kill), true);
                                else
                                    AttackerClient.SendScreen(Packets.Attack(Attacker.UID, 0, 0, Monster.Entity.UID, 1, AttackType.Kill), true);
                            }
                        }
                        break;
                    case EntityFlag.Pole:
                    case EntityFlag.Gate:
                    case EntityFlag.TrainingGround:
                        {
                            var sob = Opponent.Owner as SOBMonster;
                            if (sob == null)
                                return;

                            if (sob.OnAttack != null)
                            {
                                if (Attacker.EntityFlag == EntityFlag.Player)
                                    sob.OnAttack?.Invoke(Attacker.Owner as GameClient, DE.Value.Show, MData.SpellID);
                            }
                            if (sob.Hitpoints <= 0)
                                sob.OnDie?.Invoke(Attacker.Owner as GameClient, 0, 0);
                            break;
                        }
                }
            }
        }

        private static void AddFlash(GameClient client)
        {
            client.Hero.Flashing = true;
            client.Hero.Stamps.Flashing = DateTime.Now.AddMilliseconds(DataStructures.Flashing);

            foreach (var obj in client.Screen.Objects)
            {
                if (obj.MapObjType == MapObjectType.Monster)
                {
                    var mob = obj.Owner as Monster;
                    if ((mob != null) && mob.IsGuard)
                        mob.Target = client;
                }
            }
        }

        private static void IncrementKillCounter(GameClient Client)
        {
            if (Client.Hero.EntityFlag != EntityFlag.Player)
                return;

            var UpdateCounter = false;

            Client.XpSkillCounter++;
            if ((Client.Hero.StatusFlag & StatusFlag.Cyclone) == StatusFlag.Cyclone)
            {
                if (UpdateCounter = Client.HeadKillCounter < 30)
                    Client.Hero.Stamps.CycloneFinish = Client.Hero.Stamps.CycloneFinish.AddSeconds(2);
            }
            if ((Client.Hero.StatusFlag & StatusFlag.Superman) == StatusFlag.Superman)
            {
                if (UpdateCounter = Client.HeadKillCounter < 30)
                    Client.Hero.Stamps.SupermanFinish = Client.Hero.Stamps.SupermanFinish.AddSeconds(2);
            }
            if (UpdateCounter)
                Client.HeadKillCounter++;
        }
    }
}