﻿using System;

namespace ConquerEmulator.Attacking
{
    public struct Damage
    {
        public int Experience;
        public int Show;
        public Damage(int show, int exp)
        {
            Show = show;
            Experience = exp;
        }
    }
}
