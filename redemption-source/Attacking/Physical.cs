﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Attacking
{
	public partial class AttackProcessor
	{
	    public static bool PhysicalAttack(Entity Attacker, IBaseEntity Opponent, AttackType type, bool IsGuard)
		{
			try
			{
                var Success = false;
                sbyte AtkRange = 2;
				IConquerItem LeftHand, RightHand;
				GameClient Client = null;

			    var Targets = new Dictionary<IBaseEntity, Damage>(1);
				if (Attacker.EntityFlag == EntityFlag.Player)
				{
					Client = (GameClient)Attacker.Owner;

                    if (Client.InTournament)
                        return false;

                    AtkRange += Client.AttackRange;

                    if (!SafeAttack(Attacker, Opponent, AtkRange))
                        return false;

				    Client.Equipment.TryGetValue((ushort)ItemPosition.Right, out RightHand);
                    Client.Equipment.TryGetValue((ushort)ItemPosition.Left, out LeftHand);

                    if (RightHand != null && ConquerItem.GetItemType(RightHand.ID) == 500)
                        Success = ArcherAttack(Client, Opponent, ref Targets);
                    else
                    {
                        Success = WeaponSkill(Client, Opponent, LeftHand, RightHand, Targets);
                    }
                }
                if (Targets.Count == 0 && !Success && type == AttackType.Physical && Opponent != null)
                {
                    Success = true;
                    var damage = new Damage();
                    damage.Show = CalculatePhysDmg(Attacker, Opponent, 1.00);
                    damage.Experience = Math.Min(damage.Show, Opponent.Hitpoints);
                    Opponent.Hitpoints -= Math.Min(damage.Show, Opponent.Hitpoints);
                    Targets.Add(Opponent, damage);
                }
                if (Success && Targets.Count > 0)
                {
                    var owner = Opponent.Owner as GameClient;
                    if (owner != null)
                        owner.SendScreen(Packets.Attack(Attacker.UID, Attacker.X, Attacker.Y, Opponent.UID, Targets[Opponent].Show, AttackType.Physical), true);
                    else
                        Client?.SendScreen(Packets.Attack(Attacker.UID, Attacker.X, Attacker.Y, Opponent.UID, Targets[Opponent].Show, AttackType.Physical), true);
                    FinalizeAttack(Attacker, Targets, new MAttackData());
                }
                return Success;
			}
			catch (Exception e)
			{
				LogHandler.WriteLine(e);
				return false;
			}
		}

	    private static bool ArcherAttack(GameClient Attacker, IBaseEntity Opponent, ref Dictionary<IBaseEntity, Damage> Targets) 
		{
			var Quivery = Attacker.GetEquipmentItem(ItemPosition.Left);
		    if (Quivery != null)
		    {
		        if (Attacker.Hero.MapID.ID != 1039)
		        {
		            //Quivery.Arrows -= 1;
		            if (Quivery.Arrows == 0)
		            {
		                Attacker.Unequip(Attacker, (ushort)ItemPosition.Left);
                        Attacker.RemoveInventory(Quivery);

                        var quivery = Attacker.Equipment.Values.FirstOrDefault(x => ConquerItem.IsItemType(x.ID, ItemType.Arrow));
		                if (quivery != null)
		                {
		                    Attacker.Equip(quivery, (ushort)ItemPosition.Left, Attacker);
		                    return true;
		                }
		                return false;
		            }
		            var Update = new ItemUsagePacket(Quivery.UID, Quivery.Durability, ItemUsageID.UpdateDurability);
		            Attacker.Send(Update, Update.Size);
		        }

		        if (Opponent != null)
		        {
                    var damage = new Damage();
                    damage.Show = CalculateArchDmg(RandomGenerator.Generator.Next((int)Attacker.Hero.MinAttack, (int)Attacker.Hero.MaxAttack),
                        Attacker.Hero, Opponent, 1.00);
                    damage.Experience = Math.Min(damage.Show, Opponent.Hitpoints);
                    Opponent.Hitpoints -= Math.Min(damage.Show, Opponent.Hitpoints);
                    Targets.Add(Opponent, damage);
                }
		        return true;
		    }

		    return false;
		}
	}
}