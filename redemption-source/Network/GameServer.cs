﻿using System;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;
using ConquerEmulator.PacketHandling;

namespace ConquerEmulator.Network
{
	internal unsafe class GameServer
	{
	    public ServerSocket GServer = new ServerSocket();

	    public void Start()
		{
			GServer.OnConnect += GameServer_OnClientConnect;
			GServer.OnDisconnect += GameServer_OnClientDisconnect;
			GServer.OnReceive += GameServer_OnClientReceive;

			GServer.Enable(MySqlDatabase.GPort, DataStructures.Backlog, DataStructures.MaxPacketSize);
		}

	    private void GameServer_OnClientReceive(SystemSocket Sender, byte[] Arg)
        {
            try
            {
                if (Sender.Owner != null)
                {
                    var Client = Kernel.GameClients[Sender];
                    Client.Crypter.Decrypt(Arg);

                    var Packet = Arg;
                    if (Packet.Length <= 4)
                    {
                        Sender.Disconnect();
                        Kernel.GameClients.Remove(Sender);
                        return;
                    }
                    if (!Client.ExchangeComplete)
                    {
                        Client.CompleteExchange(Packet);
                        return;
                    }
                    #region Packet Splitter
                    fixed (byte* ptr = Packet)
                    {
                        var offset = 0;
                        while (offset < Packet.Length)
                        {
                            var size = *(ushort*)(ptr + offset) + 8;
                            if (size < Packet.Length)
                            {
                                var sub = new byte[size];
                                fixed (byte* pSub = sub)
                                {
                                    Native.memcpy(pSub, ptr + offset, size);
                                    PacketThread.Process(Client, sub, pSub);
                                }
                            }
                            else if (size > Packet.Length)
                            {
                                Client.Socket.Disconnect();
                                return;
                            }
                            else 
                            {
                                PacketThread.Process(Client, Packet, ptr);
                            }
                            offset += size;
                        }
                    }
                    #endregion
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
                Sender.Disconnect();
                Kernel.GameClients.Remove(Sender);
            }
        }

	    private void GameServer_OnClientDisconnect(SystemSocket Sender, object Arg)
        {
            if (Sender.Owner != null && Kernel.GameClients.ContainsKey(Sender))
            {
                var Client = Kernel.GameClients[Sender];

                Kernel.GameClients.Remove(Sender);
                Kernel.UpdateGameClients();

                if (!new MapSettings(Client.Hero.MapID.ID).Status.CanSaveLocation)
                    Client.Teleport(1002, 430, 380);

                #region Remove StatusFlag(s)
                Client.Hero.StatusFlag = StatusFlag.None;
                Client.Hero.Flashing = false;
                #endregion

                #region If Pet Active Kill it
                Client.Pet?.Kill();
                #endregion

                #region If Vending Remove Stall
                if (Client.Vendor.IsVending)
                    Client.Vendor.StopVending();
                #endregion

                #region If Trading Close Window
                if (Client.Trade.Trading)
                    PacketThread.CloseTradeWindow(Client);
                #endregion

                #region If active Team dismiss/leave it
                if (Client.Team.Active)
                {
                    var packet2 = new TeamPacket();
                    packet2.UID = Client.Hero.UID;
                    if (Client.Team.IsLeader)
                    {
                        packet2.ID = TeamIDs.Dismiss;
                        PacketThread.DismissTeam(Client, packet2, PacketKernel.Serialize(packet2, packet2.Size));
                    }
                    else
                    {
                        packet2.ID = TeamIDs.LeaveTeam;
                        PacketThread.LeaveCurrentTeam(Client, packet2, PacketKernel.Serialize(packet2, packet2.Size));
                    }
                }
                #endregion

                #region Associates
                foreach (var frand in Client.Friends.Values)
                {
                    var fc = Kernel.FindClient(frand.UID);
                    if (fc != null)
                    {
                        IAssociate temp;
                        if (fc.Friends.TryGetValue(Client.Hero.UID, out temp))
                        {
                            temp.Online = false;
                            temp.ID = AssociationID.SetOfflineFriend;
                            fc.Send(temp.GetBytes()); // tell them we're offline
                        }
                    }
                }
                foreach (var gc in Kernel.Clients)
                {
                    IAssociate temp;
                    if (gc.Enemies.TryGetValue(Client.Hero.UID, out temp))
                    {
                        temp.Online = false;
                        temp.ID = AssociationID.SetOfflineEnemy;
                        gc.Send(temp.GetBytes()); // tell them we're offline
                    }
                }
                #endregion

                #region Saving
                MySqlDatabase.SaveCharacter(Client);
                if (Client.Guild != null)
                    MySqlDatabase.SaveGuild(Client.Guild);
                #endregion

                #region PlayerScreen Removing
                Client.SendScreen(new DataPacket(Client.Hero.UID, DataIDs.RemoveEntity).Serialize(), false);
                Client.Screen.Cleanup();
                Client.Screen.FullWipe();
                #endregion

                if (Client.Hero.Name != "")
                    Console.WriteLine("{0} has logged off.", Client.Hero.Name);
            }
        }

	    private void GameServer_OnClientConnect(SystemSocket Sender, object Arg)
		{
			var Client = new GameClient(Sender);
			Kernel.GameClients.Add(Client.Socket, Client);
			Client.StartExchange();
		}
	}
}