﻿using System;
using System.Net.Sockets;

namespace ConquerEmulator.Network
{
	public class SystemSocket
	{
	    public byte[] Buffer;
	    public bool Connected;
	    public object Owner = null;
	    public ServerSocket Server = null;
	    public Socket Socket = null;
	    public uint UID = 0;

	    public SystemSocket(uint Size)
		{
			Buffer = new byte[Size];
		}

	    public string RemoteAddress
		{
			get
			{
				try
				{
					return Socket.RemoteEndPoint.ToString().Split(':')[0];
				}
				catch (Exception)
				{
					Server?.InvokeDisconect(this);
					return "";
				}
			}
		}

	    public void Disconnect()
		{
			Connected = false;
			Server.InvokeDisconect(this);
			Socket.Disconnect(false);
		}

	    public void Send(byte[] Packet)
        {
            if (Connected)
            {
                try
                {
                    Socket.BeginSend(Packet, 0, Packet.Length, SocketFlags.None, EndSend, null);
                }
                catch (SocketException)
                {
                    Server?.InvokeDisconect(this);
                }
            }
        }

	    private void EndSend(IAsyncResult Result)
		{
			try
			{
				Socket.EndSend(Result);
			}
			catch (SocketException)
			{
				Server?.InvokeDisconect(this);
			}
		}
	}
}