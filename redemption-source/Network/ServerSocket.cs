﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using ConquerEmulator.Database;

namespace ConquerEmulator.Network
{
	public delegate void SocketEvent<in T, in T2>(T Sender, T2 Arg);

	public class ServerSocket
	{
	    public ushort Backlog;

	    public ConcurrentDictionary<uint, SystemSocket> Connections = new ConcurrentDictionary<uint, SystemSocket>();

	    public uint MaxPacketSize;

	    public ushort Port;

	    public Socket Socket;
	    public uint UID;
	    public event SocketEvent<SystemSocket, object> OnConnect;
	    public event SocketEvent<SystemSocket, object> OnDisconnect;
	    public event SocketEvent<SystemSocket, byte[]> OnReceive;

	    public void Enable(ushort port, ushort backlog, uint Buffer)
		{
			Port = port;
			Backlog = backlog;
			MaxPacketSize = Buffer;

			Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			Socket.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, true);
			Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontLinger, true);

			Socket.Bind(new IPEndPoint(IPAddress.Any, Port));
			Socket.Listen(Backlog);
			Socket.BeginAccept(Accept, new SystemSocket(MaxPacketSize));
		}

	    public void Accept(IAsyncResult Result)
		{
			try
			{
				UID++;
				var SSocket = (SystemSocket)Result.AsyncState;
				SSocket.Socket = Socket.EndAccept(Result);

				SSocket.UID = UID;
				SSocket.Owner = this;
				SSocket.Connected = true;
				SSocket.Server = this;

				Connections.TryAdd(SSocket.UID, SSocket);

				OnConnect?.Invoke(SSocket, this);

				Socket.BeginAccept(Accept, new SystemSocket(MaxPacketSize));
				SSocket.Socket.BeginReceive(SSocket.Buffer, 0, (int)MaxPacketSize, SocketFlags.None, Receive, SSocket);
			}
			catch
			{
				Socket.BeginAccept(Accept, new SystemSocket(MaxPacketSize));
			}
		}

	    public void Receive(IAsyncResult Result)
		{
			var SSocket = (SystemSocket)Result.AsyncState;
			try
			{
				var Error = SocketError.Success;
				if (!SSocket.Connected || !SSocket.Socket.Connected)
					return;
				var Size = SSocket.Socket.EndReceive(Result, out Error);
				if ((Size != 0) && (Error == SocketError.Success))
				{
					var Return = new byte[Size];
					Array.Copy(SSocket.Buffer, Return, Size);
					OnReceive?.Invoke(SSocket, Return);
					SSocket.Socket.BeginReceive(SSocket.Buffer, 0, (int)MaxPacketSize, SocketFlags.None, Receive, SSocket);
				}
				else
				{
					SSocket.Connected = false;
					InvokeDisconect(SSocket);
					Connections.Remove(SSocket.UID);
				}
			}
			catch (Exception e)
			{
				LogHandler.WriteLine(e);
				SSocket.Connected = false;
				InvokeDisconect(SSocket);
				Connections.Remove(SSocket.UID);
			}
		}

	    public void InvokeDisconect(SystemSocket Client)
		{
			if (Client.Connected)
			{
				Client.Socket.Shutdown(SocketShutdown.Both);
				Client.Socket.Close();
			}
			else
			{
				OnDisconnect?.Invoke(Client, null);
				Client.Owner = null;
			}
		}
	}
}