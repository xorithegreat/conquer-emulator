﻿using System.Collections.Generic;
using ConquerEmulator.Attacking;
using ConquerEmulator.Client;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Network.PacketStructures
{
	public unsafe class Packets
	{
	    public static byte[] GetMessageBoardList(uint Index, ChatType ChatType, string[] Messages, ActionType Action)
		{
			#region Checks
			var Length = 0;
			foreach (var message in Messages)
			{
				if ((message == null) || (message.Length > 255))
					return null;

				Length += message.Length + 1;
			}
			#endregion
			var Buffer = new byte[10 + Length + 8];
			fixed (byte* Packet = Buffer)
			{
				*(ushort*)(Packet + 0) = (ushort)(Buffer.Length - 8);
				*(ushort*)(Packet + 2) = 0x457;
				*(uint*)(Packet + 4) = Index;
				*(uint*)(Packet + 6) = (uint)ChatType;
				*(Packet + 8) = (byte)Action;
				*(Packet + 9) = (byte)Messages.Length;

				var Pos = 10;
				foreach (var message in Messages)
				{
					*(Packet + Pos) = (byte)message.Length;
					for (byte x = 0; x < (byte)message.Length; x++)
						*(Packet + Pos + 1 + x) = (byte)message[x];
					Pos += message.Length + 1;
				}
			}
			return Buffer;
		}

	    public static byte[] Message(MessageInfo Info)
		{
			var Buffer = new byte[32 + Info.StrSize() + 8];
			fixed (byte* Packet = Buffer)
			{
				*(ushort*)Packet = (ushort)(Buffer.Length - 8);
				*(ushort*)(Packet + 2) = 0x3EC;
				*(ARGBColor*)(Packet + 4) = Info.Color;
				*(ChatType*)(Packet + 8) = Info.ChatType;
				*(uint*)(Packet + 12) = Info.dwParam;
				Packet[24] = 0x04;
				Packet[25] = (byte)Info.From.Length;
				Packet[26 + Info.From.Length] = (byte)Info.To.Length;
				Packet[28 + Info.From.Length + Info.To.Length] = (byte)Info.Message.Length;
				PacketKernel.Encode(Packet, Info.From, 26);
				PacketKernel.Encode(Packet, Info.To, 27 + Info.From.Length);
				PacketKernel.Encode(Packet, Info.Message, 29 + Info.From.Length + Info.To.Length);
			}
			return Buffer;
		}

	    public static byte[] AssociateInfo(GameClient Client, bool Enemy)
		{
			var Buffer = new byte[46 + 8];
			fixed (byte* Packet = Buffer)
			{
				*(ushort*)Packet = (ushort)(Buffer.Length - 8);
				*(ushort*)(Packet + 2) = 2033;
				*(uint*)(Packet + 4) = Client.Hero.UID;
				*(uint*)(Packet + 8) = Client.Hero.Model;
				*(Packet + 12) = (byte)Client.Hero.Level;
				*(Packet + 13) = Client.Job;
				*(ushort*)(Packet + 14) = Client.Hero.PkPoints;
				*(ushort*)(Packet + 16) = Client.Hero.GuildID;
				*(Packet + 19) = (byte)Client.Hero.GuildRank;
				*(Packet + 36) = (byte)(Enemy ? 1 : 0);
				PacketKernel.Encode(Packet, Client.Spouse, 20 + Client.Spouse.Length);
			}
			return Buffer;
		}

	    public static byte[] CharacterInfo(GameClient Client)
		{
			var Buffer = new byte[90 + Client.Hero.Name.Length + Client.Spouse.Length + 8];
			fixed (byte* Packet = Buffer)
			{
				*(ushort*)Packet = (ushort)(Buffer.Length - 8);
				*(ushort*)(Packet + 2) = 0x3EE;
				*(uint*)(Packet + 4) = Client.Hero.UID;
				*(uint*)(Packet + 8) = Client.Hero.Model;
				*(ushort*)(Packet + 12) = Client.Hero.Hairstyle;
				*(uint*)(Packet + 14) = Client.Silvers;
				*(uint*)(Packet + 18) = Client.ConquerPoints;
				*(uint*)(Packet + 22) = 0; // Exp?
				*(ushort*)(Packet + 50) = Client.Strength;
				*(ushort*)(Packet + 52) = Client.Agility;
				*(ushort*)(Packet + 54) = Client.Vitality;
				*(ushort*)(Packet + 56) = Client.Spirit;

				*(ushort*)(Packet + 60) = (ushort)Client.Hero.Hitpoints;
				*(ushort*)(Packet + 62) = Client.Mana;
				*(ushort*)(Packet + 64) = Client.Hero.PkPoints;
				Packet[66] = (byte)Client.Hero.Level;
				Packet[67] = Client.Job;
				Packet[68] = 0x05;
				Packet[69] = (byte)Client.Hero.Reborn;

				Packet[86] = 0x01;
				Packet[87] = 0x02;
				Packet[88] = (byte)Client.Hero.Name.Length;
				Packet[89 + Client.Hero.Name.Length] = (byte)Client.Spouse.Length;
				PacketKernel.Encode(Packet, Client.Hero.Name, 89);
				PacketKernel.Encode(Packet, Client.Spouse, 90 + Client.Hero.Name.Length);
			}
			return Buffer;
		}

	    public static byte[] Attack(uint Attacker, ushort X, ushort Y, uint Opponent, int Damage, AttackType type)
		{
			var Buffer = new byte[28 + 8];
			fixed (byte* Packet = Buffer)
			{
				*(ushort*)Packet = (ushort)(Buffer.Length - 8);
				*(ushort*)(Packet + 2) = 0x3FE;
				*(uint*)(Packet + 8) = Attacker;
				*(uint*)(Packet + 12) = Opponent;
				*(ushort*)(Packet + 16) = X;
				*(ushort*)(Packet + 18) = Y;
				*(uint*)(Packet + 20) = (uint)type;
				*(uint*)(Packet + 24) = (uint)Damage;
			}
			return Buffer;
		}

	    public static void ShowAttack(Entity Attacker, Dictionary<IBaseEntity, Damage> Targets, ConquerSpell Spell, ushort X, ushort Y)
		{
			var Buffer = new byte[28 + Targets.Count * 12 + 8];
			fixed (byte* lpBuffer = Buffer)
			{
				*(ushort*)lpBuffer = (ushort)(Buffer.Length - 8);
				*(ushort*)(lpBuffer + 2) = 0x451;
				*(uint*)(lpBuffer + 4) = Attacker.UID;
				*(ushort*)(lpBuffer + 8) = X;
				*(ushort*)(lpBuffer + 10) = Y;
				*(ushort*)(lpBuffer + 12) = Spell.ID;
				*(ushort*)(lpBuffer + 14) = Spell.Level;
				*(uint*)(lpBuffer + 16) = (uint)Targets.Count;
				ushort ax = 0;
				foreach (var DE in Targets)
				{
					*(uint*)(lpBuffer + 20 + ax) = DE.Key.UID;
				    *(int*)(lpBuffer + 24 + ax) = DE.Value.Show;
					ax += 12;
				}
			}
			PacketKernel.SendRangedPacket(Attacker, Buffer);
		}

	    public static byte[] StringPacket(StringPacket String, string Text)
		{
			var Buffer = new byte[11 + Text.Length + 8];
			fixed (byte* Packet = Buffer)
			{
				*(ushort*)Packet = (ushort)(Buffer.Length - 8);
				*(ushort*)(Packet + 2) = 0x3F7;
				*(uint*)(Packet + 4) = String.UID;
				*(Packet + 8) = (byte)String.ID;
				*(Packet + 9) = 1;
				*(Packet + 10) = (byte)Text.Length;
				PacketKernel.Encode(Packet, Text, 11);
			}
			return Buffer;
		}

	    public static byte[] StringPacket(StringPacket String, string[] Text)
		{
			byte Length = 0;
			foreach (var text in Text)
				Length += (byte)text.Length;
			var Buffer = new byte[13 + Length + 8];
			fixed (byte* Packet = Buffer)
			{
				*(ushort*)Packet = (ushort)(Buffer.Length - 8);
				*(ushort*)(Packet + 2) = 0x3F7;
				*(uint*)(Packet + 4) = String.UID;
				*(Packet + 8) = (byte)String.ID;
				*(Packet + 9) = (byte)Text.Length;
				ushort Pos = 10;
				for (ushort i = 0; i < Text.Length; i++)
				{
					*(Packet + Pos + i) = (byte)Text[i].Length;
					Pos++;
					PacketKernel.Encode(Packet, Text[i], Pos + i, out Pos);
				}
			}
			return Buffer;
		}

	    public static byte[] TradeItem(IConquerItem Item)
		{
			var Buffer = new byte[44 + 8];
			fixed (byte* Packet = Buffer)
			{
				*(ushort*)Packet = (ushort)(Buffer.Length - 8);
				*(ushort*)(Packet + 2) = 0x3F0;
				*(uint*)(Packet + 4) = Item.UID;
				*(uint*)(Packet + 8) = Item.ID;
				*(Packet + 12) = 1;
				*(Packet + 14) = 1;
				*(Packet + 16) = 2;
				*(Packet + 18) = 255;
				*(Packet + 24) = Item.SocketOne;
				*(Packet + 25) = Item.SocketTwo;
				*(Packet + 28) = Item.Plus;
				*(Packet + 29) = Item.Bless;
				*(Packet + 30) = Item.Enchant;
				*(Packet + 40) = Item.Color;
			}
			return Buffer;
		}

	    public static byte[] GuildInfo(ConquerGuild CGuild, GameClient Player)
		{
			var Buffer = new byte[21 + CGuild.Leader.Length + 8];
            fixed (byte* Packet = Buffer)
            {
                *(ushort*)Packet = (ushort)(Buffer.Length - 8);
                *(ushort*)(Packet + 2) = 0x452;
                if (CGuild != null)
                {
                    *(ushort*)(Packet + 4) = Player.Hero.GuildID;
                    *(uint*)(Packet + 8) = Player.GuildDonation;
                    *(uint*)(Packet + 12) = CGuild.Fund;
                    *(uint*)(Packet + 16) = (uint)CGuild.Members.Count;
                    *(GuildPosition*)(Packet + 20) = Player.Hero.GuildRank;
                    PacketKernel.Encode(Packet, CGuild.Leader, 21);
                }
            }
			return Buffer;
		}
	}
}