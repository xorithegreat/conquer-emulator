﻿using System;
using System.Runtime.InteropServices;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;

namespace ConquerEmulator.Network.PacketStructures
{
	internal unsafe class PacketKernel
	{
	    public static void AppendNetStringPacker(byte* buffer, NetStringPacker packer)
		{
			var bufPacker = packer.ToArray();
			fixed (byte* ptr = bufPacker)
			{
				Native.memcpy(buffer, ptr, bufPacker.Length);
			}
		}

	    public static void DisplayPacket(byte[] Packet)
		{
			foreach (var b in Packet)
				Console.Write(b.ToString("X2") + " ");
			Console.WriteLine();
		}

	    public static void Encode(byte* Packet, string Str, int Index)
		{
			fixed (char* Ptr = Str)
			{
				Copy(Ptr, Packet, Index, 0, Str.Length);
			}
		}

	    public static void Encode(byte* Packet, string Str, int Index, out ushort Position)
		{
			fixed (char* Ptr = Str)
			{
				Copy(Ptr, Packet, Index, 0, Str.Length);
			}
			Position = (ushort)(Index + Str.Length - 1);
		}

	    public static string Decode(byte[] Packet, int StartIndex, int EndIndex)
		{
			var ret = "";
            for (var x = StartIndex; x < EndIndex; x++)
            {
                if (Packet[x] != 0)
                    ret += Convert.ToChar(Packet[x]);
            }
			return ret;
		}

	    public static byte[] Serialize(object PacketStruct, ushort Size)
		{
			var ret = new byte[Size + 8];
			fixed (byte* Ptr = ret)
			{
				Marshal.StructureToPtr(PacketStruct, (IntPtr)Ptr, false);
			}
			return ret;
		}

	    public static object Deserialize(byte[] Bytes, Type StructType)
		{
			object ret;
			fixed (byte* Ptr = Bytes)
			{
				ret = Marshal.PtrToStructure((IntPtr)Ptr, StructType);
			}
			return ret;
		}

	    public static void Copy(char* pSrc, byte* pDst, int dstIndex, int srcIndex, int Count)
		{
			byte* ps = (byte*)(pSrc + srcIndex), pd = pDst + dstIndex;
			for (var i = 0; i < Count; i++)
			{
				*pd = *ps;
				pd++;
				ps += 2;
			}
		}

	    public static byte[] GetBytes(void* Ptr, int Size)
		{
			var Block = new byte[Size + 8];
			var bPtr = (byte*)Ptr;
			for (var i = 0; i < Size; i++)
				Block[i] = bPtr[i];
			return Block;
		}

	    public static byte[] ToBytes(void* PacketPtr)
		{
			var Size = *(ushort*)PacketPtr;
			var Bytes = new byte[Size + 8];
			fixed (byte* dst = Bytes)
			{
				Native.memcpy(dst, PacketPtr, Bytes.Length);
			}
			return Bytes;
		}

	    public static void SendRangedPacket(IBaseEntity Entity, byte[] Packet)
		{
            foreach (var Client in Kernel.Clients)
            {
                if (!Client.LoggedIn || (Client.Hero.MapID.ID != Entity.MapID.ID))
                    continue;
                if (Kernel.GetDistance(Entity.X, Entity.Y, Client.Hero.X, Client.Hero.Y) < DataStructures.ViewDistance)
                    Client.Send(Packet);
            }
		}

	    public static void SendRangedPacket(uint MapID, ushort X, ushort Y, byte[] Packet)
		{
            foreach (var Client in Kernel.Clients)
            {
                if (!Client.LoggedIn || (Client.Hero.MapID.ID != MapID))
                    continue;
                if (Kernel.GetDistance(X, Y, Client.Hero.X, Client.Hero.Y) < DataStructures.ViewDistance)
                    Client.Send(Packet);
            }
		}

	    public static void SendRangedPacket(ConquerItem Item, byte[] Packet)
		{
            foreach (var Client in Kernel.Clients)
            {
                if (!Client.LoggedIn || (Client.Hero.MapID.ID != Item.MapID.ID))
                    continue;
                if (Kernel.GetDistance(Item.X, Item.Y, Client.Hero.X, Client.Hero.Y) < DataStructures.ViewDistance)
                {
                    Item.SendSpawn(Client, false);
                }
            }
		}

	    public static void SendGlobalPacket(byte[] Packet)
		{
            foreach (var Client in Kernel.Clients)
            {
                if (Client.LoggedIn)
                    Client.Send(Packet);
            }
		}
	}
}