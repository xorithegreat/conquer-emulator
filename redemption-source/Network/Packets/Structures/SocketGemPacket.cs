﻿using System.Runtime.InteropServices;

namespace ConquerEmulator.Network.Packets.Structures
{
    public class GemsConst
    {
        public const byte PhoenixGem = 0;
        public const byte DragonGem = 1;
        public const byte FuryGem = 2;
        public const byte RainbowGem = 3;
        public const byte KylinGem = 4;
        public const byte VioletGem = 5;
        public const byte MoonGem = 6;
        public const byte TortoiseGem = 7;
        public const byte ThunderGem = 10;
        public const byte GloryGem = 12;
        public const byte OpenSocket = 0xFF;
        public const byte NoSocket = 0x00;
        public const byte MaxGems = 10;
        public const byte MaxMineGems = 8;
        public const byte EmptySocket = 255;
    }

    [StructLayout(LayoutKind.Sequential)]
	internal unsafe struct SocketGemPacket
	{
		public ushort Size;
		public ushort Type;
		public uint CharacterUID;
		public uint UID;
		public uint GemUID;
		public ushort Slot;
		public byte ID;
		public fixed sbyte Padding[17];
	}
}