﻿using System.Runtime.InteropServices;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum StringIds : byte
	{
		None = 0x00,
		GuildName = 0x03,
		Spouse = 0x06,
		Effect = 0x0A,
		GuildList = 0x0B,
		ViewEquipSpouse = 16,
		Sound = 0x24,
		GuildEnemies = 0x15,
		GuildAllies = 0x15
	}

	[StructLayout(LayoutKind.Explicit)]
	public struct StringPacket
	{
		[FieldOffset(0)]
		public ushort Size;
		[FieldOffset(2)]
		public ushort Type;
		[FieldOffset(4)]
		public uint UID;
		[FieldOffset(8)]
		public StringIds ID;
		[FieldOffset(10)]
		public byte TextLength;

		public StringPacket(byte extraSize)
		{
			Size = (ushort)(13 + extraSize);
			Type = 0x3F7;
			UID = 0;
			ID = StringIds.None;
			TextLength = extraSize;
		}
	}
}