﻿using System.Runtime.InteropServices;
using ConquerEmulator.Crypto;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum AttackType : uint
	{
		None = 0x00,
		Physical = 0x02,
		Magic = 0x18,
		Archer = 0x1c,
		RequestMariage = 0x08,
		AcceptMariage = 0x09,
		Kill = 0xE,
		AcceptMerchant = 0x21,
		DeclineMerchant = 0x22,
		RequestMerchant = 0x23,
        CloudSaintJar = 0x1E
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct RequestAttack
    {
        public ushort Size, Type;
        public uint TimeStamp, AttackerUID, TargetUID;
        public ushort TargetX, TargetY;
        public AttackType AttackType;
        public ushort SpellID, SpellLevel;
        public ushort KillCount;

        public void Decrypt()
        {
            TargetUID = (uint)Assembler.RollRight(TargetUID, 13, 32);
            TargetUID = (TargetUID ^ 0x5F2D2463 ^ AttackerUID) - 0x746F4AE6;

            SpellID = (ushort)(SpellID ^ AttackerUID ^ 0x915D);
            SpellID = (ushort)(Assembler.RollLeft(SpellID, 3, 16) - 0xEB42);

            TargetX = (ushort)(TargetX ^ AttackerUID ^ 0x2ED6);
            TargetX = (ushort)(Assembler.RollLeft(TargetX, 1, 16) - 0x22ee);

            TargetY = (ushort)(TargetY ^ AttackerUID ^ 0xB99B);
            TargetY = (ushort)(Assembler.RollLeft(TargetY, 5, 16) - 0x8922);
        }
        public void Decrypt(SpellCrypto Crypt)
        {
            Crypt.Decrypt(ref TargetUID, ref SpellID, ref TargetX, ref TargetY);
        }
    }

    //Encrypt
}