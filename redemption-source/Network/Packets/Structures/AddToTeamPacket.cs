﻿using System.Runtime.InteropServices;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum TeamIDs : uint
	{
		AcceptInvite = 3,
		AcceptJoin = 5,
		Create = 0,
		Dismiss = 6,
		Kick = 7,
		LeaveTeam = 2,
		RequestInvite = 4,
		RequestJoin = 1,
		ForbidJoin = 0x08,
		UnforbidJoin = 0x09,
		ForbidMoney = 0x0A,
		UnforbidMoney = 0x0B,
		ForbidItems = 0x0C,
		UnforbidItems = 0x0D
	}

	[StructLayout(LayoutKind.Explicit)]
	internal class AddToTeamPacket
	{
		[FieldOffset(34)]
		public ushort Hitpoints;
		[FieldOffset(32)]
		public ushort MaxHitpoints;
		[FieldOffset(28)]
		public uint Mesh;
		[FieldOffset(8)]
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)]
		public string Name;
		[FieldOffset(5)]
		public bool ShowName = true;
		[FieldOffset(0)]
		public ushort Size = 0x24;
		[FieldOffset(2)]
		public ushort Type = 0x402;
		[FieldOffset(24)]
		public uint UID;
	}
}