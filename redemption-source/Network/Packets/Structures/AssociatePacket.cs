﻿using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum AssociationID : byte
	{
		AddFriend = 0x0F, //
		RemoveFriend = 0x0E,
		SetOfflineFriend = 0x0D,
		SetOnlineFriend = 0x0C,
		NewFriend = 0x0B, //
		RequestFriend = 0x0A,

		AddEnemy = 0x13,
		RemoveEnemy = 0x12,
		SetOfflineEnemy = 0x11,
		SetOnlineEnemy = 0x10
	}

    internal unsafe struct AssociatePacket : IAssociate
    {
        public ushort Size;
        public ushort Type;
        public uint UID { get; set; }
        public AssociationID ID { get; set; }
        private byte m_Online;
        private fixed sbyte Junk[10];
        private fixed sbyte szName[16];

        public bool Online
        {
            get { return m_Online == 1; }
            set { m_Online = (byte)(value ? 1 : 0); }
        }
        public string Name
        {
            get
            {
                fixed (sbyte* sb = szName)
                {
                    return new string(sb);
                }
            }
            set
            {
                fixed (sbyte* sb = szName)
                {
                    for (var i = 0; i < value.Length; i++)
                        sb[i] = (sbyte)value[i];
                }
            }
        }

        public static AssociatePacket Create()
		{
			var p = new AssociatePacket();
			p.Size = (ushort)sizeof(AssociatePacket);
			p.Type = 0x3fb;
			return p;
		}

		public byte[] GetBytes()
		{
			fixed (void* pThis = &this)
			{
				return PacketKernel.GetBytes(pThis, Size);
			}
		}
	}
}