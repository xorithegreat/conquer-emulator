﻿using System.Runtime.InteropServices;

namespace ConquerEmulator.Network.Packets.Structures
{
	[StructLayout(LayoutKind.Sequential)]
	public unsafe struct MapStatusPacket
	{
		public ushort Size;
		public ushort Type;
		public uint DynamicMapID;
		public uint MapID;
		public uint Status;
		private fixed byte TQServer[8];

		public static MapStatusPacket Create()
		{
			var p = new MapStatusPacket();
			p.Size = (ushort)sizeof(MapStatusPacket);
			p.Type = 0x456;
			return p;
		}
	}
}