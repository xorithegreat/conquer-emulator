﻿using ConquerEmulator.Item;
using ConquerEmulator.Main.Interfaces;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum VendMode : ushort
	{
		VendByGold = 0x01,
		VendByConquerPoints = 0x03
	}

	public unsafe struct VendingItem
	{
		public uint UID;
		public uint ShopID;
		public uint Price;
		public uint ItemID;
		public ushort Amount;
		public ushort MaxAmount;
		public VendMode CurrencyType;
		public byte Effect;
		public byte SocketOne;
		public byte SocketTwo;
		public byte Unknown;
		public byte Unknown2;
		public byte Plus;
		public byte Bless;
		public byte Enchant;
		public fixed byte Junk2[9]; // suspicious, locked (?)
		public byte Color;

		public void FromItem(IConquerItem Item, bool VendByGold)
		{
			var realitem = Item as ConquerItem;
			if (realitem != null)
			{
				UID = realitem.UID;
				ItemID = realitem.ID;
				Amount = realitem.Durability;
				MaxAmount = realitem.MaxDurability;
				Plus = realitem.Plus;
				Bless = realitem.Bless;
				Enchant = realitem.Enchant;
				SocketOne = realitem.SocketOne;
				SocketTwo = realitem.SocketTwo;
				CurrencyType = VendByGold ? VendMode.VendByGold : VendMode.VendByConquerPoints;
				Effect = (byte)realitem.Effect;
				Color = realitem.Color;
			}
		}

		public byte[] GetBytes()
		{
			var Packet = new byte[33];
			fixed (byte* lpPacket = Packet)
			{
				*(ushort*)lpPacket = 33;
				*(ushort*)(lpPacket + 2) = 0x454;
				*(VendingItem*)(lpPacket + 4) = this;
			}
			return Packet;
		}

		public IConquerItem GetItem()
		{
			var Item = new ConquerItem(true);
			Item.UID = ConquerItem.NextItemUID;
			Item.ID = ItemID;
			Item.Durability = Amount;
			Item.MaxDurability = MaxAmount;
			Item.Plus = Plus;
			Item.Bless = Bless;
			Item.Enchant = Enchant;
			Item.SocketOne = SocketOne;
			Item.SocketTwo = SocketTwo;
			Item.Effect = Effect;
			Item.Color = Color;
			return Item;
		}
	}
}