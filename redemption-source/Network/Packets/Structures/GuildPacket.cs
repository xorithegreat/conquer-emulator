﻿using System.Runtime.InteropServices;

namespace ConquerEmulator.Network.Packets.Structures
{
	internal enum GuildIds : uint
	{
		None = 0,
		JoinRequest = 1,
		AcceptRequest = 2,
		Quit = 3,
		Info = 6,
		Allied = 7,
		Neutral = 8,
		Enemied = 9,
		Donate = 11,
		Refresh = 12,
		Disband = 19
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct GuildPacket
	{
		public ushort Size;
		public ushort Type;
		public GuildIds ID;
		public uint UID;

		public static GuildPacket Create()
		{
			var packet = new GuildPacket();
			packet.Size = 0x0c;
			packet.Type = 0x453;
			return packet;
		}
	}
}