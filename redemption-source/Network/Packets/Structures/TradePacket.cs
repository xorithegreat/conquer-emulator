﻿using System.Runtime.InteropServices;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum TradeIDs : byte
	{
		None = 0,
		Request = 1,
		CloseTrade = 2,
		OpenWindow = 3,
		CloseWindow = 5,
		AddItem = 6,
		SpecifyMoney = 7,
		ShowMoney = 8,
		Agree = 10,
		RemoveItem = 11,
		ShowCPs = 12,
		SpecifyCPs = 13
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct TradePacket
	{
		public ushort Size;
		public ushort Type;
		public uint UID;
		public TradeIDs ID;

		public static TradePacket Create()
		{
			var trade = new TradePacket();
			trade.Size = 0x0c;
			trade.Type = 0x420;
			trade.UID = 0;
			return trade;
		}
	}
}