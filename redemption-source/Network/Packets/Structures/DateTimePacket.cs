﻿using System;

namespace ConquerEmulator.Network.Packets.Structures
{
    public struct DateTimePacket
    {
        public ushort Size;
        public ushort Type;
        public int Header;
        public int Year;
        public int Month;
        public int DayOfYear;
        public int DayOfMonth;
        public int Hour;
        public int Minute;
        public int Seconds;

        private void Format()
        {
            var dNow = DateTime.Now;
            Year = dNow.Year - 1900;
            Month = dNow.Month - 1;
            DayOfMonth = dNow.Day;
            DayOfYear = dNow.DayOfYear;
            Hour = dNow.Hour;
            Minute = dNow.Minute;
            Seconds = dNow.Second;
        }
        public static DateTimePacket Create()
        {
            var retn = new DateTimePacket();
            retn.Size = 0x24;
            retn.Type = 0x409;
            retn.Format();
            return retn;
        }
    }
}
