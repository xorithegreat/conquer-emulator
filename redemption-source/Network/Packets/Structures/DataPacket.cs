﻿using System.Runtime.InteropServices;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum MoneyType : sbyte
	{
		Unknown = -1,
		Gold = 0,
		ConquerPoints = 1
	}

	internal enum GUINpcType : byte
	{
		Compose = 0x01,
		Warehouse = 0x04
	}

	internal enum DataIDs : ushort
	{
		ViewEquipment = 0x75,
		Hotkeys = 0x4B,
		Synchro = 0x66,
		RemoveEntity = 0x87,
		ConfirmFriends = 0x4C,
		EnterPortal = 0x55,
		ChangeMap = 0x56,
		Revive = 0x5E,
		DeleteCharacter = 0x5F,
		ConfirmProfinceys = 0x4D,
		ConfirmSpells = 0x4E,
		ConfirmGuild = 0x61,
		ChangePkMode = 0x60,
		CompleteLogin = 0x84,
		Dead = 0x91,
		StartVend = 0x6F,
		ChangeAngle = 0x4F,
		ChangeAction = 0x51,
		Jump = 0x89,
		SetLocation = 0x4A,
		SetMapColor = 0x68,
		GetSurroundings = 0x72,
		GuiNpcInterface = 0x7E,
		None = 0x00,
        RequestFriendInfo = 0x94,
        Mining = 0x63,
        RequestEnemyInfo = 0x7B,
        ChangeAvatar = 0x97,
		EndFly = 0x78,
		Switch = 0x74,
		SpawnEffect = 0x83,
		GuardJump = 0x82,
		UnTransform = 0x76,
		Leveled = 0x5C,
		UnlearnSpell = 0x6D,
        RequestTeamMember = 0x6A
	}

	[StructLayout(LayoutKind.Sequential)]
	internal unsafe struct DataPacket
	{
		public ushort Size;
		public ushort Type;
		public uint UID;
		public uint lParam;
		public ushort dwParam_Lo
		{
			get { return (ushort)lParam; }
			set { lParam = (uint)((dwParam_Hi << 16) | value); }
		}
		public ushort dwParam_Hi
		{
			get { return (ushort)(lParam >> 16); }
			set { lParam = (uint)((value << 16) | dwParam_Lo); }
		}
        public uint TimeStamp;
        public DataIDs ID;
        public ushort wFacing;
		public ushort wParam1;
		public ushort wParam2;
        public uint dwParam2;
        public uint dwParam3;
        private fixed byte TQServer[8];

		public DataPacket(uint _UID, DataIDs _ID)
		{
			Size = 32;
			Type = 0x271A;
			TimeStamp = Native.timeGetTime();
			UID = _UID;
			lParam = 0;
			wFacing = 0;
			wParam1 = 0;
			wParam2 = 0;
		    dwParam2 = 0;
		    dwParam3 = 0;
			ID = _ID;
		}

		public DataPacket(uint _UID, uint _lParam, ushort _wFacing, ushort _wParam1, ushort _wParam2, DataIDs _ID)
		{
			Size = 32;
			Type = 0x271A;
			TimeStamp = Native.timeGetTime();
			UID = _UID;
			ID = _ID;
			lParam = _lParam;
			wFacing = _wFacing;
			wParam1 = _wParam1;
			wParam2 = _wParam2;
            dwParam2 = 0;
            dwParam3 = 0;
            ID = _ID;
		}

		public static byte[] DataUpdate(uint UID, uint lParam, ushort wFacing, ushort wParam1, ushort wParam2, DataIDs ID)
		{
			var Buffer = new byte[32 + 8];
			fixed (byte* Packet = Buffer)
			{
				*(ushort*)(Packet + 0) = (ushort)(Buffer.Length - 8);
				*(ushort*)(Packet + 2) = 0x271A;
				*(uint*)(Packet + 4) = UID;
				*(uint*)(Packet + 8) = lParam;
                *(uint*)(Packet + 12) = Native.timeGetTime();
                *(ushort*)(Packet + 16) = (ushort)ID;
                *(ushort*)(Packet + 18) = wFacing;
				*(ushort*)(Packet + 20) = wParam1;
				*(ushort*)(Packet + 22) = wParam2;
			}
			return Buffer;
		}

		public byte[] Serialize()
		{
			return PacketKernel.Serialize(this, Size);
		}
	}
}