﻿using System.Runtime.InteropServices;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum NpcType : byte
	{
		None = 0x00,
		Dialogue = 0x01,
		Option = 0x02,
		Input = 0x03,
		Avatar = 0x04,
		Finish = 0x64
	}

	[StructLayout(LayoutKind.Explicit)]
	internal unsafe struct NpcPacket
	{
		[FieldOffset(0)]
		public ushort Size;
		[FieldOffset(2)]
		public ushort Type;
		[FieldOffset(4)]
		public uint NpcID;
		[FieldOffset(8)]
		public ushort Avatar;
		[FieldOffset(10)]
		public byte OptionID;
		[FieldOffset(11)]
		public NpcType ID;
		[FieldOffset(12)]
		public bool DontDisplay;
		[FieldOffset(13)]
		public byte TextLength;

		public NpcPacket(byte extraSize)
		{
			Size = (ushort)(0x11 + extraSize);
			Type = 0x7F0;
			NpcID = 0;
			Avatar = 0;
			OptionID = 0xFF;
			ID = NpcType.None;
			DontDisplay = true;
			TextLength = extraSize;
		}

		public static NpcPacket* Alloc(byte* Ptr, byte extraSize)
		{
			var Npc = (NpcPacket*)Ptr;
			Npc->Size = (ushort)(extraSize + 0x11);
			Npc->Type = 0x7F0;
			Npc->OptionID = 0xFF;
			Npc->DontDisplay = true;
			Npc->TextLength = extraSize;
			return Npc;
		}

		public byte[] Serialize()
		{
			return PacketKernel.Serialize(this, Size);
		}
	}
}