﻿using System.Runtime.InteropServices;

namespace ConquerEmulator.Network.Packets.Structures
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct LearnSpellPacket
	{
		public ushort Size;
		public ushort Type;
		public int Experience;
		public ushort ID;
		public ushort Level;

		public static LearnSpellPacket Create()
		{
			var packet = new LearnSpellPacket();
			packet.Size = 0x0C;
			packet.Type = 0x44F;
			return packet;
		}
	}
}