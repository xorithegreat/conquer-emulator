﻿namespace ConquerEmulator.Network.Packets.Structures
{
	public struct DistributeStatPacket
	{
		public ushort Size;
		public ushort Type;
		public bool Strength;
		public bool Agility;
		public bool Vitality;
		public bool Spirit;
	}
}