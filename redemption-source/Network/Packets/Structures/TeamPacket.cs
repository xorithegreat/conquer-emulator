﻿using System.Runtime.InteropServices;

namespace ConquerEmulator.Network.Packets.Structures
{
	[StructLayout(LayoutKind.Explicit)]
	public class TeamPacket
	{
		[FieldOffset(4)]
		public TeamIDs ID;
		[FieldOffset(0)]
		public ushort Size = 12;
		[FieldOffset(2)]
		public ushort Type = 0x3ff;
		[FieldOffset(8)]
		public uint UID;
	}
}