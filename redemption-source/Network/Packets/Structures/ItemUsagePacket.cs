﻿using System.Runtime.InteropServices;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum ItemUsageID : byte
	{
		BuyItem = 1,
		SellItem = 2,
		RemoveInventory = 3,
		Equip = 4,
		UpdateItem = 5,
		UnequipItem = 6,
		ViewWarehouse = 9,
		DepositCash = 10,
		WithdrawCash = 11,
        Repair = 14,
        DBUpgrade = 19,
		MetUpgrade = 20,
		ShowVendingList = 0x15,
		AddVendingItemGold = 0x16,
		RemoveVendingItem = 0x17,
		BuyVendingItem = 0x18,
		AddVendingItemCPs = 0x1D,
		UpdateDurability = 25,
		Fireworks = 26,
		Ping = 27,
        UpdateEnchant = 28,
        DropItem = 37,
        DropMoney = 38
    }

	[StructLayout(LayoutKind.Sequential)]
	public struct ItemUsagePacket : IClassPacket
	{
		public ushort Size;
		public ushort Type;
		public uint UID;
		public uint lParam;
		public ItemUsageID ID;
		public uint TimeStamp;
		public uint Amount;

		public static ItemUsagePacket Create()
		{
			var retn = new ItemUsagePacket();
			retn.Size = 24;
			retn.Type = 0x3F1;
			retn.TimeStamp = Native.timeGetTime();
			return retn;
		}

		public ItemUsagePacket(uint uid, uint lparam, ItemUsageID _id)
		{
			Size = 24;
			Type = 0x3F1;
			UID = uid;
			lParam = lparam;
			ID = _id;
			TimeStamp = Native.timeGetTime();
			Amount = 1;
		}
		#region IClassPacket Members
		public void Deserialize(byte[] Bytes)
		{
		}

		public byte[] Serialize()
		{
			return PacketKernel.Serialize(this, Size);
		}
		#endregion
	}
}