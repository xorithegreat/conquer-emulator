﻿namespace ConquerEmulator.Network.Packets.Structures
{
	public struct ComposeItemPacket
	{
		public ushort Size;
		public ushort Type;
		public uint dwJunk;
		public uint MainItem;
		public uint MinorItem1;
		public uint MinorItem2;
	}
}