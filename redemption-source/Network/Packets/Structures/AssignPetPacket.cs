﻿namespace ConquerEmulator.Network.Packets.Structures
{
	public unsafe struct AssignPetPacket
	{
		public ushort Size;
		public ushort Type;
		public uint UID;
		public uint Model;
		public uint AIType;
		public ushort X;
		public ushort Y;
		public fixed sbyte Name[16];

		public static AssignPetPacket Create()
		{
			var packet = new AssignPetPacket();
			packet.Size = 0x24;
			packet.Type = 0x7F3;
			return packet;
		}
	}
}