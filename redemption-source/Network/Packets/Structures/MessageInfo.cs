﻿using System.Drawing;
using ConquerEmulator.Main.Interfaces;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum ChatType : ushort
	{
		Talk = 2000,
		Whisper = 2001,
		Action = 2002,
		Team = 2003,
		Guild = 2004,
		TopLeft = 2005,
		Spouse = 2006,
		Yell = 2008,
		Friend = 2009,
		Broadcast = 2500,
		Center = 2011,
		Ghost = 2013,
		Service = 2014,
		Dialog = 2100,
		LoginInformation = 2101,
		Slogan = 2104,
		TopRight = 2109,
		ClearTopRight = 2108,
		FriendsOfflineMessage = 2110,
		GuildBulletin = 2111,
		TradeBoard = 2201,
		FriendBoard = 2202,
		TeamBoard = 2203,
		GuildBoard = 2204,
		OthersBoard = 2205,
		SystemBoard = 2206
	}

	public unsafe struct ARGBColor
	{
		private byte Alpha;
		private byte Red, Green, Blue;

		public static implicit operator ARGBColor(Color _Color)
		{
			var ret = new ARGBColor();
			*(int*)&ret = _Color.ToArgb();
			return ret;
		}
	}

	public struct MessageInfo : IClassPacket
	{
		public string Message, From, To;
		public uint dwParam;
		public ARGBColor Color;
		public ChatType ChatType;

		public int StrSize()
		{
			return Message.Length + From.Length + To.Length;
		}

		public MessageInfo(string _Message, ARGBColor _Color, ChatType _ChatType)
		{
			Message = _Message;
			To = "ALL";
			From = "SYSTEM";
			Color = _Color;
			ChatType = _ChatType;
			dwParam = 0;
		}

		public MessageInfo(string _Message, string _To, ARGBColor _Color, ChatType _ChatType)
		{
			Message = _Message;
			To = _To;
			From = "SYSTEM";
			Color = _Color;
			ChatType = _ChatType;
			dwParam = 0;
		}

		public MessageInfo(string _Message, string _To, string _From, ARGBColor _Color, ChatType _ChatType)
		{
			Message = _Message;
			To = _To;
			From = _From;
			Color = _Color;
			ChatType = _ChatType;
			dwParam = 0;
		}

		public unsafe MessageInfo(byte[] Packet)
		{
			fixed (byte* Pointer = Packet)
			{
				Color = *(ARGBColor*)(Pointer + 4);
				ChatType = *(ChatType*)(Pointer + 8);
				dwParam = *(uint*)(Pointer + 12);
				From = new string((sbyte*)(Pointer + 26), 0, *(Pointer + 25));
				To = new string((sbyte*)(Pointer + 27 + From.Length), 0, Pointer[26 + From.Length]);
				Message = new string((sbyte*)(Pointer + 29 + From.Length + To.Length), 0, Pointer[28 + To.Length + From.Length]);
			}
		}

		public void Deserialize(byte[] Bytes)
		{
			this = new MessageInfo(Bytes);
		}

		public byte[] Serialize()
		{
			return PacketStructures.Packets.Message(this);
		}
	}
}