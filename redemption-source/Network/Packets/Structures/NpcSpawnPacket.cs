﻿using System;
using ConquerEmulator.Client;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Network.Packets.Structures
{
	internal unsafe class NpcSpawnPacket : INpc, IMapObject
	{
		public byte[] Packet;

		public NpcSpawnPacket()
		{
			Packet = new byte[20 + 8];
			InitPacket();
		}

		public byte NameLength
		{
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(Buffer + 19);
				}
			}
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(Buffer + 19) = value;
				}
			}
		}
		public MapObjectType MapObjType
		{
			get { return MapObjectType.Npc; }
		}
		public object Owner
		{
			get { return this; }
		}

		public void SendSpawn(GameClient Client, bool Ignore)
		{
			if (Client.Screen.Add(this) || Ignore)
				Client.Send(Packet);
		}

		public bool IsVendor
		{
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(bool*)(Buffer + 18);
				}
			}
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(bool*)(Buffer + 18) = value;
				}
			}
		}

		public void ConvertToVendor(string Name)
		{
			var nPacket = new byte[21 + Name.Length + 8];
			StatusFlag = 0;
			NameLength = (byte)Name.Length;
			IsVendor = true;
			Type = 0x196;
			Interaction = 0x0E;
			X += 3;
			Array.Copy(Packet, nPacket, Packet.Length);
			fixed (byte* _Strings = nPacket)
			{
				PacketKernel.Encode(_Strings, Name, 20);
			}
			Packet = nPacket;
			InitPacket();
		}

		public void ConvertToStandard()
		{
			var nPacket = new byte[20 + 8];
			StatusFlag = 0;
			Type = 0x43E;
			X -= 3;
			Interaction = 0x10;
			Array.Copy(Packet, nPacket, 20);
			Packet = nPacket;
			InitPacket();
		}

		// ..
		public uint UID
		{
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(uint*)(Buffer + 4) = value;
				}
			}
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(uint*)(Buffer + 4);
				}
			}
		}
		public ushort X
		{
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(ushort*)(Buffer + 8) = value;
				}
			}
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(ushort*)(Buffer + 8);
				}
			}
		}
		public ushort Y
		{
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(ushort*)(Buffer + 10) = value;
				}
			}
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(ushort*)(Buffer + 10);
				}
			}
		}
		public uint Type
		{
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(uint*)(Buffer + 12) = value;
				}
			}
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(uint*)(Buffer + 12);
				}
			}
		}
		public ushort Interaction
		{
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(ushort*)(Buffer + 14) = value;
				}
			}
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(ushort*)(Buffer + 14);
				}
			}
		}
		public uint StatusFlag
		{
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(uint*)(Buffer + 16) = value;
				}
			}
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(uint*)(Buffer + 16);
				}
			}
		}
		public Map MapID { get; set; }
		public ushort Avatar { get; set; }

		public void SendSpawn(GameClient Client)
		{
			SendSpawn(Client, false);
		}

		private void InitPacket()
		{
			fixed (byte* Buffer = Packet)
			{
				*(ushort*)(Buffer + 0) = (ushort)(Packet.Length - 8);
				*(ushort*)(Buffer + 2) = 0x7EE;
			}
		}
	}
}