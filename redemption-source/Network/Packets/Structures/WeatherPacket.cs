﻿using System.Runtime.InteropServices;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum MapEffectID : uint
	{
		DisableWeather = 0x00,
		EnableWeather = 0x20,
		MiniMap = 0x14
	}

	public enum MapEffectValue : uint
	{
		None = 0,
		MiniMapOn = 1,
		MinMapOff = 0,
		Rain = 2,
		Snow = 3,
		RainWithWind = 4,
		FallingLeaves = 5,
		CherryBlossoms = 7,
		CherryBlossomPetalsWind = 8,
		BlowingCotten = 9,
		FireFlies = 10
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct WeatherPacket
	{
		public ushort Size;
		public ushort Type;
		public MapEffectValue Value;
		public int Density;
		public MapEffectID ID;
		public uint Appearance;
		public uint TimeStamp;

		public WeatherPacket(MapEffectValue _Value, MapEffectID _ID, int WDensity)
		{
			Size = 20;
			Type = 0x3F8;
			Value = _Value;
			Density = WDensity;
			ID = _ID;
			TimeStamp = Native.timeGetTime();
			Appearance = 255;
		}
	}
}