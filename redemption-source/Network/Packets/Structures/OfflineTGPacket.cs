﻿using System.Runtime.InteropServices;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Network.Packets.Structures
{
    public enum OfflineTgPacketType : uint
    {
        RequestTime = 0x00,
        RequestEnter = 0x01,
        RequestExperience = 0x03,
        ClaimRewards = 0x04
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct OfflineTgPacket
    {
        [FieldOffset(0)]
        public ushort Size;
        [FieldOffset(2)]
        public ushort Type;
        [FieldOffset(4)]
        public OfflineTgPacketType PacketType;
        [FieldOffset(8)]
        public ulong TrainingTime;

        public OfflineTgPacket(OfflineTgPacketType _type, ulong _time)
        {
            Size = 0x1C;
            Type = 0x7FC;
            PacketType = _type;
            TrainingTime = _time;
        }

        public byte[] Serialize()
        {
            return PacketKernel.Serialize(this, Size);
        }
    }

    internal struct OfflineTgInfoPacket
    {
        public ushort Size;
        public ushort Type;
        public ushort UsedTime;
        public ushort RemainingTime;
        public uint Level;
        public uint Experience;

        public static OfflineTgInfoPacket Create()
        {
            var Packet = new OfflineTgInfoPacket();
	        Packet.Size = 0x50;
	        Packet.Type = 0x7FB;
            return Packet;
        }

        public byte[] Serialize()
        {
            return PacketKernel.Serialize(this, Size);
        }
    }
}
