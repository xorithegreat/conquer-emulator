﻿using System.Runtime.InteropServices;

namespace ConquerEmulator.Network.Packets.Structures
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct LearnProfPacket
	{
		public ushort Size;
		public ushort Type;
		public uint ID;
		public uint Level;
		public uint Experience;

		public static LearnProfPacket Create()
		{
			var LSpell = new LearnProfPacket();
			LSpell.Size = 0x10;
			LSpell.Type = 0x401;
			return LSpell;
		}
	}
}