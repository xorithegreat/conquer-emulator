﻿using ConquerEmulator.Client;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum NobilityAction : byte
	{
		None = 0,
		Donate,
		List,
		Info,
		QueryRemainingSilver
	}
	public unsafe struct NobilityPacket
    {
		public const uint
		   Donate = 1,
		   List = 2,
		   Icon = 3,
		   NextRank = 4;
		public NobilityAction Type;
		public long Donation;

		private fixed sbyte _string3[35];
		public string String3
		{
			get
			{
				fixed (sbyte* ptr = _string3)
				{
					return new string(ptr).TrimEnd('\0');
				}
			}
			set
			{
				fixed (sbyte* ptr = _string3)
				{
					Native.memset(ptr, 0, 35);
					value.CopyTo(ptr);
				}
			}
		}

		public long Data1
		{
			get { return (uint)Donation; }
			set { Donation = (Data2 << 32) | value; }
		}

		public uint Data2
		{
			get { return (uint)(Donation >> 32); }
			set { Donation = (value << 32) | Data1; }
		}
		public ushort Data2Low
		{
			get { return (ushort)Data2; }
			set { Data2 = (uint)((Data2High << 16) | value); }
		}
		public ushort Data2High
		{
			get { return (ushort)(Data2 >> 16); }
			set { Data2 = (uint)((value << 16) | Data2Low); }
		}
		public uint Data3;
		public uint Data4;
		public uint Data5;
		public uint Data6;
		public NetStringPacker Strings;

		public static NobilityPacket UpdateIcon(GameClient Client)
		{
			var packet = new NobilityPacket();
			packet.Type = NobilityAction.Info;
			packet.Data1 = Client.Hero.UID;
			packet.String3 = Client.Hero.UID + " " + Client.NobleDonation + " " + (byte)Client.NobleMedal + " " + Client.NobleRank;
			return packet;
		}
		public static implicit operator NobilityPacket(byte* ptr)
		{
			var packet = new NobilityPacket();
			packet.Type = *((NobilityAction*)(ptr + 4));
			packet.Data1 = *((uint*)(ptr + 8));
			packet.Data2 = *((uint*)(ptr + 12));
			packet.Data3 = *((uint*)(ptr + 16));
			packet.Data4 = *((uint*)(ptr + 20));
			packet.Data5 = *((uint*)(ptr + 24));
			packet.Data6 = *((uint*)(ptr + 30));
			return packet;
		}


		public static implicit operator byte[] (NobilityPacket packet)
		{
			var buffer = new byte[40 + 8 + Size(packet)];
			fixed (byte* ptr = buffer)
			{
				*((ushort*)ptr) = (ushort)(buffer.Length - 8);
				*((ushort*)(ptr + 2)) = 2064;
				*((NobilityAction*)(ptr + 4)) = packet.Type;
				*((long*)(ptr + 8)) = packet.Data1;

				switch (packet.Type)
				{
					case NobilityAction.List:
						{
							*((short*)(ptr + 8)) = (short)packet.Data1;  //Page n° (Param)
							*((short*)(ptr + 10)) = (short)ConquerNobility.PageCount; //Page Count (Param)
							*((int*)(ptr + 12)) = 0x00; //(Param)2
							if (packet.Strings != null)
								PacketKernel.AppendNetStringPacker(ptr + 28, packet.Strings);

							break;
						}
					case NobilityAction.Info:
						{
							*(uint*)(ptr + 28) = 1;//packet.Unknown16;
							*(uint*)(ptr + 29) = (uint)packet.String3.Length;
							Native.memcpy(ptr + 30, packet._string3, packet.String3.Length);
							break;
						}
				}
			}
			return buffer;
		}

		public static uint Size(NobilityPacket packet)
		{
			switch (packet.Type)
			{
				case NobilityAction.Info:
					return (uint)(40 + packet.String3.Length);
				case NobilityAction.Donate:
					return 40;
				case NobilityAction.QueryRemainingSilver:
					return 40;
				case NobilityAction.List:
					var page = ConquerNobility.GetPage((int)packet.Data1);
					if (page != null)
						return (uint)page.Length;
					return 60;
				default:
					return 40;
			}
		}
	}
}
