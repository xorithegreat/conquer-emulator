﻿using System.Runtime.InteropServices;
using ConquerEmulator.Item;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Network.Packets.Structures
{
	internal enum GroundIDs : ushort
	{
		Drop = 0x01,
		Remove = 0x02,
        GroundEffect = 10,
        RemoveGroundEffect = 12,
        ScreenEffect = 13
	}
    internal enum ScreenEffect : byte
    {
        Shake = 1,
        Zoom = 2,
        Zoom2 = 3,
        Darkness = 4
    }

    /*
     * Type 10 requires modification of the ID. Valid ranges are between 1-1000
     * Type13 requires modification of the UID for different screen based effects
     */

    [StructLayout(LayoutKind.Sequential)]
	internal struct ItemGroundPacket : IClassPacket
	{
		public ushort Size;
		public ushort Type;
		public uint UID;
		public uint ID;
		public ushort X;
		public ushort Y;
		public ushort Color;
		public GroundIDs Flag;

		public ItemGroundPacket(ConquerItem Item, GroundIDs flag, ushort x, ushort y)
		{
			Size = 0x14;
			Type = 0x44D;
			UID = Item.UID;
			ID = Item.ID;
			X = x;
			Y = y;
			Color = Item.Color;
			Flag = flag;
		}
        public ItemGroundPacket(uint _UID, uint EffectID, GroundIDs flag, ushort x, ushort y)
        {
            Size = 0x14;
            Type = 0x44D;
            UID = _UID;
            ID = EffectID;
            X = x;
            Y = y;
            Color = 0;
            Flag = flag;
        }
        #region IClassPacket Members
        public void Deserialize(byte[] Bytes)
		{
		}

		public byte[] Serialize()
		{
			return PacketKernel.Serialize(this, Size);
		}
		#endregion
	}
}