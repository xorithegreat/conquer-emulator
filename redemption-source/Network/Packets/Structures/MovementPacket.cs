﻿using System.Runtime.InteropServices;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Network.Packets.Structures
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct MovementPacket : IClassPacket
	{
		public ushort Size;
		public ushort Type;
        public ConquerAngle Direction;
        public uint UID;
		public bool Running;
        public uint TimeStamp;

		public MovementPacket(uint _UID, ConquerAngle _Facing, bool _Running)
		{
			Size = 0x14;
			Type = 0x2715;
			UID = _UID;
			Direction = _Facing;
			Running = _Running;
			TimeStamp = Native.timeGetTime();
		}
		#region IClassPacket Members
		public void Deserialize(byte[] Bytes)
		{
		}

		public byte[] Serialize()
		{
			return PacketKernel.Serialize(this, Size);
		}
		#endregion
	}
}