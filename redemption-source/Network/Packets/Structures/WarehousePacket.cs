﻿using System.Runtime.InteropServices;

namespace ConquerEmulator.Network.Packets.Structures
{
	internal enum WhIDs : uint
	{
		Mobile = 0,
		TwinCity = 8,
		Market = 44,
		Phoenix = 10012,
		Desert = 10011,
		Bird = 10027,
		Ape = 10028
	}

	internal enum WhItemIDs : byte
	{
		Load = 0,
		Deposit = 1,
		Withdraw = 2
	}

	[StructLayout(LayoutKind.Explicit)]
	internal struct WarehousePacket
	{
		public const ushort cSize = 0x10;
		public const ushort cType = 0x44E;

		[FieldOffset(0)]
		public ushort Size;
		[FieldOffset(2)]
		public ushort Type;
		[FieldOffset(4)]
		public uint ID;
		[FieldOffset(8)]
		public WhItemIDs Mode;
		[FieldOffset(9)]
		public uint Unknown;
		[FieldOffset(12)]
		public uint UID;
	}
}