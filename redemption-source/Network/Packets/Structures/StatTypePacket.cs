﻿using System;
using System.Runtime.InteropServices;
using ConquerEmulator.Client;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Network.Packets.Structures
{
	public enum StatIDs : uint
	{
		None = uint.MaxValue,
		Hitpoints = 0,
		MaxHitPoints, 
		Mana,
		MaxMana,
		Money,
		Experience = 5,
		PkPoint = 6,
		Job = 7,
		Stamina = 8,
        SizeAdd = 9,
        StatPoints = 10,
        Model,
		Level,
		Spirit,
		Vitality = 14,
		Strength,
		Agility,
		HeavenBlessing = 17,
		DoubleExpTime = 18,
		CurseTime = 20,
		Reborn = 22,
		RaiseFlag = 25,
		Hairstyle,
		Xp,
		LuckyTime,
		ConquerPoints,
		OnlineTraining = 31,
		ExtraBP = 36,
		Merchant = 38,
        VIPLevel = 39,
		QuizPoints = 40,
		EnlightPoints = 41,
		BonusBP = 44,
		BoundCp = 45,
	}

	[Flags]
	public enum StatusFlag : ulong
	{
		FlashingName = 0x01,
		Poisoned = 0x02,
		Invisible = 0x04,
		XPList = 0x10,
		StarOfAccuracy = 0x80,
		MagicShield = 0x100,
		FadeAway = 0x800,
		RedName = 0x4000,
		BlackName = 0x8000,
		ReflectMelee = 0x20000,
		Superman = 0x40000,
		Ball = 0x80000,
		Ball2 = 0x100000,
		Invisibility = 0x400000,
		Cyclone = 0x800000,
		Dodge = 0x4000000,
		Fly = 0x8000000,
		Intensify = 0x10000000,
		CastingPrayer = 0x40000000,
		GettingPrayer = 0x80000000,
        Curse = 0x100000000,
        Blessing = 0x200000000,
        Faded = 0x820,
		Stigma = 0x200,
		Dead = 0x400,
		Ghost = 0x20,
		None = 0x00,
		TeamLeader = 0x40
	}

	[StructLayout(LayoutKind.Explicit)]
	public unsafe struct StatTypePacket : IClassPacket
	{
        [FieldOffset(0)]
        public ushort Size;
        [FieldOffset(2)]
        public ushort Type;
        [FieldOffset(4)]
        public uint UID;
        [FieldOffset(8)]
        public uint TotalUpdates;
        [FieldOffset(12)]
        public StatIDs ID;
        [FieldOffset(16)]
        public uint Value;
        [FieldOffset(20)]
        public uint Footer;
        [FieldOffset(16)]
        public ulong BigValue;
        [FieldOffset(24)]
        public fixed uint Pad[3];

		public static StatTypePacket Create()
		{
			var Stat = new StatTypePacket();
			Stat.Size = 36;
			Stat.Type = 10017;
			Stat.TotalUpdates = 1;
			return Stat;
		}

		public StatTypePacket(uint _UID, ulong _Value, StatIDs _ID)
		{
			Size = 24;
			Type = 10017;
			UID = _UID;
			TotalUpdates = 1;
			ID = _ID;
		    Value = 0;
		    BigValue = _Value;
		    Footer = 0;
		}

        #region IClassPacket Members
        public void Deserialize(byte[] Bytes)
		{
		}

		public byte[] Serialize()
		{
			return PacketKernel.Serialize(this, Size);
		}
		#endregion
	}

	public unsafe class BigUpdatePacket
	{
		private const int SizeOf_Data = 12;
		private readonly byte[] Buffer;

		public BigUpdatePacket(uint TotalUpdates)
		{
			Buffer = new byte[12 + TotalUpdates * SizeOf_Data + 8];
			fixed (byte* _iUpdate = Buffer)
			{
				var iUpdate = (internalUpdate*)_iUpdate;
				iUpdate->Size = (ushort)(Buffer.Length - 8);
				iUpdate->Type = 10017;
				iUpdate->UpdateCount = TotalUpdates;
			}
		}

		public uint UID
		{
			get
			{
				fixed (byte* iUpdate = Buffer)
				{
					return ((internalUpdate*)iUpdate)->UID;
				}
			}
			set
			{
				fixed (byte* iUpdate = Buffer)
				{
					((internalUpdate*)iUpdate)->UID = value;
				}
			}
		}

		public static implicit operator byte[](BigUpdatePacket big)
		{
			return big.Buffer;
		}

		public void Append(int UpdateNumber, StatIDs ID, ulong Value)
		{
			fixed (byte* iUpdate = Buffer)
			{
				*(Data*)(iUpdate + 12 + UpdateNumber * SizeOf_Data) = Data.Create(ID, Value);
			}
		}

		public void Append(int UpdateNumber, StatIDs ID, uint Value)
		{
			fixed (byte* iUpdate = Buffer)
			{
				*(Data*)(iUpdate + 12 + UpdateNumber * SizeOf_Data) = Data.Create(ID, Value);
			}
		}

		public void Append(int UpdateNumber, StatIDs ID, ushort Value)
		{
			fixed (byte* iUpdate = Buffer)
			{
				*(Data*)(iUpdate + 12 + UpdateNumber * SizeOf_Data) = Data.Create(ID, Value);
			}
		}

		public void Append(int UpdateNumber, StatIDs ID, int Value)
		{
			fixed (byte* iUpdate = Buffer)
			{
				*(Data*)(iUpdate + 12 + UpdateNumber * SizeOf_Data) = Data.Create(ID, Value);
			}
		}

		public void Append(int UpdateNumber, StatIDs ID, byte Value)
		{
			fixed (byte* iUpdate = Buffer)
			{
				*(Data*)(iUpdate + 12 + UpdateNumber * SizeOf_Data) = Data.Create(ID, Value);
			}
		}

		public void HitpointsAndMana(GameClient Client, int StartIndex)
		{
			Append(StartIndex, StatIDs.Hitpoints, Client.Hero.Hitpoints);
			Append(StartIndex + 1, StatIDs.Mana, Client.Mana);
		}

		public void AllStats(GameClient Client, int StartIndex)
		{
			Append(StartIndex, StatIDs.Spirit, Client.Spirit);
			Append(StartIndex + 1, StatIDs.Vitality, Client.Vitality);
			Append(StartIndex + 2, StatIDs.Strength, Client.Strength);
			Append(StartIndex + 3, StatIDs.Agility, Client.Agility);
			Append(StartIndex + 4, StatIDs.StatPoints, Client.StatPoints);
		}

		private struct internalUpdate
		{
			public ushort Size;
			public ushort Type;
			public uint UID;
			public uint UpdateCount;
		}

		[StructLayout(LayoutKind.Explicit, Size = SizeOf_Data)]
		public struct Data
		{
			[FieldOffset(0)]
			public StatIDs ID;
			[FieldOffset(4)]
			public uint Value;
			[FieldOffset(8)]
			public uint Footer;
			[FieldOffset(4)]
			public ulong qwValue;

			public static Data Create(StatIDs _ID, ulong _Value)
			{
				var retn = new Data();
				retn.ID = _ID;
				retn.qwValue = _Value;
				return retn;
			}

			public static Data Create(StatIDs _ID, uint _Value)
			{
				var retn = new Data();
				retn.ID = _ID;
				retn.Value = _Value;
				return retn;
			}

			public static Data Create(StatIDs _ID, int _Value)
			{
				var retn = new Data();
				retn.ID = _ID;
				retn.Value = (uint)_Value;
				return retn;
			}

			public static Data Create(StatIDs _ID, ushort _Value)
			{
				var retn = new Data();
				retn.ID = _ID;
				retn.Value = _Value;
				return retn;
			}

			public static Data Create(StatIDs _ID, byte _Value)
			{
				var retn = new Data();
				retn.ID = _ID;
				retn.Value = _Value;
				return retn;
			}
		}
	}
}