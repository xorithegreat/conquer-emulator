﻿using System;
using ConquerEmulator.Attacking;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Mob
{
	public unsafe class MonsterPet : Entity
	{
	    public AssignPetPacket Assignment;

	    public MonsterPet(GameClient Client, string Name, AssignPetPacket Assign) : base(EntityFlag.Pet, Client)
		{
			if (FlatDatabase.Pets.SectionExists(Name))
			{
				Assignment = Assign;
				var gameClient = Owner as GameClient;
				if (gameClient != null)
					gameClient.Pet = this;

				var MS = new MonsterStatus();
				MS.Level = FlatDatabase.Pets.ReadByte(Name, "Level", 0);
				MS.MaxAttack = FlatDatabase.Pets.ReadUInt32(Name, "Attack", 0);
				MS.MinAttack = MS.MaxAttack;
				MS.Mesh = FlatDatabase.Pets.ReadUInt16(Name, "Mesh", 0);
				MS.Hitpoints = FlatDatabase.Pets.ReadInt32(Name, "Hitpoints", 0);
				MS.Defence = FlatDatabase.Pets.ReadUInt16(Name, "Defence", 0);
				MS.AttackRange = (uint)FlatDatabase.Pets.ReadSByte(Name, "AttackRange", 0);

				this.Name = FlatDatabase.Pets.ReadString(Name, "Name", "ERROR");
				MapID = Client.Hero.MapID;
				UID = 700000 + (Client.Hero.UID - 1000000);
				Level = MS.Level;
				MaxAttack = MS.MaxAttack;
				MinAttack = MS.MinAttack;
				Model = MS.Mesh;
				MaxHitpoints = MS.Hitpoints;
				Hitpoints = MS.Hitpoints;
				Defence = (ushort)MS.Defence;
				var client = Owner as GameClient;
				if (client != null)
				{
					X = client.Hero.X;
					Y = client.Hero.Y;
				}
			}
			else
			{
				throw new ArgumentException("Name");
			}
		}

	    public static void GetAssignmentData(GameClient Client, string Name, AssignPetPacket* pData)
		{
			pData->Model = FlatDatabase.Pets.ReadUInt16(Name, "Mesh", 0);
			if (pData->Model != 0)
			{
				pData->X = Client.Hero.X;
				pData->Y = Client.Hero.Y;
				pData->AIType = 1;
				pData->UID = 700000 + (Client.Hero.UID - 1000000);
				Name.CopyTo(pData->Name);
			}
			else
			{
				throw new ArgumentException("Bad pet name");
			}
		}

	    public void Attach()
		{
			var Client = Owner as GameClient;
			if (Client != null)
			{
				Client.SendScreen(new DataPacket(UID, DataIDs.SpawnEffect).Serialize(), true);
				Client.SendScreen(SpawnPacket, true);
			}
		}

	    public void Reattach()
		{
			fixed (AssignPetPacket* pAssign = &Assignment)
			{
				var gameClient = Owner as GameClient;
				if (gameClient != null)
				{
					gameClient.Send(PacketKernel.GetBytes(pAssign, pAssign->Size));
					Attach();
				}
			}
		}

	    public void Process(IBaseEntity Target)
		{
			if (Stamps.Check(Stamps.Attack))
			{
				if (!Target.Dead)
					AttackProcessor.PhysicalAttack(this, Target, AttackType.Physical, true);
				Stamps.Attack = DateTime.Now.AddSeconds(1);
			}
		}

	    public void Kill()
		{
			Dead = true;

			var Client = Owner as GameClient;
			if (Client != null)
			{
				Client.Pet = null;
				Client.Screen.Cleanup();
				Client.SendScreen(new DataPacket(UID, DataIDs.RemoveEntity).Serialize(), true);
			}
		}
	}
}