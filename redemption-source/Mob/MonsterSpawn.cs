﻿namespace ConquerEmulator.Mob
{
	public class MonsterSpawn
	{
	    public uint ManualSpawnCounter;
	    public uint MapID;
	    public ushort NumberToSpawn;
	    public ushort RestSecs;
	    public uint Type;
	    public uint UniqueID;

	    public ushort XStart;
	    public ushort XStop;

	    public ushort YStart;
	    public ushort YStop;

	    public MonsterSpawn()
        {
            ManualSpawnCounter = 4000000000;
        }
	}
}