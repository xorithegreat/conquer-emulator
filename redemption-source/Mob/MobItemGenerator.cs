﻿using System;
using ConquerEmulator.Database;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.MultiThreading;

namespace ConquerEmulator.Mob
{
	public struct MobRateWatcher
	{
	    private readonly double tick;
        public static implicit operator bool(MobRateWatcher q)
        {
            return DataStructures.ChanceSuccess(q.tick);
        }

        public MobRateWatcher(double Tick)
		{
			tick = Tick;
		}
	}

	public struct SpecialItemWatcher 
	{
		public uint ID;
	    public byte Amount;
		public MobRateWatcher Rate;

        public SpecialItemWatcher(uint ID, double Tick, byte Amount = 1)
		{
			this.ID = ID;
		    this.Amount = Amount;
			Rate = new MobRateWatcher(Tick);
		}
	}

	public class MobItemGenerator
	{
	    private static readonly ushort[] NecklaceType = { 120, 121 };
	    private static readonly ushort[] RingType = { 120, 121 };
	    private static readonly ushort[] ArmetType = { 111, 112, 113, 114, 117, 118 };
	    private static readonly ushort[] ArmorType = { 130, 131, 132, 133, 134 };
	    private static readonly ushort[] OneHanderType = { 410, 420, 421, 430, 440, 450, 460, 480, 481, 490, 500 };
	    private static readonly ushort[] TwoHanderType = { 510, 530, 560, 561, 580, 900 };
	    private readonly MobRateWatcher Elite;
	    private readonly MobRateWatcher PlusOne;

	    private readonly MobRateWatcher Refined;
	    private readonly MonsterStatus Status;
	    private readonly MobRateWatcher Super;
	    private readonly MobRateWatcher Unique;

	    public MobItemGenerator(MonsterStatus family)
		{
			Status = family;
			Refined = new MobRateWatcher(10000 / family.Level);
			Unique = new MobRateWatcher(40000 / family.Level);
			Elite = new MobRateWatcher(80000 / family.Level);
			Super = new MobRateWatcher(100000 / family.Level);
		    PlusOne = new MobRateWatcher(DataStructures.PlusOne);
		}

	    public uint GenerateItemId(out byte dwItemQuality, out bool Special, out byte Amount)
		{
			Special = false;
			foreach (var sp in Status.Drops.DropSpecials)
			{
				if (sp.Rate)
				{
                    Special = true;
                    Amount = sp.Amount;
					dwItemQuality = (byte)(sp.ID % 10);
					return sp.ID;
				}
			}

		    Amount = 1;
			dwItemQuality = GenerateQuality();
			uint dwItemSort = 0;
			uint dwItemLev = 0;

			var nRand = RandomGenerator.Generator.Next(0, 1200);
			if ((nRand >= 0) && (nRand <= 20)) // 0.17%
			{
				dwItemSort = 160;
				dwItemLev = Status.Drops.DropBoots;
			}
			else if ((nRand >= 20) && (nRand < 50)) // 0.25%
			{
				dwItemSort = NecklaceType[RandomGenerator.Generator.Next(0, NecklaceType.Length)];
				dwItemLev = Status.Drops.DropNecklace;
			}
			else if ((nRand >= 50) && (nRand < 100)) // 4.17%
			{
				dwItemSort = RingType[RandomGenerator.Generator.Next(0, RingType.Length)];
				dwItemLev = Status.Drops.DropRing;
			}
			else if ((nRand >= 100) && (nRand < 400)) // 25%
			{
				dwItemSort = ArmetType[RandomGenerator.Generator.Next(0, ArmetType.Length)];
				dwItemLev = Status.Drops.DropArmet;
			}
			else if ((nRand >= 400) && (nRand < 700)) // 25%
			{
				dwItemSort = ArmorType[RandomGenerator.Generator.Next(0, ArmorType.Length)];
				dwItemLev = Status.Drops.DropArmor;
			}
			else // 45%
			{
				var nRate = RandomGenerator.Generator.Next(0, 1000) % 100;
				if ((nRate >= 0) && (nRate < 20)) // 20% of 45% (= 9%) - Backswords
				{
					dwItemSort = 421;
				}
				else if ((nRate >= 40) && (nRate < 80)) // 40% of 45% (= 18%) - One handers
				{
					dwItemSort = OneHanderType[RandomGenerator.Generator.Next(0, OneHanderType.Length)];
					dwItemLev = Status.Drops.DropWeapon;
				}
				else if ((nRand >= 80) && (nRand < 100)) // 20% of 45% (= 9%) - Two handers (and shield)
				{
					dwItemSort = TwoHanderType[RandomGenerator.Generator.Next(0, TwoHanderType.Length)];
					dwItemLev = dwItemSort == 900 ? Status.Drops.DropShield : Status.Drops.DropWeapon;
				}
			}
			if (dwItemLev != 99)
			{
				dwItemLev = AlterItemLevel(dwItemLev, dwItemSort);
				var idItemType = dwItemSort * 1000 + dwItemLev * 10 + dwItemQuality;
				if (MySqlDatabase.FindItem(idItemType))
					return idItemType;
			}
			return 0;
		}

	    public byte GeneratePurity()
		{
			if (PlusOne)
				return 1;
			return 0;
		}

	    public byte GenerateBless()
		{
			if (RandomGenerator.Generator.Next(0, 1000) < 250) // 25%
			{
				var selector = RandomGenerator.Generator.Next(0, 100);
				if (selector < 1)
					return 5;
				if (selector < 6)
					return 3;
			}
			return 0;
		}

	    public byte GenerateSocketCount(uint ItemID)
		{
			if ((ItemID >= 410000) && (ItemID <= 601999))
			{
				var nRate = RandomGenerator.Generator.Next(0, 1000) % 100;
				if (nRate < 5) // 5%
					return 2;
				if (nRate < 20) // 15%
					return 1;
			}
			return 0;
		}

	    private byte GenerateQuality()
		{
			if (Refined)
				return 6;
			if (Unique)
				return 7;
			if (Elite)
				return 8;
			if (Super)
				return 9;
			return 3;
		}

	    public int GenerateGold(out uint ItemID)
		{
			var amount = RandomGenerator.Generator.Next(Status.Drops.DropMoney, Status.Drops.DropMoney * 10);
			ItemID = ConquerItem.IdFromAmount((uint)amount);
			return amount;
		}

	    private uint AlterItemLevel(uint dwItemLev, uint dwItemSort)
		{
			var nRand = RandomGenerator.Generator.Next(0, 1000) % 100;
			if (nRand < 50) // 50% down one level
			{
				var dwLev = dwItemLev;
				dwItemLev = (uint)(RandomGenerator.Generator.Next(0, (int)(dwLev / 2)) + dwLev / 3);

				if (dwItemLev > 1)
					dwItemLev--;
			}
			else if (nRand > 80) // 20% up one level
			{
				if (((dwItemSort >= 110) && (dwItemSort <= 114)) || ((dwItemSort >= 130) && (dwItemSort <= 134)) || ((dwItemSort >= 900) && (dwItemSort <= 999)))
					dwItemLev = Math.Min(dwItemLev + 1, 9);
				else
					dwItemLev = Math.Min(dwItemLev + 1, 23);
			}
			return dwItemLev;
		}
	}
}