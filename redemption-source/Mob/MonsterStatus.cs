﻿using System.Collections.Generic;

namespace ConquerEmulator.Mob
{
	public struct DropData
	{
		public int DropMoney;
		public uint DropBoots;
		public uint DropArmor;
		public uint DropRing;
		public uint DropNecklace;
		public uint DropShield;
		public uint DropWeapon;
		public uint DropHP;
		public uint DropMP;
		public uint DropArmet;
		public SpecialItemWatcher[] DropSpecials;
	}

	public class MonsterStatus
	{
	    private static readonly Dictionary<uint, MobItemGenerator> ItemGeneratorLinker;
	    public int Action;
	    public uint AttackRange;
	    public uint Defence;
	    public uint Dodge;
	    public DropData Drops;
	    public int Hitpoints;
	    public uint Id;
	    public MobItemGenerator ItemGenerator;
	    public byte Level;
	    public uint MagicDefence;
	    public uint MagicType;
	    public uint MaxAttack;
	    public uint Mesh;
	    public uint MinAttack;
	    public string Name;
	    public uint Speed;
	    public uint ViewDistance;

	    static MonsterStatus()
		{
			ItemGeneratorLinker = new Dictionary<uint, MobItemGenerator>();
		}

	    public void CreateItemGenerator()
		{
			if (!ItemGeneratorLinker.TryGetValue(Id, out ItemGenerator))
			{
				ItemGenerator = new MobItemGenerator(this);
				ItemGeneratorLinker.Add(Id, ItemGenerator);
			}
		}
	}
}