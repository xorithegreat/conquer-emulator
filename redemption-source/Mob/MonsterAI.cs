﻿using System;
using ConquerEmulator.Attacking;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.DMap;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Mob
{
    public class MonsterAI
    {
        public static void Search(Monster Monster)
        {
            foreach (var Client in Kernel.Clients)
            {
                if (Monster.Entity.MapID.ID == Client.Hero.MapID.ID && Client.Hero.MapID.DynamicID == Monster.Entity.MapID.DynamicID)
                {
                    if ((Kernel.GetDistance(Monster.Entity.X, Monster.Entity.Y, Client.Hero.X, Client.Hero.Y) <= DataStructures.ViewDistance) && !Monster.Entity.Dead)
                        Monster.Target = Client;
                }
            }
        }

        public static void Process(Monster Monster, GameClient Target)
        {
            try
            {
                Monster.Target = Target;

                if (Target.Hero.Dead)
                {
                    Monster.Target = null;
                    return;
                }
                if (Monster.IsGuard && !Target.Hero.Flashing)
                {
                    MoveBack(Monster);
                    return;
                }
                if (Monster.Entity.Dead)
                {
                    MoveBack(Monster);
                    return;
                }

                var Distance = Kernel.GetDistance(Monster.Entity.X, Monster.Entity.Y, Monster.Target.Hero.X, Monster.Target.Hero.Y);
                if ((Distance <= Monster.ViewDistance) && (Distance > Monster.AttackRange))
                    MoveMob(Monster);
                else if (Distance <= Monster.AttackRange)
                {
                    if (!Target.LoggedIn)
                        return;
                    if (!Stamps.Check(Target.Hero.Stamps.Protection))
                        return;
                    if (Monster.MagicType == 0)
                        AttackProcessor.PhysicalAttack(Monster.Entity, Monster.Target.Hero, AttackType.Physical, true);
                    else 
                        AttackProcessor.MagicalAttack(Monster.Entity, Monster.Target.Hero, Monster.MagicType, Monster.Target.Hero.X, Monster.Target.Hero.Y);
                }
                else if (Distance >= Monster.ViewDistance)
                {
                    Monster.Target = null;
                    return;
                }
                Monster.Entity.Stamps.Attack.AddMilliseconds(Monster.Entity.Speed + RandomGenerator.Generator.Next(1000, 6000));
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }

        public static void MoveMob(Monster Monster)
        {
            try
            {
                if ((Monster.Target == null) || Monster.Target.Hero.Dead)
                    return;

                var x = Monster.Entity.X;
                var y = Monster.Entity.Y;

                var walk = Kernel.GetFacing(Kernel.GetAngle(Monster.Entity.X, Monster.Entity.Y, Monster.Target.Hero.X, Monster.Target.Hero.Y));
                Kernel.IncXY(walk, ref x, ref y);

                var Continue = false;
                var DMap = ConquerDMap.Maps[Monster.Entity.MapID.ID];
                if (DMap != null)
                {
                    if (!DMap.Invalid(x, y) && !DMap.MonsterOnTile(x, y))
                    {
                        Monster.CurrentDMap.SetMonsterOnTile(Monster.Entity.X, Monster.Entity.Y, false);
                        Monster.CurrentDMap.SetMonsterOnTile(x, y, true);
                        Continue = true;
                    }
                }
                if (Continue)
                {
                    Monster.Entity.X = x;
                    Monster.Entity.Y = y;
                    Monster.Entity.Facing = walk;

                    var Move = new MovementPacket(Monster.Entity.UID, Monster.Entity.Facing, false);
                    PacketKernel.SendRangedPacket(Monster.Entity, Move.Serialize());
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }

        public static void MoveBack(Monster Monster)
        {
            PacketKernel.SendRangedPacket(Monster.Entity, new DataPacket(Monster.Entity.UID, DataIDs.RemoveEntity).Serialize());
            Monster.CurrentDMap.SetMonsterOnTile(Monster.Entity.X, Monster.Entity.Y, false);
            Monster.CurrentDMap.SetMonsterOnTile(Monster.SpawnX, Monster.SpawnY, true);
            Monster.Entity.X = Monster.SpawnX;
            Monster.Entity.Y = Monster.SpawnY;
            PacketKernel.SendRangedPacket(Monster.Entity, Monster.Entity.SpawnPacket);

            Monster.Target = null;
        }

        public static void Kill(Monster Monster, Entity Attacker)
        {
            Monster.Target = null;
            Monster.Entity.StatusFlag |= StatusFlag.Dead | StatusFlag.Ghost | StatusFlag.FadeAway;
            PacketKernel.SendRangedPacket(Monster.Entity, new StatTypePacket(Monster.Entity.UID, (uint)Monster.Entity.StatusFlag, StatIDs.RaiseFlag).Serialize());
            Monster.Remove = true;
            Monster.Entity.Stamps.Remove = DateTime.Now.AddSeconds(3);
        }

        public static void Drop(Monster Monster, Entity eKiller)
        {
            if (Monster.IsGuard)
                return;

            var Status = Kernel.MonsterStats[Monster.Type];
            if (Status == null)
                return;

            byte ItemQuality, Amount; 
            bool Special;
            uint ItemID;
            #region Meteors and DragonBalls
            if (DataStructures.ChanceSuccess(DataStructures.Meteor)) // 1:144 chance to drop a Meteor
            {
                var Item = new ConquerItem(true);
                Item.ID = 1088001;
                Item.DropperUID = eKiller.UID;
                Item.Durability = 1;
                Item.MaxDurability = Item.Durability;
                FloorItem.Drop(Monster.Entity, Item);
            }
            else if (DataStructures.ChanceSuccess(DataStructures.DragonBall)) // 1:1538 chance to drop a DragonBall
            {
                var Item = new ConquerItem(true);
                Item.ID = 1088000;
                Item.DropperUID = eKiller.UID;
                Item.Durability = 1;
                Item.MaxDurability = Item.Durability;
                FloorItem.Drop(Monster.Entity, Item);
            }
            #endregion
            #region Potions
            else if (Status.Drops.DropHP != 0 && DataStructures.ChanceSuccess(5.0)) // 5% chance to drop a HP Potion
            {
                var Item = new ConquerItem(true);
                Item.ID = Status.Drops.DropHP;
                Item.DropperUID = eKiller.UID;
                Item.Durability = 1;
                Item.MaxDurability = Item.Durability;
                FloorItem.Drop(Monster.Entity, Item);
            }
            else if (Status.Drops.DropMP != 0 && DataStructures.ChanceSuccess(5.0)) // 5% chance to drop a MP potion
            {
                var Item = new ConquerItem(true);
                Item.ID = Status.Drops.DropMP;
                Item.DropperUID = eKiller.UID;
                Item.Durability = 1;
                Item.MaxDurability = Item.Durability;
                FloorItem.Drop(Monster.Entity, Item);
            }
            #endregion
            #region Gold
            else if (DataStructures.ChanceSuccess(50.0)) // 50% chance to drop cash
            {
                if (Status.Drops.DropMoney > 0)
                {
                    uint itemid;
                    var amount = (uint)Status.ItemGenerator.GenerateGold(out itemid);
                    var Item = new ConquerItem(true);
                    Item.ID = itemid;
                    Item.DropperUID = eKiller.UID;
                    FloorItem.Drop(Monster.Entity, Item, amount);
                }
            }
            #endregion
            #region Equipment
            else if (DataStructures.ChanceSuccess(60.0) && (ItemID = Status.ItemGenerator.GenerateItemId(out ItemQuality, out Special, out Amount)) != 0)
            {
                for (var i = 0; i < Amount; i++)
                {
                    var itemstats = new StanderdItemStats(ItemID);
                    var Item = new ConquerItem(true);
                    Item.ID = ItemID;
                    Item.DropperUID = eKiller.UID;
                    Item.MaxDurability = itemstats.Durability;
                    Item.Durability = (ushort)Math.Min(Item.MaxDurability, RandomGenerator.Generator.Next(1, 3) * 100);

                    if (!Special)
                    {
                        var lucky = ItemQuality > 7; // q>unique
                        if (!lucky)
                            lucky = (Item.Plus = Status.ItemGenerator.GeneratePurity()) != 0;
                        if (!lucky)
                            lucky = (Item.Bless = Status.ItemGenerator.GenerateBless()) != 0;

                        if (!lucky)
                        {
                            var sockets = Status.ItemGenerator.GenerateSocketCount(ItemID);
                            if (sockets >= 1)
                                Item.SocketOne = GemsConst.OpenSocket;
                            else if (sockets == 2)
                                Item.SocketTwo = GemsConst.OpenSocket;
                        }
                    }
                    FloorItem.Drop(Monster.Entity, Item);
                }
            }
            #endregion
        }

        public static void Remove(Monster Monster)
        {
            if (Monster.UniqueID == 9999) // AncientDevil6
                Kernel.AncientDevil = false;

            Monster.Entity.Stamps.Revive = DateTime.Now.AddSeconds(Monster.RestSecs);
            Monster.Remove = false;
            Monster.Respawn = true;
            PacketKernel.SendRangedPacket(Monster.Entity, new DataPacket(Monster.Entity.UID, DataIDs.RemoveEntity).Serialize());

            Monster.CurrentDMap.SetMonsterOnTile(Monster.Entity.X, Monster.Entity.Y, false);

            Monster.Target = null;
        }

        public static void Respawn(Monster Mob)
        {
            if (!Kernel.MonsterSpawns.ContainsKey(Mob.UniqueID))
                return;

            var Spawn = Kernel.MonsterSpawns[Mob.UniqueID];
            var MS = Kernel.MonsterStats[Mob.Type];

            if ((Spawn == null) || (MS == null))
                return;

            Mob.Entity.Dead = false;
            Mob.Entity.StatusFlag &= ~StatusFlag.Dead;
            Mob.Entity.StatusFlag &= ~StatusFlag.Ghost;
            Mob.Entity.StatusFlag &= ~StatusFlag.FadeAway;
            Mob.Entity.Hitpoints = MS.Hitpoints;
            Mob.Entity.X = (ushort)RandomGenerator.Generator.Next(Spawn.XStart, Spawn.XStop);
            Mob.Entity.Y = (ushort)RandomGenerator.Generator.Next(Spawn.YStart, Spawn.YStop);
            Mob.Entity.Facing = (ConquerAngle)RandomGenerator.Generator.Next(0, 7);
            Mob.CurrentDMap.SetMonsterOnTile(Mob.Entity.X, Mob.Entity.Y, true);

            PacketKernel.SendRangedPacket(Mob.Entity, Mob.Entity.SpawnPacket);
            Mob.Respawn = false;

            PacketKernel.SendRangedPacket(Mob.Entity, new DataPacket(Mob.Entity.UID, DataIDs.SpawnEffect).Serialize());
        }
    }
}