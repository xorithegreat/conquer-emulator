﻿using System;
using System.Collections.Generic;
using ConquerEmulator.Client;
using ConquerEmulator.Main;
using ConquerEmulator.Main.DMap;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.MultiThreading;

namespace ConquerEmulator.Mob
{
	public class Monster
	{
	    private const uint NextMobUID_Start = 400001;
	    private const uint NextMobUID_Reset = 499999;
	    private static uint m_NextMobUID = NextMobUID_Start;
	    private static readonly int Multiplier = 3;
	    public ushort AttackRange;
	    public bool CanJump = false;

	    public Entity Entity;

	    private ConquerDMap m_CurrentDMap;
	    public ushort MagicType;

	    public DateTime MoveBack = DateTime.Now;
	    public bool Remove = false;
	    public bool Respawn = false;
	    public ushort RestSecs;

	    public bool Revive = false;
	    public ushort SpawnX;
	    public ushort SpawnY;

	    public GameClient Target;
	    public uint Type;
	    public uint UniqueID;
	    public ushort ViewDistance;

	    public ConquerDMap CurrentDMap
        {
            get
            {
                if (m_CurrentDMap == null)
                    ConquerDMap.Maps.TryGetValue(Entity.MapID.ID, out m_CurrentDMap);
                return m_CurrentDMap;
            }
        }

	    public static uint NextMobUID
		{
			get
			{
				m_NextMobUID++;
				return m_NextMobUID;
			}
			set
			{
				if (m_NextMobUID >= NextMobUID_Reset)
					m_NextMobUID = NextMobUID_Start;
			}
		}

	    public bool IsGuard => (Entity.Model == 900) || (Entity.Model == 901);

	    public static void SpawnMonsters()
		{
			uint Count = 0;
			foreach (var DE in Kernel.MonsterSpawns)
			{
				var Spawn = DE.Value;
				var MS = Kernel.MonsterStats[Spawn.Type];

				int Total = Spawn.NumberToSpawn;
				if (Total > 1)
					Total *= Multiplier;

				if (MS == null)
					continue;

				for (byte i = 0; i < Total; i++)
				{
					var Result = new Monster();
					Result.Entity = new Entity(EntityFlag.Monster, Result);
					LoadMobStatus(Result, MS, Spawn);

					GetMobDictionary:
					Dictionary<uint, Monster> Mobs;
					if (Kernel.Monsters.TryGetValue(Result.Entity.MapID, out Mobs))
						Mobs.Add(Result.Entity.UID, Result);
					else
					{
						Mobs = new Dictionary<uint, Monster>();
						Kernel.Monsters.TryAdd(Result.Entity.MapID, Mobs);
						goto GetMobDictionary;
					}
					Count++;
				}
			}
			Console.WriteLine("Loaded {0} Monsters.", Count);
		}

	    public static void LoadMobStatus(Monster Result, MonsterStatus MS, MonsterSpawn Spawn)
		{
			Result.Entity.UID = NextMobUID;
			Result.UniqueID = Spawn.UniqueID;
			Result.Entity.Name = MS.Name;
			Result.Entity.Model = MS.Mesh;
			Result.Entity.X = (ushort)RandomGenerator.Generator.Next(Spawn.XStart, Spawn.XStop);
			Result.Entity.Y = (ushort)RandomGenerator.Generator.Next(Spawn.YStart, Spawn.YStop);
			Result.SpawnX = Result.Entity.X;
			Result.SpawnY = Result.Entity.Y;
			Result.Entity.Level = MS.Level;
			Result.Entity.Hitpoints = MS.Hitpoints;
			Result.Entity.MaxHitpoints = MS.Hitpoints;
			Result.Entity.Facing = (ConquerAngle)RandomGenerator.Generator.Next(0, 7);
			Result.Entity.MinAttack = MS.MinAttack;
			Result.Entity.MaxAttack = MS.MaxAttack;
			Result.Entity.MagicDefence = (ushort)MS.MagicDefence;
			Result.Entity.Defence = (ushort)MS.Defence;
			Result.AttackRange = (ushort)MS.AttackRange;
			Result.ViewDistance = (ushort)MS.ViewDistance;
			Result.Entity.Speed = (ushort)MS.Speed;
			Result.Entity.Dodge = (sbyte)MS.Dodge;
			Result.MagicType = (ushort)MS.MagicType;
			Result.Type = Spawn.Type;
		    Result.RestSecs = Spawn.RestSecs;
			Result.Entity.MapID = new Map(Spawn.MapID);
        }
	}
}