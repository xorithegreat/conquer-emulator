﻿using System;
using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Main;
using ConquerEmulator.Main.DMap;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Mob;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Item
{
	public class FloorItem
	{
	    private static uint Money;

	    public static bool Drop(Entity Entity, ConquerItem item, uint Amount = 0)
		{
			try
			{
				Money = Amount;
                switch (Entity.EntityFlag)
                {
                    case EntityFlag.Player:
                        {
                            var Client = (GameClient)Entity.Owner;

                            if (item.Locked == 1)
                                return false;

                            ushort drop_x = Client.Hero.X, drop_y = Client.Hero.Y;
                            if (Client.CurrentDMap.FindValidDropLocation(ref drop_x, ref drop_y, 5))
                            {
                                if (Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, drop_x, drop_y) <= 5)
                                {
                                    Client.CurrentDMap.SetItemOnTile(drop_x, drop_y, true);

                                    item.DropperUID = Client.Hero.UID;
                                    item.X = drop_x;
                                    item.Y = drop_y;
                                    item.MapID = Client.Hero.MapID;
                                    item.CanPickup = true;
                                    item.DroppedTime = DateTime.Now;

                                    var GDrop = new ItemGroundPacket(item, GroundIDs.Drop, item.X, item.Y);
                                    Client.SendScreen(GDrop.Serialize(), true);

                                    if (!Kernel.GroundItems.ContainsKey(item.UID))
                                        Kernel.GroundItems.TryAdd(item.UID, item);

                                    if (!ConquerItem.IsItemType(item.ID, ItemType.Money))
                                        Client.RemoveInventory(item);

                                    return true;
                                }
                            }
                            return false;
                        }
					case EntityFlag.Monster:
						{
							var Monster = (Monster)Entity.Owner; 
							item.UID = ConquerItem.NextItemUID;
							if (Amount > 0)
								item.ID = ConquerItem.IdFromAmount(Money);

							ushort drop_x = Monster.Entity.X, drop_y = Monster.Entity.Y;
							if ((Monster.CurrentDMap != null) && Monster.CurrentDMap.FindValidDropLocation(ref drop_x, ref drop_y, 5))
							{
								Monster.CurrentDMap.SetItemOnTile(drop_x, drop_y, true);

								item.X = drop_x;
								item.Y = drop_y;
								item.MapID = Monster.Entity.MapID;
								item.DroppedTime = DateTime.Now;

								var GDrop = new ItemGroundPacket(item, GroundIDs.Drop, item.X, item.Y);
								PacketKernel.SendRangedPacket(item, GDrop.Serialize());

								if (!Kernel.GroundItems.ContainsKey(item.UID))
									Kernel.GroundItems.TryAdd(item.UID, item);

								return true;
							}
							return false;
						}
				}
				return false;
			}
			catch (Exception e)
			{
				LogHandler.WriteLine(e);
				return false;
			}
		}

	    public static void PickupItem(GameClient Client, ConquerItem Item)
		{
			var Name = MySqlDatabase.LoadItemString(Item.ID, "ItemName");
			if ((Client.Hero.X != Item.X) && (Client.Hero.Y != Item.Y))
				return;
			if (!Item.CanPickup && (Client.Hero.UID != Item.DropperUID))
			{
				if (Client.Team.Active)
				{
					var Owner = Item.Owner as GameClient;
					if (Owner == null)
						return;

					if (Owner.Team.IsLeader)
						Item.CanPickup = ConquerItem.IsItemType(Item.ID, ItemType.Money) ? Client.Team.MoneyPickup : Client.Team.ItemPickup;
				}
			    if (DateTime.Now <= Item.DroppedTime.AddSeconds(15))
			    {
			        Client.Speak(Color.Red, ChatType.TopLeft, "That item is not yours, please wait to pick it up.");
			        return;
			    }
			    Item.CanPickup = true;
			}

			if (Client.Hero.UID == Item.DropperUID || Item.CanPickup)
			{
				if (!ConquerItem.IsItemType(Item.ID, ItemType.Money))
				{
					Client.AddInventory(Item);
					if (Name != null)
						Client.Speak(Color.Red, ChatType.TopLeft, "You have got a(n) " + Name + ".");
				}
				else
				{
					Client.Silvers += Money;
					Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());
					Client.Speak(Color.Red, ChatType.TopLeft, string.Format("You have gained {0} silver(s).", Money));
				}
				Remove(Item);
			}
		}

	    public static void Remove(ConquerItem Item)
		{
			if (Kernel.GroundItems.TryGetValue(Item.UID, out Item))
			{
				var DMap = ConquerDMap.Maps[Item.MapID.ID];
				DMap?.SetItemOnTile(Item.X, Item.Y, false);

				Kernel.GroundItems.Remove(Item.UID);

				var GDrop = new ItemGroundPacket(Item, GroundIDs.Remove, Item.X, Item.Y);
				PacketKernel.SendRangedPacket(Item.MapID.ID, Item.X, Item.Y, GDrop.Serialize());
			}
		}
	}
}