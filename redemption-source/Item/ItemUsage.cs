﻿using System;
using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Events;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Item
{
	internal class ItemUsage
	{
	    public void Process(GameClient Client, uint ItemID)
		{
			var Item = Client.GetInventoryItem(ItemID);
			if (Item == null)
				return;

			switch (Item.ID)
			{
                #region Exp Potion
                case 723017:
                    {
                        Client.Hero.Stamps.ExpPotion = DateTime.Now.AddHours(2);

                        Client.Send(new StatTypePacket(Client.Hero.UID, (uint)Kernel.SecondsFromNow(Client.Hero.Stamps.ExpPotion), StatIDs.DoubleExpTime).Serialize());
                        Client.RemoveInventory(Item);
                        break;
                    }
                #endregion
                #region Bomb
                case 721261:
                    {
                        var rightGate = GuildWar.GuildMobs[GuildWar.RightGateUID];
                        var leftGate = GuildWar.GuildMobs[GuildWar.LeftGateUID];

                        if (Client.Hero.MapID != 1038)
                            return;

                        var Success = false;

                        // Let's blow those fucking hinges off
                        if (Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, rightGate.X, rightGate.Y) <= 5)
                        {
                            if (rightGate.Hitpoints <= 2500000)
                            {
                                rightGate.Hitpoints = 0;
                                rightGate.OnDie?.Invoke(Client, 0, 0);
                            }
                            else
                            {
                                rightGate.Hitpoints -= 2500000;
                            }
                            Success = true;
                        }
                        else if (Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, leftGate.X, leftGate.Y) <= 5)
                        {
                            if (leftGate.Hitpoints <= 2500000)
                            {
                                leftGate.Hitpoints = 0;
                                leftGate.OnDie?.Invoke(Client, 0, 0);
                            }
                            else
                            {
                                leftGate.Hitpoints -= 2500000;
                            }
                            Success = true;
                        }

                        if (Success)
                        {
                            var SPacket = new StringPacket(6);
                            SPacket.UID = Client.Hero.UID;
                            SPacket.ID = StringIds.Effect;
                            Client.SendScreen(Packets.StringPacket(SPacket, "change"), true);

                            Client.Hero.Die(null);
                            Client.RemoveInventory(Item);   
                        }
                        break;
                    }
                #endregion
                #region ExpBall
                case 723700:
                    // To-Do Limit?
                    if (Client.Hero.Level <= 130)
                    {
                        Client.RemoveInventory(Item);
                        Client.GainExpBall();
                    }
			        break;
                #endregion
                #region HeavensBlessing
                case 1200000:
					Client.RemoveInventory(Item);
					Client.AddBlessing(3);
					break;
				case 1200001:
					Client.RemoveInventory(Item);
					Client.AddBlessing(7);
					break;
				case 1200002:
					Client.RemoveInventory(Item);
					Client.AddBlessing(30);
					break;
				#endregion
				#region CPBags
				case 729910:
					Client.ConquerPoints += 1;
					Client.RemoveInventory(Item);
					break;
				case 729911:
					Client.ConquerPoints += 3;
					Client.RemoveInventory(Item);
					break;
				case 729912:
					Client.ConquerPoints += 6;
					Client.RemoveInventory(Item);
					break;
				#endregion
				#region MoneyBags
				case 723713:
					Client.Silvers += 300000;
					Client.RemoveInventory(Item);
					Client.Speak(Color.Red, ChatType.Talk, "300,000 silvers have been added.");
					break;
				case 723714:
					Client.Silvers += 800000;
					Client.RemoveInventory(Item);
					Client.Speak(Color.Red, ChatType.Talk, "800,000 silvers have been added.");
					break;
				case 723715:
					Client.Silvers += 1200000;
					Client.RemoveInventory(Item);
					Client.Speak(Color.Red, ChatType.Talk, "1,200,000 silvers have been added.");
					break;
				case 723716:
					Client.Silvers += 1800000;
					Client.RemoveInventory(Item);
					Client.Speak(Color.Red, ChatType.Talk, "1,800,000 silvers have been added.");
					break;
				case 723717:
					Client.Silvers += 20000000;
					Client.RemoveInventory(Item);
					Client.Speak(Color.Red, ChatType.Talk, "20,000,000 silvers have been added.");
					break;
				case 723719:
					Client.Silvers += 25000000;
					Client.RemoveInventory(Item);
					Client.Speak(Color.Red, ChatType.Talk, "25,000,000 silvers have been added.");
					break;
				case 723720:
					Client.Silvers += 80000000;
					Client.RemoveInventory(Item);
					Client.Speak(Color.Red, ChatType.Talk, "80,000,000 silvers have been added.");
					break;
				case 723721:
					Client.Silvers += 100000000;
					Client.RemoveInventory(Item);
					Client.Speak(Color.Red, ChatType.Talk, "100,000,000 silvers have been added.");
					break;
				case 723722:
					Client.Silvers += 300000000;
					Client.RemoveInventory(Item);
					Client.Speak(Color.Red, ChatType.Talk, "300,000,000 silvers have been added.");
					break;
				case 723723:
					Client.Silvers += 500000000;
					Client.RemoveInventory(Item);
					Client.Speak(Color.Red, ChatType.Talk, "500,000,000 silvers have been added.");
					break;
				#endregion
				#region Skill Books
				case 725018:
					if (LearnSkill(Client, 1380))
						Client.RemoveInventory(Item);
					break;
				case 725019:
					if (LearnSkill(Client, 1385))
						Client.RemoveInventory(Item);
					break;
				case 725020:
					if (LearnSkill(Client, 1390))
						Client.RemoveInventory(Item);
					break;
				case 725021:
					if (LearnSkill(Client, 1395))
						Client.RemoveInventory(Item);
					break;
				case 725022:
					if (LearnSkill(Client, 1400))
						Client.RemoveInventory(Item);
					break;
				case 725023:
					if (LearnSkill(Client, 1405))
						Client.RemoveInventory(Item);
					break;
				case 725024:
					if (LearnSkill(Client, 1410))
						Client.RemoveInventory(Item);
					break;
				case 725000:
					{
						if (Client.Spirit >= 20)
							if (LearnSkill(Client, 1000))
								Client.RemoveInventory(Item);
						break;
					}
				case 725001:
					{
						if ((Client.Job >= 130) && (Client.Job <= 145) && (Client.Spirit >= 80))
							if (LearnSkill(Client, 1001))
								Client.RemoveInventory(Item);
						break;
					}
				case 725002:
					{
						if ((Client.Spirit >= 160) && (Client.Job >= 140) && (Client.Job <= 145))
							if (LearnSkill(Client, 1002))
								Client.RemoveInventory(Item);
						break;
					}
				case 725003:
					{
						if (Client.Spirit >= 30)
							if (LearnSkill(Client, 1005))
								Client.RemoveInventory(Item);
						break;
					}
				case 725004:
					{
						if (Client.Spirit >= 25)
							if (LearnSkill(Client, 1010))
								Client.RemoveInventory(Item);
						break;
					}
				case 725005:
					{
						if (Client.Hero.Level >= 40)
							if (LearnSkill(Client, 1045))
								Client.RemoveInventory(Item);
						break;
					}
				case 725010:
					{
						if (Client.Hero.Level >= 40)
							if (LearnSkill(Client, 1046))
								Client.RemoveInventory(Item);
						break;
					}
				case 725011:
					{
						if (LearnSkill(Client, 1250))
							Client.RemoveInventory(Item);
						break;
					}
				case 725012:
					{
						if (LearnSkill(Client, 1260))
							Client.RemoveInventory(Item);
						break;
					}
				case 725013:
					{
						if (LearnSkill(Client, 1290))
							Client.RemoveInventory(Item);
						break;
					}
				case 725014:
					{
						if (LearnSkill(Client, 1300))
							Client.RemoveInventory(Item);
						break;
					}
				case 725015:
					{
						if ((Client.Job >= 130) && (Client.Job <= 135))
							if (LearnSkill(Client, 1350))
								Client.RemoveInventory(Item);
						break;
					}
				case 725016:
					{
						if (LearnSkill(Client, 1360))
							Client.RemoveInventory(Item);
						break;
					}
				case 725025:
					{
						if (LearnSkill(Client, 1320))
							Client.RemoveInventory(Item);
						break;
					}
				case 725026:
					{
						if (LearnSkill(Client, 5010))
							Client.RemoveInventory(Item);
						break;
					}
				case 725027:
					{
						if (LearnSkill(Client, 5020))
							Client.RemoveInventory(Item);
						break;
					}
				case 725028:
					{
						if ((Client.Job >= 130) && (Client.Job <= 145) && (Client.Hero.Level >= 70))
							if (LearnSkill(Client, 5001))
								Client.RemoveInventory(Item);
						break;
					}
				case 725029:
					{
						if (LearnSkill(Client, 5030))
							Client.RemoveInventory(Item);
						break;
					}
				case 725030:
					{
						if (LearnSkill(Client, 5040))
							Client.RemoveInventory(Item);
						break;
					}
				case 725031:
					{
						if (LearnSkill(Client, 5050))
							Client.RemoveInventory(Item);
						break;
					}
				case 725040:
					{
						if (LearnSkill(Client, 7000))
							Client.RemoveInventory(Item);
						break;
					}
				case 725041:
					{
						if (LearnSkill(Client, 7010))
							Client.RemoveInventory(Item);
						break;
					}
				case 725042:
					{
						if (LearnSkill(Client, 7020))
							Client.RemoveInventory(Item);
						break;
					}
				case 725043:
					{
						if (LearnSkill(Client, 7030))
							Client.RemoveInventory(Item);
						break;
					}
				case 725044:
					{
						if (LearnSkill(Client, 7040))
							Client.RemoveInventory(Item);
						break;
					}
				case 1060100:
					{
						if ((Client.Job >= 135) && (Client.Job <= 145) && (Client.Hero.Level >= 82))
							if (LearnSkill(Client, 1160))
								Client.RemoveInventory(Item);
						break;
					}
				case 1060101:
					{
						if ((Client.Job >= 135) && (Client.Job <= 145) && (Client.Hero.Level >= 84))
							if (LearnSkill(Client, 1165))
								Client.RemoveInventory(Item);
						break;
					}
				#endregion
				#region Meteor and DBScroll
				case 720027:
					Client.OpenScroll(Item);
					break;
				case 720028:
					Client.OpenScroll(Item, false);
					break;
				#endregion
				#region Potion Boxes
				case 720010:
				case 720011:
				case 720012:
				case 720013:
				case 720014:
				case 720015:
				case 720016:
				case 720017:
					{
						if (Client.Inventory.Length > 37)
						{
							Client.Speak(Color.Turquoise, ChatType.TopLeft, "No enough room in Inventory.");
						}
						else
						{
							var BoxType = byte.Parse(Item.ID.ToString()[5].ToString());
							Client.AddInventory(PotionBoxes[BoxType]);
							Client.RemoveInventory(Item);
						}
						break;
					}
				#endregion
				#region HairDyes
				case 1060030:
				case 1060040:
				case 1060050:
				case 1060060:
				case 1060070:
				case 1060080:
				case 1060090:
					var ID = HairDies[byte.Parse(Item.ID.ToString()[5].ToString()) - 3];
					Client.Hero.Hairstyle = (ushort)(Client.Hero.Hairstyle % 100 + ID * 100);
					Client.SendScreen(new StatTypePacket(Client.Hero.UID, Client.Hero.Hairstyle, StatIDs.Hairstyle).Serialize(), true);
					Client.RemoveInventory(Item);
					break;
				#endregion
				#region Scrolls
				#region TwinCityGate
				case 1060020:
					Client.Teleport(1002, 430, 380);
					Client.RemoveInventory(Item);
					break;
				#endregion
				#region PhoenixCastleGate
				case 1060023:
					Client.Teleport(1011, 193, 266);
					Client.RemoveInventory(Item);
					break;
				#endregion
				#region ApeCityGate
				case 1060022:
					Client.Teleport(1020, 566, 565);
					Client.RemoveInventory(Item);
					break;
				#endregion
				#region DesertCityGate
				case 1060021:
					Client.Teleport(1000, 496, 650);
					Client.RemoveInventory(Item);
					break;
				#endregion
				#region BirdIslandGate
				case 1060024:
					Client.Teleport(1015, 717, 577);
					Client.RemoveInventory(Item);
					break;
				#endregion
				#region StoneCityGate
				case 1060102:
					Client.Teleport(1213, 448, 272);
					Client.RemoveInventory(Item);
					break;
				#endregion
				#endregion
				#region MP Pots
				#region Agrypnotic
				case 1001000:
					{
						MPPotion(70, Item, Client);
						break;
					}
				#endregion
				#region Tonic
				case 1001010:
					{
						MPPotion(200, Item, Client);
						break;
					}
				#endregion
				#region PearlOintment
				case 1001020:
					{
						MPPotion(450, Item, Client);
						break;
					}
				#endregion
				#region RecoveryPill
				case 1001030:
					{
						MPPotion(1000, Item, Client);
						break;
					}
				#endregion
				#region SoulPill
				case 1001040:
					{
						MPPotion(2000, Item, Client);
						break;
					}
				#endregion
				#region RefreshingPill
				case 1002030:
					{
						MPPotion(3000, Item, Client);
						break;
					}
				#endregion
				#region ChantPill
				case 1002040:
					{
						MPPotion(4500, Item, Client);
						break;
					}
				#endregion
				#endregion
				#region HP Pots
				#region Amrita
				case 1000030:
					{
						HPPotion(500, Item, Client);
						break;
					}
				#endregion
				#region Painkiller
				case 1000020:
					{
						HPPotion(250, Item, Client);
						break;
					}
				#endregion
				#region Resolutive
				case 1000010:
					{
						HPPotion(100, Item, Client);
						break;
					}
				#endregion
				#region Panacea
				case 1002000:
					{
						HPPotion(800, Item, Client);
						break;
					}
				#endregion
				#region Ginseng
				case 1002010:
					{
						HPPotion(1200, Item, Client);
						break;
					}
				#endregion
				#region Stancher
				case 1000000:
					{
						HPPotion(70, Item, Client);
						break;
					}
				#endregion
				#region Vanilla Pot
				case 1002020:
					{
						HPPotion(2000, Item, Client);
						break;
					}
				#endregion
				#region Mil. Ginseng
				case 1002050:
					{
						HPPotion(3000, Item, Client);
						break;
					}
				#endregion
				#endregion
				#region Fireworks
				case 720030:
					{
						Firework("firework-like", Item, Client);
						break;
					}
				case 720031:
					{
						Firework("firework-1love", Item, Client);
						break;
					}
				case 720032:
					{
						Firework("firework-2love", Item, Client);
						break;
					}
				#endregion
				#region Misc
				case 723727: // Penitence Amulet
					{
						if (Client.Hero.PkPoints <= 29)
						{
							Client.Speak(Color.Red, ChatType.Talk, "You must have more then 30 PKPoints to use this item.");
							return;
						}
						Client.Hero.PkPoints -= 30;
						Client.Hero.SetNameColor();
						Client.RemoveInventory(Item);
						break;
					}
				case 723712: // +1StonePack
					{
						if (Client.Inventory.Length > 35)
						{
							Client.Speak(Color.Red, ChatType.Talk, "You must free up 5 spaces in your inventory first.");
							return;
						}
						Client.AddItems(730001, 5);
						Client.RemoveInventory(Item);
						break;
					}
				case 723711: // MetTearPack
					{
						if (Client.Inventory.Length > 35)
						{
							Client.Speak(Color.Red, ChatType.Talk, "You must free up 5 spaces in your inventory first.");
							return;
						}
						Client.AddItems(1088002, 5);
						Client.RemoveInventory(Item);
						break;
					}
				#endregion
				#region Default
				default:
					Client.Speak(Color.Red, ChatType.TopLeft, Item.ID + " is a Invalid item, it doesn't have a use yet.");
					break;
					#endregion
			}
		}

	    #region Item Variables
	    private static readonly byte[] HairDies = { 3, 9, 8, 7, 6, 5, 4 };
	    private static readonly uint[] PotionBoxes = { 1000030, 1002000, 1002010, 1002020, 1001030, 1001040, 1002030, 1002040 };
	    #endregion
	    #region Item Functions
	    private static bool LearnSkill(GameClient Client, ushort skillID)
		{
			if (Client.Spells.ContainsKey(skillID))
			{
				Client.Speak(Color.Red, ChatType.TopLeft, "You've already learned this skill.");
				return false;
			}
			ConquerSpell.LearnSpell(Client, skillID, 0);
			return true;
		}

	    public void HPPotion(ushort HitPoints, IConquerItem Item, GameClient Client)
		{
			if (Client.Hero.Hitpoints >= Client.Hero.MaxHitpoints)
			{
				Client.Speak(Color.Red, ChatType.TopLeft, "Your health is full, don't waste your pots.");
				return;
			}
			Client.RemoveInventory(Item);
			Client.Hero.Hitpoints += HitPoints;
			Client.Send(new StatTypePacket(Client.Hero.UID, (uint)Client.Hero.Hitpoints, StatIDs.Hitpoints).Serialize());
			Client.Hero.Stamps.Equip = DateTime.Now.AddMilliseconds(750);
		}

	    public void MPPotion(ushort ManaPoints, IConquerItem Item, GameClient Client)
		{
			if (Client.Mana >= Client.MaxMana)
			{
				Client.Speak(Color.Red, ChatType.TopLeft, "Your health is full, don't waste your pots.");
				return;
			}
			Client.RemoveInventory(Item);
			Client.Mana += ManaPoints;
			Client.Send(new StatTypePacket(Client.Hero.UID, Client.Mana, StatIDs.Mana).Serialize());
			Client.Hero.Stamps.Equip = DateTime.Now.AddMilliseconds(750);
		}

	    public void Firework(string Effect, IConquerItem Item, GameClient Client)
		{
			if (Client.Stamina >= 25)
			{
				Client.Stamina -= 25;
				Client.Send(new StatTypePacket(Client.Hero.UID, Client.Stamina, StatIDs.Stamina).Serialize());

				Client.RemoveInventory(Item);

				var SPacket = new StringPacket((byte)Effect.Length);
				SPacket.UID = Client.Hero.UID;
				SPacket.ID = StringIds.Effect;
				Client.SendScreen(Packets.StringPacket(SPacket, Effect), true);
			}
			else
			{
				Client.Speak(Color.Turquoise, ChatType.TopLeft, "No enough stamina.");
			}
		}
	    #endregion
	}
}