﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RedemptionCO.Structures.Interfaces;
using RedemptionCO.Structures.Instances;
using RedemptionCO.Structures.Client;
using RedemptionCO.Structures.Main;
using RedemptionCO.Network.Packets;
using System.Drawing;
using RedemptionCO.Network.Packets.Structures;

namespace RedemptionCO.Structures.Item
{
    enum GroundIDs : byte
    {
        Drop = 0x01,
        Pickup = 0x02
    }

    unsafe class GroundItem : IClassPacket, IMapObject
    {
        private byte[] Buffer;
        private ushort m_MapID;

        public bool Pickup = false;

        public uint Money = 0;
        public uint Dropper = 0;

        public DateTime Dropped;

        public GroundItem()
        {
            m_MapID = 0;
            Buffer = new byte[0x14];
            fixed (byte* Packet = Buffer)
            {
                *(ushort*)(Packet + 0) = (ushort)Buffer.Length;
                *(ushort*)(Packet + 2) = 0x44D;
                *(uint*)(Packet + 17) = Stamps.timeGetTime();
            }
        }

        public uint UID
        {
            get
            {
                fixed (byte* Packet = Buffer)
                    return *(uint*)(Packet + 4);
            }
            set
            {
                fixed (byte* Packet = Buffer)
                    *(uint*)(Packet + 4) = value;
            }
        }
        public uint ID
        {
            get
            {
                fixed (byte* Packet = Buffer)
                    return *(uint*)(Packet + 8);
            }
            set
            {
                fixed (byte* Packet = Buffer)
                    *(uint*)(Packet + 8) = value;
            }
        }
        public ushort X
        {
            get
            {
                fixed (byte* Packet = Buffer)
                    return *(ushort*)(Packet + 12);
            }
            set
            {
                fixed (byte* Packet = Buffer)
                    *(ushort*)(Packet + 12) = value;
            }
        }
        public ushort Y
        {
            get
            {
                fixed (byte* Packet = Buffer)
                    return *(ushort*)(Packet + 14);
            }
            set
            {
                fixed (byte* Packet = Buffer)
                    *(ushort*)(Packet + 14) = value;
            }
        }
        public GroundIDs Flag
        {
            get
            {
                fixed (byte* Packet = Buffer)
                    return *(GroundIDs*)(Packet + 16);
            }
            set
            {
                fixed (byte* Packet = Buffer)
                    *(GroundIDs*)(Packet + 16) = value;
            }
        }

        #region IClassPacket Members

        public void Deserialize(byte[] Bytes) { Buffer = Bytes; }
        public byte[] Serialize() { return Buffer; }

        #endregion

        #region IMapObject Members

        public ushort MapID
        {
            get { return m_MapID; }
            set { m_MapID = value; }
        }

        public object Owner { get { return this; } }
        public MapObjectType MapObjType { get { return MapObjectType.Item; } }

        public void SendSpawn(GameClient Client)
        {
            SendSpawn(Client, false);
        }
        public void SendSpawn(GameClient Client, bool IgnoreScreen)
        {
            if (Client.Screen.Add(this) || IgnoreScreen)
                Client.Send(Buffer);
        }

        #endregion

        public void Drop(GameClient Client, bool _Pickup, uint Amount)
        {
            try
            {
                Pickup = _Pickup;
                this.Money = Amount;
                decimal range = decimal.Divide(3, 2);
                for (int x = (int)(0 - range); x < 0 + range; x++)
                {
                    for (int y = (int)(0 - range); y < 0 + range; y++)
                    {
                        if (GroundOpen(Client.Entity.MapID, (ushort)(Client.Entity.X + x), (ushort)(Client.Entity.Y + y)))
                        {
                            Dictionary<uint, GroundItem> Items;
                            if (Kernel.GroundItems.TryGetValue(Client.Entity.MapID, out Items))
                            {
                                if (Items.ContainsKey(UID)) return;
                                Dropper = Client.Entity.UID;
                                X = (ushort)(Client.Entity.X + x);
                                Y = (ushort)(Client.Entity.Y + y);
                                Flag = GroundIDs.Drop;
                                MapID = Client.Entity.MapID;
                                Dropped = DateTime.Now;

                                PacketKernel.SendGlobalPacket(this.Serialize());
                                Items.Add(this.UID, this);

                                if (!ConquerItem.IsType(this.ID, ItemType.Money))
                                    Client.RemoveInventory(this.UID);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine("Failed to drop item!");
                return;
            }
        }
        public void PickupItem(GameClient Client)
        {
            string Name = Database.LoadItemString(this.ID, "ItemName");
            if (Client.Entity.UID == this.Dropper || this.Pickup)
            {
                if (!ConquerItem.IsType(this.ID, ItemType.Money))
                {
                    Client.AddInventory(this as IConquerItem);
                    if (Name != null)
                        Client.Speak(Color.Red, ChatType.TopLeft, "You have got a(n) " + Name + ".");
                }
                else
                {
                    Client.Money += (int)this.Money;
                    Client.Send(new StatTypePacket(Client.Entity.UID, (uint)Client.Money, StatIDs.Money).Serialize());
                    Client.Speak(Color.Red, ChatType.TopLeft, string.Format("You have gained {0} silver(s).", this.Money));
                }
                this.Flag = GroundIDs.Pickup;
                PacketKernel.SendGlobalPacket(this.Serialize());
            }
            else
            {
                if (this.Dropped >= DateTime.Now)
                {
                    if (!ConquerItem.IsType(this.ID, ItemType.Money))
                    {
                        Client.AddInventory(this as IConquerItem);
                        if (Name != null)
                            Client.Speak(Color.Red, ChatType.TopLeft, "You have got a(n) " + Name + ".");
                    }
                    else
                    {
                        Client.Money += (int)this.Money;
                        Client.Send(new StatTypePacket(Client.Entity.UID, (uint)Client.Money, StatIDs.Money).Serialize());
                        Client.Speak(Color.Red, ChatType.TopLeft, string.Format("You have gained {0} silver(s).", this.Money));
                    }
                    this.Flag = GroundIDs.Pickup;
                    PacketKernel.SendGlobalPacket(this.Serialize());
                }
                else
                    Client.Speak(Color.Red, ChatType.TopLeft, "That item is not yours, please wait to pick it up.");
            }
        }

        public static bool GroundOpen(ushort MapID, ushort X, ushort Y)
        {
            Dictionary<uint, GroundItem> Items;
            if (Kernel.GroundItems.TryGetValue(MapID, out Items))
            {
                foreach (KeyValuePair<uint, GroundItem> DE in Items)
                {
                    GroundItem Item = DE.Value;
                    if (Item.X == X && Item.Y == Y)
                        return false;
                }
                return true;
            }
            else
                Kernel.GroundItems.Add(MapID, new Dictionary<uint, GroundItem>());
            return false;
        }
    }
}
