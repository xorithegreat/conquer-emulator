﻿using System.Collections.Generic;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Item
{
	public class Warehouse
	{
	    public int Money;
	    public sbyte PasswordTries;
	    public string TmpPass, Password;
	    public bool Validated;

	    public Dictionary<uint, Dictionary<uint, IConquerItem>> Warehouses;

	    public Warehouse()
		{
			PasswordTries = 0;
			Password = "";

			Money = 0;

			Warehouses = new Dictionary<uint, Dictionary<uint, IConquerItem>>
				             {
						             { (uint)WhIDs.Market, new Dictionary<uint, IConquerItem>() },
						             { (uint)WhIDs.Mobile, new Dictionary<uint, IConquerItem>() },
						             { (uint)WhIDs.TwinCity, new Dictionary<uint, IConquerItem>() },
						             { (uint)WhIDs.Phoenix, new Dictionary<uint, IConquerItem>() },
						             { (uint)WhIDs.Desert, new Dictionary<uint, IConquerItem>() },
						             { (uint)WhIDs.Bird, new Dictionary<uint, IConquerItem>() },
						             { (uint)WhIDs.Ape, new Dictionary<uint, IConquerItem>() }
				             };
		}

	    public static IConquerItem GetItem(uint UID, Dictionary<uint, IConquerItem> Items)
		{
			IConquerItem item;
			Items.TryGetValue(UID, out item);
			return item;
		}
	}
}