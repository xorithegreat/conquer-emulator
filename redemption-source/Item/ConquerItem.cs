﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Item
{
	#region Enums
	public enum ItemPosition : ushort
	{
		Inventory = 0,
		Headgear = 1,
		Necklace = 2,
		Armor = 3,
		Right = 4,
		Left = 5,
		Ring = 6,
		Bottle = 7,
		Boots = 8,
		Garment = 9
	}

	public enum ItemType : ushort
	{
		None = 0,
		Money = 109,
		Blade = 410,
		Sword = 420,
		Backsword = 421,
		Hook = 430,
		Whip = 440,
		Mace = 441,
		Axe = 450,
		Hammer = 460,
		Club = 480,
		Scepter = 481,
		Dagger = 490,
		Bow = 500,
		Glaive = 510,
		Poleaxe = 530,
		Longhammer = 540,
		Spear = 560,
		Wand = 561,
		Pickaxe = 562,
		Halberd = 580,
		GemId = 700,
		Arrow = 1050,
		Shield = 900
	}

	public enum ItemSort
	{
		Armor = 3,
		BowCoat = 0x11,
		DamageArtifact = 12,
		Expendable = 0,
		Helmet = 1,
		HorseWhip = 0x15,
		Invalid = -1,
		Mount = 14,
		MountDecorator = 20,
		Necklace = 2,
		Other = 9,
		Overcoat = 11,
		RingL = 10,
		RingR = 7,
		Shield = 6,
		ShieldCoat = 0x12,
		Shoes = 8,
		Unknown13 = 13,
		Weapon1 = 4,
		Weapon1Coat = 15,
		Weapon2 = 5,
		Weapon2Coat = 0x10
	}

	public enum ItemStatus
	{
		None = 0,
		AddOrUpdate = 1,
		Deleted = 2
	}

	public enum ItemMode : ushort
	{
		Default = 0x01,
		Trade = 0x02,
		View = 0x04
	}
	#endregion
	public unsafe class ConquerItem : IClassPacket, IMapObject, IConquerItem
	{
	    public const ItemPosition FirstSlot = ItemPosition.Headgear;
	    public const ItemPosition LastSlot = ItemPosition.Garment;
	    private static readonly ThreadSafeCounter IDCounter;
	    public bool CanPickup;
	    public DateTime DroppedTime;

	    // Used for FloorItems
	    public uint DropperUID;

	    private byte[] Packet;
	    public DateTime UnlockTime;

	    static ConquerItem()
		{
			IDCounter = new ThreadSafeCounter(MySqlDatabase.GetLastItemId(), int.MaxValue);
		}

	    public ConquerItem(bool CreateInstance)
		{
			if (CreateInstance)
			{
				Packet = new byte[44 + 8];
				fixed (byte* Buffer = Packet)
				{
					*(ushort*)(Buffer + 0) = (ushort)(Packet.Length - 8);
					*(ushort*)(Buffer + 2) = 0x3F0;
				}
				Color = 3;
				Mode = (ushort)ItemMode.Default;
			}
		}

	    public static uint NextItemUID => (uint)IDCounter.Counter;

	    public ushort Mode
		{
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(ushort*)(Buffer + 16);
				}
			}
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(ushort*)(Buffer + 16) = value;
				}
			}
		}

	    public ushort Effect
		{
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(ushort*)(Buffer + 26);
				}
			}
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(ushort*)(Buffer + 26) = value;
				}
			}
		}

	    public ushort CloudSaintID
        {
            get => MaxDurability;
	        set => MaxDurability = value;
	    }

	    public ushort CloudSaintKillsRequired
        {
            get => Durability;
	        set => Durability = value;
	    }

	    public ItemStatus Status { get; set; }

	    public uint ID
		{
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(uint*)(Buffer + 8);
				}
			}
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(uint*)(Buffer + 8) = value;
				}
			}
		}

	    public ushort Durability
		{
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(ushort*)(Buffer + 12);
				}
			}
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(ushort*)(Buffer + 12) = value;
				}
			}
		}

	    public ushort MaxDurability
		{
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(ushort*)(Buffer + 14);
				}
			}
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(ushort*)(Buffer + 14) = value;
				}
			}
		}

	    public ItemPosition Position
		{
			get => (ItemPosition)Packet[18];
	        set => Packet[18] = (byte)value;
	    }

	    public byte SocketOne
		{
			get => Packet[24];
	        set => Packet[24] = value;
	    }

	    public byte SocketTwo
		{
			get => Packet[25];
	        set => Packet[25] = value;
	    }

	    public byte Plus
		{
			get => Packet[28];
	        set => Packet[28] = value;
	    }

	    public byte Bless
		{
			get => Packet[29];
	        set => Packet[29] = value;
	    }

	    public byte Enchant
		{
			get => Packet[30];
	        set => Packet[30] = value;
	    }

	    public byte Locked
		{
			get => Packet[38];
	        set => Packet[38] = value;
	    }

	    public byte Color
		{
			get => Packet[40];
	        set => Packet[40] = value;
	    }
	    //31 = restrain.ini string

	    public ushort Arrows { get => Durability;
	        set => Durability = value;
	    }

	    // TODO: limit item lock to 3 days per item
	    public bool Unlocking { get; set; }

	    public void Send(GameClient Client)
		{
			Client.Send(Packet);
		}

	    public void SendInventoryUpdate(GameClient Client)
		{
			var remove = ItemUsagePacket.Create();
			remove.ID = ItemUsageID.RemoveInventory;
			remove.UID = UID;
			Client.Send(remove, remove.Size);

			Status = ItemStatus.AddOrUpdate;
			Position = (ushort)ItemPosition.Inventory;
			Send(Client);
		}

	    public uint UID
		{
			get
			{
				fixed (byte* Buffer = Packet)
				{
					return *(uint*)(Buffer + 4);
				}
			}
			set
			{
				fixed (byte* Buffer = Packet)
				{
					*(uint*)(Buffer + 4) = value;
				}
			}
		}

	    public static uint TryGetUpgradeId(uint ID)
		{
			var ItemType = (byte)Convert.ToInt32(Math.Floor(ID * 0.0001));
			byte Increment = 10;
			if ((ItemType == 12) || (ItemType == 20) || (ItemType == 20))
				Increment = 20;
			while (MySqlDatabase.LoadItemString(ID + Increment, "ItemID") == "0")
				Increment += 10;
			return ID + Increment;
		}

	    public static void DowngradeItem(ConquerItem Item)
		{
			if (Item == null)
				return;

			var newLvl = 0;
			var itemId = Item.ID / 1000;

			// Headgear(Helmets, Hats, Caps, Earrings, Coronets), Necklaces/Bags, Armors(Troj, Warr, Archer, Tao)
			uint[] Type1 = { 111, 113, 114, 117, 118, 120, 121, 130, 131, 133, 134 };
			// Rings/Bracelets, Bows and Boots
			uint[] Type2 = { 150, 151, 152, 500, 160 };
			// Blade, Sword, Backsword, Hook, Whip, Axe, Hammer, Club, Dagger, Glaive, PoleAxe, LongHammer Spear, Wand
			uint[] Type3 = { 410, 420, 421, 430, 440, 450, 460, 480, 490, 510, 530, 540, 560, 561 };

			if (Type1.Contains(itemId))
				newLvl = 0;
			else if (Type2.Contains(itemId))
				newLvl = 1;
			else if (Type3.Contains(itemId))
				newLvl = 3;

			Item.ID = (uint)(Item.ID / 1000 * 1000 + newLvl * 10 + Item.ID % 10);
		}

	    public static int GetGemBlessWorth(uint ItemID)
		{
			var high = 1;
			var low = 0;
			if (ItemID % 10 == 1) // refined
			{
				high = 59;
				low = 1;
			}
			else
			{
				switch (ItemID)
				{
					/* unique */
					// dragon
					case 700012: high = 159; low = 100; break;
					// phoenix, moon, violet
					case 700002:
					case 700062:
					case 700052: high = 109; low = 60; break;
					// rainbow
					case 700032: high = 129; low = 80; break;
					// tortoise, kylin, fury
					case 700072:
					case 700042:
					case 700022: high = 89; low = 40; break;
					/* super */
					// dragon
					case 700013: high = 255; low = 200; break;
					// phoenix, tortoise, rainbow
					case 700003:
					case 700073:
					case 700033: high = 229; low = 170; break;
					// moon, violet
					case 700063:
					case 700053: high = 199; low = 140; break;
					// fury
					case 700023: high = 149; low = 90; break;
					// kylin
					case 700043: high = 119; low = 70; break;
				}
			}
			return RandomGenerator.Generator.Next(low, high);
		}

	    public static bool GetUpLevelInfo(IConquerItem Item, out double Chance)
		{
			var Type = (byte)(Item.ID / 10000);
			if ((Type == 11) || (Type == 13) || (Type == 90)) //Head || Armor || Shield
			{
				switch ((int)(Item.ID % 100 / 10))
				{
					case 5: Chance = 50.00; break;
					case 6: Chance = 40.00; break;
					case 7: Chance = 30.00; break;
					case 8: Chance = 20.00; break;
					case 9: Chance = 15.00; break;
					default: Chance = 500.00; break;
				}

				switch (Item.ID % 10)
				{
					case 6: Chance = Chance * 0.90; break;
					case 7: Chance = Chance * 0.70; break;
					case 8: Chance = Chance * 0.30; break;
					case 9: Chance = Chance * 0.10; break;
				}
			}
			else
			{
				switch ((int)(Item.ID % 1000 / 10))
				{
					case 11: Chance = 95.00; break;
					case 12: Chance = 90.00; break;
					case 13: Chance = 85.00; break;
					case 14: Chance = 80.00; break;
					case 15: Chance = 75.00; break;
					case 16: Chance = 70.00; break;
					case 17: Chance = 65.00; break;
					case 18: Chance = 60.00; break;
					case 19: Chance = 55.00; break;
					case 20: Chance = 50.00; break;
					case 21: Chance = 45.00; break;
					case 22: Chance = 40.00; break;
					default: Chance = 500.00; break;
				}

				switch (Item.ID % 10)
				{
					case 6: Chance = Chance * 0.90; break;
					case 7: Chance = Chance * 0.70; break;
					case 8: Chance = Chance * 0.30; break;
					case 9: Chance = Chance * 0.10; break;
				}
			}
			return true;
		}

	    public static bool GetUpQualityInfo(IConquerItem Item, out double Chance)
		{
			var standerd = new StanderdItemStats(Item.ID);

			Chance = 0;

			// NewYearCoat, GMRobe, etc 
			if (Item.ID / 1000 == 137)
				return false;

			// Love Forever, Love Forever, LoveForever, LuckyBlade, LuckyBacksword, LuckyBow
			if ((Item.ID == 150000) || (Item.ID == 150310) || (Item.ID == 150320) || (Item.ID == 410301) || (Item.ID == 421301) || (Item.ID == 500301))
				return false;

			switch (Item.ID % 10)
			{
				case 6: Chance = 50.00; break;
				case 7: Chance = 33.00; break;
				case 8: Chance = 20.00; break;
				default: Chance = 100.00; break;
			}

			double Factor = standerd.ReqLvl;
			if (Factor > 70)
				Chance = Chance * (100.00 - (Factor - 70.00)) / 100.00;
			return true;
		}

	    public static uint CalcRepairMoney(IConquerItem Item)
		{
			var standerd = new StanderdItemStats();

			var RecoverDurability = Math.Max(0, Item.MaxDurability - Item.Durability);
			if (RecoverDurability == 0)
				return 0;

			double RepairCost = 0;
			if (standerd.MoneyPrice > 0)
				RepairCost = (double)standerd.MoneyPrice * RecoverDurability / Item.MaxDurability;

			switch (Item.ID % 10)
			{
				case 9: RepairCost *= 1.125; break;
				case 8: RepairCost *= 0.975; break;
				case 7: RepairCost *= 0.900; break;
				case 6: RepairCost *= 0.825; break;
				default: RepairCost *= 0.750; break;
			}
			return (uint)Math.Max(1, (int)Math.Floor(RepairCost));
		}

	    #region IMapObject Members
	    public Map MapID { get; set; }
	    public ushort X { get; set; }
	    public ushort Y { get; set; }
	    public MapObjectType MapObjType => MapObjectType.Item;
	    public object Owner => this;

	    public void SendSpawn(GameClient Client, bool Ignore)
		{
			if (Client.Screen.Add(this) || Ignore)
			{
				var GDrop = new ItemGroundPacket(this, GroundIDs.Drop, X, Y);
				Client.Send(GDrop.Serialize());
			}
		}

	    public void SendSpawn(GameClient Client)
		{
			SendSpawn(Client, false);
		}
	    #endregion
	    #region IClassPacket Members
	    public void Deserialize(byte[] Bytes)
		{
			Packet = Bytes;
		}

	    public byte[] Serialize()
		{
			return Packet;
		}
	    #endregion
	    #region Loading
	    public static bool Parse(string Item, out IConquerItem ItemData)
		{
			ItemData = new ConquerItem(false);
			if (Item == "")
				return false;
			ItemData = new ConquerItem(true);
			try
			{
				var Info = Item.Split('~');
				ItemData.ID = uint.Parse(Info[0]);
				ItemData.Plus = byte.Parse(Info[1]);
				ItemData.Bless = byte.Parse(Info[2]);
				ItemData.Enchant = byte.Parse(Info[3]);
				ItemData.SocketOne = byte.Parse(Info[4]);
				ItemData.SocketTwo = byte.Parse(Info[5]);
				ItemData.Durability = ushort.Parse(Info[6]);
				ItemData.MaxDurability = ushort.Parse(Info[7]);
				ItemData.Position = (ItemPosition)byte.Parse(Info[8]);
				ItemData.Color = byte.Parse(Info[9]);
				ItemData.Locked = byte.Parse(Info[10]);
				ItemData.UID = NextItemUID;
				return true;
			}
			catch
			{
				return false;
			}
		}

	    public override string ToString()
		{
			return ID + "~" + Plus + "~" + Bless + "~" + Enchant + "~" + SocketOne + "~" + SocketTwo + "~" + Durability + "~" + MaxDurability + "~" + (byte)Position + "~" + Color + "~" + Locked;
		}

	    public static Dictionary<uint, IConquerItem> StringToDictionary(string Hash)
		{
			var retn = new Dictionary<uint, IConquerItem>();
			foreach (var str in Hash.Split('-').Where(str => (str.Trim() != "") && (str != "0")))
			{
				IConquerItem i;
				if (Parse(str, out i))
					retn.Add(i.UID, i);
			}
			return retn;
		}

	    public static string DictionaryToString(Dictionary<uint, IConquerItem> Hash)
		{
			try
			{
				if (Hash.Count == 0)
					return "";
				var ret = Hash.Aggregate("", (current, DE) => current + "-" + DE.Value.ToString() + "-");
				ret = ret.Replace("--", "-");
				ret = ret.Trim('-');
				return ret;
			}
			catch (Exception e)
			{
				LogHandler.WriteLine(e);
				return "";
			}
		}
	    #endregion
	    #region Item Types
	    public static ushort GetItemType(uint ID)
		{
			return (ushort)(ID / 1000);
		}

	    public static int GetSmallItemType(uint ID)
		{
			return (int)(ID / 10000);
		}

	    public static byte GetQuality(uint ID)
		{
			return (byte)(ID % 10);
		}

	    public static bool IsTwoHander(uint ID)
		{
			return (GetItemSort(ID) == ItemSort.Weapon2) && (GetItemType(ID) != (int)ItemType.Shield);
		}

	    public static bool CheckBacksword(uint type)
		{
			return (GetItemSort(type) == ItemSort.Weapon1) && (GetItemType(type) == (int)ItemType.Backsword);
		}

	    public static bool CheckBow(uint type)
		{
			return (GetItemSort(type) == ItemSort.Weapon2) && (GetItemType(type) == (int)ItemType.Bow);
		}

	    public bool IsEquipment
		{
			get
			{
				var sort = GetItemSort(ID);
				if ((sort >= ItemSort.Helmet) && (sort <= ItemSort.Shoes))
					return true;

				return false;
			}
		}

	    public static ItemSort GetItemSort(uint type)
		{
			switch (type % 10000000 / 100000)
			{
				case 1:
					{
						switch (type % 1000000 / 10000)
						{
							case 11:
								return ItemSort.Helmet;
							case 12:
								return ItemSort.Necklace;
							case 13:
								return ItemSort.Armor;
							case 14:
								return ItemSort.Helmet;
							case 15:
								return ItemSort.RingR;
							case 16:
								return ItemSort.Shoes;
							case 17:
								break;
							case 18:
								return ItemSort.Overcoat;
							case 19:
								return ItemSort.Overcoat;
						}
						break;
					}
				case 4:
					return ItemSort.Weapon1;
				case 5:
					return ItemSort.Weapon2;
				case 7:
					return ItemSort.Other;
				case 9:
					return ItemSort.Shield;
				case 10:
					return ItemSort.Expendable;
				default:
					{
						break;
					}
			}

			var sort = type % 10000000 / 100000;
			if ((sort >= 20) && (sort < 30))
				return ItemSort.RingL;

			return ItemSort.Invalid;
		}

	    public static uint IdFromAmount(uint Ammount)
		{
			uint Id = 0;
			if ((Ammount <= 10) && (Ammount >= 1))
				Id = 1090000; //Silver
			else if ((Ammount <= 100) && (Ammount >= 10))
				Id = 1090010; //Sycee
			else if ((Ammount <= 1000) && (Ammount >= 100))
				Id = 1090020; //Gold
			else if ((Ammount <= 10000) && (Ammount >= 1000))
				Id = 1091010; //GoldBar
			else if (Ammount > 10000)
				Id = 1091020; //GoldBars
			else
				Id = 0;
			return Id;
		}

	    public static bool IsItemType(uint id, ItemType type)
		{
			return id.ToString().StartsWith(((ushort)type).ToString());
		}
	    #endregion
	}
}