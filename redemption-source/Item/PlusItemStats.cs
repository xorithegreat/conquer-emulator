﻿using ConquerEmulator.Database;
using ConquerEmulator.Main;

namespace ConquerEmulator.Item
{
	public struct PlusItemStats
	{
		public static string GetBaseID(uint ID)
		{
			switch ((byte)(ID / 10000))
			{
				case 11:
				case 90:
				case 13:
					{
						ID = ID / 1000 * 1000 + // [3] = 0
						     (ID % 100 - ID % 10);
						break;
					}
				case 12:
				case 15:
				case 16:
				case 50:
					{
						ID = ID - ID % 10;
						break;
					}
				default:
					{
						if (ConquerItem.IsItemType(ID, ItemType.Backsword))
						{
							ID = ID - ID % 10;
						}
						else
						{
							var head = (byte)(ID / 100000);
							ID = (uint)(head * 100000 + head * 10000 + head * 1000 + // [1] = [0], [2] = [0]
							            (ID % 1000 - ID % 10) // [5] = 0
							           );
						}
						break;
					}
			}
			return ID.ToString();
		}

		public const string Section = "ItemInformation";
		private readonly IniFile ini;

		public PlusItemStats(uint ItemID, byte Plus)
		{
			ini = new IniFile(FlatDatabase.Location + "\\PItems\\" + GetBaseID(ItemID) + "[" + Plus + "].ini");
		}

		public uint MinAttack => ini.ReadUInt32("ItemInformation", "MinAttack", 0);
		public uint MaxAttack => ini.ReadUInt32("ItemInformation", "MaxAttack", 0);
		public uint MAttack => ini.ReadUInt32("ItemInformation", "MAttack", 0);
		public ushort PhysicalDefence => ini.ReadUInt16("ItemInformation", "PhysDefence", 0);
		public sbyte Dodge => ini.ReadSByte("ItemInformation", "Dodge", 0);
		public ushort PlusMDefence => ini.ReadUInt16("ItemInformation", "MDefence", 0);
		public ushort HP => ini.ReadUInt16("ItemInformation", "HP", 0);
	}
}