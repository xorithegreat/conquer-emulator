﻿using ConquerEmulator.Database;

namespace ConquerEmulator.Item
{
	public struct StanderdItemStats
	{
		private readonly uint Id;

		public StanderdItemStats(uint ItemID)
		{
			Id = ItemID;
		}

		public uint MinAttack => uint.Parse(MySqlDatabase.LoadItemString(Id, "MinPhysAtk"));
		public uint MaxAttack => uint.Parse(MySqlDatabase.LoadItemString(Id, "MaxPhysAtk"));
		public ushort PhysicalDefence => ushort.Parse(MySqlDatabase.LoadItemString(Id, "PhysDefence"));
		public ushort MDefence => ushort.Parse(MySqlDatabase.LoadItemString(Id, "MDefence"));
		public sbyte Dodge => sbyte.Parse(MySqlDatabase.LoadItemString(Id, "Dodge"));
		public uint MAttack => uint.Parse(MySqlDatabase.LoadItemString(Id, "MAttack"));
		public ushort HP => ushort.Parse(MySqlDatabase.LoadItemString(Id, "PotAddHP"));
		public ushort MP => ushort.Parse(MySqlDatabase.LoadItemString(Id, "PotAddMP"));
		public uint Frequency => uint.Parse(MySqlDatabase.LoadItemString(Id, "Frequency"));
		public sbyte AttackRange => sbyte.Parse(MySqlDatabase.LoadItemString(Id, "Range"));
		public ushort Dexerity => ushort.Parse(MySqlDatabase.LoadItemString(Id, "Dexerity"));
		public ushort Durability => ushort.Parse(MySqlDatabase.LoadItemString(Id, "Durability"));
		public ushort ReqProfLvl => ushort.Parse(MySqlDatabase.LoadItemString(Id, "ReqProfLvl"));
		public ushort ReqStr => ushort.Parse(MySqlDatabase.LoadItemString(Id, "ReqStr"));
		public ushort ReqAgi => ushort.Parse(MySqlDatabase.LoadItemString(Id, "ReqAgi"));
		public ushort ReqLvl => ushort.Parse(MySqlDatabase.LoadItemString(Id, "ReqLvl"));
		public ushort ReqJob => ushort.Parse(MySqlDatabase.LoadItemString(Id, "ReqJob"));
		public ushort ReqSex => ushort.Parse(MySqlDatabase.LoadItemString(Id, "ReqSex"));
		public uint MoneyPrice => uint.Parse(MySqlDatabase.LoadItemString(Id, "ShopBuyPrice"));
		public uint ConquerPointsPrice => uint.Parse(MySqlDatabase.LoadItemString(Id, "ShopCPPrice"));
	}
}