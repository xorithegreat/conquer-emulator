﻿using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Item;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.PacketHandling
{
	public unsafe partial class PacketThread
	{
	    public static void SocketProcesser(GameClient Client, byte[] Packet, byte* Ptr)
		{
			var SGPacket = (SocketGemPacket*)Ptr;
			switch (SGPacket->ID)
			{
				case 0: InsertSocketNpc(Client, SGPacket, Packet); break;
				case 1: RemoveSocketNpc(Client, SGPacket, Packet); break;
			}
		}

	    private static void InsertSocketNpc(GameClient Client, SocketGemPacket* Packet, byte[] BPacket)
		{
			var CItem = Client.GetInventoryItem(Packet->UID);
			var RGem = Client.GetInventoryItem(Packet->GemUID);

			if ((CItem == null) || (RGem == null) || (Packet->Slot == 0))
				return;
			if (Client.Hero.Dead)
				return;
			if (!CItem.IsEquipment)
				return;

			var Socket1void = CItem.SocketOne == GemsConst.OpenSocket;
			var Socket2void = CItem.SocketTwo == GemsConst.OpenSocket;

			if (ConquerItem.IsItemType(RGem.ID, ItemType.GemId))
			{
				if (Socket1void && Socket2void && (Packet->Slot == 1))
				{
					CItem.SocketOne = (byte)(RGem.ID % 1000);
					CItem.SendInventoryUpdate(Client);
					Client.RemoveInventory(RGem);
				}
				else if (!Socket1void && Socket2void && (Packet->Slot == 2))
				{
					CItem.SocketTwo = (byte)(RGem.ID % 1000);
					CItem.SendInventoryUpdate(Client);
					Client.RemoveInventory(RGem);
				}
			}
			else
				Client.Speak(Color.White, ChatType.TopLeft, "Invalid socketing procedure.");
		}

	    private static void RemoveSocketNpc(GameClient Client, SocketGemPacket* Packet, byte[] BPacket)
		{
			var CItem = Client.GetInventoryItem(Packet->UID);
			if ((CItem == null) || (Packet->Slot == 0))
				return;
			if (Client.Hero.Dead)
				return;
			if (!CItem.IsEquipment)
				return;

			var Socket1void = CItem.SocketOne == GemsConst.OpenSocket;
			var Socket2void = CItem.SocketTwo == GemsConst.OpenSocket;

			if (Socket2void && !Socket1void && (Packet->Slot != 2))
			{
				CItem.SocketOne = GemsConst.OpenSocket;
				CItem.SendInventoryUpdate(Client);
			}
			else if (!Socket2void && !Socket1void && (Packet->Slot != 1))
			{
				CItem.SocketTwo = GemsConst.OpenSocket;
				CItem.SendInventoryUpdate(Client);
			}
			else
				Client.Speak(Color.White, ChatType.TopLeft, "Invalid socketing procedure.");
		}
	}
}