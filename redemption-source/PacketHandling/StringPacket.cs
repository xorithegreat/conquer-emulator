﻿using ConquerEmulator.Client;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.PacketHandling
{
	public unsafe partial class PacketThread
	{
	    public static void StringProcessor(GameClient Client, byte[] Packet, byte* Ptr)
		{
			var String = (StringPacket)PacketKernel.Deserialize(Packet, typeof(StringPacket));
			if (String.ID == (StringIds)0x10)
				ViewEquipment(Client, String);
			if (String.ID == StringIds.GuildList)
				SendGuildMembers(Client, Packet);
		}

	    private static void SendGuildMembers(GameClient Client, byte[] Packet)
		{
			Client.Send(Packet);
			Client.Guild.ListMembers(Client);
		}

	    private static void ViewEquipment(GameClient Client, StringPacket SPacket)
		{
			var Viewed = Kernel.FindClient(SPacket.UID);
			if (Viewed == null)
				return;

			foreach (var DE in Viewed.Equipment)
			{
				var Item = (ConquerItem)DE.Value;
				Item.Mode = (ushort)ItemMode.View;
				Item.UID = Viewed.Hero.UID;
				Item.Send(Client);
			}
			Client.Send(Packets.StringPacket(SPacket, Client.Spouse));
		}
	}
}