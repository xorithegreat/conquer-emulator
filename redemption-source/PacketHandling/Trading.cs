﻿using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.PacketHandling
{
    public unsafe partial class PacketThread
    {
        public static void TradeProcessor(GameClient Client, byte[] Packet, byte* Ptr)
        {
            var TRPacket = (TradePacket*)Ptr;
            switch (TRPacket->ID)
            {
                case TradeIDs.Request: RequestTrade(Client, TRPacket, Packet); break;
                case TradeIDs.CloseTrade: CloseTradeWindow(Client); break;
                case TradeIDs.Agree: TradeAgree(Client, TRPacket, Packet); break;
                case TradeIDs.AddItem: AddTradeItem(Client, TRPacket, Packet); break;
                case TradeIDs.SpecifyCPs: SpecifyTradeCPs(Client, TRPacket, Packet); break;
                case TradeIDs.SpecifyMoney: SpecifyTradeMoney(Client, TRPacket, Packet); break;
                //  default: Console.Write("Trade Packet: {0}", TRPacket->ID.ToString()); break;
            }
        }

        private static void AddTradeItem(GameClient Client, TradePacket* TRPacket, byte[] Packet)
        {
            var TClient = Kernel.FindClient(Client.Trade.Partner);
            if (TClient == null)
                return;

            if (Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, TClient.Hero.X, TClient.Hero.Y) > DataStructures.ViewDistance)
            {
                CloseTradeWindow(Client);
                return;
            }
           
            if (Client.Trade.Items.Count + TClient.Inventory.Length < 40)
            {
                var Item = (ConquerItem)Client.GetInventoryItem(TRPacket->UID);
                if (Item == null)
                    return;
                if (Item.Locked == 1)
                    return;

                TClient.Send(Packets.TradeItem(Item));
                Client.Trade.Items.Add(Item.UID, Item);
            }
            else
            {
                TRPacket->ID = TradeIDs.RemoveItem;
                Client.Send(Packet);
                Client.Speak(Color.White, ChatType.TopLeft, "Your trade partner cant hold any more items.");
            }
        }

        private static void TradeAgree(GameClient Client, TradePacket* TRPacket, byte[] Packet)
        {
            var TClient = Kernel.FindClient(Client.Trade.Partner);
            if (TClient == null)
                return;

            #region Checks
            if (Client.Hero.MapID.ID != TClient.Hero.MapID.ID)
                return;
            if (Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, TClient.Hero.X, TClient.Hero.Y) > DataStructures.ViewDistance)
                return;
            if (Client.Silvers < Client.Trade.Money || Client.ConquerPoints < Client.Trade.CPs)
                return;
            if (TClient.Silvers < TClient.Trade.Money || TClient.ConquerPoints < TClient.Trade.CPs)
                return;
            if (Client.Trade.Items.Count > 0)
            {
                if (TClient.Inventory.Length < Client.Trade.Items.Count)
                    return;
            }
            if (TClient.Trade.Items.Count > 0)
            {
                if (Client.Inventory.Length < TClient.Trade.Items.Count)
                    return;
            }
            #endregion
            if (TClient.Trade.Aggree)
            {
                TRPacket->ID = TradeIDs.CloseWindow;
                Client.Send(Packet);
                TClient.Send(Packet);

                Client.Trade.TradeItems(TClient);
                Client.Trade.ExchangeMoney(TClient);
     
                Client.Trade = new Trade(Client);
                TClient.Trade = new Trade(TClient);

                Client.Speak(Color.White, ChatType.TopLeft, "Trading succeeded.");
                TClient.Speak(Color.White, ChatType.TopLeft, "Trading succeeded.");
            }
            else
            {
                Client.Trade.Aggree = true;
                TRPacket->UID = 0;
                TClient.Send(Packet);
            }
        }

        public static void CloseTradeWindow(GameClient Client)
        {
            if (Client.Trade == null)
                return;

            var TClient = Kernel.FindClient(Client.Trade.Partner);
            if (TClient == null)
                return;

            var trade = TradePacket.Create();
            trade.UID = TClient.Hero.UID;
            trade.ID = TradeIDs.CloseWindow;
            TClient.Send(trade, trade.Size);

            trade.UID = Client.Hero.UID;
            Client.Send(trade, trade.Size);

            Client.Speak(Color.White, ChatType.TopLeft, "Trading Failed!");
            TClient.Speak(Color.White, ChatType.TopLeft, "Trading Failed!");

            foreach (var Item in Client.Trade.Items.Values)
                Item.Send(Client);

            foreach (var conquerItem in TClient.Trade.Items.Values)
            {
                var Item = (ConquerItem)conquerItem;
                Item.Send(Client);
            }

            TClient.Trade = new Trade(TClient);
            Client.Trade = new Trade(Client);
        }

        private static void SpecifyTradeMoney(GameClient Client, TradePacket* TRPacket, byte[] Packet)
        {
            var TClient = Kernel.FindClient(Client.Trade.Partner);
            if (TClient == null)
                return;

            if (Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, TClient.Hero.X, TClient.Hero.Y) > DataStructures.ViewDistance)
            {
                CloseTradeWindow(Client);
                return;
            }
            if (Client.Silvers < TRPacket->UID)
            {
                CloseTradeWindow(Client);
                return;
            }

            Client.Trade.Money = TRPacket->UID;
            TRPacket->ID = TradeIDs.ShowMoney;
            TClient.Send(Packet);
        }

        private static void SpecifyTradeCPs(GameClient Client, TradePacket* TRPacket, byte[] Packet)
        {
            var TClient = Kernel.FindClient(Client.Trade.Partner);
            if (TClient == null)
                return;

            if (Client.ConquerPoints < TRPacket->UID)
                return;
            if (Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, TClient.Hero.X, TClient.Hero.Y) > DataStructures.ViewDistance)
            {
                CloseTradeWindow(Client);
                return;
            }

            Client.Trade.CPs = TRPacket->UID;
            TRPacket->ID = TradeIDs.ShowCPs;
            TClient.Send(Packet);
        }

        private static void RequestTrade(GameClient Client, TradePacket* TRPacket, byte[] Packet)
        {
            var Trader = Kernel.FindClient(TRPacket->UID);
            if ((Trader == null) || (Client == null))
                return;
            if (Client.Hero.UID == TRPacket->UID)
                return;
            if (Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, Trader.Hero.X, Trader.Hero.Y) > DataStructures.ViewDistance)
                return;
            if (Trader.Trade.Trading)
            {
                Trader.Speak(Color.White, ChatType.TopLeft, "The target is already trding with someone else, Please try again later.");
                return;
            }

            if (Trader.Hero.UID != Client.Trade.Partner)
            {
                Trader.Trade.Partner = Client.Hero.UID;
                Client.Trade.Partner = Trader.Hero.UID;
                TRPacket->UID = Client.Hero.UID;
                Trader.Send(Packet);
                Trader.Speak(Color.White, ChatType.TopLeft, "Request for trade has been sent out.");
            }
            else
            {
                Trader.Trade.Trading = true;
                Client.Trade.Trading = true;

                TRPacket->ID = TradeIDs.OpenWindow;
                TRPacket->UID = Client.Hero.UID;
                Trader.Send(Packet);

                TRPacket->UID = Trader.Hero.UID;
                Client.Send(Packet);
            }
        }
    }
}