﻿using System;
using System.Drawing;
using System.Linq;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Events;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.PacketHandling
{
	public unsafe partial class PacketThread
	{
		public static void ItemUsageProcessor(GameClient Client, byte[] Packet, byte* Ptr)
		{
			var IUPacket = (ItemUsagePacket*)Ptr;
			switch (IUPacket->ID)
			{
				case ItemUsageID.Ping:
					{
						var TimeStamp = Native.timeGetTime();
						Client.TimeFraud.Ping(TimeStamp);
						Client.Send(Packet);
						break;
					}
				case ItemUsageID.BuyItem: BuyItem(Client, IUPacket); break;
				case ItemUsageID.SellItem: SellItem(Client, IUPacket); break;
				case ItemUsageID.Equip: EquipGear(Client, IUPacket); break;
				case ItemUsageID.UnequipItem: UnequipGear(Client, IUPacket); break;
				case ItemUsageID.DropMoney: DropMoney(Client, IUPacket); break;
				case ItemUsageID.DropItem: DropItem(Client, IUPacket); break;
				case ItemUsageID.RemoveVendingItem: RemoveVendingItem(Client, IUPacket); break;
				case ItemUsageID.AddVendingItemGold: AddVendingItemGold(Client, IUPacket); break;
				case ItemUsageID.AddVendingItemCPs: AddVendingItemCPs(Client, IUPacket); break;
				case ItemUsageID.BuyVendingItem: BuyVendingItem(Client, IUPacket); break;
				case ItemUsageID.ShowVendingList: ShowVendingItems(Client, IUPacket); break;
				#region Warehouse Refresh
				case ItemUsageID.ViewWarehouse:
					IUPacket->lParam = (uint)Client.Warehouse.Money;
					Client.Send(Packet);
					break;
				#endregion
				#region Warehouse Money withdraw
				case ItemUsageID.WithdrawCash:
					WithdrawCash(Client, IUPacket, Packet);
					Client.Send(Packet);
					break;
				#endregion
				#region Warehouse Money Deposit
				case ItemUsageID.DepositCash:
					DepositCash(Client, IUPacket, Packet);
					Client.Send(Packet);
					break;
				#endregion
				case ItemUsageID.MetUpgrade: MetUpgrade(Client, IUPacket); break;
				case ItemUsageID.DBUpgrade: DBUpgrade(Client, IUPacket); break;
				case ItemUsageID.Repair: Repair(Client, IUPacket); break;
				case ItemUsageID.UpdateEnchant: UpgradeItemEnchant(Client, IUPacket); break;
				//default: LogHandler.WriteLine("ItemUsagePacket Type:{0}", IUPacket->ID); break;
			}
		}
		#region UpgradeEnchant
		public static void UpgradeItemEnchant(GameClient Client, ItemUsagePacket* Packet)
		{
			if (Client.Hero.Dead)
				return;

			var MainItem = Client.Inventory.FirstOrDefault(x => x.UID == Packet->UID);
			var SubItem = Client.Inventory.FirstOrDefault(x => x.UID == Packet->lParam);

			if ((MainItem == null) || (MainItem.Position != 0))
				return;
			if (MainItem.Enchant >= 255)
				return;
			if (SubItem == null)
				return;
			if (!MainItem.IsEquipment)
				return;

			if (ConquerItem.IsItemType(SubItem.ID, ItemType.GemId))
			{
				var num = ConquerItem.GetGemBlessWorth(SubItem.ID);
				if (num > MainItem.Enchant)
					MainItem.Enchant = (byte)num;

				Client.RemoveInventory(SubItem);
				MainItem.SendInventoryUpdate(Client);
			}
		}
		#endregion
		private static void Repair(GameClient Client, ItemUsagePacket* Packet)
		{
			var Item = Client.GetInventoryItem(Packet->UID);
			if ((Item == null) || (Item.Position != 0))
				return;
			if (Client.Hero.Dead)
				return;
			if (!Item.IsEquipment)
				return;

			var std = new StanderdItemStats(Item.ID);
			var nRecoverDurability = Math.Max(0, std.Durability - Item.Durability);
			if (nRecoverDurability > 0)
			{
				var nRepairCost = ConquerItem.CalcRepairMoney(Item);
				if (Client.Silvers >= nRepairCost)
				{
					Item.Durability = Item.MaxDurability;
					var Update = new ItemUsagePacket(Item.UID, Item.Durability, ItemUsageID.UpdateDurability);
					Client.Send(Update, Update.Size);

					Client.Silvers -= nRepairCost;
					Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());
				}
			}
			else
			{
				if (!Client.TryExpendItemOfId(1088001, 5))
				{
					Client.Speak(Color.Red, ChatType.TopLeft, "You don't have enough meteors to repair this item.");
					return;
				}
				Item.Durability = Item.MaxDurability;
				Item.Send(Client);
			}
		}

		private static void EquipGear(GameClient Client, ItemUsagePacket* Packet)
		{
			var EquipItem = Client.GetInventoryItem(Packet->UID);
			var position = (ItemPosition)Packet->lParam;

            #region Checks
            if (EquipItem == null)
		        return;
			if (Client.Hero.Dead)
				return;
			if (!Stamps.Check(Client.Hero.Stamps.Equip))
				return;
            if (Client.InTournament)
            {
                var success = TeamDeathMatch.Teams.ContainsKey(Client);
                if (success)
                {
                    if (position == ItemPosition.Garment)
                    {
                        TeamDeathMatch.Leave(Client);
                        return;
                    }
                }
            }
            if (Client.AutoAttaking)
            {
                if (position == ItemPosition.Left || position == ItemPosition.Right)
                {
                    Client.AATarget = null;
                    Client.AutoAttaking = false;
                }
            }
            if ((Client.Hero.StatusFlag & StatusFlag.Fly) == StatusFlag.Fly)
            {
                if (position == ItemPosition.Left || position == ItemPosition.Right)
                    return; // Disable unequiping weapons while flying.
            }
            #endregion
            if (((position <= ConquerItem.LastSlot) && (position >= ConquerItem.FirstSlot)) || (position == ItemPosition.Inventory))
			{
				#region Item Usage
				if ((position == ItemPosition.Inventory) && !ConquerItem.IsItemType(EquipItem.ID, ItemType.Arrow))
				{
					var Usage = new ItemUsage();
					Usage.Process(Client, Packet->UID);
					return;
				}
				#endregion
				if (Client.ValidateEquipment(EquipItem))
				{
					#region Arrow
					if ((position == ItemPosition.Inventory) && ConquerItem.IsItemType(EquipItem.ID, ItemType.Arrow))
					{
						var Right = Client.GetEquipmentItem(ItemPosition.Right);
						if ((Right == null) || !ConquerItem.IsItemType(Right.ID, ItemType.Bow))
							return;

						position = ItemPosition.Left;
						Client.RemoveInventory(EquipItem, EquipItem.Status);
					}
					#endregion
					if (Client.Equipment.ContainsKey((ushort)position))
					{
						Client.Unequip(Client, (ushort)position);
						if (ConquerItem.IsItemType(EquipItem.ID, ItemType.Bow))
							Client.Unequip(Client, (ushort)ItemPosition.Left);
					}
					if (Client.Equip(EquipItem, (ushort)position, Client))
						Client.RemoveInventory(EquipItem, ItemStatus.AddOrUpdate);

					Client.CalculateStatBonus();
					Client.SendStatMessage();
					Client.Hero.Stamps.Equip = DateTime.Now.AddMilliseconds(250);

					Client.SendScreen(Packet->Serialize(), true);
				}
			}
		}

		private static void UnequipGear(GameClient Client, ItemUsagePacket* Packet)
		{
			if (!Client.Hero.Dead && (Client.Inventory.Length < 40))
			{
                var EquipSlot = (ushort)Packet->lParam;
                #region Checks
                if (Client.AutoAttaking)
                {
                    if (EquipSlot == (int)ItemPosition.Left || EquipSlot == (int)ItemPosition.Right)
                    {
                        Client.AATarget = null;
                        Client.AutoAttaking = false;
                    }
                }
                if ((Client.Hero.StatusFlag & StatusFlag.Fly) == StatusFlag.Fly)
                {
                    if ((EquipSlot == (ushort)ItemPosition.Left) || (EquipSlot == (ushort)ItemPosition.Right))
                        return;
                }
                if (Client.InTournament)
                {
                    var success = TeamDeathMatch.Teams.ContainsKey(Client);
                    if (success)
                    {
                        if (EquipSlot == (ushort)ItemPosition.Garment)
                        {
                            TeamDeathMatch.Leave(Client);
                            return;
                        }
                    }
                }
                #endregion
                if (Client.Equipment.ContainsKey(EquipSlot))
				{
					if (!Client.Unequip(Client, EquipSlot))
						throw new Exception("PacketProcessor::UnequipGear() -> Failed to call Client.Unequip()");

					Client.CalculateStatBonus();
					Client.SendStatMessage();
					Client.Send(Packet->Serialize());
				}
			}
			else
				Client.Speak(Color.Yellow, ChatType.TopLeft, "Your inventory is full.");
		}

		private static void BuyItem(GameClient Client, ItemUsagePacket* IUPacket)
		{
			var Validated = false;
			if (Client.Hero.Dead)
				return;
			if (Client.Inventory.Length == 40)
			{
				Client.Speak(Color.Red, ChatType.TopRight, "Your inventory is full.");
				return;
			}

			if (FlatDatabase.ShopItemExists(IUPacket->UID, IUPacket->lParam))
			{
				var Type = (MoneyType)FlatDatabase.Shops.ReadInt32(IUPacket->UID.ToString(), "MoneyType", (int)MoneyType.Unknown);
				uint Price;

                if (IUPacket->UID != 2888)
                {
                    if (Client.Screen.FindObject(IUPacket->UID) == null)
                        return;
                }
				var stats = new StanderdItemStats(IUPacket->lParam);
				switch (Type)
				{
					case MoneyType.Gold:
						{
							Price = stats.MoneyPrice;
							if (Price == 0)
								return;
							if (Client.Silvers >= Price)
							{
								Client.Silvers -= Price;
								Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());
								Validated = true;
							}
							else
								Client.Speak(Color.Red, ChatType.TopLeft, "No Enough Money.");
							break;
						}
					case MoneyType.ConquerPoints:
						{
							Price = stats.ConquerPointsPrice;
							if (Client.ConquerPoints >= Price)
							{
								Client.ConquerPoints -= Price;
								Client.Send(new StatTypePacket(Client.Hero.UID, Client.ConquerPoints, StatIDs.ConquerPoints).Serialize());
								Validated = true;
							}
							else
								Client.Speak(Color.Red, ChatType.TopLeft, "No Enough Money.");
							break;
						}
				}
				if (Validated)
				{
					IConquerItem item = new ConquerItem(true);
					item.UID = ConquerItem.NextItemUID;
					item.ID = IUPacket->lParam;
					var item_stats = new StanderdItemStats(item.ID);
					item.Durability = item_stats.Durability;
					item.MaxDurability = item_stats.Durability;
					Client.AddInventory(item);
				}
			}
		}

		private static void SellItem(GameClient Client, ItemUsagePacket* IUPacket)
		{
			var InventoryItem = Client.GetInventoryItem(IUPacket->lParam);
			if (Client.Hero.Dead)
				return;
			if (InventoryItem == null)
				return;
			if (Client.Screen.FindObject(IUPacket->UID) == null)
				return;

			uint Profit;
			uint.TryParse(MySqlDatabase.LoadItemString(InventoryItem.ID, "ShopBuyPrice"), out Profit);
            if (Profit > 0)
            {
                Math.Floor((decimal)Profit / 3);
                Profit = (uint)Math.Round((decimal)(Profit / 3));
                Client.Silvers += Profit;

                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());
                Client.RemoveInventory(InventoryItem);
            }
		}

		private static void DropMoney(GameClient Client, ItemUsagePacket* IUPacket)
		{
			var Amount = IUPacket->UID;
			if (Amount == 0)
				return;
			if (Client.Hero.Dead)
				return;

			var Item = new ConquerItem(true);
			Item.UID = ConquerItem.NextItemUID;
			Item.ID = ConquerItem.IdFromAmount(Amount);

			if (Client.Silvers >= Amount)
			{
                if (Stamps.Check(Client.Hero.Stamps.GoldDrop))
                {
                    if (FloorItem.Drop(Client.Hero, Item, Amount))
                    {
                        Client.Silvers -= IUPacket->UID;
                        Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());
                        Client.Hero.Stamps.GoldDrop = DateTime.Now.AddMilliseconds(500);
                    }
                }
			}
			else
				Client.Speak(Color.White, ChatType.TopLeft, "You don't have enough Silvers.");
		}

		private static void DropItem(GameClient Client, ItemUsagePacket* IUPacket)
		{
			if (Client.Hero.Dead)
				return;

			var Item = (ConquerItem)Client.GetInventoryItem(IUPacket->UID);
			if (Item != null)
				FloorItem.Drop(Client.Hero, Item);
		}

		private static void DepositCash(GameClient Client, ItemUsagePacket* IUPacket, byte[] Packet)
		{
			if (Client.Hero.Dead)
				return;
			if (Client.Screen.FindObject(IUPacket->UID) == null) //Hacking bitch
				return;

			if (Client.Silvers >= IUPacket->lParam)
			{
				Client.Warehouse.Money += (int)IUPacket->lParam;
				Client.Silvers -= IUPacket->lParam;
				Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());
			}
		}

		private static void WithdrawCash(GameClient Client, ItemUsagePacket* IUPacket, byte[] Packet)
		{
			if (Client.Hero.Dead)
				return;
			if (Client.Screen.FindObject(IUPacket->UID) == null) //Hacking bitch
				return;
			if ((Client.Warehouse.Password != "") && !Client.Warehouse.Validated)
				return;

			if (Client.Warehouse.Money >= IUPacket->lParam)
			{
				Client.Silvers += IUPacket->lParam;
				Client.Warehouse.Money -= (int)IUPacket->lParam;
				Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());
			}
		}

		private static void DBUpgrade(GameClient Client, ItemUsagePacket* IUPacket)
		{
			var Item = Client.GetInventoryItem(IUPacket->UID);
			var UPItem = Client.GetInventoryItem(IUPacket->lParam);

			if ((Item == null) || (UPItem == null))
				return;
			if ((Item.Position != 0) || (Item.ID % 10 == 9))
				return;
			if ((UPItem.Position != 0) || (UPItem.ID != 1088000))
				return;
			if (Client.Hero.Dead)
				return;
			if (!Item.IsEquipment)
				return;
			if (Item.Durability != Item.MaxDurability)
			{
				Client.Speak(Color.Red, ChatType.TopLeft, "The item is damaged. Please repair it first.");
				return;
			}

			double Chance;
			if (!ConquerItem.GetUpQualityInfo(Item, out Chance))
				return;

			if (DataStructures.ChanceSuccess(Chance))
			{
				byte Increment = 1;
				while ((Item.ID + Increment) % 10 < 6)
					Increment++;
				Item.ID += Increment;
				Client.RemoveInventory(UPItem);
				Item.SendInventoryUpdate(Client);
			}
			else
			{
				Item.Durability /= 2;
				Item.Send(Client);
				Client.Speak(Color.Red, ChatType.TopLeft, "Your item has failed to upgrade.");

				Client.RemoveInventory(UPItem);
			}
            #region Socket Chance
            if (Item.SocketOne == 0)
            {
                if (DataStructures.ChanceSuccess(DataStructures.DBSock1))
                {
                    Item.SocketOne = 255;
                    GameClient.SpeakToAll(Color.Red, ChatType.TopLeft, "Congratulations!, " + Client.Hero.Name + " got a socket in his/her item!");
                }
            }
            if (Item.SocketTwo == 0)
            {
                if (DataStructures.ChanceSuccess(DataStructures.DBSock2))
                {
                    Item.SocketTwo = 255;
                    GameClient.SpeakToAll(Color.Red, ChatType.TopLeft, "Congratulations!, " + Client.Hero.Name + " got a 2nd socket in his/her item!");
                }
            }
            #endregion
        }

        private static void MetUpgrade(GameClient Client, ItemUsagePacket* IUPacket)
		{
			var Item = Client.GetInventoryItem(IUPacket->UID);
			var UPItem = Client.GetInventoryItem(IUPacket->lParam);

			if ((Item == null) || (UPItem == null))
				return;
			if (Item.Position != 0)
				return;
			if ((UPItem.Position != 0) || (UPItem.ID != 1088001))
				return;
			if (Client.Hero.Dead)
				return;
			if (!Item.IsEquipment)
				return;

			var standerd = new StanderdItemStats(Item.ID);
			if (standerd.ReqLvl >= 120)
			{
				Client.Speak(Color.Red, ChatType.TopLeft, "You cannot upgrade this item any further through TwinCity.");
				return;
			}
			if (Item.Durability != Item.MaxDurability)
			{
				Client.Speak(Color.Red, ChatType.TopLeft, "The item is damaged. Please repair it first.");
				return;
			}

			double Chance;
			if (!ConquerItem.GetUpLevelInfo(Item, out Chance))
				return;

			if (DataStructures.ChanceSuccess(Chance))
			{
				var ItemType = (byte)Convert.ToInt32(Math.Floor(Item.ID * 0.0001));
				byte Increment = 10;
				if ((ItemType == 12) || (ItemType == 20) || (ItemType == 20))
					Increment = 20;
				while (MySqlDatabase.LoadItemString(Item.ID + Increment, "ItemID") == "0")
					Increment += 10;
				#region Upgrade Level
				Client.RemoveInventory(UPItem);
				Item.ID += Increment;
				Item.SendInventoryUpdate(Client);
				#endregion
			}
			else
			{
				Item.Durability /= 2;
				Item.Send(Client);
				Client.RemoveInventory(UPItem);
				Client.Speak(Color.Red, ChatType.TopLeft, "Your item has failed to upgrade.");
			}
            #region Socket Chance
            if (Item.SocketOne == 0)
            {
                if (DataStructures.ChanceSuccess(DataStructures.MetSock1))
                {
                    Item.SocketOne = 255;
                    GameClient.SpeakToAll(Color.Red, ChatType.TopLeft, "Congratulations!, " + Client.Hero.Name + " got a socket in his/her item!");
                }
            }
            if (Item.SocketTwo == 0)
            {
                if (DataStructures.ChanceSuccess(DataStructures.MetSock2))
                {
                    Item.SocketTwo = 255;
                    GameClient.SpeakToAll(Color.Red, ChatType.TopLeft, "Congratulations!, " + Client.Hero.Name + " got a 2nd socket in his/her item!");
                }
            }
            #endregion
        }
        #region Vending
        private static void ShowVendingItems(GameClient Client, ItemUsagePacket* Packet)
		{
			var vClient = ClientVendor.FindVendorClient(Packet->UID);
			if (vClient != null)
				foreach (var vItem in vClient.Vendor.Items)
					Client.Send(vItem.GetBytes());
		}

		private static void RemoveVendingItem(GameClient Client, ItemUsagePacket* Packet)
		{
			if (Client.Vendor.IsVending)
			{
				Client.Vendor.RemoveItem(Packet->UID);
				Client.Send(PacketKernel.GetBytes(Packet, Packet->Size));
			}
		}

		private static void AddVendingItemGold(GameClient Client, ItemUsagePacket* Packet)
		{
			if (Client.Vendor.IsVending)
			{
				var item = Client.GetInventoryItem(Packet->UID);
				if (item != null)
				{
					Client.Vendor.AddItem(item, Math.Max(Packet->lParam, 1), true);
					Client.Send(PacketKernel.GetBytes(Packet, Packet->Size));
				}
			}
		}

		private static void AddVendingItemCPs(GameClient Client, ItemUsagePacket* Packet)
		{
			if (Client.Vendor.IsVending)
			{
				var item = Client.GetInventoryItem(Packet->UID);
				if (item != null)
				{
					Client.Vendor.AddItem(item, Math.Max(Packet->lParam, 1), false);
					Client.Send(PacketKernel.GetBytes(Packet, Packet->Size));
				}
			}
		}

        private static void BuyVendingItem(GameClient Client, ItemUsagePacket* Packet)
        {
            if (Client.Inventory.Length >= 40)
                return;

            var vClient = ClientVendor.FindVendorClient(Packet->lParam);
            if (vClient != null)
            {
                if (vClient.Vendor.IsVending)
                {
                    VendingItem vItem;
                    if (vClient.Vendor.SelectItem(Packet->UID, out vItem))
                    {
                        if (vClient.GetInventoryItem(vItem.UID) != null)
                        {
                            bool Purchased;
                            if (Purchased = Client.Silvers >= vItem.Price)
                            {
                                Client.Silvers -= vItem.Price;
                                vClient.Silvers += vItem.Price;
                            }
                            if (Purchased)
                            {
                                vClient.Vendor.RemoveItem(vItem.UID);
                                vClient.Send(new StatTypePacket(vClient.Hero.UID, vClient.Silvers, StatIDs.Money).Serialize());
                                Client.AddInventory(vItem.GetItem());
                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());

                                Packet->ID = ItemUsageID.RemoveVendingItem;
                                vClient.Send(PacketKernel.GetBytes(Packet, Packet->Size));
                                Client.Send(PacketKernel.GetBytes(Packet, Packet->Size));
                                vClient.RemoveInventory(vItem.GetItem());
                            }
                        }
                    }
                }
            }
        }
		#endregion
	}
}