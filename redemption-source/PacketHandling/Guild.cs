﻿using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Main;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.PacketHandling
{
	public unsafe partial class PacketThread
	{
	    public static void GuildProcessor(GameClient Client, byte[] Packet, byte* Ptr)
		{
			var GPacket = (GuildPacket*)Ptr;
			switch (GPacket->ID)
			{
				case GuildIds.JoinRequest: GuildJoinRequest(Client, GPacket, Packet); break;
				case GuildIds.AcceptRequest: GuildAcceptRequest(Client, GPacket, Packet); break;
				case GuildIds.Quit: QuitGuild(Client, GPacket, Packet); break;
				case GuildIds.Info: GuildInfo(Client, GPacket, Packet); break;
				case GuildIds.Donate: DonateGuild(Client, GPacket, Packet); break;
				case GuildIds.Refresh: RefreshGuild(Client, GPacket, Packet); break;
				//  default: LogHandler.WriteLine("Guild Packet: {0}", GPacket->ID.ToString()); break;
			}
		}

	    public static void GuildKickPlayer(GameClient Client, byte[] Packet)
		{
			var Name = PacketKernel.Decode(Packet, 14, 14 + Packet[13]);
			var GClient = Kernel.FindClient(Name);

            if (GClient != null)
            {
                if ((Client.Guild == null) || (GClient.Guild == null))
                    return;
                if (Client == GClient)
                    return;
                if (Client.Hero.GuildRank != GuildPosition.Leader)
                    return;

                var GPacket = new GuildPacket();
                GPacket.Size = 12;
                GPacket.Type = 0x453;
                GPacket.UID = Client.Hero.GuildID;
                GPacket.ID = GuildIds.Quit;
                GClient.Send(GPacket, GPacket.Size);
                GClient.Send(Packet);
                GClient.Guild.SendGuildMsg(GClient, GClient.Hero.Name + " has been driven out of the guild.");
                GClient.Guild.MemberQuits(GClient, true);
                GClient.Send(Packets.GuildInfo(null, GClient));
                GClient.SendScreen(GClient.Hero.SpawnPacket, false);
            }
		}

	    private static void GuildInfo(GameClient Client, GuildPacket* GPacket, byte[] Packet)
		{
		}

	    private static void RefreshGuild(GameClient Client, GuildPacket* GPacket, byte[] Packet)
		{
			if (Client.Guild == null)
				return;

			Client.Send(Packets.GuildInfo(Client.Guild, Client));
			Client.Speak(Color.White, ChatType.GuildBulletin, Client.Guild.Bulletin);
		}

	    private static void DonateGuild(GameClient Client, GuildPacket* GPacket, byte[] Packet)
		{
			if ((Client.Silvers < GPacket->UID) || (Client.Guild == null))
				return;

			Client.Silvers -= GPacket->UID;
			Client.GuildDonation += GPacket->UID;
			Client.Guild.Fund += GPacket->UID;
			Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());
			Client.Send(Packets.GuildInfo(Client.Guild, Client));
		}

	    private static void QuitGuild(GameClient Client, GuildPacket* GPacket, byte[] Packet)
		{
			if ((Client.Guild == null) || (Client.Hero.GuildRank == GuildPosition.Leader))
				return;

			GPacket->UID = Client.Hero.GuildID;
			GPacket->ID = GuildIds.Quit;
			Client.Send(Packet);
            Client.Guild.SendGuildMsg(Client, Client.Hero.Name + " has left the guild.");
            Client.Guild.MemberQuits(Client, false);
            Client.Send(Packets.GuildInfo(null, Client));
            Client.Guild = null;
            Client.SendScreen(Client.Hero.SpawnPacket, false);
		}

	    private static void GuildAcceptRequest(GameClient Client, GuildPacket* GPacket, byte[] Packet)
		{
			var NewMember = Kernel.FindClient(GPacket->UID);
			if ((NewMember == null) || (NewMember.Guild != null) || (Client?.Guild == null))
				return;

			NewMember.Guild = Client.Guild;
			NewMember.Guild.NewMember(NewMember);
			NewMember.Hero.GuildID = Client.Guild.UID;
			NewMember.Hero.GuildRank = GuildPosition.Member;
			NewMember.Guild.Name = Client.Guild.Name;
			NewMember.Send(Packets.GuildInfo(NewMember.Guild, NewMember));

			var SPacket = new StringPacket((byte)NewMember.Guild.Name.Length);
			SPacket.ID = StringIds.GuildName;
			SPacket.UID = NewMember.Guild.UID;
			NewMember.Send(Packets.StringPacket(SPacket, NewMember.Guild.Name));

			NewMember.SendScreen(NewMember.Hero.SpawnPacket, false);
		}

	    private static void GuildJoinRequest(GameClient Client, GuildPacket* GPacket, byte[] Packet)
		{
			var Member = Kernel.FindClient(GPacket->UID);
			var canAccept = (Member.Hero.GuildRank == GuildPosition.Leader) || (Member.Hero.GuildRank == GuildPosition.DeputyLeader);

			if ((Client.Guild != null) || (Member.Guild == null) || !canAccept)
				return;

			GPacket->UID = Client.Hero.UID;
			Member.Send(Packet);
		}
	}
}