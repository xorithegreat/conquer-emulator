﻿using System;
using System.Collections.Generic;
using ConquerEmulator.Client;
using ConquerEmulator.Item;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.PacketHandling
{
	public unsafe partial class PacketThread
	{
	    public static void WarehouseProcessor(GameClient Client, byte* Ptr)
		{
			var WPacket = (WarehousePacket*)Ptr;
			switch (WPacket->Mode)
			{
				case WhItemIDs.Load: LoadWHItems(Client, WPacket); break;
				case WhItemIDs.Withdraw: WithdrawItem(Client, WPacket); break;
				case WhItemIDs.Deposit: DepositItem(Client, WPacket); break;
			}
		}

	    private static void DepositItem(GameClient Client, WarehousePacket* WPacket)
		{
            if (Client.Warehouse.Warehouses.TryGetValue(WPacket->ID, out Dictionary<uint, IConquerItem> Warehouses))
            {
                if (Client.Screen.FindObject(WPacket->ID) == null)
                    return;
                if ((WPacket->ID == (uint)WhIDs.Market) && (Warehouses.Count >= 40))
                    return;
                if ((WPacket->ID != (uint)WhIDs.Market) && (Warehouses.Count >= 20))
                    return;

                var Item = Client.GetInventoryItem(WPacket->UID);
                if (Item == null)
                    return;
                if (Client.Hero.Dead)
                    return;

                Warehouses.Add(Item.UID, Item);
                AddWHItem(Client, WPacket);
                Client.RemoveInventory(Item);
            }
        }

	    private static void WithdrawItem(GameClient Client, WarehousePacket* WPacket)
		{
            if (Client.Warehouse.Warehouses.TryGetValue(WPacket->ID, out Dictionary<uint, IConquerItem> Items))
            {
                if (Client.Screen.FindObject(WPacket->ID) == null)
                    return;
                if (Client.Hero.Dead)
                    return;
                if ((Client.Warehouse.Password != "") && !Client.Warehouse.Validated)
                    return;

                if (Client.Inventory.Length < 40)
                {
                    var Item = Warehouse.GetItem(WPacket->UID, Items);
                    if (Item == null)
                        return;

                    Client.AddInventory(Item);
                    Items.Remove(Item.UID);
                    RemoveWHItem(Client, WPacket);
                }
            }
        }

        private static void AddWHItem(GameClient Client, WarehousePacket* WPacket)
        {
            if (Client.Warehouse.Warehouses.TryGetValue(WPacket->ID, out Dictionary<uint, IConquerItem> Warehouse))
            {
                var item = Client.GetInventoryItem(WPacket->UID);
                var Buffer = new byte[20 + Warehouse.Count * 20 + 8];
                fixed (byte* Packet = Buffer)
                {
                    *(ushort*)(Packet + 0) = (ushort)(Buffer.Length - 8);
                    *(ushort*)(Packet + 2) = 1102;
                    *(uint*)(Packet + 4) = WPacket->ID;
                    *(Packet + 8) = (byte)WPacket->Mode;
                    *(uint*)(Packet + 12) = 1;

                    if (item.ID != 0)
                    {
                        *(uint*)(Packet + 16) = item.UID;
                        *(uint*)(Packet + 20) = item.ID;
                        *(Packet + 25) = item.SocketOne;
                        *(Packet + 26) = item.SocketTwo;
                        *(Packet + 29) = item.Plus;
                        *(Packet + 30) = item.Bless;
                        *(Packet + 32) = item.Enchant;
                        *(Packet + 38) = item.Locked;
                        *(Packet + 39) = item.Color;
                    }
                }
                Client.Send(Buffer);
            }
        }

        private static void RemoveWHItem(GameClient Client, WarehousePacket* WPacket)
        {
            if (Client.Warehouse.Warehouses.TryGetValue(WPacket->ID, out Dictionary<uint, IConquerItem> Warehouse))
            {
                var Buffer = new byte[20 + Warehouse.Count * 20 + 8];
                fixed (byte* Packet = Buffer)
                {
                    *(ushort*)(Packet + 0) = (ushort)(Buffer.Length - 8);
                    *(ushort*)(Packet + 2) = 1102;
                    *(uint*)(Packet + 4) = WPacket->ID;
                    *(Packet + 8) = (byte)WPacket->Mode;
                    *(uint*)(Packet + 12) = WPacket->UID;
                }
                Client.Send(Buffer);
            }
        }
        
        private static void LoadWHItems(GameClient Client, WarehousePacket* WPacket)
		{
            if (Client.Warehouse.Warehouses.TryGetValue(WPacket->ID, out Dictionary<uint, IConquerItem> Warehouse))
            {
                foreach (var item in Warehouse.Values)
                {
                    var Buffer = new byte[20 + Warehouse.Count * 20 + 8];
                    fixed (byte* Packet = Buffer)
                    {
                        *(ushort*)(Packet + 0) = (ushort)(Buffer.Length - 8);
                        *(ushort*)(Packet + 2) = 1102;
                        *(uint*)(Packet + 4) = WPacket->ID;
                        *(Packet + 8) = 1;
                        *(uint*)(Packet + 12) = 1;

                        if (item.ID != 0)
                        {
                            *(uint*)(Packet + 16) = item.UID;
                            *(uint*)(Packet + 20) = item.ID;
                            *(Packet + 25) = item.SocketOne;
                            *(Packet + 26) = item.SocketTwo;
                            *(Packet + 29) = item.Plus;
                            *(Packet + 30) = item.Bless;
                            *(Packet + 32) = item.Enchant;
                            *(Packet + 38) = item.Locked;
                            *(Packet + 39) = item.Color;
                        }
                    }
                    Client.Send(Buffer);
                }
            }
        }
	}
}