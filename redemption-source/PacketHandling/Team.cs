﻿using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Main;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.PacketHandling
{
	public unsafe partial class PacketThread
	{
	    public static void TeamProcessor(GameClient Client, byte[] Packet, byte* Ptr)
		{
			var TPacket = (TeamPacket)PacketKernel.Deserialize(Packet, typeof(TeamPacket));
			switch (TPacket.ID)
			{
				case TeamIDs.RequestInvite: SendInviteToTeam(Client, TPacket, Packet); break;
				case TeamIDs.Create: CreateTeam(Client, TPacket, Packet); break;
				case TeamIDs.RequestJoin: RequestJoinTeam(Client, TPacket, Packet); break;
				case TeamIDs.LeaveTeam: LeaveCurrentTeam(Client, TPacket, Packet); break;
				case TeamIDs.AcceptInvite: AcceptInviteToTeam(Client, TPacket, Packet); break;
				case TeamIDs.AcceptJoin: AcceptJoinTeam(Client, TPacket, Packet); break;
				case TeamIDs.Dismiss: DismissTeam(Client, TPacket, Packet); break;
				case TeamIDs.Kick: KickFromTeam(Client, TPacket, Packet); break;
				case TeamIDs.ForbidJoin: Client.Team.CanJoin = false; break;
				case TeamIDs.UnforbidJoin: Client.Team.CanJoin = true; break;
				case TeamIDs.ForbidMoney: Client.Team.MoneyPickup = false; break;
				case TeamIDs.UnforbidMoney: Client.Team.MoneyPickup = true;	break;
				case TeamIDs.ForbidItems: Client.Team.ItemPickup = false; break;
				case TeamIDs.UnforbidItems: Client.Team.ItemPickup = true; break;
				//default: Console.WriteLine("TeamPacket Type:{0}", TPacket.ID.ToString()); break;
			}
		}

	    private static void AcceptInviteToTeam(GameClient Client, TeamPacket Packet, byte[] Bytes)
		{
			if (Client.Team.Active || Client.Hero.Dead)
				return;

			var client = Kernel.FindClient(Packet.UID);
			if ((client != null) && client.Team.Active && (client.Team.TeamDictionary.Count != 5))
			{
				var packetStruct = new AddToTeamPacket();
				packetStruct.MaxHitpoints = (ushort)Client.Hero.MaxHitpoints;
				packetStruct.Hitpoints = (ushort)Client.Hero.Hitpoints;
				packetStruct.Mesh = Client.Hero.Model;
				packetStruct.UID = Client.Hero.UID;
				packetStruct.Name = Client.Hero.Name;
				var bytes = PacketKernel.Serialize(packetStruct, packetStruct.Size);
				var packet2 = new AddToTeamPacket();
				foreach (var client2 in client.Team.Teammates)
				{
					client2.Send(bytes);
					Client.Team.Add(client2);
					packet2.MaxHitpoints = (ushort)client2.Hero.MaxHitpoints;
					packet2.Hitpoints = (ushort)client2.Hero.Hitpoints;
					packet2.Mesh = client2.Hero.Model;
					packet2.UID = client2.Hero.UID;
					packet2.Name = client2.Hero.Name;
					Client.Send(packet2, packet2.Size);
					if (client2.Hero.UID != client.Hero.UID)
						client2.Team.Add(Client);
				}
				client.Team.Add(Client);
				Client.Team.Add(Client);
				Client.Team.Active = true;
				Client.Team.IsLeader = false;
				Client.Team.Leader = client;
				Client.Send(bytes);
			}
		}

	    private static void RequestJoinTeam(GameClient Client, TeamPacket Packet, byte[] Bytes)
		{
			var client = Kernel.FindClient(Packet.UID);
			if ((client != null) && (client.Team.TeamDictionary.Count != 5) && client.Team.Active)
			{
				if (!client.Team.CanJoin)
					Client.Speak(Color.White, ChatType.TopLeft, "The team is not accepting new members.");
				else
				{
					Packet.UID = Client.Hero.UID;
					client.Send(Packet, Packet.Size);
				}
			}
		}

	    private static void AcceptJoinTeam(GameClient Client, TeamPacket Packet, byte[] Bytes)
		{
			if (Client.Team.Active && !Client.Hero.Dead && (Client.Team.TeamDictionary.Count != 5))
			{
				var teammate = Kernel.FindClient(Packet.UID);
				if ((teammate != null) && !teammate.Team.Active)
				{
					var packetStruct = new AddToTeamPacket();
					packetStruct.MaxHitpoints = (ushort)teammate.Hero.MaxHitpoints;
					packetStruct.Hitpoints = (ushort)teammate.Hero.Hitpoints;
					packetStruct.Mesh = teammate.Hero.Model;
					packetStruct.UID = teammate.Hero.UID;
					packetStruct.Name = teammate.Hero.Name;
					var bytes = PacketKernel.Serialize(packetStruct, packetStruct.Size);
					var packet2 = new AddToTeamPacket();
					foreach (var client2 in Client.Team.Teammates)
					{
						client2.Send(bytes);
						teammate.Team.Add(client2);
						packet2.MaxHitpoints = (ushort)client2.Hero.MaxHitpoints;
						packet2.Hitpoints = (ushort)client2.Hero.Hitpoints;
						packet2.Mesh = client2.Hero.Model;
						packet2.UID = client2.Hero.UID;
						packet2.Name = client2.Hero.Name;
						teammate.Send(PacketKernel.Serialize(packet2, packet2.Size));
						if (client2.Hero.UID != Client.Hero.UID)
							client2.Team.Add(teammate);
					}
					Client.Team.Add(teammate);
					Client.Team.Leader = Client;
					teammate.Team.Add(teammate);
					teammate.Team.Active = true;
					teammate.Team.IsLeader = false;
					teammate.Send(bytes);
				}
			}
		}

	    public static void LeaveCurrentTeam(GameClient Client, TeamPacket Packet, byte[] Bytes)
		{
			if (Client.Team.Active && !Client.Team.IsLeader)
			{
				foreach (var client in Client.Team.Teammates)
				{
					if (client.Hero.UID == Client.Hero.UID)
						continue;

					client.Send(Bytes);
					client.Team.TeamDictionary.Remove(Client.Hero.UID);
				}
				Client.Send(Bytes);
				Client.Team.Active = false;
				Client.Team.IsLeader = false;
				Client.Team.Leader = null;
				Client.Team.TeamDictionary.Clear();
			}
		}

	    private static void SendInviteToTeam(GameClient Client, TeamPacket Packet, byte[] Bytes)
		{
			if (Client.Team.IsLeader && Client.Team.Active && (Client.Team.TeamDictionary.Count != 5))
			{
				var client = Kernel.FindClient(Packet.UID);
				if ((client != null) && !client.Team.Active)
				{
					Packet.UID = Client.Hero.UID;
					client.Send(Packet, Packet.Size);
				}
			}
		}

	    private static void KickFromTeam(GameClient Client, TeamPacket Packet, byte[] Bytes)
		{
			if (Client.Team.IsLeader)
			{
				var client = Kernel.FindClient(Packet.UID);
				if ((client != null) && client.Team.Active && client.Team.TeamDictionary.ContainsKey(Client.Hero.UID))
					LeaveCurrentTeam(client, Packet, Bytes);
			}
		}

	    private static void CreateTeam(GameClient Client, TeamPacket Packet, byte[] Bytes)
		{
			if (!Client.Hero.Dead && !Client.Team.Active)
			{
				Client.Team.Active = true;
				Client.Team.IsLeader = true;
				Client.Team.Leader = Client;
				Client.Team.Add(Client);
				Client.Hero.StatusFlag |= StatusFlag.TeamLeader;
				Client.SendScreen(new StatTypePacket(Client.Hero.UID, (uint)Client.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize(), true);
				Client.Send(Bytes);
			}
		}

	    public static void DismissTeam(GameClient Client, TeamPacket Packet, byte[] Bytes)
		{
			if (Client.Team.Active && Client.Team.IsLeader)
			{
				foreach (var client in Client.Team.Teammates)
				{
					if (client == null)
						continue;
					if (client.Hero.UID == Client.Hero.UID)
						continue;

					client.Send(Bytes);
					client.Team.TeamDictionary.Clear();
					client.Team.Active = false;
				}
				Client.Send(Bytes);
				Client.Team.TeamDictionary.Clear();
				Client.Team.IsLeader = false;
				Client.Team.Active = false;
				Client.Team.Leader = null;
				Client.Hero.StatusFlag &= ~StatusFlag.TeamLeader;
				Client.SendScreen(new StatTypePacket(Client.Hero.UID, (uint)Client.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize(), true);
			}
		}
	}
}