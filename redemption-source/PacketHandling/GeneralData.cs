﻿using System;
using System.Drawing;
using System.Linq;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Events;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.PacketHandling
{
	public unsafe partial class PacketThread
	{
	    public static void GeneralData(GameClient Client, byte[] Packet, byte* Ptr)
		{
			var DPacket = (DataPacket*)Ptr;
			switch (DPacket->ID)
			{
				case DataIDs.SetLocation: SetLocation(Client, DPacket, Packet); break;
				case DataIDs.Synchro: SyncEntities(Client, DPacket->lParam); break; //Synchro Sync
				case DataIDs.Jump: AppendJump(Client, DPacket, Packet); break;
				case DataIDs.Hotkeys: Client.Send(Packet);break;
				case DataIDs.ChangePkMode: ChangePkMode(Client, DPacket); break;
				case DataIDs.GetSurroundings: GetSuroundings(Client, DPacket); break;
				case DataIDs.ConfirmSpells: SendSpells(Client, Packet); break;
				case DataIDs.ConfirmProfinceys: SendProfs(Client, Packet); break;
				case DataIDs.ChangeAction: AppendAction(Client, DPacket, Packet); break;
				case DataIDs.ConfirmGuild: SendGuild(Client, Packet); break;
				case DataIDs.ConfirmFriends: LoadAssociates(Client, DPacket); break; //Loading Of Friends/Enemies
				case DataIDs.ChangeAngle: AppendAngle(Client, DPacket, Packet);break;
				case DataIDs.EnterPortal: AppendPortal(Client, DPacket, Packet); break;
				case DataIDs.Revive: Revive(Client, Packet); break;
				case DataIDs.Dead: Dead(Client); break;
				case DataIDs.CompleteLogin: CompleteLogin(Client); break;
				case DataIDs.DeleteCharacter: DeleteCharacter(Client, DPacket); break;
				case DataIDs.StartVend: StartVending(Client, DPacket, Packet); break;
				case DataIDs.RequestFriendInfo: GetFriendInfo(Client, DPacket); break;
				case DataIDs.RequestEnemyInfo: GetEnemyInfo(Client, DPacket); break;
				case DataIDs.Mining: Mining(Client); break;
				case DataIDs.ChangeAvatar: ChangeAvatar(Client, DPacket); break;
				case DataIDs.EndFly: EndFly(Client); break;
				case DataIDs.GuardJump: GuardJump(Client, DPacket); break;
				case DataIDs.UnTransform: UnTransform(Client); break;
                case DataIDs.RequestTeamMember: RequestTeamPosition(Client, DPacket); break;

                default: Console.WriteLine("DataPacket Type:{0}", DPacket->ID); break;
            }
        }

	    private static void RequestTeamPosition(GameClient Client, DataPacket* DPacket)
        {
            if (!Client.Team.Active)
                return;

            foreach (var Target in Client.Team.TeamDictionary.Values)
            {
                if (Target.Hero.UID == DPacket->lParam && Target.Hero.MapID.ID == Client.Hero.MapID.ID)
                {
                    DPacket->lParam = 0;
                    DPacket->wParam1 = Target.Hero.X;
                    DPacket->wParam2 = Target.Hero.Y;
                    Client.Send(PacketKernel.GetBytes(DPacket, DPacket->Size));
                }
            }
        }

	    private static void LoadAssociates(GameClient Client, DataPacket* dPtr)
		{
			var friends = new IAssociate[Client.Friends.Count];
			Client.Friends.Values.CopyTo(friends, 0);
			foreach (var friend in friends)
			{
				var fc = Kernel.FindClient(friend.UID);
				if (fc != null)
				{
					friend.Online = true;
					friend.ID = AssociationID.AddFriend;
					Client.Send(friend.GetBytes()); // tell us they're online
					IAssociate temp;
					if (fc.Friends.TryGetValue(Client.Hero.UID, out temp))
					{
						temp.Online = true;
						temp.ID = AssociationID.SetOnlineFriend;
						fc.Send(temp.GetBytes()); // tell them we're online
					}
				}
				else
				{
					friend.Online = false;
					friend.ID = AssociationID.AddFriend;
					Client.Send(friend.GetBytes()); // tell us they're offline
				}
			}
			// note: the thing with the enemy system is it isn't linear like the friend system
			// just because you have them enemied, doesn't mean they have you.
			// do not make this assumption [ever].

			var Enemys = new IAssociate[Client.Enemies.Count];
			Client.Enemies.Values.CopyTo(Enemys, 0);
			/* get our enemies */
			foreach (var enemey in Enemys)
			{
				var fc = Kernel.FindClient(enemey.UID);
				if (fc != null)
				{
					enemey.Online = true;
					enemey.ID = AssociationID.AddEnemy;
					Client.Send(enemey.GetBytes()); // tell us they're online
				}
				else
				{
					enemey.Online = false;
					enemey.ID = AssociationID.AddEnemy;
					Client.Send(enemey.GetBytes()); // tell us they're offline
				}
			}
			// tell anyone who has us enemied we're online
			foreach (var gc in Kernel.Clients)
			{
				IAssociate temp;
				if (gc.Enemies.TryGetValue(Client.Hero.UID, out temp))
				{
					temp.Online = true;
					temp.ID = AssociationID.SetOnlineEnemy;
					gc.Send(temp.GetBytes()); // tell them we're online
				}
			}
			Client.Send(PacketKernel.GetBytes(dPtr, dPtr->Size));
		}

	    private static void GetFriendInfo(GameClient Client, DataPacket* dPtr)
		{
			var uid = *(uint*)((byte*)dPtr + 12);
			IAssociate friend;
			if (Client.Friends.TryGetValue(uid, out friend))
			{
				var friendClient = Kernel.FindClient(friend.UID);
				if (friendClient != null)
					Client.Send(Packets.AssociateInfo(friendClient, false));
			}
		}

	    private static void GetEnemyInfo(GameClient Client, DataPacket* dPtr)
		{
			var uid = *(uint*)((byte*)dPtr + 12);
			IAssociate friend;
			if (Client.Enemies.TryGetValue(uid, out friend))
			{
				var friendClient = Kernel.FindClient(friend.UID);
				if (friendClient != null)
					Client.Send(Packets.AssociateInfo(friendClient, true));
			}
		}

	    private static void StartVending(GameClient Client, DataPacket* DPacket, byte[] Packet)
		{
			if (Client.Hero.Dead)
				return;
			if (Client.Vendor.IsVending)
				return;
		    if (Client.Hero.MapID != 1036)
		        return;

			Client.Vendor = new ClientVendor(Client);
			if (Client.Vendor.StartVending())
			{
				DPacket->lParam = Client.Vendor.ShopID;
				Client.Send(PacketKernel.GetBytes(DPacket, DPacket->Size));
			}
		}

	    private static void SendGuild(GameClient Client, byte[] Packet)
		{
			if (Client.Guild != null)
			{
				Client.Send(Packets.GuildInfo(Client.Guild, Client));
				var SPacket = new StringPacket((byte)Client.Guild.Name.Length);
				SPacket.ID = StringIds.GuildName;
				SPacket.UID = Client.Guild.UID;
				Client.Send(Packets.StringPacket(SPacket, Client.Guild.Name));
				if (Client.Guild.Bulletin != null)
				{
					var Respond = new MessageInfo();
					Respond.To = "ALL";
					Respond.From = "SYSTEM";
					Respond.ChatType = ChatType.GuildBulletin;
					Respond.Message = Client.Guild.Bulletin;
					Client.Send(Packets.Message(Respond));
				}
			}
			Client.Send(Packet);
		}

	    private static void UnTransform(GameClient Client)
		{
			if (Client.Hero.InTransformation)
			{
				Client.Transform.Stop();
				Client.Transform.SendUpdates();
			}
		}

	    private static void EndFly(GameClient Client)
		{
			Client.Hero.StatusFlag &= ~StatusFlag.Fly;
			Client.Send(new StatTypePacket(Client.Hero.UID, (uint)Client.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize());
		}

	    private static void ChangeAvatar(GameClient Client, DataPacket* DPacket)
		{
			if (Client.Hero.Dead)
				return;

			if (Client.Silvers >= 500)
			{
				Client.Silvers -= 500;
				Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());

				var avatar = (ushort)DPacket->lParam;
				var gender = (byte)(Client.Hero.Sex / 1000);
				if (gender == 1 && avatar < 201 || gender == 2 && avatar > 200 && avatar < 400)
				{
					Client.Hero.Avatar = avatar;
					Client.SendScreen(new StatTypePacket(Client.Hero.UID, Client.Hero.Model, StatIDs.Model).Serialize(), true);
				}
			}
		}

	    private static void Mining(GameClient Client)
		{
			if (Client.Hero.Dead)
				return;
			if (Client.Miner.Mining)
				return;

			if (!Client.Miner.Start())
				Client.Miner.Stop();
		}

	    private static void SendProfs(GameClient Client, byte[] Packet)
		{
			Client.LoadProficiencies();
			Client.Send(Packet);
		}

	    private static void SendSpells(GameClient Client, byte[] Packet)
		{
			Client.LoadSpells();
			Client.Send(Packet);
		}

	    private static void ChangePkMode(GameClient Client, DataPacket* DPacket)
		{
			Client.PkMode = (PkMode)DPacket->lParam;
			Client.Send(DPacket->Serialize());
		}

	    private static void SyncEntities(GameClient Client, uint lParam)
		{
			IBaseEntity Base = Kernel.FindEntity(lParam);
			if (Base == null)
				return;

			if (Base.EntityFlag == EntityFlag.Player || Base.EntityFlag == EntityFlag.Monster)
			{
				var entity = (Entity)Base;
				if (Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, Base.X, Base.Y) < DataStructures.ViewDistance)
				{
					Client.Screen.Add(entity);
					Client.Send(entity.SpawnPacket);
				}
			}
		}

	    private static void Dead(GameClient Client)
		{
            if (TournamentBase.Started && TournamentBase.Entries.Contains(Client))
                TournamentBase.Leave(Client);
        }

	    private static void SetLocation(GameClient Client, DataPacket* DPacket, byte[] Packet)
        {
            Client.Send(DataPacket.DataUpdate(Client.Hero.UID, Client.Hero.MapID.MapDoc, 0, Client.Hero.X, Client.Hero.Y, DataIDs.SetLocation));
        }

	    private static void GuardJump(GameClient Client, DataPacket* DPacket)
		{
			if (Client.Pet != null)
			{
				if (!Client.CurrentDMap.Invalid(DPacket->dwParam_Lo, DPacket->dwParam_Hi))
				{
					/* Make sure the guard is only jumping close to it's owner */
					if (Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, DPacket->dwParam_Lo, DPacket->dwParam_Hi) >= 12)
					{
						DPacket->dwParam_Lo = Client.Hero.X;
						DPacket->dwParam_Hi = Client.Hero.Y;
					}
				}
				else
				{
					DPacket->dwParam_Lo = Client.Hero.X;
					DPacket->dwParam_Hi = Client.Hero.Y;
				}

				Client.Pet.Facing = Kernel.GetFacing(Kernel.GetAngle(Client.Hero.X, Client.Hero.Y, DPacket->dwParam_Lo, DPacket->dwParam_Hi));
				Client.Pet.X = DPacket->dwParam_Lo;
				Client.Pet.Y = DPacket->dwParam_Hi;

				DPacket->ID = DataIDs.Jump;
				DPacket->UID = Client.Pet.UID;
				Client.SendScreen(PacketKernel.GetBytes(DPacket, DPacket->Size), true);
			}
		}

	    private static void AppendJump(GameClient Client, DataPacket* DPacket, byte[] Packet)
		{
			var TimeStamp = Native.timeGetTime();

			if (DPacket->UID != Client.Hero.UID)
				return;
			if (Client.Hero.Dead)
				return;
			if (Kernel.GetDistance(DPacket->dwParam_Lo, DPacket->dwParam_Hi, Client.Hero.X, Client.Hero.Y) > 16)
			{
				Client.PullBack("Pullback - Jump too far.");
				return;
			}
			#region Disable Mining
			if (Client.Miner.Mining)
				Client.Miner.Stop();
			#endregion
			#region Speeder Check(s)
			if (!Client.TimeFraud.Validate(TimeStamp, "ActionQuery"))
				return;
			if ((Client.Hero.StatusFlag != StatusFlag.Cyclone) && (Client.Hero.OverlappingMesh != 207) // DivineHare
			    && !Client.TimeFraud[TimestampFraud.Jump].ValidateFrame(TimeStamp, 410))
			{
				Client.PullBack("Pullback - possible speed hacking!");
				return;
			}
			if (Client.CurrentDMap.Invalid(DPacket->dwParam_Lo, DPacket->dwParam_Hi))
			{
				Client.PullBack("Pullback - invalid coordinate!");
				return;
			}
            #endregion
            #region Ancient Devil
            if (!Client.FloorEffect && Client.Hero.MapID == 1052)
            {
                var Distance = Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, 218, 208);
                if (Distance < DataStructures.ViewDistance)
                {
                    Client.FloorEffect = true;
                    var GDrop = new ItemGroundPacket((uint)RandomGenerator.Generator.Next(), 20, GroundIDs.GroundEffect, 215, 210);
                    Client.Send(GDrop.Serialize());
                }
            }
            if (DPacket->dwParam_Lo == 215 && DPacket->dwParam_Hi == 210)
            {
                uint[] req = { 710011, 710012, 710013, 710014, 710015 };
                var cont = req.All(element => Client.CheckMultiples(req));
                if (cont)
                {
                    // Remove Items
                    foreach (var item in req)
                    {
                        Client.RemoveInventory(item);
                    }
                    // Spawn Boss
                    if (!Kernel.AncientDevil)
                    {
                        Kernel.AncientDevil = true;
                        Kernel.SpawnMonster(9999, 218, 208, 1052);
                    }
                }
            }
            #endregion
            Client.Hero.Facing = Kernel.GetFacing(Kernel.GetAngle(Client.Hero.X, Client.Hero.Y, DPacket->dwParam_Lo, DPacket->dwParam_Hi));

			if (Client.Hero.MapID.ID == 1038)
			{
				if (!GuildWar.ValidJump(Client.TileColor, out Client.TileColor, DPacket->dwParam_Lo, DPacket->dwParam_Hi))
				{
					Client.PullBack("Please, only walk to this area.");
					return;
				}
			}
			Client.SendScreen(Packet, true);
			Client.Hero.X = DPacket->dwParam_Lo;
			Client.Hero.Y = DPacket->dwParam_Hi;
			Client.Screen.Reload(false, null);
		}

	    private static void CompleteLogin(GameClient Client)
		{
			if (!Client.LoggedIn)
			{
				Client.LoggedIn = true;
				Client.LoadEquipment();
				Client.CalculateStatBonus();
				#region Nobility
				Client.NobleInfo = MySqlDatabase.LoadNobilityInfo(Client);
				if (Client.NobleInfo == null)
				{
					Client.NobleInfo = new NobilityInfo();
					Client.NobleInfo.Donation = 0;
					Client.NobleInfo.UID = Client.Hero.UID;
					Client.NobleInfo.playerName = Client.Hero.Name;
				}

                if(Client.Hero.Level >= 70)
                {
	                Client.Send(NobilityPacket.UpdateIcon(Client));
					fixed (byte* ptr = Client.Hero.SpawnPacket)
						*(sbyte*)(ptr + 84) = Client.NobleMedal;
				}
                #endregion
                #region Stamina
                Client.Stamina = Client.MaxStamina;
				Client.Send(new StatTypePacket(Client.Hero.UID, Client.Stamina, StatIDs.Stamina).Serialize());
				#endregion
				#region Merchant
				switch (Client.Merchant)
				{
					case MerchantTypes.Yes:
						Client.Send(new StatTypePacket(Client.Hero.UID, (uint)MerchantTypes.Yes, StatIDs.Merchant).Serialize());
						break;
					case MerchantTypes.Asking:
						Client.Send(Packets.Attack(Client.Hero.UID, 0, 0, 0, 0, AttackType.RequestMerchant));
						break;
				}
				#endregion
				#region Revive Player if Dead
				if (Client.Hero.Hitpoints <= 0)
					Client.Revive(Client, true);
				#endregion
				#region Welcome Messages
				Client.Speak(Color.White, ChatType.Talk, string.Format("Welcome {0}, we are glad to have you with us!", Client.Hero.Name));
				Client.Speak(Color.White, ChatType.Talk, "Stay in touch via our Discord Channel!");
				Client.Speak(Color.White, ChatType.Talk, "Current Players Online: " + Kernel.GameClients.Count);
				#endregion
				#region Prize
				if (Client.PrizeNPC > 0)
					Client.Speak(Color.White, ChatType.Center, "You have a prize waiting for you! Please go to the PrizeNPC in Market to retrieve it!");
                #endregion
                #region HeavensBlessing
                if (Client.Hero.Stamps.HeavenBless > DateTime.Now)
                {
                    Client.Hero.StatusFlag |= StatusFlag.Blessing;

                    var SPacket = new StringPacket(5);
                    SPacket.UID = Client.Hero.UID;
                    SPacket.ID = StringIds.Effect;
                    Client.Send(Packets.StringPacket(SPacket, "bless"));

                    Client.Send(new StatTypePacket(Client.Hero.UID, (ulong)Client.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize());
                    Client.Send(new StatTypePacket(Client.Hero.UID, (ulong)Kernel.SecondsFromNow(Client.Hero.Stamps.HeavenBless), StatIDs.HeavenBlessing).Serialize());

                    if (Client.Hero.MapID != 601)
                        Client.Send(new StatTypePacket(Client.Hero.UID, 0, StatIDs.OnlineTraining).Serialize());
                    else
                        Client.Send(new StatTypePacket(Client.Hero.UID, 1, StatIDs.OnlineTraining).Serialize());
                }
                #endregion
                #region Exp Potion
                if (Client.Hero.Stamps.ExpPotion > DateTime.Now)
                {
                    Client.Send(new StatTypePacket(Client.Hero.UID, (uint)Kernel.SecondsFromNow(Client.Hero.Stamps.ExpPotion), StatIDs.DoubleExpTime).Serialize());
                }
                #endregion
                #region OTG
                if (Client.Hero.Stamps.OfflineTG != DateTime.MinValue)
				{
					Client.Teleport(601, 60, 54);
				}
                #endregion
                Client.Send(new StatTypePacket(Client.Hero.UID, 0, StatIDs.VIPLevel).Serialize());
            }
		}

	    private static void GetSuroundings(GameClient Client, DataPacket* Packet)
		{
			var Settings = new MapSettings(Client.Hero.MapID.ID);
			Packet->ID = DataIDs.SetMapColor;
			Packet->lParam = Settings.Color;
			Client.Send(PacketKernel.GetBytes(Packet, Packet->Size));

			var status = MapStatusPacket.Create();
			status.MapID = Client.Hero.MapID.MapDoc;
			status.DynamicMapID = Client.Hero.MapID.DynamicID;
			status.Status = Settings.Status;
			Client.Send(status, status.Size);

			if (Client.Vendor.IsVending)
				Client.Vendor.StopVending();

			Client.Screen.Reload(true, delegate(IBaseEntity Sender, IBaseEntity Caller)
				{
					if ((Caller.EntityFlag == EntityFlag.Player) && (Sender.EntityFlag == EntityFlag.Player))
						Client.Hero.SendSpawn(Caller.Owner as GameClient);
					else if ((Caller.EntityFlag == EntityFlag.Player) && (Sender.EntityFlag == EntityFlag.Monster))
						Client.Hero.SendSpawn(Caller.Owner as GameClient);
					return 0;
				});
		}

	    private static void AppendAction(GameClient Client, DataPacket* DPacket, byte[] Packet)
		{
			#region Checks
			if (!Stamps.Check(Client.Hero.Stamps.CanShowCool))
				return;
			if (Client.Miner.Mining)
				Client.Miner.Stop();
			if (Client.Hero.Dead)
				return;
			if (DPacket->UID != Client.Hero.UID)
				return;
			#endregion
			if (Client.Hero.Action != (ConquerAction)DPacket->lParam)
			{
				Client.Hero.Action = (ConquerAction)DPacket->lParam;
                switch (Client.Hero.Action)
                {
                    #region Cool Effect
                    case ConquerAction.Cool:
                        if (Client.IsFullSuper())
                            *&DPacket->lParam |= (uint)(Client.Job * 0x00010000 + 0x01000000);
                        else
                        {
                            var armor = Client.GetEquipmentItem(ItemPosition.Armor);
                            if (armor == null)
                                return;
                            if (ConquerItem.GetQuality(armor.ID) == 9)
                                *&DPacket->lParam |= (uint)(Client.Job * 0x00010000);
                        }
                        Client.Hero.Stamps.CanShowCool = DateTime.Now.AddMilliseconds(1500);
                        break;
                        #endregion
                }
				Client.SendScreen(Packet, true);
			}
		}

	    private static void AppendAngle(GameClient Client, DataPacket* DPacket, byte[] Packet)
		{
			if (DPacket->UID != Client.Hero.UID)
				return;

			Client.Hero.Facing = (ConquerAngle)(DPacket->lParam % 8);
			Client.SendScreen(Packet, false);
		}

	    private static void AppendPortal(GameClient Client, DataPacket* DPacket, byte[] Packet)
		{
		    var X = DPacket->dwParam_Lo;
		    var Y = DPacket->dwParam_Hi;

            uint destMapId;
		    ushort destX, destY;
            if (FlatDatabase.LoadPortal(Client, X, Y, out destMapId, out destX, out destY))
				Client.Teleport(destMapId, destX, destY);
			else
			{
                for (byte i = 0; i < 3; i++)
                    Kernel.IncXY(ConquerAngle.South, ref X, ref Y);

                Client.Hero.X = X;
                Client.Hero.Y = Y;
                Client.PullBack($"Invalid Portal MapID: {Client.Hero.MapID} X: {Client.Hero.X} Y: {Client.Hero.Y} Please report this.");
			}
		}

	    public static void Revive(GameClient Client, byte[] Packet)
		{
			if (Stamps.Check(Client.Hero.Stamps.Revive))
			{
				var reviveHere = Packet[12] == 1;
				if (reviveHere && (Client.Hero.MapID.ID == 1038 && GuildWar.Active || Client.Hero.Stamps.HeavenBless > DateTime.Now))
					reviveHere = false;

				Client.Revive(Client, reviveHere);
			}
		}

	    private static void DeleteCharacter(GameClient Client, DataPacket* DPacket)
		{
			if (DPacket->lParam.ToString() != Client.Warehouse.Password && Client.Warehouse.Password != "")
				return;

			MySqlDatabase.DeleteCharacter(Client);

			Client.LoggedIn = false;
			Client.Socket.Disconnect();
		}
	}
}