﻿using System.Drawing;
using MySql.Data.MySqlClient;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.PacketHandling
{
	public unsafe partial class PacketThread
	{
	    public static void AssociateProcessor(GameClient Client, byte* Ptr)
		{
			var ap = (AssociatePacket*)Ptr;
			switch (ap->ID)
			{
				case AssociationID.RequestFriend: RequestFriend(Client, ap); break;
				case AssociationID.RemoveFriend: RemoveFriend(Client, ap); break;
				case AssociationID.RemoveEnemy: RemoveEnemy(Client, ap); break;
			}
		}

	    private static void RequestFriend(GameClient Client, AssociatePacket* Packet)
		{
			if (!Client.Friends.ContainsKey(Packet->UID))
			{
                var request = Kernel.FindClient(Packet->UID);
			    if (request == null)
			        return;
                if (Packet->UID == Client.Hero.UID)
                    return;

                if (request.PendingFriendUID != Client.Hero.UID)
				{
					Client.PendingFriendUID = request.Hero.UID;
					Packet->UID = Client.Hero.UID;
					Packet->Name = Client.Hero.Name;
					request.Send(PacketKernel.GetBytes(Packet, Packet->Size));
				}
				else
				{
					if (!request.Friends.ContainsKey(Client.Hero.UID))
					{
						if ((request.Friends.Count < 50) && (Client.Friends.Count < 50))
						{
							request.AddFriend(Client.Hero.UID, Client.Hero.Name);
							Client.AddFriend(request.Hero.UID, request.Hero.Name);
						}
						else
						{
							request.Speak(Color.Red, ChatType.TopLeft, "Friends list full :(");
							Client.Speak(Color.Red, ChatType.TopLeft, "Friends list full :(");
						}
					}
				}
			}
		}

	    private static void RemoveFriend(GameClient Client, AssociatePacket* Packet)
		{
			IAssociate ClientAssociate;
			IAssociate friendAssociate;

			if (Client.Friends.TryGetValue(Packet->UID, out friendAssociate))
			{
				var friend = Kernel.FindClient(Packet->UID);

				using (var m_Conn = new MySqlConnection(MySqlDatabase.ConnectionString))
				{
					m_Conn.Open();

					MySqlDatabase.DeleteFriend(Packet->UID, Client.Hero.Name);
					MySqlDatabase.DeleteFriend(Client.Hero.UID, friendAssociate.Name);
				}

				if (friend != null)
				{
					if (friend.Friends.TryGetValue(Client.Hero.UID, out ClientAssociate))
					{
						friend.Friends.Remove(Client.Hero.UID);
						Packet->UID = Client.Hero.UID;
						friend.Send(PacketKernel.GetBytes(Packet, Packet->Size));
						Packet->UID = friend.Hero.UID;
					}
				}
				Client.Friends.Remove(Packet->UID);
			}
			Client.Send(PacketKernel.GetBytes(Packet, Packet->Size));
		}

	    private static void RemoveEnemy(GameClient Client, AssociatePacket* Packet)
		{
			IAssociate friendAssociate;
			if (Client.Enemies.TryGetValue(Packet->UID, out friendAssociate))
			{
				MySqlDatabase.DeleteEnemey(Packet->UID, Client.Hero.Name);
				Client.Enemies.Remove(Packet->UID);
			}
			Client.Send(PacketKernel.GetBytes(Packet, Packet->Size));
		}
	}
}