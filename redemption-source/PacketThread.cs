﻿using System;
using System.Drawing;
using System.Linq;
using ConquerEmulator.Attacking;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Events;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Commands;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;
using ConquerEmulator.Npc;

namespace ConquerEmulator.PacketHandling
{
	public unsafe partial class PacketThread
	{
		public static void Process(GameClient Client, byte[] Packet, byte* Ptr)
		{
			var Type = *(ushort*)(Ptr + 2);
			switch (Type)
			{
				case 0x3E9: CreateCharacter(Client, Packet); break;
				case 0x41C: PacketLanguage(Client, Ptr); break;
				case 0x3EC: Talk(Client, Packet); break;
				case 0x400: DistributeStat(Client, (DistributeStatPacket*)Ptr); break;
				case 0x2715: Walk(Client, (MovementPacket*)Ptr, Packet); break;
				case 0x3FE: HandleAttack(Client, Ptr); break;
				case 0x802: AppendBroadCast(Client, Ptr); break;
				case 0x457: MessageBoard(Client, Packet); break;
				case 0x44D: PickUpItem(Client, Ptr);	break;
				case 0x7EF: NpcStartup(Client, Packet, Ptr); break;
				case 0x7F0:	NpcContinue(Client, Packet, Ptr); break;
				case 0x7F4: ComposeItems(Client, (ComposeItemPacket*)Ptr); break;
				case 0x453: GuildProcessor(Client, Packet, Ptr); break;
                case 0x458: AppendGuildMemberInfo(Client, Packet); break;
                case 0x3FB: AssociateProcessor(Client, Ptr); break;
				case 0x271A: GeneralData(Client, Packet, Ptr); break;
				case 0x3F1: ItemUsageProcessor(Client, Packet, Ptr); break;
				case 0x44E: WarehouseProcessor(Client, Ptr); break;
				case 0x3FF: TeamProcessor(Client, Packet, Ptr); break;
				case 0x420: TradeProcessor(Client, Packet, Ptr); break;
				case 0x3F7: StringProcessor(Client, Packet, Ptr); break;
				case 0x403: SocketProcesser(Client, Packet, Ptr); break;
                case 0x7FC: OfflineTgProcessor(Client, Packet, Ptr); break;
                case 0x800: ItemLock(Client, Packet); break;
                case 0x810: NobilityProcessor(Client, Ptr); break;

                default: Console.WriteLine("Global Packet Type (Hex:0x{0:X}) \\ (Dec:{1})", Type, Type); break;
			}
		}

        #region Offline TG
        private static void OfflineTgProcessor(GameClient Client, byte[] packet, byte* ptr)
        {
            OfflineTgPacket* Packet = (OfflineTgPacket*)ptr;
            ulong Time;
            switch (Packet->PacketType)
            {
                case OfflineTgPacketType.RequestTime:
					{
						Time = (ulong)Math.Min(900, (DateTime.Now.Ticks - Client.Hero.Stamps.LoginTime.Ticks) / TimeSpan.TicksPerMinute * 10);

						Client.OfflineTrainingTime = (uint)Time;
						Packet->TrainingTime = Time;

						Client.Send(Packet->Serialize());

						break;
					}
                case OfflineTgPacketType.RequestEnter:
					{
						if (Client.Hero.Stamps.HeavenBless > DateTime.Now && Client.Hero.MapID != 1036 && Client.Hero.MapID != 1039)
						{
							Time = (ulong)Math.Min(900, ((DateTime.Now.Ticks - Client.Hero.Stamps.LoginTime.Ticks) / TimeSpan.TicksPerMinute) * 10);

							Client.OfflineTrainingTime = Math.Min(900, Client.OfflineTrainingTime += (uint)Time);

							Client.Hero.Stamps.OfflineTG = DateTime.Now;
							Client.Send(packet);
						}
						else
							Client.Speak(Color.Red, ChatType.TopLeft, "You may not enter Offline Training Grounds.");

						break;
					}
                case OfflineTgPacketType.RequestExperience:
		            {
			            Client.SendOtgResults();
						break;
					}
                case OfflineTgPacketType.ClaimRewards:
					{
						Time = (ulong)Math.Min(Client.OfflineTrainingTime, DateTime.Now.Subtract(Client.Hero.Stamps.OfflineTG).TotalMinutes);

						Client.GainExpBall(Time);

						Client.OfflineTrainingTime = Math.Max(0, Client.OfflineTrainingTime -= Time);

						Client.Hero.Stamps.OfflineTG = DateTime.MinValue;
						Client.Teleport(1002, 400, 400);
						break;
					}
            }
        }
        #endregion
        #region Nobility
        private static void NobilityProcessor(GameClient client, NobilityPacket Packet)
        {
            switch (Packet.Type)
            {
				case NobilityAction.Donate:
		            uint Donation = (uint)Packet.Donation;

                    if (client.Silvers >= Donation)
                    {
                        client.Silvers -= Donation;
                        client.NobleDonation += Donation;
                    }
                    else if (client.ConquerPoints >= Donation / 50000)
                    {
                        client.ConquerPoints -= (Donation / 50000);
                        client.NobleDonation += Donation;
                    }
                    client.Send(NobilityPacket.UpdateIcon(client));
                    break;
				case NobilityAction.List:
		            Packet.Strings = ConquerNobility.GetPage((int)Packet.Data1);
		            client.Send(Packet);
					break;
				case NobilityAction.QueryRemainingSilver:
                    uint Upper = 3000000;
                    switch(Packet.Data1)
                    {
                        case 12://King
                            if (ConquerNobility.AllRanks.Count >= 3)
                                Upper = ConquerNobility.AllRanks[2].Donation;
                            break;
                        case 9://Prince
                            if (ConquerNobility.AllRanks.Count >= 12)
                                Upper = ConquerNobility.AllRanks[11].Donation;
                            break;
                        case 7://Duke
                            if (ConquerNobility.AllRanks.Count >= 35)
                                Upper = ConquerNobility.AllRanks[34].Donation;
                            break;
                        case 5://Earl
                            Upper = 200000000;
                            break;
                        case 3://Baron
                            Upper = 100000000;
                            break;
                        case 1://Knight
                            if (ConquerNobility.AllRanks.Count >= 3)
                                Upper = 30000000;
                            break;
                    }
                    Packet.Data1 = Math.Max(Upper - client.NobleDonation, 3000000) + 1;
		            client.Send(Packet);
                    break;
            }
        }
        #endregion
        #region Compose
        public static void ComposeItems(GameClient Client, ComposeItemPacket* Packet)
		{
			var main = Client.GetInventoryItem(Packet->MainItem);
			var minor1 = Client.GetInventoryItem(Packet->MinorItem1);
			var minor2 = Client.GetInventoryItem(Packet->MinorItem2);

			if ((main == null) || (minor1 == null) || (minor2 == null))
				return;
			if ((main.UID == minor1.UID) || (main.UID == minor2.UID))
				return;
			if (minor1.UID == minor2.UID)
				return;
			if (!main.IsEquipment)
				return;

			var MainType = ConquerItem.GetItemType(main.ID);
			var Minor1_Type = ConquerItem.GetItemType(minor1.ID);
			var Minor2_Type = ConquerItem.GetItemType(minor2.ID);

            //Plus Stones
            if ((Minor1_Type != 730) && (Minor2_Type != 730))
            {
                if (!minor1.IsEquipment || minor2.IsEquipment)
                    return;

                // Bow
                if (((MainType == 500) && (Minor1_Type != 500)) || (Minor2_Type != 500))
                    return;

                // Backsword
                if (((MainType == 421) && (Minor1_Type != 421)) || (Minor2_Type != 421))
                    return;

                // 1Handed Weapon
                if (((MainType / 100 == 4) && (Minor1_Type / 100 != 4)) || (Minor2_Type / 100 != 4))
                    return;

                // 2Handed Weapon
                if (((MainType / 100 == 5) && (Minor1_Type / 100 != 5)) || (Minor2_Type / 100 != 5))
                    return;

                // Shield
                if (((MainType / 100 == 9) && (Minor1_Type / 100 != 9)) || (Minor2_Type / 100 != 9))
                    return;

                // Armor, Necklace, Boots and Helmet
                if ((MainType / 100 != 4) && (MainType / 100 != 5) && (MainType / 100 != 9))
                {
                    if ((MainType != Minor1_Type) || (MainType != Minor2_Type))
                        return;
                }
            }

			if (main.Plus >= 9)
			{
				Client.Speak(Color.Red, ChatType.TopRight, "You cannot upgrade your item any further.");
				return;
			}
			if ((minor1.Plus == 0) || (minor2.Plus == 0) || (minor1.Plus != minor2.Plus))
			{
				Client.Speak(Color.Red, ChatType.TopRight, "You must insert two minor items of equal value.");
				return;
			}
			if (minor1.Plus - main.Plus > 0)
			{
				main.Plus = minor1.Plus;
				main.SendInventoryUpdate(Client);
			}
			else
			{
				main.Plus += 1;
				main.SendInventoryUpdate(Client);
			}
			Client.RemoveInventory(minor1);
			Client.RemoveInventory(minor2);
		}
		#endregion
		#region MessageBoard
		private static void MessageBoard(GameClient Client, byte[] Packet)
		{
			fixed (byte* Buffer = Packet)
			{
				var Index = *(ushort*)(Buffer + 4);
				var ChatType = *(ChatType*)(Buffer + 6);
				var Action = (ActionType)Buffer[8];
				string Author = null;

                if (Action == ActionType.Del || Action == ActionType.GetWords)
                {
                    Author = PacketKernel.Decode(Packet, 11, 11 + Packet.Length - 19);
                    if (Author == null)
                        return;
                }

                MessageBoard board = null;
				switch (ChatType)
				{
					case ChatType.TradeBoard: board = Kernel.TradeBoard; break;
					case ChatType.FriendBoard: board = Kernel.FriendBoard; break;
					case ChatType.TeamBoard: board = Kernel.TeamBoard; break;
					case ChatType.GuildBoard: board = Kernel.SynBoard; break;
					case ChatType.OthersBoard: board = Kernel.OtherBoard; break;
					case ChatType.SystemBoard: board = Kernel.SystemBoard; break;
				}

				if (board == null)
					return;

				switch (Action)
				{
					case ActionType.Del:
                        {
                            if (Author != Client.Hero.Name && Client.Flag != PlayerFlag.ServerAdmin)
                                return;

                            var message = board.GetMsgInfoByAuthor(Author);
                            board.Delete(message);
                            break;
                        }
					case ActionType.GetList:
						{
							var List = board.GetList(Index);
							if (List != null)
								Client.Send(Packets.GetMessageBoardList(Index, ChatType, List, ActionType.List));
							break;
						}
					case ActionType.GetWords:
						{
							var Message = board.GetWords(Author);

							var Notify = new MessageInfo
								             {
									             Message = Message,
									             From = Author,
									             To = Client.Hero.Name,
									             ChatType = ChatType
								             };
							Client.Send(Packets.Message(Notify));
							break;
						}
				}
			}
		}
		#endregion
		#region BroadCast
		public static void AppendBroadCast(GameClient Client, byte* Packet)
		{
			if (Packet[4] == 0x03)
			{
				if (Client.Trade.Trading)
					return;
				if (Client.Hero.Level < 50)
					return;

				if (Client.Flag == PlayerFlag.Normal)
				{
					if (!Stamps.Check(Client.Hero.Stamps.BroadCast))
						return;
					if (Client.ConquerPoints <= 200)
						return;

					Client.ConquerPoints -= 200;
					Client.Hero.Stamps.BroadCast = DateTime.Now.AddSeconds(15);
				}

				var Msg = new MessageInfo();
				Msg.ChatType = ChatType.Broadcast;
				Msg.Message = new string((sbyte*)(Packet + 14), 0, *(Packet + 13));
				Msg.From = Client.Hero.Name;
				GameClient.SpeakToAll(Client, Color.White, ChatType.Broadcast, Msg.Message);
			}
		}
		#endregion
		#region DistrubuteStats
		private static void DistributeStat(GameClient Client, DistributeStatPacket* Packet)
		{
			if (Client.StatPoints > 0)
			{
				Client.StatPoints -= 1;

				if (Packet->Strength)
					Client.Strength++;
				else if (Packet->Agility)
					Client.Agility++;
				else if (Packet->Spirit)
					Client.Spirit++;
				else if (Packet->Vitality)
					Client.Vitality++;

				Client.CalculateStatBonus();

				var big = new BigUpdatePacket(7);
				big.UID = Client.Hero.UID;
				big.AllStats(Client, 0);
				big.HitpointsAndMana(Client, 5);
				Client.Send(big);
			}
		}
		#endregion
		#region ItemGroundPacket
		private static void PickUpItem(GameClient Client, byte* Packet)
		{
			var GPacket = (ItemGroundPacket*)Packet;

			ConquerItem Item;
			if (Kernel.GroundItems.TryGetValue(GPacket->UID, out Item))
				FloorItem.PickupItem(Client, Item);
		}
		#endregion
		#region Game
		private static void PacketLanguage(GameClient Client, byte* Packet)
		{
			var Identifier = *(uint*)(Packet + 8);
			var PasswordCheckSum = *(uint*)(Packet + 4);

			var Respond = new MessageInfo();
			Respond.To = "ALLUSERS";
			Respond.From = "SYSTEM";
			Respond.ChatType = ChatType.LoginInformation;
			if (!MySqlDatabase.Authenticate(Client, Identifier, PasswordCheckSum))
			{
				Respond.Message = "Failed to authenticate.";
				Client.Send(Packets.Message(Respond));
				return;
			}
			if (Kernel.BannedIPs.Contains(Client.Socket.RemoteAddress))
			{
				Respond.Message = "Your IP Address has been permenently banned.";
				Client.Send(Packets.Message(Respond));
				return;
			}
			#region LoginAction
			switch (Client.LoginAction)
			{
				case LoginAction.Normal:
					MySqlDatabase.LoadCharacter(Client);
					if (Kernel.FindClient(Client.Hero.UID) != null)
					{
						Client.Socket.Disconnect();
						return;
					}
					Kernel.UpdateGameClients();

					Respond.dwParam = Client.Hero.UID;
					Respond.Message = "ANSWER_OK";

                    DateTimePacket date = DateTimePacket.Create();
                    Client.Send(Packets.Message(Respond));
					Client.Send(Packets.CharacterInfo(Client));
                    Client.Send(date, date.Size);
                    Console.WriteLine("{0} has logged into the server.", Client.Hero.Name);

					Client.Hero.Stamps.Protection = DateTime.Now.AddSeconds(5.0);
					break;

				case LoginAction.Create:
					Respond.Message = "NEW_ROLE";
					Client.Send(Packets.Message(Respond));
					break;

				case LoginAction.Banned:
					Respond.Message = "This character has been banned.";
					Client.Send(Packets.Message(Respond));
					break;
			}
			#endregion
		}
		#endregion
		#region NpcPacket
		private static void NpcStartup(GameClient Client, byte[] NPacket, byte* Ptr)
		{
			var Packet = (NpcPacket*)Ptr;
			if (Packet->NpcID == 0)
				return;
			Client.LastNpc = Packet->NpcID;

			var Npc = Client.Screen.FindObject(Packet->NpcID) as INpc;
			if (Npc != null)
				NpcDialog.HandleNpcDialog(Client, Npc, 0, NPacket);
		}

		private static void NpcContinue(GameClient Client, byte[] NPacket, byte* Ptr)
		{
			var Packet = (NpcPacket*)Ptr;
			if (Packet->NpcID == 0 && Packet->OptionID == 255)
				return;
			if (Packet->NpcID == 0)
				Packet->NpcID = Client.LastNpc;

			var Npc = Client.Screen.FindObject(Packet->NpcID) as INpc;
			if (Npc != null || NPacket[11] == 102)
				NpcDialog.HandleNpcDialog(Client, Npc, Packet->OptionID, NPacket);
		}
        #endregion
        #region Other
        public static void AppendGuildMemberInfo(GameClient Client, byte[] Packet)
        {
            var memberName = PacketKernel.Decode(Packet, 9, 9 + 15);
            GameClient member = Kernel.FindClient(memberName);
            if (member != null)
            {
                fixed (byte* p = Packet)
                {
                    *((uint*)(p + 4)) = member.GuildDonation;
                    *(p + 8) = (byte)member.Hero.GuildRank;
                }
                Client.Send(Packet);
            }
        }
        private static void ItemLock(GameClient Client, byte[] Packet)
		{
			fixed (byte* Buffer = Packet)
			{
				var itemUID = *(uint*)(Buffer + 4);

				var item = Client.Inventory.FirstOrDefault(x => x.UID == itemUID);
				if (item != null)
				{
					item.Locked = (byte)(item.Locked == 0 ? 1 : 0);
					item.SendInventoryUpdate(Client);

					if (item.Locked == 1)
						Buffer[9] = 1;
					Client.Send(Packet);
				}
			}
		}

		private static void Talk(GameClient Client, byte[] Packet)
		{
			var Message = new MessageInfo(Packet);
			#region Checks
			if (Message.From != Client.Hero.Name)
				return;
			if ((Message.Message.Length < 1) || (Message.Message.Length > 255))
				return;
			if ((Message.From.Length > 15) || (Message.To.Length > 15))
				return;

			if (Native.timeGetTime() <= Client.Hero.Stamps.LastMessage + 500)
			{
				Client.Speak(Color.Turquoise, ChatType.TopLeft, "Please wait to send another message.");
				return;
			}
			#endregion
			if (ConquerCommand.Parse(Client, Message.Message))
				return;
            if ((Message.ChatType != ChatType.Ghost) && (Message.ChatType != ChatType.Team))
            {
                if (Client.Hero.Dead)
                    return;
            }

			switch (Message.ChatType)
			{
				case ChatType.Talk:
				case ChatType.Ghost:
					{
						Client.SendScreen(Packet, false);
						break;
					}
				case ChatType.Whisper:
					{
						var tClient = Kernel.FindClient(Message.To);
						if (tClient == null)
							Client.Speak(Color.Red, ChatType.Talk, "Player isn't online.");
						else
							tClient.Send(Packets.Message(Message));
						break;
					}
				case ChatType.Friend:
					{
						foreach (var gc in Kernel.Clients)
						{
							if (Client.Friends.ContainsKey(gc.Hero.UID))
								gc.Send(Packets.Message(Message));
						}
						break;
					}
                case ChatType.GuildBulletin:
                    {
                        if (Client.Hero.GuildID != 0 && Client.Hero.GuildRank == GuildPosition.Leader)
                        {
                            foreach (var member in Client.Guild.Members.Values)
                            {
                                var Content = member.Split('~');
                                var Member = Kernel.FindClient(uint.Parse(Content[1]));
                                Member.Guild.Bulletin = Message.Message;
                                Member.Send(Packet);
                            }
                        }
                        break;
                    }
                case ChatType.Guild:
                    {
                        if (Client.Guild.UID != 0)
                        {
                            Client.Guild.SendGuildMsg(Client, Message.Message);
                        }
                        break;
                    }
                case ChatType.Team:
					{
                        if (Client.Team.Active)
                        {
                            foreach (var client in Client.Team.Teammates)
                            {
                                if ((client != null) && (client.Hero.UID != Client.Hero.UID))
                                    client.Send(Packet);
                            }
                        }
						break;
					}
				case ChatType.Service:
					{
						if (Native.timeGetTime() <= Client.Hero.Stamps.LastServiceMessage + 120000)
						{
							GameClient.SpeakToAll(Client, Color.Yellow, ChatType.Service, Message.Message);
							Client.Hero.Stamps.LastServiceMessage = Native.timeGetTime();
						}
						break;
					}
				case ChatType.TradeBoard:
					{
						var message = Kernel.TradeBoard.GetMsgInfoByAuthor(Client.Hero.Name);

						Kernel.TradeBoard.Delete(message);
						Kernel.TradeBoard.Add(Client.Hero.Name, Message.Message);
						break;
					}
				case ChatType.FriendBoard:
					{
						var message = Kernel.FriendBoard.GetMsgInfoByAuthor(Client.Hero.Name);

						Kernel.FriendBoard.Delete(message);
						Kernel.FriendBoard.Add(Client.Hero.Name, Message.Message);
						break;
					}
				case ChatType.TeamBoard:
					{
						var message = Kernel.TeamBoard.GetMsgInfoByAuthor(Client.Hero.Name);

						Kernel.TeamBoard.Delete(message);
						Kernel.TeamBoard.Add(Client.Hero.Name, Message.Message);
						break;
					}
				case ChatType.GuildBoard:
					{
						var message = Kernel.SynBoard.GetMsgInfoByAuthor(Client.Hero.Name);

						Kernel.SynBoard.Delete(message);
						Kernel.SynBoard.Add(Client.Hero.Name, Message.Message);
						break;
					}
				case ChatType.OthersBoard:
					{
						var message = Kernel.OtherBoard.GetMsgInfoByAuthor(Client.Hero.Name);

						Kernel.OtherBoard.Delete(message);
						Kernel.OtherBoard.Add(Client.Hero.Name, Message.Message);
						break;
					}
				case ChatType.SystemBoard:
					{
						var message = Kernel.SystemBoard.GetMsgInfoByAuthor(Client.Hero.Name);

						Kernel.SystemBoard.Delete(message);
						Kernel.SystemBoard.Add(Client.Hero.Name, Message.Message);
						break;
					}
			}
		}

		private static void Walk(GameClient Client, MovementPacket* MovementPacket, byte[] Packet)
		{
			var TimeStamp = Native.timeGetTime();
			var petUID = false;
			if (Client.Pet != null)
				petUID = MovementPacket->UID == Client.Pet.UID;
			if (!petUID && (MovementPacket->UID != Client.Hero.UID))
				return;

            ushort walkX, walkY;
            var dir = (ushort)MovementPacket->Direction % 8;
            if (!petUID)
            {
                if (Client.Miner.Mining)
                    Client.Miner.Stop();

                if (!Client.TimeFraud.Validate(TimeStamp, "WalkQuery"))
                    return;

                walkX = Client.Hero.X;
                walkY = Client.Hero.Y;
                Client.Hero.Facing = (ConquerAngle)dir;
            }
            else
            {
                walkX = Client.Pet.X;
                walkY = Client.Pet.Y;
                Client.Pet.Facing = (ConquerAngle)dir;
            }
            Kernel.IncXY((ConquerAngle)dir, ref walkX, ref walkY);

            #region Ancient Devil
            if (!Client.FloorEffect && Client.Hero.MapID == 1052)
            {
                var Distance = Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, 218, 208);
                if (Distance < DataStructures.ViewDistance)
                {
                    Client.FloorEffect = true;
                    var GDrop = new ItemGroundPacket((uint)RandomGenerator.Generator.Next(), 20, GroundIDs.GroundEffect, 215, 210);
                    Client.Send(GDrop.Serialize());
                }
            }
            if (walkX == 215 && walkY == 210)
            {
                uint[] req = { 710011, 710012, 710013, 710014, 710015 };
                var cont = req.All(element => Client.CheckMultiples(req));
                if (cont)
                {
                    // Remove Items
                    foreach (var item in req)
                    {
                        Client.RemoveInventory(item);
                    }
                    // Spawn Boss
                    if (!Kernel.AncientDevil)
                    {
                        Kernel.AncientDevil = true;
                        Kernel.SpawnMonster(9999, 218, 208, 1052);
                    }
                }
            }
            #endregion
            if (!petUID)
			{
                #region GuildWar
                if (Client.Hero.MapID == 1038)
                {
                    if (!GuildWar.ValidWalk(Client.TileColor, out Client.TileColor, Client.Hero.X, Client.Hero.Y))
                    {
                        Client.PullBack("No.");
                        return;
                    }
                }
				#endregion
				if (!Client.CurrentDMap.Invalid(walkX, walkY))
				{
				    Client.Hero.X = walkX;
				    Client.Hero.Y = walkY;

					Client.SendScreen(Packet, true);
					Client.Screen.Reload(false, null);
					Client.Hero.Action = ConquerAction.None;
				}
				else
				{
					Client.PullBack("Pullback - Invalid coordinates.");
				}
			}
			else
            {
                Client.Pet.X = walkX;
                Client.Pet.Y = walkY;
			    Client.SendScreen(Packet, true);
			}
		}

		private static void HandleAttack(GameClient Client, byte* Packet)
		{
			if (DataStructures.ChanceSuccess(10.0))
			{
				if (Client.Gems.Length > 0)
					GemEffect.GemEffects(Client);
			}
            if (DataStructures.ChanceSuccess(10.0))
            {
                var Equip = Client.Equipment.Values.Where(x => x.Bless > 0);
                if (Equip.Any())
                    GemEffect.BlessEffects(Client, Equip.GetRandomElement());
            }
			AttackHandler.Handle(Client, Packet);
		}

		private static void CreateCharacter(GameClient Client, byte[] Packet)
		{
			try
			{
				fixed (byte* Buffer = Packet)
				{
					var Sex = *(ushort*)(Buffer + 0x34);
					var Job = *(Buffer + 0x36);
					var Name = new string((sbyte*)Buffer + 0x14);
					var Avatar = (Sex == 1003) || (Sex == 1004) ? 1 : 201;
					#region Hacker Message
					var Hacker = new MessageInfo();
					Hacker.To = "ALLUSERS";
					Hacker.From = "SYSTEM";
					Hacker.ChatType = ChatType.Dialog;
					Hacker.Message = "Cheating isn't cool bro";
					#endregion
					#region Create Charactor Checks
					if ((Sex != 1003) && (Sex != 1004) && (Sex != 2001) && (Sex != 2002))
					{
						Client.Send(Packets.Message(Hacker));
						return;
					}
					if ((Job != 10) && (Job != 20) && (Job != 40) && (Job != 100))
					{
						Client.Send(Packets.Message(Hacker));
						return;
					}
					#endregion
					#region Respond Message
					var Respond = new MessageInfo();
					Respond.To = "ALLUSERS";
					Respond.From = "SYSTEM";
					Respond.ChatType = ChatType.Dialog;
					#endregion
					if (FlatDatabase.ValidName(Name, false))
					{
						if (Client.Flag > PlayerFlag.Normal)
						{
							if (Name.Length > 11)
							{
								Respond.Message = "Our records indicate that you are a server employee. A name should be 4 - 12 characters long.";
								Client.Send(Packets.Message(Respond));
							}
							switch (Client.Flag)
							{
								case PlayerFlag.GameManager: Name += "[GM]"; break;
								case PlayerFlag.ProjectManager: Name += "[PM]"; break;
								case PlayerFlag.ServerAdmin: Name += "[SA]"; break;
							}
						}
						if (!MySqlDatabase.CharacterExists(Client, Name))
						{
							if (Name.Length > 15)
							{
								Respond.Message = "Your name is to long, please shorten it.";
								Client.Send(Packets.Message(Respond));
								return;
							}
							MySqlDatabase.CreateCharacter(Client.Account, Name, Job, Sex, (byte)Avatar);

							Respond.Message = "ANSWER_OK";
							Client.Send(Packets.Message(Respond));
						}
						else
						{
							Respond.Message = "Character Name is already in use.";
							Client.Send(Packets.Message(Respond));
						}
					}
					else
					{
						Respond.Message = "Invalid Character Name.";
						Client.Send(Packets.Message(Respond));
					}
				}
			}
			catch (Exception e)
			{
				LogHandler.WriteLine(e);
			}
		}
		#endregion
	}
}