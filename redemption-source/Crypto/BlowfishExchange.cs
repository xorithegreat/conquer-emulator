﻿using System.IO;
using System.Text;
using OpenSSL;
using ConquerEmulator.MultiThreading;

namespace ConquerEmulator.Crypto
{
	public class ServerKeyExchange
	{
	    private const string P = "E7A69EBDF105F2A6BBDEAD7E798F76A209AD73FB466431E2E7352ED262F8C558F10BEFEA977DE9E21DCEE9B04D245F300ECCBBA03E72630556D011023F9E857F";
	    private const string G = "05";
	    private const string TQServer = "TQServer";
	    private byte[] clientIv;

	    private DH keyExchange;
	    private byte[] serverIv;

	    public byte[] CreateServerKeyPacket()
		{
			clientIv = new byte[8];
			serverIv = new byte[8];

			keyExchange = new DH(BigNumber.FromHexString(P), BigNumber.FromHexString(G));
			keyExchange.GenerateKeys();

			return GeneratePacket();
		}

	    public void HandleClientKeyPacket(string publicKey, ref GameCryptography crypto)
		{
			var key = keyExchange.ComputeKey(BigNumber.FromHexString(publicKey));
			crypto.SetKey(key);
			crypto.SetIvs(clientIv, serverIv);
		}

	    public byte[] GeneratePacket()
		{
			var data = new byte[331];
			using (var writer = new BinaryWriter(new MemoryStream(data)))
			{
				for (var i = 0; i < 11; i++)
					writer.Write((byte)RandomGenerator.Generator.Next(255));

				writer.Write(320);
				writer.Write(10);
				for (var i = 0; i < 10; i++)
					writer.Write((byte)RandomGenerator.Generator.Next(sbyte.MaxValue));
				writer.Write(8);
				writer.Write(serverIv);

				writer.Write(8);
				writer.Write(clientIv);

				writer.Write(128);
				writer.Write(Encoding.ASCII.GetBytes(P));

				writer.Write(2);
				writer.Write(Encoding.ASCII.GetBytes(G));

				writer.Write(128);
				writer.Write(Encoding.ASCII.GetBytes(keyExchange.PublicKey.ToHexString()));

				writer.Write(Encoding.ASCII.GetBytes(TQServer));
			}
			return data;
		}
	}
}