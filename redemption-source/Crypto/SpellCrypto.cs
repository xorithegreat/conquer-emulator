﻿using System;

namespace ConquerEmulator.Crypto
{
    class SwordfishCrypto
    {
        private static uint[] InitializeKey = {
            0x243f6a66, 0x85a308c3, 0x13198a2f, 0x03707348,
            0xa4093825, 0x299f31e0, 0x082efa92, 0xec4e6c83,
            0x452821a6, 0x38d01327, 0xbe5466df, 0x34e90c5c,
            0xc0ac29e7, 0xc97c50ad, 0x3f84d5b7, 0xb5470918,
            0x9216d5d1, 0x8979fb1c
        };
        public static int RollLeft(uint Value, byte Roll, byte Size)
        {
            Roll = (byte)(Roll & 0x1F);
            return (int)((Value << Roll) | (Value >> (Size - Roll)));
        }
        public static int RollRight(uint Value, byte Roll, byte Size)
        {
            Roll = (byte)(Roll & 0x1F);
            return (int)((Value << (Size - Roll)) | (Value >> Roll));
        }

        private uint[] Key;
        private byte[] Dec_Ivec;
        private byte[] Enc_Ivec;
        private byte nDec;
        private byte nEnc;

        unsafe void SmallCrypt(ref uint I, byte* I2, uint key)
        {
            I = (I ^ key) ^ ((Key[I2[3] % Key.Length] << 8) | Key[I2[1] % Key.Length]);
        }
        unsafe void BigCrypt(ref uint l, ref uint r)
        {
            l = (l ^ Key[0]);
            fixed (void* Ptr_R = &r, Ptr_L = &l)
            {
                for (byte i = 0; i < 16; i++)
                {
                    if (i % 2 == 0)
                        SmallCrypt(ref r, (byte*)Ptr_L, Key[i]);
                    else
                        SmallCrypt(ref l, (byte*)Ptr_R, Key[i]);
                }
            }
            r = (r ^ Key[17]);
            uint swap = l;
            l = r;
            r = swap;
        }

        public SwordfishCrypto(byte[] userkey)
        {
            Key = new uint[InitializeKey.Length];
            InitializeKey.CopyTo(Key, 0);
            Dec_Ivec = new byte[8];
            Enc_Ivec = new byte[8];
            nDec = 0;
            nEnc = 0;

            for (byte i = 0; i < userkey.Length; i++)
            {
                if (i % 2 == 0)
                    Key[i] = (uint)RollLeft(Key[i], i, 32);
                else
                    Key[i] = (uint)RollRight(Key[i], i, 32);
                Key[i] ^= userkey[i];
            }
            uint l = 0, r = 0;
            for (int j = 0; j < Key.Length - 1; j++)
            {
                BigCrypt(ref l, ref r);
                Key[j] ^= l;
                Key[j + 1] ^= r;
            }
        }
        public unsafe void Encrypt(byte* Data, int DataLength)
        {
            for (int i = 0; i < DataLength; i++)
            {
                if (nEnc == 0)
                {
                    fixed (void* ptr = Enc_Ivec)
                    {
                        uint l = *((uint*)ptr);
                        uint r = *((uint*)ptr + 1);
                        BigCrypt(ref l, ref r);
                        *((uint*)ptr) = l;
                        *((uint*)ptr + 1) = r;
                    }
                }
                Data[i] ^= Enc_Ivec[nEnc];
                nEnc = (byte)((nEnc + 1) % Enc_Ivec.Length);
            }
        }
        public unsafe void Decrypt(byte* Data, int DataLength)
        {
            for (int i = 0; i < DataLength; i++)
            {
                if (nDec == 0)
                {
                    fixed (void* ptr = Dec_Ivec)
                    {
                        uint l = *((uint*)ptr);
                        uint r = *((uint*)ptr + 1);
                        BigCrypt(ref l, ref r);
                        *((uint*)ptr) = l;
                        *((uint*)ptr + 1) = r;
                    }
                }
                Data[i] ^= Dec_Ivec[nDec];
                nDec = (byte)((nDec + 1) % Dec_Ivec.Length);
            }
        }
    }
    /// <summary>
    /// Used for decrypting information in the 0x3FE packet for spells,
    /// Sent by the client.
    /// </summary>
    public class SpellCrypto
    {
        private SwordfishCrypto fisherman;
        private static byte[] sfkey = new byte[] { 0x75, 0x22, 0x09, 0xC9, 0x88, 0xD9, 0x99, 0x06 };

        public SpellCrypto()
        {
            fisherman = new SwordfishCrypto(sfkey);
        }
        public unsafe void Decrypt(ref uint UID, ref ushort SpellID, ref ushort X, ref ushort Y)
        {
            fixed (uint* pUID = &UID)
            {
                fixed (ushort* pSpellID = &SpellID, pX = &X, pY = &Y)
                {
                    fisherman.Decrypt((byte*)pUID, sizeof(uint));
                    fisherman.Decrypt((byte*)pX, sizeof(ushort));
                    fisherman.Decrypt((byte*)pY, sizeof(ushort));
                    fisherman.Decrypt((byte*)pSpellID, sizeof(ushort));
                }
            }
        }
    }
}
