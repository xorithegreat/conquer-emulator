﻿namespace ConquerEmulator.Crypto
{
	public class PasswordCrypter
	{
	    public static uint[] PasswordKey = { 0xebe854bc, 0xb04998f7, 0xfffaa88c, 0x96e854bb, 0xa9915556, 0x48e44110, 0x9f32308f, 0x27f41d3e, 0xcf4f3523, 0xeac3c6b4, 0xe9ea5e03, 0xe5974bba, 0x334d7692, 0x2c6bcf2e, 0xdc53b74, 0x995c92a6, 0x7e4f6d77, 0x1eb2b79f, 0x1d348d89, 0xed641354, 0x15e04a9d, 0x488da159, 0x647817d3, 0x8ca0bc20, 0x9264f7fe, 0x91e78c6c, 0x5c9a07fb, 0xabd4dcce, 0x6416f98d, 0x6642ab5b };

	    public static unsafe sbyte* Decrypt(uint* Password)
		{
			for (sbyte i = 1; i >= 0; i = (sbyte)(i - 1))
			{
				var num = Password[i * 2 + 1];
				var num2 = Password[i * 2];
				for (sbyte j = 11; j >= 0; j = (sbyte)(j - 1))
				{
					num = (uint)Assembler.RollRight(num - PasswordKey[j * 2 + 7], (byte)num2, 0x20) ^ num2;
					num2 = (uint)Assembler.RollRight(num2 - PasswordKey[j * 2 + 6], (byte)num, 0x20) ^ num;
				}
				Password[i * 2 + 1] = num - PasswordKey[5];
				Password[i * 2] = num2 - PasswordKey[4];
			}
			return (sbyte*)Password;
		}

	    public static unsafe sbyte* Encrypt(uint* Password)
		{
			for (sbyte i = 1; i >= 0; i = (sbyte)(i - 1))
			{
				var num = PasswordKey[5] + Password[i * 2 + 1];
				var num2 = PasswordKey[4] + Password[i * 2];
				for (sbyte j = 0; j < 12; j = (sbyte)(j + 1))
				{
					num2 = (uint)Assembler.RollLeft(num ^ num2, (byte)num, 0x20) + PasswordKey[j * 2 + 6];
					num = (uint)Assembler.RollLeft(num ^ num2, (byte)num2, 0x20) + PasswordKey[j * 2 + 7];
				}
				Password[i * 2] = num2;
				Password[i * 3] = num;
			}
			return (sbyte*)Password;
		}
	}

	public class Assembler
	{
	    public static int RollLeft(uint Value, byte Roll, byte Size)
		{
			Roll = (byte)(Roll & 0x1f);
			return (int)((Value << Roll) | (Value >> (Size - Roll)));
		}

	    public static int RollRight(uint Value, byte Roll, byte Size)
		{
			Roll = (byte)(Roll & 0x1f);
			return (int)((Value << (Size - Roll)) | (Value >> Roll));
		}
	}
}