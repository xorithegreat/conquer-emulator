﻿using System;
using OpenSSL;

namespace ConquerEmulator.Crypto
{
	public class GameCryptography
	{
	    public GameCryptography(byte[] key)
		{
			Blowfish = new Blowfish(BlowfishAlgorithm.CFB64);
			Blowfish.SetKey(key);
		}

	    public Blowfish Blowfish { get; }

	    public void Decrypt(byte[] packet)
		{
			var buffer = Blowfish.Decrypt(packet);
			Buffer.BlockCopy(buffer, 0, packet, 0, buffer.Length);
		}

	    public void Encrypt(byte[] packet)
		{
			var buffer = Blowfish.Encrypt(packet);
			Buffer.BlockCopy(buffer, 0, packet, 0, buffer.Length);
		}

	    public void SetKey(byte[] k)
		{
			Blowfish.SetKey(k);
		}

	    public void SetIvs(byte[] i1, byte[] i2)
		{
			Blowfish.EncryptIV = i1;
			Blowfish.DecryptIV = i2;
		}
	}
}