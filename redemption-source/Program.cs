using System;
using System.Drawing;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using ConquerEmulator;
using ConquerEmulator.Attacking;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Events;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Commands;
using ConquerEmulator.Main.DMap;
using ConquerEmulator.Mob;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Npc.Quests;

namespace ConquerEmulator
{
	public class ConsoleHandler
	{
	    public delegate bool HandlerRoutine(CtrlTypes CtrlType);

	    public enum CtrlTypes
		{
			CTRL_C_EVENT = 0,
			CTRL_BREAK_EVENT,
			CTRL_CLOSE_EVENT,
			CTRL_LOGOFF_EVENT = 5,
			CTRL_SHUTDOWN_EVENT
		}

	    [DllImport("Kernel32")]
		public static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);

	    public static bool ConsoleCtrlCheck(CtrlTypes ctrlType)
		{
			switch (ctrlType)
			{
				case CtrlTypes.CTRL_C_EVENT:
				case CtrlTypes.CTRL_BREAK_EVENT:
				case CtrlTypes.CTRL_CLOSE_EVENT:
				case CtrlTypes.CTRL_LOGOFF_EVENT:
				case CtrlTypes.CTRL_SHUTDOWN_EVENT:
					Program.ShuttingDown = true;
					Console.WriteLine("The server's shutting down. . .");
					Kernel.SaveAll(true);
					break;
			}
			return true;
		}
	}

	internal class Program
	{
	    private static readonly GameServer GServer = new GameServer();

	    public static bool ShuttingDown;

	    private static void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			LogHandler.WriteLine(e);

			if (e.IsTerminating)
				Kernel.Restart();
		}

	    private static void ThreadException(object sender, ThreadExceptionEventArgs e)
		{
			LogHandler.WriteLine(e);
		}

	    private static void Main(string[] args)
		{
			Console.Title = "Conquer Emulator - Game Server";
            
            Console.WriteLine(string.Format("{0," + ((Console.WindowWidth / 2) + (Settings.Credits.Length / 2)) + "}", Settings.Credits));
           
            ConsoleHandler.SetConsoleCtrlHandler(ConsoleHandler.ConsoleCtrlCheck, true);

			AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException;
			Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
			Application.ThreadException += ThreadException;

			#region Ini Loading
			FlatDatabase.SpellExp = new IniFile(FlatDatabase.Location + "SpellExp.ini");
			FlatDatabase.ProfExp = new IniFile(FlatDatabase.Location + "ProfExp.ini");
			FlatDatabase.LevExp = new IniFile(FlatDatabase.Location + "LevelExp.ini");
			FlatDatabase.Shops = new IniFile(FlatDatabase.Location + "Shop.ini");
			FlatDatabase.Pets = new IniFile(FlatDatabase.Location + "Pets.ini");
			FlatDatabase.Transform = new IniFile(FlatDatabase.Location + "Transform.ini");
			FlatDatabase.RevivePoints = new IniFile(FlatDatabase.Location + "RevivePoints.ini");
			FlatDatabase.SpecialDrops = new IniFile(FlatDatabase.Location + "SpecialDrops.ini");
            FlatDatabase.DropRates = new IniFile(FlatDatabase.Location + "DropRates.ini");
            FlatDatabase.StatPoints = new IniFile(FlatDatabase.Location + "Stats.ini");
            MySqlDatabase.Config = new IniFile(FlatDatabase.Location + "Config.ini");
			MySqlDatabase.LoadConfig();
			Console.WriteLine(MySqlDatabase.isPVP ? "Server is in PVP mode." : "Server is in LEVEL mode.");

			FlatDatabase.LoadTgMonsters();
			#endregion
			#region MySql Loading
			if (!MySqlDatabase.CreateConnection())
			{
				Console.WriteLine("MySQL connection has failed!");
                Console.ReadLine();
				return;
			}
			Console.WriteLine("MySQL succesfully connected.");

			MySqlDatabase.LoadNpcs();
			MySqlDatabase.LoadMonsters();
			MySqlDatabase.LoadGuildMonsters();
			MySqlDatabase.LoadMonsterSpawns();
		    MySqlDatabase.LoadKo();
			Monster.SpawnMonsters();
			MySqlDatabase.LoadGuilds();
			FlatDatabase.LoadRevivers();
			FlatDatabase.LoadLotteryItems();
			ConquerNobility.UpdateNobility();
			#endregion
			#region Misc Loading
			GuildWar.Init();
			Bruteforce.Start(10);
		    ConquerDMap.Load();
            FlatDatabase.LoadBanned();
		    FlatDatabase.LoadRates();
            #endregion
            #region PlayerTimer
            Kernel.PlayerTimer.AutoReset = true;
			Kernel.PlayerTimer.Interval = 100;
			Kernel.PlayerTimer.Elapsed += WorldTimer_Elapsed;
			Kernel.PlayerTimer.Start();
			#endregion
			#region MonsterTimer
			Kernel.MonsterTimer.AutoReset = true;
			Kernel.MonsterTimer.Interval = 1000;
			Kernel.MonsterTimer.Elapsed += MonsterTimer_Elapsed;
			Kernel.MonsterTimer.Start();
			#endregion
			#region AutoAttackTimer
			Kernel.AutoAttackTimer.AutoReset = true;
			Kernel.AutoAttackTimer.Interval = 500;
			Kernel.AutoAttackTimer.Elapsed += AutoAttackTimer_Elapsed;
			Kernel.AutoAttackTimer.Start();
            #endregion
            #region Tournament Timer
            Kernel.TournamentTimer.AutoReset = true;
            Kernel.TournamentTimer.Interval = 15000;
            Kernel.TournamentTimer.Elapsed += TournamentTimer_Elapsed;
            Kernel.TournamentTimer.Start();
            #endregion

            GServer.Start();

			Console.WriteLine("Server is Ready.");

			while (!ShuttingDown)
				ConsoleCommand.Parse(Console.ReadLine());
		}

	    private static void AutoAttackTimer_Elapsed(object sender, ElapsedEventArgs e)
		{
			try
			{
				foreach (var pClient in Kernel.Clients)
				{
					if (!pClient.AutoAttaking || !Stamps.Check(pClient.Hero.Stamps.Attack))
						continue;
					if ((pClient.AAX != pClient.Hero.X) || (pClient.AAY != pClient.Hero.Y))
					{
						pClient.AATarget = null;
						pClient.AutoAttaking = false;
						continue;
					}

					if (pClient.AAMagic == null) // Physical
					{
						if (!AttackProcessor.PhysicalAttack(pClient.Hero, pClient.AATarget, pClient.AaType, false))
						{
							pClient.AATarget = null;
							pClient.AutoAttaking = false;
						}
					}
					else
					{
						unsafe
						{
							fixed (void* lpPtr = pClient.AAMagic)
							{
								var Request = (RequestAttack*)lpPtr;
								if (!AttackProcessor.MagicalAttack(pClient.Hero, pClient.AATarget, Request->SpellID, Request->TargetX, Request->TargetY))
								{
									pClient.AATarget = null;
									pClient.AutoAttaking = false;
								}
							}
						}
					}
					if ((pClient.Hero.StatusFlag & StatusFlag.Cyclone) == StatusFlag.Cyclone)
						pClient.Hero.Stamps.Attack = DateTime.Now.AddMilliseconds((double)pClient.Hero.Speed / 10);
					else
						pClient.Hero.Stamps.Attack = DateTime.Now.AddMilliseconds(pClient.Hero.Speed);
				}
			}
			catch
			{
			}
		}

	    private static void WorldTimer_Elapsed(object sender, ElapsedEventArgs e)
		{
			try
			{
				#region Save
				if (Kernel.SaveTime <= DateTime.Now)
				{
					Kernel.SaveAll(false);
					Kernel.SaveTime = DateTime.Now.AddMinutes(10);
				}
                #endregion
                #region ShuffleGScores
                if (Kernel.GuildScores <= DateTime.Now)
                {
                    if (GuildWar.Scores.Count != 0)
                    {
                        GameClient.SpeakToMap(Color.Yellow, ChatType.ClearTopRight, "", 1038, 0);
                        foreach (var scoreline in GuildWar.ShuffleScores())
                            GameClient.SpeakToMap(Color.Yellow, ChatType.TopRight, scoreline, 1038, 0);
                    }
                    Kernel.GuildScores = DateTime.Now.AddSeconds(5);
                }
                #endregion
                #region Item Removal
                if (Kernel.LastFloorCleanUp <= DateTime.Now)
                {
                    foreach (var Item in Kernel.GroundItems.Values)
                    {
                        if (DateTime.Now > Item.DroppedTime.AddSeconds(60))
                            FloorItem.Remove(Item);
                    }
                    Kernel.LastFloorCleanUp = DateTime.Now.AddSeconds(5);
                }
				#endregion
				foreach (var pClient in Kernel.Clients)
				{
					#region Mining
					if (pClient.Miner.Mining)
					{
						if (pClient.Miner.CanMine)
							pClient.Miner.SwingPickaxe();
					}
					#endregion
					#region PointLoss
					if (Stamps.Check(pClient.Hero.Stamps.Pointloss) && (pClient.Hero.PkPoints > 0))
					{
						pClient.Hero.PkPoints -= 1;
						pClient.Hero.Stamps.Pointloss = DateTime.Now.AddMinutes(6);
					}
					#endregion
					#region Flashing
					if (pClient.Hero.Flashing && Stamps.Check(pClient.Hero.Stamps.Flashing))
						pClient.Hero.Flashing = false;
					#endregion
					#region Stamina
					if (Stamps.Check(pClient.Hero.Stamps.Stamina))
					{
						if (pClient.Stamina < pClient.MaxStamina)
						{
							if (pClient.Hero.Action == ConquerAction.Sit)
							{
								if (pClient.Flag == PlayerFlag.ServerAdmin)
									pClient.Stamina = pClient.MaxStamina;
								else
									pClient.Stamina += 15;
							}
							else
							{
								if (pClient.Hero.Action != ConquerAction.Sit)
									if (pClient.Flag == PlayerFlag.ServerAdmin)
										pClient.Stamina = pClient.MaxStamina;
									else
										pClient.Stamina += 4;
							}
							pClient.Send(new StatTypePacket(pClient.Hero.UID, pClient.Stamina, StatIDs.Stamina).Serialize());
						}
						pClient.Hero.Stamps.Stamina = DateTime.Now.AddMilliseconds(1250);
					}
                    #endregion
                    #region Advanced Fly
                    if ((pClient.Hero.StatusFlag & StatusFlag.Fly) == StatusFlag.Fly)
                    {
                        if (Stamps.Check(pClient.Hero.Stamps.FlyFinish))
                        {
                            pClient.Hero.StatusFlag &= ~StatusFlag.Fly;
                            pClient.SendScreen(new StatTypePacket(pClient.Hero.UID, (uint)pClient.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize(), true);
                        }
                    }
                    #endregion
                    #region Invisibility
                    if ((pClient.Hero.StatusFlag & StatusFlag.Invisibility) == StatusFlag.Invisibility)
                    {
                        if (Stamps.Check(pClient.Hero.Stamps.Invisibility))
                        {
                            pClient.Hero.StatusFlag &= ~StatusFlag.Invisibility;
                            pClient.SendScreen(new StatTypePacket(pClient.Hero.UID, (uint)pClient.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize(), true);
                        }
                    }
                    #endregion
                    #region XP Shield
                    if ((pClient.Hero.StatusFlag & StatusFlag.MagicShield) == StatusFlag.MagicShield)
                    {
                        if (Stamps.Check(pClient.Hero.Stamps.XPShield))
                        {
                            pClient.Hero.StatusFlag &= ~StatusFlag.MagicShield;
                            pClient.SendScreen(new StatTypePacket(pClient.Hero.UID, (uint)pClient.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize(), true);
                        }
                    }
                    #endregion
                    #region Cyclone
                    if ((pClient.Hero.StatusFlag & StatusFlag.Cyclone) == StatusFlag.Cyclone)
                    {
                        if (Stamps.Check(pClient.Hero.Stamps.CycloneFinish))
                        {
                            Kernel.UpdateKoBoard(pClient);

                            pClient.Hero.StatusFlag &= ~StatusFlag.Cyclone;
                            pClient.SendScreen(new StatTypePacket(pClient.Hero.UID, (uint)pClient.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize(), true);
                        }
                    }
                    #endregion
                    #region Superman
                    if ((pClient.Hero.StatusFlag & StatusFlag.Superman) == StatusFlag.Superman)
                    {
                        if (Stamps.Check(pClient.Hero.Stamps.SupermanFinish))
                        {
                            pClient.Hero.StatusFlag &= ~StatusFlag.Superman;
                            pClient.SendScreen(new StatTypePacket(pClient.Hero.UID, (uint)pClient.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize(), true);
                        }
                    }
					#endregion
					#region XP Spells
					if (Stamps.Check(pClient.Hero.Stamps.XPSpells))
					{
						if ((pClient.Hero.StatusFlag & StatusFlag.XPList) == StatusFlag.XPList)
						{
							if (Stamps.Check(pClient.Hero.Stamps.CanSeeXPSpells))
							{
								pClient.Hero.StatusFlag &= ~StatusFlag.XPList;
								pClient.Send(new StatTypePacket(pClient.Hero.UID, (uint)pClient.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize());
							}
						}
						else
						{
							pClient.XpSkillCounter++;
							if (pClient.XpSkillCounter >= 100)
							{
								pClient.Hero.StatusFlag |= StatusFlag.XPList;
								pClient.Hero.StatusFlag &= ~StatusFlag.Superman;
								pClient.Hero.StatusFlag &= ~StatusFlag.Cyclone;
								pClient.Hero.Stamps.CanSeeXPSpells = DateTime.Now.AddSeconds(20);
								pClient.XpSkillCounter = 0;

								pClient.Send(new StatTypePacket(pClient.Hero.UID, (uint)pClient.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize());
							}
						}
						pClient.Hero.Stamps.XPSpells = DateTime.Now.AddMilliseconds(2250);
					}
					#endregion
					#region Stigma
					if (((pClient.Hero.StatusFlag & StatusFlag.Stigma) == StatusFlag.Stigma) && Stamps.Check(pClient.Hero.Stamps.StigmaTimeUp))
					{
						pClient.Hero.StatusFlag -= StatusFlag.Stigma;
                        pClient.SendScreen(new StatTypePacket(pClient.Hero.UID, (uint)pClient.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize(), true);
                    }
					#endregion
                    #region Transformations
                    if (pClient.Hero.InTransformation)
                    {
                        if (Stamps.Check(pClient.Hero.Stamps.TransformationTime))
                        {
                            pClient.Transform.Stop();
                            pClient.Transform.SendUpdates();
                        }
                    }
                    #endregion
                    #region Pet
                    if (pClient.Pet != null && pClient.PetTarget != null)
                    {
                        if (!pClient.PetTarget.Dead)
                        {
                            pClient.Pet.Process(pClient.PetTarget);
                        }
                        else
                        {
                            pClient.PetTarget = null;
                            return;
                        }
                    }
				    #endregion
                    #region PTBTimer
                    if (pClient.HasBomb && Stamps.Check(pClient.Hero.Stamps.BombTime))
                    {
                        var GDrop = new ItemGroundPacket((uint)RandomGenerator.Generator.Next(), 23, GroundIDs.GroundEffect, pClient.Hero.X, pClient.Hero.Y);
                        pClient.SendScreen(GDrop.Serialize(), true);
                        pClient.Hero.Die(null);
                    }
                    #endregion
                    #region HeavensBless
                    if (Stamps.Check(pClient.Hero.Stamps.HeavenBless))
                    {
                        pClient.Hero.StatusFlag &= ~StatusFlag.Blessing;
                        pClient.SendScreen(new StatTypePacket(pClient.Hero.UID, (uint)pClient.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize(), true);
                    }
                    #endregion
                }
			}
			catch
			{
				// ignored
			}
		}

	    private static void MonsterTimer_Elapsed(object sender, ElapsedEventArgs e)
		{
			try
			{
				foreach (var Monsters in Kernel.Monsters.Values)
				{
					foreach (var Mob in Monsters.Values)
					{
						#region Remove
						if (Mob.Entity.Dead && Mob.Remove && Stamps.Check(Mob.Entity.Stamps.Remove))
							MonsterAI.Remove(Mob);
						#endregion
						#region Restart
						if ((Mob.Target != null) && Stamps.Check(Mob.Entity.Stamps.Attack) && !Mob.Entity.Dead)
							MonsterAI.Process(Mob, Mob.Target);
						#endregion
						#region Respawn
						if (Mob.Entity.Dead && Mob.Respawn && Stamps.Check(Mob.Entity.Stamps.Revive))
							MonsterAI.Respawn(Mob);
                        #endregion
                        #region Find Target
                        if (Mob.Target == null)
					        MonsterAI.Search(Mob);
                        #endregion
                    }
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}

	    private static void TournamentTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                var Time = DateTime.Now;
                if (!TournamentBase.Started && !TeamDeathMatch.Started)
                {
                    switch (Time.Minute)
                    {
                        case 0: TeamDeathMatch.CreateTDM(1767); break;
                        case 15: TournamentBase.CreateTournament(1091, 34, 37, "FFA"); break;
                        case 30: TournamentBase.CreateTournament(1767, 50, 50, "PassTheBomb"); break;
                        case 45: TournamentBase.CreateTournament(1091, 34, 37, "MostKills"); break;
                    }
                }
                #region TeamDeathMatch
                if ((!TeamDeathMatch.PkEnabled && TeamDeathMatch.Started) && Stamps.Check(TeamDeathMatch.TournyStartTimer))
                {
                    TeamDeathMatch.Start();
                }
                if ((TeamDeathMatch.PkEnabled && TeamDeathMatch.Started) && Stamps.Check(TeamDeathMatch.TournyEndTimer))
                {
                    TeamDeathMatch.EndWithScore(250);
                }
                #endregion
                #region TournamentBase
                if ((!TournamentBase.PkEnabled && TournamentBase.Started) && Stamps.Check(TournamentBase.StartTimer))
                    TournamentBase.Start();
                if ((TournamentBase.PkEnabled && TournamentBase.Started) && Stamps.Check(TournamentBase.EndTimer))
                    TournamentBase.End();
                #endregion
                #region Dueling
                if ((!Dueling.PkEnabled && Dueling.Started) && Stamps.Check(Dueling.DuelStartTimer))
                    Dueling.StartDuel();
                #endregion
                #region DisCity
                if (Time.DayOfWeek == DayOfWeek.Monday || Time.DayOfWeek == DayOfWeek.Wednesday)
                {
                    var AMPM = Time.ToString("tt", CultureInfo.InvariantCulture);
                    if (Time.Minute == 0 && Time.Hour == 5 && AMPM == "PM")
                    {
                        GameClient.SpeakToAll(Color.Yellow, ChatType.Center, "DisCity has started!");
                        DisCity.Syrens = 4;
                        DisCity.Started = true;
                    }
                    else if (Time.Hour == 5 && Time.Minute == 6 && AMPM == "PM")
                    {
                        GameClient.SpeakToAll(Color.Yellow, ChatType.Center, "DisCity has ended for the unlucky guys who couldn't get in this time come next time!");
                        DisCity.Clear();
                    }
                    else if (Time.Hour == 4 && Time.Minute == 30 && AMPM == "PM")
                    {
                        GameClient.SpeakToAll(Color.Yellow, ChatType.Center, "Discity will start in 30 minutes!");
                    }
                    else if (Time.Hour == 4 && Time.Minute == 55 && AMPM == "PM")
                    {
                        GameClient.SpeakToAll(Color.Yellow, ChatType.Center, "DisCity will start in 5 minutes hurry up!");
                    }
                    else if (Time.Hour == 5 && Time.Minute == 30 && AMPM == "PM")
                    {
                        foreach (var pClient in Kernel.Clients)
                        {
                            if (pClient.Hero.MapID == 4021)
                            {
                                pClient.Teleport(1020, 566, 564);
                                pClient.Speak(Color.Yellow, ChatType.Talk, "The time to get to the next stage has passed! Better luck next time!");
                            }
                        }
                    }
                    else if (Time.Hour == 6 && Time.Minute == 30 && AMPM == "PM")
                    {
                        foreach (var pClient in Kernel.Clients)
                        {
                            if (pClient.Hero.MapID == 2022)
                            {
                                pClient.Teleport(1020, 566, 564);
                                pClient.Speak(Color.Yellow, ChatType.Talk, "The time to get to the next stage has passed! Better luck next time!");
                            }
                        }
                    }
                    else if (Time.Minute == 0 && Time.Hour == 7 && AMPM == "PM")
                    {
                        foreach (var pClient in Kernel.Clients)
                        {
                            if (pClient.Hero.MapID == 2023)
                            {
                                pClient.Teleport(1020, 566, 564);
                                pClient.Speak(Color.Yellow, ChatType.Talk, "The time to get to the next stage has passed! Better luck next time!");
                            }
                        }
                    }
                    else if (Time.Minute == 30 && Time.Hour == 7 && AMPM == "PM")
                    {
                        foreach (var pClient in Kernel.Clients)
                        {
                            if (pClient.Hero.MapID == 2024)
                            {
                                pClient.Teleport(1020, 566, 564);
                                pClient.Speak(Color.Yellow, ChatType.Talk, "The time to kill UltimatePluto has passed! Better luck next time!");
                            }
                        }
                    }
                }
                #endregion
            }
            catch { }
        }
	}
}