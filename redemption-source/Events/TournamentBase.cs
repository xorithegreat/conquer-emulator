﻿using System;
using System.Collections.Generic;
using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Main;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Events
{
    public struct KillScore : IScorable
    {
        public static string Name;
        public int Score { get; set; }
    }
    public class TournamentBase
    {
        public static Map Map;
        public static ushort X, Y;
        public static bool AcceptingEntries;
        public static bool Started;
        public static bool PkEnabled;
        public static DateTime StartTimer;
        public static DateTime EndTimer;
        public static string TournamentType;
        public static List<GameClient> Entries = new List<GameClient>();
        private static readonly Dictionary<uint, KillScore> Scores = new Dictionary<uint, KillScore>();

        public static void CreateTournament(uint MapID, ushort _X, ushort _Y, string Type)
        {
            Map.ID = MapID;
            X = _X;
            Y = _Y;
            TournamentType = Type;
            Map.MakeDynamic();

            Started = true;
            PkEnabled = false;

            AcceptingEntries = true;
            StartTimer = DateTime.Now.AddMinutes(3);
            GameClient.SpeakToAll(Color.White, ChatType.Center, $"A {TournamentType} tournament has started! Type in \"@scroll tournament\" or go to NPC \"GeneralBravery\" in Twin City (438, 377)! You have 3 minutes before it starts!");
        }

        public static void Join(GameClient Client, int hitPoints = 1)
        {
            Client.Hero.Hitpoints = hitPoints;
            Client.Send(new StatTypePacket(Client.Hero.UID, (uint)hitPoints, StatIDs.Hitpoints).Serialize());

            Client.Teleport(Map.ID, X, Y, Map.DynamicID);
            Client.InTournament = true;
            Entries.Add(Client);
        }

        public static void Leave(GameClient Client)
        {
            Client.Speak(Color.White, ChatType.ClearTopRight, "");
            Client.InTournament = false;

            Entries.Remove(Client);
            if (Entries.Count == 1)
                End();
            if (Client.HasBomb)
                PassTheBomb.RemoveBomb(Client);

            if (Client.Hero.Dead)
            {
                Client.Revive(Client, true);
                Client.Teleport(1002, 430, 378);
            }
        }

        public static void Start()
        {
            if (Entries.Count <= 1)
            {
                if (Entries.Count == 1)
                {
                    var player = Entries[0];
                    player.Teleport(1002, 430, 378);
                }
                Reset();
                GameClient.SpeakToMap(Color.White, ChatType.TopLeft, string.Format("The {0} tournament has ended, because there wasn't enough players.", TournamentType), Map.ID, Map.DynamicID);
                return;
            }
            AcceptingEntries = false;
            PkEnabled = true;
            EndTimer = DateTime.Now.AddMinutes(5);

            switch (TournamentType)
            {
                case "PassTheBomb":
                    PassTheBomb.GiveRandomBomb();
                    GameClient.SpeakToMap(Color.Red, ChatType.Center, "The bomb has been given out.", Map.ID, Map.DynamicID);
                    break;
                default:
                    GameClient.SpeakToMap(Color.White, ChatType.Center, "FIGHT!", Map.ID, Map.DynamicID);
                    break;
            }
        }

        public static void End()
        {
            switch (TournamentType)
            {
                case "PassTheBomb":
                    EndTournament(100);
                    break;
                case "FFA":
                    EndTournament(100);
                    break;
                case "MostKills":
                    EndWithScore(150);
                    break;
            }
        }

        private static void EndTournament(ushort CPs)
        {
            var winner = Entries[0];
            winner.Teleport(1002, 430, 378);
            winner.ConquerPoints += Math.Min(CPs, DataStructures.MaxCurrency);
            winner.Send(new StatTypePacket(winner.Hero.UID, winner.ConquerPoints, StatIDs.ConquerPoints).Serialize());

            Reset();
            GameClient.SpeakToAll(Color.White, ChatType.TopLeft, $"The tournament has ended, with {winner.Hero.Name} as the winner!");
        }

        #region Score Based Tournaments

        public static void EndWithScore(ushort CPs)
        {
            if (Scores.Count == 0)
            {
                EndTournament(125);
                return;
            }
            #region Sorting Winner
            var Score = 0;
            uint winnerUID = 0;
            foreach (var kvp in Scores)
            {
                if (kvp.Value.Score > Score)
                {
                    Score = kvp.Value.Score;
                    winnerUID = kvp.Key;
                }
                var client = Kernel.FindClient(kvp.Key);
                if (client != null)
                {
                    client.ConquerPoints += Math.Min((uint)kvp.Value.Score, DataStructures.MaxCurrency);
                    client.Send(new StatTypePacket(client.Hero.UID, client.ConquerPoints, StatIDs.ConquerPoints).Serialize());
                    client.Speak(Color.White, ChatType.Talk, $"You've been given bonus CPs for {kvp.Value.Score} kills.");
                }
            }
            #endregion
            #region Reward Winner
            var winner = Kernel.FindClient(winnerUID);
            if (winner != null)
            {
                winner.Speak(Color.White, ChatType.ClearTopRight, "");
                winner.Teleport(1002, 430, 378);
                winner.Hero.Hitpoints = winner.Hero.MaxHitpoints;
                winner.ConquerPoints += Math.Min(CPs, DataStructures.MaxCurrency);
                winner.Send(new StatTypePacket(winner.Hero.UID, (uint)winner.Hero.Hitpoints, StatIDs.Hitpoints).Serialize());
                winner.Send(new StatTypePacket(winner.Hero.UID, winner.ConquerPoints, StatIDs.ConquerPoints).Serialize());

                GameClient.SpeakToAll(Color.White, ChatType.Center, $"{winner.Hero.Name} has won the {TournamentType} tournament with a kill count of {Score}.");
            }
            #endregion
            Reset();
        }

        private static void Reset()
        {
            AcceptingEntries = false;
            Started = false;
            PkEnabled = false;
            Scores.Clear();
            Entries.Clear();
        }

        public static string[] ShuffleScores()
        {
            var ret = new string[5];
            for (byte i = 0; i < ret.Length; i++)
                ret[i] = "";

            var values = new KillScore[Scores.Count];
            Scores.Values.CopyTo(values, 0);

            Array.Sort(values, ScoreComparer.CMP);
            for (sbyte i = 0; i < values.Length; i++)
            {
                ret[i] = "No " + (i + 1) + ". " + KillScore.Name + " (" + values[i].Score + ")";
                if (i == 4)
                    break;
            }
            return ret;
        }

        public static void IncrementScore(GameClient Attacker)
        {
            KillScore tmp;
            if (Scores.TryGetValue(Attacker.Hero.UID, out tmp))
            {
                tmp.Score += 1;
                Scores[Attacker.Hero.UID] = tmp;
            }
            else
            {
                tmp.Score = 1;
                KillScore.Name = Attacker.Hero.Name;
                Scores.Add(Attacker.Hero.UID, tmp);
            }

            GameClient.SpeakToMap(Color.White, ChatType.ClearTopRight, "", Map.ID, Map.DynamicID);
            foreach (var scoreline in ShuffleScores())
                GameClient.SpeakToMap(Color.Yellow, ChatType.TopRight, scoreline, Map.ID, Map.DynamicID);

            Attacker.Speak(Color.White, ChatType.Talk, "Your kills: " + Scores[Attacker.Hero.UID].Score);
        }

        #endregion
    }
}
