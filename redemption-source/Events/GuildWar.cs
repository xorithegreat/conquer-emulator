﻿using System;
using System.Collections.Concurrent;
using ConquerEmulator.Database;
using ConquerEmulator.Main;
using ConquerEmulator.Main.DMap;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Events
{
	public struct GuildWarScore : IScorable
	{
		public static string GuildName;
		public int Score { get; set; }
	}

	internal class GuildWar
	{
	    public const int LeftGateUID = 30001, RightGateUID = 30002, PoleUID = 30000;

	    public const int GuildWarHour = 21;

	    private static string m_PoleHolder;
	    public static bool Active;

	    public static ConcurrentDictionary<ushort, GuildWarScore> Scores = new ConcurrentDictionary<ushort, GuildWarScore>();
	    public static ConcurrentDictionary<uint, SOBMonster> GuildMobs = new ConcurrentDictionary<uint, SOBMonster>();
	    private static CompressedBitmap GuildWarMap;

	    private static IniFile GSettings;

	    public static string PoleHolder
		{
			get
			{
				var name = GSettings.ReadString("Settings", "PoleHolder", "");
				if (m_PoleHolder != name)
					return m_PoleHolder = name;
				return m_PoleHolder;
			}
			set
			{
				m_PoleHolder = value;
				GSettings.WriteString("Settings", "PoleHolder", m_PoleHolder);
			}
		}

	    public static bool LeftGateOpen => GuildMobs[LeftGateUID].Hitpoints == 0;
	    public static bool RightGateOpen => GuildMobs[RightGateUID].Hitpoints == 0;

	    public static void Init()
		{
			Scores = new ConcurrentDictionary<ushort, GuildWarScore>();
			GSettings = new IniFile(FlatDatabase.Location + "GuildWar.ini");
			GuildWarMap = new CompressedBitmap(FlatDatabase.Location + @"\Maps\GuildWarMap.bmp");

			var Pole = GuildMobs[PoleUID];
			if (PoleHolder != "")
				Pole.Name = m_PoleHolder;
			else
				Pole.Name = "None";

			if ((DateTime.Now.DayOfWeek == DayOfWeek.Saturday) || ((DateTime.Now.DayOfWeek == DayOfWeek.Sunday) && (DateTime.Now.Hour < GuildWarHour)))
				Active = true;
		}

	    public static void ReverseGate(SOBMonster Gate)
		{
			if (Gate.UID == LeftGateUID)
				Gate.Mesh = LeftGateOpen ? (uint)277 : 241;
			else if (Gate.UID == RightGateUID)
				Gate.Mesh = RightGateOpen ? (uint)241 : 277;
			PacketKernel.SendRangedPacket(1038, Gate.X, Gate.Y, Gate.SpawnPacket);
		}

	    public static string[] ShuffleScores()
		{
			var ret = new string[5];
			for (byte i = 0; i < ret.Length; i++)
				ret[i] = "";

			var values = new GuildWarScore[Scores.Count];
			Scores.Values.CopyTo(values, 0);

			Array.Sort(values, ScoreComparer.CMP);
			for (sbyte i = 0; i < values.Length; i++)
			{
				ret[i] = "No " + (i + 1) + ". " + GuildWarScore.GuildName + " (" + values[i].Score + ")";
				if (i == 4)
					break;
			}
			return ret;
		}

	    public static bool ValidJump(CompressBitmapColor Current, out CompressBitmapColor New, ushort X, ushort Y)
		{
			New = Current;
			var temp = GuildWarMap[X, Y];
			if (temp != CompressBitmapColor.Black)
			{
				if (Current == CompressBitmapColor.White)
					if (temp < Current)
						return false;
				New = temp;
				return true;
			}
			return false;
		}

	    public static bool ValidWalk(CompressBitmapColor CurrentColor, out CompressBitmapColor NewColor, ushort X, ushort Y)
		{
			NewColor = CurrentColor;
			var temp = GuildWarMap[X, Y];
			if (temp != CompressBitmapColor.Blue)
			{
				if (Y == 209)
				{
					if (Kernel.GetDistance(X, Y, 164, 209) <= 3)
						if (!LeftGateOpen)
							return false;
				}
				else if (X == 222)
				{
					if (Kernel.GetDistance(X, Y, 222, 177) <= 3)
						if (!RightGateOpen)
							return false;
				}
				NewColor = temp;
				return true;
			}
			return false;
		}
	}
}