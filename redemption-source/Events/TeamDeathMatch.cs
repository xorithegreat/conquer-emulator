﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ConquerEmulator.Client;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Events
{
    public enum TeamGarment : uint
    {
        WhiteGarment = 181325,
        BlackGarment = 181515
    }
    public class TeamDeathMatch
    {
        public static Map Map;
        public static bool AcceptingEntries;
        public static bool Started;
        public static bool PkEnabled;
        public static DateTime TournyStartTimer;
        public static DateTime TournyEndTimer;

        public static Dictionary<GameClient, TeamGarment> Teams = new Dictionary<GameClient, TeamGarment>();
        public static Dictionary<uint, KillScore> Scores = new Dictionary<uint, KillScore>();

        public static void CreateTDM(ushort MapID)
        {
            Map.ID = MapID;
            Map.MakeDynamic();

            Scores.Clear();
            Teams.Clear();

            AcceptingEntries = true;
            Started = true;
            TournyStartTimer = DateTime.Now.AddMinutes(3);

            GameClient.SpeakToAll(Color.White, ChatType.Center, "Team Death Match has started! Type in \"@scroll tournament\" or go to NPC \"GeneralBravery\" in Twin City (438, 377)! You have 3 minutes before it starts!");
        }

        public static void Join(GameClient Client)
        {
            var Item = new ConquerItem(true);
            Item.UID = ConquerItem.NextItemUID;

            var whiteGarments = Teams.Where(x => x.Value == TeamGarment.WhiteGarment).ToList();
            var blackGarments = Teams.Where(x => x.Value == TeamGarment.BlackGarment).ToList();

            if (whiteGarments.Count < blackGarments.Count)
            {
                Teams.Add(Client, TeamGarment.WhiteGarment);
                Item.ID = (uint)TeamGarment.WhiteGarment;
            }
            else if (blackGarments.Count < whiteGarments.Count)
            {
                Teams.Add(Client, TeamGarment.BlackGarment);
                Item.ID = (uint)TeamGarment.BlackGarment;
            }
            else
            {
                if (DataStructures.ChanceSuccess(50))
                {
                    Teams.Add(Client, TeamGarment.WhiteGarment);
                    Item.ID = (uint)TeamGarment.WhiteGarment;
                }
                else
                {
                    Teams.Add(Client, TeamGarment.BlackGarment);
                    Item.ID = (uint)TeamGarment.BlackGarment;
                }
            }

            Client.Equip(Item, (ushort)ItemPosition.Garment, Client);

            Client.Hero.Hitpoints = 1;
            Client.Send(new StatTypePacket(Client.Hero.UID, (uint)Client.Hero.Hitpoints, StatIDs.Hitpoints).Serialize());

            if (Item.ID == (uint)TeamGarment.WhiteGarment)
                Client.Teleport(Map.ID, 82, 192, Map.DynamicID);
            else
            {
                Client.Teleport(Map.ID, 328, 214, Map.DynamicID);
            }
            Client.InTournament = true;
        }

        public static void Leave(GameClient Client)
        {
            Client.InTournament = false;
            var Garment = Client.Equipment.Values.FirstOrDefault(x => x.Position == ItemPosition.Garment);
            if (Garment != null)
            {
                Client.Unequip(Client, (ushort)ItemPosition.Garment);
                Client.RemoveInventory(Garment.UID);
            }
            if (Client.Hero.Dead)
            {
                Client.Revive(Client, false);
                Client.Teleport(1002, 430, 378);
            }
            Teams.Remove(Client);
        }

        public static void Start()
        {
            if (Teams.Count > 0)
            {
                PkEnabled = true;
                AcceptingEntries = false;
                GameClient.SpeakToMap(Color.White, ChatType.Center, "Fight!", Map.ID, Map.DynamicID);
            }
            else
            {
                GameClient.SpeakToMap(Color.White, ChatType.Center, "The tournament has ended, because there wasn't enough players.", Map.ID, Map.DynamicID);

                foreach (var Client in Teams.Keys)
                {
                    Client.YourTdmKills = 0;
                    Client.Teleport(1002, 430, 378);
                }
                Started = false;
                PkEnabled = false;
                return;
            }
            TournyEndTimer = DateTime.Now.AddMinutes(15);
        }

        public static void EndWithScore(ushort CPs)
        {
            #region Pick Winner
            var Score = 0;
            uint winningGarment = 0;
            foreach (var kvp in Scores)
            {
                if (kvp.Value.Score > Score)
                {
                    Score = kvp.Value.Score;
                    winningGarment = kvp.Key;
                }
                var client = Kernel.FindClient(kvp.Key);
                if (client != null)
                {
                    client.ConquerPoints += Math.Min((uint)kvp.Value.Score, DataStructures.MaxCurrency);
                    client.Send(new StatTypePacket(client.Hero.UID, client.ConquerPoints, StatIDs.ConquerPoints).Serialize());
                    client.Speak(Color.White, ChatType.Talk, string.Format("You've been given bonus CPs for {0} kills.", kvp.Value.Score));
                }
            }
            #endregion
            #region Reward Winner
            if (winningGarment != 0)
            {
                foreach (var winner in Teams.Where(x => x.Value == (TeamGarment)winningGarment))
                {
                    winner.Key.ConquerPoints += Math.Min(CPs, DataStructures.MaxCurrency);
                    winner.Key.Send(new StatTypePacket(winner.Key.Hero.UID, winner.Key.ConquerPoints, StatIDs.ConquerPoints).Serialize());
                }
                GameClient.SpeakToAll(Color.White, ChatType.Center, string.Format("The {0} has won the TeamDeathMatch tournament with a kill count of {1}.", GetTypeName(winningGarment), Score));
            }
            #endregion

            GameClient.SpeakToMap(Color.White, ChatType.ClearTopRight, "", Map.ID, Map.DynamicID);
            foreach (var client in Teams.Keys)
            {
                client.Teleport(1002, 430, 378);
            }

            AcceptingEntries = false;
            Started = false;
            PkEnabled = false;
            Scores.Clear();
            Teams.Clear();
        }

        public static string[] ShuffleScores()
        {
            var ret = new string[5];
            for (byte i = 0; i < ret.Length; i++)
                ret[i] = "";

            var values = new KillScore[Scores.Count];
            Scores.Values.CopyTo(values, 0);

            Array.Sort(values, ScoreComparer.CMP);
            for (sbyte i = 0; i < values.Length; i++)
            {
                ret[i] = "No " + (i + 1) + ". " + KillScore.Name + " (" + values[i].Score + ")";
                if (i == 4)
                    break;
            }
            return ret;
        }

        public static void IncrementScore(GameClient Attacker)
        {
            var garment = Attacker.Equipment.Values.FirstOrDefault(x => x.Position == ItemPosition.Garment);
            if (garment == null)
            {
                Leave(Attacker);
                return;
            }

            KillScore tmp;
            if (Scores.TryGetValue(garment.ID, out tmp))
            {
                tmp.Score += 1;
                Scores[garment.ID] = tmp;
            }
            else
            {
                tmp.Score = 1;
                KillScore.Name = GetTypeName(garment.ID);
                Scores.Add(garment.ID, tmp);
            }

            GameClient.SpeakToMap(Color.White, ChatType.ClearTopRight, "", Map.ID, Map.DynamicID);
            foreach (var scoreline in ShuffleScores())
                GameClient.SpeakToMap(Color.Yellow, ChatType.TopRight, scoreline, Map.ID, Map.DynamicID);

            Attacker.YourTdmKills++;
            Attacker.Speak(Color.White, ChatType.Talk, string.Format("Your TeamDeathMatch kills {1}.", Attacker.YourTdmKills));
        }

        private static string GetTypeName(uint ID)
        {
            switch ((TeamGarment)ID)
            {
                case TeamGarment.BlackGarment:
                    return "BlackTeam";
                case TeamGarment.WhiteGarment:
                    return "WhiteTeam";
            }

            return "None";
        }
    }
}