﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ConquerEmulator.Client;
using ConquerEmulator.Main;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Events
{
    public class Dueling
    {
        public static Map Map;
        public static ushort X, Y;
        public static bool Started;
        public static bool PkEnabled;
        public static uint Wager;
        public static uint RequestUID;
        private static Dictionary<uint, int> scores;

        private static DateTime requestTimer;
        public static DateTime DuelStartTimer;

        public static void RequestDuel(GameClient Client)
        {
            foreach (var client in Client.Team.Teammates)
            {
                if (client != Client)
                    client.Speak(Color.White, ChatType.Team, $"{Client.Hero.Name} has sent you a duel request with a wager of {Wager} CPs. Type @accept to join.");
            }
            RequestUID = Client.Hero.UID;
            requestTimer = DateTime.Now.AddMinutes(3);
            Client.Speak(Color.White, ChatType.Team, "Your duel request has been sent. Waiting for Opponent to accept.");
        }

        public static void CreateDuel(GameClient Client, ushort MapID, ushort _X, ushort _Y)
        {
            var success = true;
            foreach (var teamMate in Client.Team.Teammates)
            {
                if (teamMate.InTournament)
                    success = false;
                if (teamMate.ConquerPoints < Wager)
                    success = false;
            }
            if (Stamps.Check(requestTimer))
            {
                success = false;
            }

            if (success)
            {
                Map.ID = MapID;
                X = _X;
                Y = _Y;
                Map.MakeDynamic();

                Started = false;
                PkEnabled = false;

                DuelStartTimer = DateTime.Now.AddSeconds(15);

                scores = new Dictionary<uint, int>();

                foreach (var client in Client.Team.Teammates)
                {
                    client.InTournament = true;
                    client.Teleport(Map.ID, X, Y, Map.DynamicID);
                }
                GameClient.SpeakToMap(Color.White, ChatType.Center, "The duel will begin shortly.", Map.ID, Map.DynamicID);
            }
            Started = true;
        }

        public static void StartDuel()
        {
            PkEnabled = true;
            GameClient.SpeakToMap(Color.White, ChatType.Center, "FIGHT!", Map.ID, Map.DynamicID);
        }

        public static void EndDuel(GameClient winner)
        {
            Wager = 0;
            RequestUID = 0;
            Started = false;
            PkEnabled = false;

            winner.ConquerPoints += winner.Wager;
            winner.Send(new StatTypePacket(winner.Hero.UID, winner.ConquerPoints, StatIDs.ConquerPoints).Serialize());

            GameClient.SpeakToMap(Color.White, ChatType.ClearTopRight, "", Map.ID, Map.DynamicID);
            GameClient.SpeakToMap(Color.White, ChatType.Team, string.Format("The duel has ended with {0} as the winner.", winner.Hero.Name), Map.ID, Map.DynamicID);

            foreach (var client in winner.Team.Teammates)
            {
                if (client != winner)
                {
                    client.ConquerPoints -= winner.Wager;
                    client.Send(new StatTypePacket(winner.Hero.UID, winner.ConquerPoints, StatIDs.ConquerPoints).Serialize());
                }
                client.InTournament = false;
                client.Teleport(1002, 400, 400);
            }
            scores.Clear();
        }

        public static void ForfeitDuel(GameClient looser)
        {
            looser.ConquerPoints -= looser.Wager;
            looser.Send(new StatTypePacket(looser.Hero.UID, looser.ConquerPoints, StatIDs.ConquerPoints).Serialize());

            foreach (var client in looser.Team.Teammates)
            {
                if (client != looser)
                {
                    client.ConquerPoints += looser.Wager;
                    client.Send(new StatTypePacket(client.Hero.UID, client.ConquerPoints, StatIDs.ConquerPoints).Serialize());
                    client.Speak(Color.White, ChatType.Team, string.Format("You have automatically won the duel because {0} has forfeited.", looser.Hero.Name));
                }
                client.InTournament = false;
                client.Teleport(1002, 400, 400);
            }
            
            scores.Clear();
        }

        public static string[] DisplayScores()
        {
            IEnumerable<KeyValuePair<uint, int>> Sorted = scores.OrderByDescending(Pair => Pair.Value);
            var retn = new string[2];
            for (byte i = 0; i < retn.Length; i++)
                retn[i] = "";
            var Index = 0;
            foreach (var Pair in Sorted)
            {
                var Obj = Kernel.FindClient(Pair.Key);
                retn[Index] = Obj.Hero.Name + " : " + Pair.Value;
                Index++;
            }
            return retn;
        }

        public static void IncrementScore(GameClient Attacker)
        {
            if (scores.ContainsKey(Attacker.Hero.UID))
                scores[Attacker.Hero.UID]++;
            else
                scores.Add(Attacker.Hero.UID, 1);

            GameClient.SpeakToMap(Color.White, ChatType.ClearTopRight, "", Map.ID, Map.DynamicID);
            var Messages = DisplayScores();
            for (var i = 0; i < Messages.Length; i++)
                GameClient.SpeakToMap(Color.Yellow, ChatType.TopRight, Messages[i], Map.ID, Map.DynamicID);

            var CurrentScore = scores[Attacker.Hero.UID];
            if (CurrentScore == 10)
                EndDuel(Attacker);
        }
    }
}









































