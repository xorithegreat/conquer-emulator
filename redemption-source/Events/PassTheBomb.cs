﻿using System;
using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Events
{
    public class PassTheBomb
    {
        public static void BombPass(GameClient Opponent)
        {
            if (!TournamentBase.PkEnabled)
                return;

            AddBomb(Opponent);
            GameClient.SpeakToMap(Color.Red, ChatType.Center, string.Format("{0} Has been given the bomb and has 5 seconds to pass it before he blows up.", Opponent.Hero.Name), TournamentBase.Map.ID, TournamentBase.Map.DynamicID);
        }

        private static void AddBomb(GameClient Opponent)
        {
            var StatPacket = StatTypePacket.Create();
            StatPacket.ID = StatIDs.RaiseFlag;
            Opponent.Hero.StatusFlag |= StatusFlag.Curse;
            StatPacket.BigValue = (ulong)Opponent.Hero.StatusFlag;
            StatPacket.UID = Opponent.Hero.UID;
            Opponent.SendScreen(StatPacket, true);

            Opponent.HasBomb = true;
            Opponent.Hero.Stamps.BombTime = DateTime.Now.AddMilliseconds(5000);
        }

        public static void GiveRandomBomb()
        {
            if (TournamentBase.Entries.Count <= 1)
                return;

            var Client = TournamentBase.Entries.GetRandomElement();
            if (Client != null)
                AddBomb(Client);
        }

        public static void RemoveBomb(GameClient Client)
        {
            var StatPacket = StatTypePacket.Create();
            StatPacket.ID = StatIDs.RaiseFlag;
            Client.Hero.StatusFlag &= ~StatusFlag.Curse;
            StatPacket.BigValue = (ulong)Client.Hero.StatusFlag;
            StatPacket.UID = Client.Hero.UID;
            Client.SendScreen(StatPacket, true);
            Client.HasBomb = false;

            GiveRandomBomb();

            var GDrop = new ItemGroundPacket((uint)RandomGenerator.Generator.Next(), 23, GroundIDs.RemoveGroundEffect, Client.Hero.X, Client.Hero.Y);
            Client.SendScreen(GDrop.Serialize(), true);
        }
    }
}
