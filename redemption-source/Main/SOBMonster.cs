﻿using System;
using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Events;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Main
{
    public delegate void SOBMobEvent(GameClient Client, int Param, ushort SpellId);

    public unsafe class SOBMonster : IBaseEntity, IMapObject
    {
        private bool m_Dead;
        public SOBMobEvent OnAttack, OnDie;
        public byte[] SpawnPacket;

        public SOBMonster(EntityFlag flag)
        {
            SpawnPacket = new byte[44 + 8];
            fixed (byte* lpPacket = SpawnPacket)
            {
                *(ushort*)lpPacket = (ushort)(SpawnPacket.Length - 8);
                *(ushort*)(lpPacket + 2) = 0x455;
                lpPacket[26] = 0x01;
            }
            EntityFlag = flag;
            switch (EntityFlag)
            {
                case EntityFlag.TrainingGround:
                    {
                        OnDie = TrainingMobDead;
                        OnAttack = TrainingMobAttacked;
                        break;
                    }
                case EntityFlag.Gate:
                    {
                        OnDie = OpenGate;
                        Name = "Gate";
                        break;
                    }
                case EntityFlag.Pole:
                    {
                        OnAttack = PoleHit;
                        OnDie = PoleDead;
                        break;
                    }
            }
        }

        private void OpenGate(GameClient Client, int Param, ushort SpellId = 0)
        {
            GuildWar.ReverseGate(this);
            Client.SendScreen(SpawnPacket, true);

            if (GuildWar.PoleHolder == "")
                return;

            if (Client.Guild.Name == GuildWar.PoleHolder)
            {
                foreach (var DE in Client.Guild.Members)
                {
                    var Content = DE.Value.Split('~');
                    var Member = Kernel.FindClient(uint.Parse(Content[1]));
                    Member?.Speak(Color.Yellow, ChatType.Guild, GuildWar.LeftGateOpen ? "The Left gate has been broken." : "The Right gate has been broken.");
                }
            }
        }

        private void PoleHit(GameClient Client, int Param, ushort SpellId = 0)
        {
            GuildWarScore tmp;
            if (GuildWar.Scores.TryGetValue(Client.Guild.UID, out tmp))
            {
                tmp.Score += Param;
                GuildWar.Scores[Client.Guild.UID] = tmp;
            }
            else
            {
                tmp.Score = Param;
                GuildWarScore.GuildName = Client.Guild.Name;
                GuildWar.Scores.TryAdd(Client.Guild.UID, tmp);
            }
        }

        private void TrainingMobAttacked(GameClient Client, int Parma, ushort SpellId)
        {
            if (Level > Client.Hero.Level)
            {
                Client.Speak(Color.Red, ChatType.TopLeft, string.Format("You need level {0} to attack this stake.", Level));
                return;
            }

            Experience.ProcessLeveling(Client, this, (uint)Parma, SpellId);
        }

        private void TrainingMobDead(GameClient Client, int Param, ushort SpellId = 0)
        {
            Dead = false;
            Hitpoints = MaxHitpoints;
            Client.SendScreen(SpawnPacket, true);
        }

        private void PoleDead(GameClient Client, int Param, ushort SpellId = 0)
        {
            var Score = 0;
            foreach (var kvp in GuildWar.Scores)
            {
                if (kvp.Value.Score > Score)
                    Score = kvp.Value.Score;
            }
            GuildWar.Scores.Clear();

            Dead = false;
            Name = Client.Guild.Name;
            Hitpoints = MaxHitpoints;
            Client.SendScreen(SpawnPacket, true);

            SOBMonster gate;
            if (GuildWar.LeftGateOpen)
            {
                gate = GuildWar.GuildMobs[GuildWar.RightGateUID];
                gate.Dead = false;
                GuildWar.ReverseGate(gate);
            }
            if (GuildWar.RightGateOpen)
            {
                gate = GuildWar.GuildMobs[GuildWar.LeftGateUID];
                gate.Dead = false;
                GuildWar.ReverseGate(gate);
            }
            GuildWar.PoleHolder = Client.Guild.Name;

            if (DateTime.Now.Hour < GuildWar.GuildWarHour)
                GameClient.SpeakToAll(Color.Yellow, ChatType.Center, string.Format("{0} has taken the pole with a score of {1}", GuildWar.PoleHolder, Score));
            else
            {
                GuildWar.Active = false;
                Client.Guild.WarWins += 1;
                GameClient.SpeakToAll(Color.Yellow, ChatType.Center, string.Format("Congratulations {0} has won GuildWar!", GuildWar.PoleHolder));
            }
        }

        #region IMapObject Members
        public Map MapID { get; set; }
        public MapObjectType MapObjType => MapObjectType.SOB;
        public object Owner => this;

        public void SendSpawn(GameClient Client, bool Ignore)
        {
            if (Client.Screen.Add(this) || Ignore)
                Client.Send(SpawnPacket);
        }

        public void SendSpawn(GameClient Client)
        {
            SendSpawn(Client, false);
        }
        #endregion
        #region IBaseEntityMembers
        public uint MinAttack => 0;
        public uint MaxAttack => 0;
        public ushort MDefence => 0;
        public ushort PlusMDefence => 0;
        public sbyte Dodge => 0;

        public StatusFlag StatusFlag
        {
            get => 0;
            set { }
        }

        public uint MagicAttack => 0;
        public EntityFlag EntityFlag { get; }

        public bool Dead
        {
            get
            {
                if ((Hitpoints <= 0) && !m_Dead)
                    Dead = true;
                return m_Dead;
            }
            set
            {
                if (value != m_Dead)
                {
                    Hitpoints = value ? 0 : MaxHitpoints;
                    m_Dead = value;
                }
            }
        }

        public ushort Level { get; set; }
        public ushort Defence { get; set; }
        #endregion
        #region Properties
        public uint UID
        {
            get
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    return *(uint*)(Pointer + 4);
                }
            }
            set
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    *(uint*)(Pointer + 4) = value;
                }
            }
        }

        public int MaxHitpoints
        {
            get
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    return *(int*)(Pointer + 8);
                }
            }
            set
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    *(int*)(Pointer + 8) = value;
                }
            }
        }

        public int Hitpoints
        {
            get
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    return *(int*)(Pointer + 12);
                }
            }
            set
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    *(int*)(Pointer + 12) = Math.Max(value, 0);
                }
            }
        }

        public ushort X
        {
            get
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    return *(ushort*)(Pointer + 16);
                }
            }
            set
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    *(ushort*)(Pointer + 16) = value;
                }
            }
        }

        public ushort Y
        {
            get
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    return *(ushort*)(Pointer + 18);
                }
            }
            set
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    *(ushort*)(Pointer + 18) = value;
                }
            }
        }

        public uint Mesh
        {
            get
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    return *(uint*)(Pointer + 20);
                }
            }
            set
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    *(uint*)(Pointer + 20) = value;
                }
            }
        }

        public EntityFlag SOBType
        {
            get => (EntityFlag)SpawnPacket[22];
            set => SpawnPacket[22] = (byte)value;
        }

        public ConquerAngle Facing
        {
            get => (ConquerAngle)SpawnPacket[24];
            set => SpawnPacket[24] = (byte)value;
        }

        public string Name
        {
            get
            {
                fixed (byte* Pointer = SpawnPacket)
                {
                    return new string((sbyte*)Pointer + 28);
                }
            }
            set
            {
                SpawnPacket[27] = (byte)value.Length;
                fixed (byte* Pointer = SpawnPacket)
                {
                    PacketKernel.Encode(Pointer, value, 28);
                }
            }
        }
        #endregion
    }
}