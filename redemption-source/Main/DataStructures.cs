﻿using ConquerEmulator.MultiThreading;

namespace ConquerEmulator.Main
{
	public class StatusConstants
	{
		public const int AdjustSet = -30000, AdjustFull = -32768, AdjustPercent = 30000;
	}

    internal class DataStructures
    {
        public static sbyte ViewDistance = 18;

        public static ushort AuthPort = 9958;
        public static ushort GamePort = 5816;
        public static ushort Backlog = 100;

        public static uint MaxPacketSize = 4096;
        public static uint MaxCurrency = 2100000000;

        public static int DefaultDefense2 = 10000;

        public static bool ChanceSuccess(double Chance)
        {
            return RandomGenerator.Generator.NextDouble() < Chance / 100;
        }

        #region Timers
        public static uint Flashing = 15000;
        public static uint Pointloss = 180000;
        #endregion
        #region Rates
        // Flat rate
        public static byte ExperienceRate;

        // Monster Drops
        public static double Meteor, DragonBall;
        public static double PlusOne;

        // Mining
        public static int IronOre, CopperOre, EuxeniteOre, SilverOre, GoldOre;
        public static int NormalGem, RefinedGem, SuperGem;

        // Socketing
        public static double MetSock1, MetSock2;
        public static double DBSock1, DBSock2;
        #endregion
    }
}