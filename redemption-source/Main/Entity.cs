﻿using System;
using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Item;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Mob;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Main
{
	public enum ConquerAngle : sbyte
	{
		Unknown = -1,
		SouthWest = 0,
		West = 1,
		NorthWest = 2,
		North = 3,
		NorthEast = 4,
		East = 5,
		SouthEast = 6,
		South = 7
	}

	public enum ConquerAction : byte
	{
		None = 0x00,
		Cool = 0xE6,
		Kneel = 0xD2,
		Sad = 0xAA,
		Happy = 0x96,
		Angry = 0xA0,
		Lie = 0x0E,
		Dance = 0x01,
		Wave = 0xBE,
		Bow = 0xC8,
		Sit = 0xFA,
		Stand = 0x64
	}

	public unsafe class Entity : IBaseEntity, IMapObject
	{
		public byte[] SpawnPacket;

		public Entity(EntityFlag flag, object AOwner)
		{
			EntityFlag = flag;
			Owner = AOwner;
			Stamps = new Stamps();
			Speed = 1000;
			SpawnPacket = new byte[128 + 8];
			fixed (byte* Packet = SpawnPacket)
			{
				*(ushort*)(Packet + 0) = (ushort)(SpawnPacket.Length - 8);
				*(ushort*)(Packet + 2) = 0x271E;
			}

			switch (EntityFlag)
			{
				case EntityFlag.Player: MapObjType = MapObjectType.Player; break;
				case EntityFlag.Monster: MapObjType = MapObjectType.Monster; break;
				case EntityFlag.Reviver: MapObjType = MapObjectType.Reviver; break;
				case EntityFlag.Item: MapObjType = MapObjectType.Item; break;
				case EntityFlag.Pet: MapObjType = MapObjectType.Monster; break;
				default: throw new ArgumentException("flag");
			}
		}

		public void SendSpawn(GameClient Client)
		{
			SendSpawn(Client, false);
		}

		public void SendSpawn(GameClient Client, bool IgnoreScreen)
		{
			if (Client.Screen.Add(this) || IgnoreScreen)
				Client.Send(SpawnPacket);
		}

		public void Die(Entity Attacker)
		{
			switch (EntityFlag)
			{
				case EntityFlag.Player:
					#region StatusFlag
					StatusFlag |= StatusFlag.Dead | StatusFlag.Ghost;
					StatusFlag &= ~StatusFlag.Fly;
					StatusFlag &= ~StatusFlag.XPList;
					StatusFlag &= ~StatusFlag.Superman;
					StatusFlag &= ~StatusFlag.Cyclone;
					StatusFlag &= ~StatusFlag.MagicShield;
					StatusFlag &= ~StatusFlag.Poisoned;
					StatusFlag &= ~StatusFlag.FlashingName;
					StatusFlag &= ~StatusFlag.Cyclone;
					#endregion
					Dead = true;
					Stamps.Revive = DateTime.Now.AddSeconds(17);

					var Client = Owner as GameClient;
					if (Client == null)
						break;

					Client.Pet?.Kill();
					if (Client.Miner.Mining)
						Client.Miner.Stop();
					#region Packets
					var big = new BigUpdatePacket(2);
					big.UID = UID;
					big.Append(0, StatIDs.RaiseFlag, (ulong)StatusFlag);
					big.Append(1, StatIDs.Model, Model);
					Client.SendScreen(big, true);
					#endregion
					#region Red - Black Name
					if (Client.Flag >= PlayerFlag.GameManager)
						return;

					var Settings = new MapSettings(Client.Hero.MapID.ID);
					if (Settings.Status.CanGainPKPoints)
						return;

					if ((PkPoints >= 30) && (PkPoints <= 99))
					{
						foreach (var Equipped in Client.Equipment)
						{
							var Item = (ConquerItem)Equipped.Value;
							if (DataStructures.ChanceSuccess(5.0)) //5% chance to Drop an item
								FloorItem.Drop(this, Item);
						}
					}
					else if (PkPoints >= 100)
					{
						foreach (var Equipped in Client.Equipment)
						{
							var Item = (ConquerItem)Equipped.Value;
							if (DataStructures.ChanceSuccess(10.0)) //10% chance to Drop an item
								FloorItem.Drop(this, Item);
						}
						if (Attacker != null)
						{
							Client.Teleport(6000, 50, 50);
							Client.Speak(Color.Red, ChatType.Talk, string.Format("You've been jailed by {0}", Attacker.Name));
							GameClient.SpeakToAll(Color.Red, ChatType.TopLeft, string.Format("{0} has been jailed by {1}.", Client.Hero.Name, Attacker.Name));
						}
					}
					#endregion
					break;
				case EntityFlag.Monster:
                    MonsterAI.Kill((Monster)Owner, Attacker);
					MonsterAI.Drop((Monster)Owner, Attacker);
					break;
				case EntityFlag.Pet:
					var gameClient = Owner as GameClient;
					gameClient?.Pet.Kill();
					break;
			}
		}

		public void SetNameColor()
		{
			if (PkPoints >= 30 && PkPoints <= 99)
				StatusFlag |= StatusFlag.RedName;
			else if (PkPoints >= 100)
				StatusFlag |= StatusFlag.BlackName;
			else
			{
				StatusFlag &= ~StatusFlag.RedName;
				StatusFlag &= ~StatusFlag.BlackName;
			}
		}
		#region PrivateVariables
		private ushort m_PkPoints;
		private bool m_Flashing;
		public ushort m_Avatar;
		public ushort m_Sex;
		private ushort m_OverlappingMesh;
        private int m_Hitpoints;
        #endregion
        #region PublicVariables
        public bool Flashing
		{
			get => m_Flashing;
            set
			{
				if (value)
				{
					SetNameColor();
					StatusFlag |= StatusFlag.FlashingName;
				}
				else
				{
					SetNameColor();
					Stamps.Flashing = DateTime.Now;
					StatusFlag &= ~StatusFlag.FlashingName;
				}
				var gameClient = Owner as GameClient;
				gameClient?.SendScreen(new StatTypePacket(UID, (uint)StatusFlag, StatIDs.RaiseFlag).Serialize(), true);
				m_Flashing = value;
			}
		}

		public ushort Speed;
		public ushort LastMap;
		public ushort PkPoints
		{
			get => m_PkPoints;
		    set
			{
				var gameClient = Owner as GameClient;
				gameClient?.Send(new StatTypePacket(UID, value, StatIDs.PkPoint).Serialize());
				m_PkPoints = value;
			}
		}
		public ushort MagicDefence;

		public string Name
		{
			get
			{
				fixed (byte* Packet = SpawnPacket)
				{
					return new string((sbyte*)(Packet + 112));
				}
			}
			set
			{
				SpawnPacket[110] = 0x01;
				SpawnPacket[111] = (byte)value.Length;
				fixed (byte* Packet = SpawnPacket)
				{
					PacketKernel.Encode(Packet, value, 112);
				}
			}
		}
        public uint Model
        {
            get
            {
                fixed (byte* Packet = SpawnPacket)
                {
                    return *(uint*)(Packet + 4);
                }
            }
            set
            {
                fixed (byte* Packet = SpawnPacket)
                {
                    *(uint*)(Packet + 4) = value;
                }
            }
        }
        public uint UID
		{
			get
			{
				fixed (byte* Packet = SpawnPacket)
				{
					return *(uint*)(Packet + 8);
				}
			}
			set
			{
				fixed (byte* Packet = SpawnPacket)
				{
					*(uint*)(Packet + 8) = value;
				}
			}
		}
        public ushort GuildID
        {
            get
            {
                fixed (byte* Packet = SpawnPacket)
                {
                    return *(ushort*)(Packet + 12);
                }
            }
            set
            {
                fixed (byte* Packet = SpawnPacket)
                {
                    *(ushort*)(Packet + 12) = value;
                }
            }
        }

        public GuildPosition GuildRank
        {
            get
            {
                fixed (byte* Packet = SpawnPacket)
                {
                    return *(GuildPosition*)(Packet + 15);
                }
            }
            set
            {
                fixed (byte* Packet = SpawnPacket)
                {
                    *(GuildPosition*)(Packet + 15) = value;
                }
            }
        }
        public StatusFlag StatusFlag
		{
			get
			{
				fixed (byte* Packet = SpawnPacket)
				{
					return *(StatusFlag*)(Packet + 16);
				}
			}
			set
			{
				if (value < 0)
					value = StatusFlag.None;
				fixed (byte* Packet = SpawnPacket)
				{
					*(ulong*)(Packet + 16) = (ulong)value;
				}
			}
		}
        public int Hitpoints
		{
			get { return m_Hitpoints; }
			set
			{
				fixed (byte* Packet = SpawnPacket)
				{
				    m_Hitpoints = value;
					*(ushort*)(Packet + 48) = (ushort)value;
				}
			}
		}
		public ushort Level
		{
			get
			{
				fixed (byte* Packet = SpawnPacket)
				{
					return *(ushort*)(Packet + 50);
				}
			}
			set
			{
				fixed (byte* Packet = SpawnPacket)
				{
					*(ushort*)(Packet + 50) = value;
				}
			}
		}
        public ushort Hairstyle
        {
            get
            {
                fixed (byte* Packet = SpawnPacket)
                {
                    return *(ushort*)(Packet + 52);
                }
            }
            set
            {
                fixed (byte* Packet = SpawnPacket)
                {
                    *(ushort*)(Packet + 52) = value;
                }
            }
        }
        public ushort X
		{
			get
			{
				fixed (byte* Packet = SpawnPacket)
				{
					return *(ushort*)(Packet + 54);
				}
			}
			set
			{
				LastX = X;
				fixed (byte* Packet = SpawnPacket)
				{
					*(ushort*)(Packet + 54) = value;
				}
			}
		}
		public ushort Y
		{
			get
			{
				fixed (byte* Packet = SpawnPacket)
				{
					return *(ushort*)(Packet + 56);
				}
			}
			set
			{
				LastY = Y;
				fixed (byte* Packet = SpawnPacket)
				{
					*(ushort*)(Packet + 56) = value;
				}
			}
		}
		public ConquerAngle Facing
		{
			get => (ConquerAngle)SpawnPacket[58];
		    set => SpawnPacket[58] = (byte)value;
		}
		public ConquerAction Action
		{
			get => (ConquerAction)SpawnPacket[59];
		    set => SpawnPacket[59] = (byte)value;
		}

		public bool InTransformation => OverlappingMesh != 0;

	    public ushort OverlappingMesh
		{
			get => m_OverlappingMesh;
		    set
			{
				m_OverlappingMesh = value;
				Model = (uint)(m_OverlappingMesh * 10000000 + m_Avatar * 10000 + m_Sex);
			}
		}

		public ushort Sex
		{
			get => m_Sex;
		    set
			{
				Model = Convert.ToUInt32(m_Avatar.ToString() + value);
				m_Sex = value;
			}
		}

		public ushort Avatar
		{
			get => m_Avatar;
		    set
			{
				Model = Convert.ToUInt32(value + m_Sex.ToString());
				m_Avatar = value;
			}
		}
		public bool Dead
		{
			get => Hitpoints <= 0;
		    set
			{
				if (value)
				{
					Hitpoints = 0;
					if (EntityFlag == EntityFlag.Player)
					{
						var Generic = (sbyte)(97 + (sbyte)Math.Floor(Sex / 1000M));
						Model = (uint)(Generic * 10000000 + Avatar * 10000 + Sex);
					}
				}
				else
				{
					Hitpoints = MaxHitpoints;
					if (EntityFlag == EntityFlag.Player)
						Model = (uint)(Avatar * 10000 + Sex);
				}
			}
		}

		public ushort Defence { get; set; }
		public sbyte Dodge { get; set; }
		public uint MagicAttack { get; set; }
		public Map MapID { get; set; }
		public uint MaxAttack { get; set; }
		public int MaxHitpoints { get; set; }
		public ushort MDefence { get; set; }
		public uint MinAttack { get; set; }
		public object Owner { get; set; }
		public ushort PlusMDefence { get; set; }

		public EntityFlag EntityFlag { get; set; }
		public MapObjectType MapObjType { get; set; }
		public Stamps Stamps;

		public ushort Reborn;

		public ushort LastX;
		public ushort LastY;

		public PlayerFlag Flag;
		#endregion
	}
}