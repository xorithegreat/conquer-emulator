﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Timers;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Item;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Mob;
using ConquerEmulator.Network;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Main
{
	public delegate int ConquerCallback(IBaseEntity sender, IBaseEntity caller);

	internal class Kernel
	{
	    public static Dictionary<SystemSocket, GameClient> GameClients = new Dictionary<SystemSocket, GameClient>();
	    public static ConcurrentDictionary<uint, ConquerItem> GroundItems = new ConcurrentDictionary<uint, ConquerItem>();
	    public static ConcurrentDictionary<ushort, ConquerGuild> Guilds = new ConcurrentDictionary<ushort, ConquerGuild>();
	    public static List<KoInfo> KoTop100 = new List<KoInfo>();

	    public static Dictionary<uint, Dictionary<uint, INpc>> Npcs = new Dictionary<uint, Dictionary<uint, INpc>>();
	    public static Dictionary<uint, SOBMonster> TrainingMobs = new Dictionary<uint, SOBMonster>();
	    public static Dictionary<uint, MonsterSpawn> MonsterSpawns = new Dictionary<uint, MonsterSpawn>();
	    public static Dictionary<uint, MonsterStatus> MonsterStats = new Dictionary<uint, MonsterStatus>();
	    public static ConcurrentDictionary<uint, Dictionary<uint, Monster>> Monsters = new ConcurrentDictionary<uint, Dictionary<uint, Monster>>();
	    public static Dictionary<uint, ReviveMonster> Revivers = new Dictionary<uint, ReviveMonster>();

	    public static Dictionary<IConquerItem, uint> ShopItems = new Dictionary<IConquerItem, uint>();
	    public static List<IConquerItem> LotteryItems = new List<IConquerItem>();
	    public static List<string> BannedIPs = new List<string>();

	    public static MessageBoard TradeBoard = new MessageBoard();
	    public static MessageBoard FriendBoard = new MessageBoard();
	    public static MessageBoard TeamBoard = new MessageBoard();
	    public static MessageBoard SynBoard = new MessageBoard();
	    public static MessageBoard OtherBoard = new MessageBoard();
	    public static MessageBoard SystemBoard = new MessageBoard();

	    public static Timer PlayerTimer = new Timer();
	    public static Timer MonsterTimer = new Timer();
	    public static Timer AutoAttackTimer = new Timer();
	    public static Timer TournamentTimer = new Timer();

	    public static DateTime SaveTime = DateTime.Now.AddMinutes(10);
	    public static DateTime GuildScores = DateTime.Now.AddSeconds(5);
	    public static DateTime LastFloorCleanUp = DateTime.Now.AddSeconds(5);

	    public static GameClient[] Clients = new GameClient[0];
	    public static bool AncientDevil { get; set; }

	    public static void UpdateGameClients()
		{
			Update:
			try
			{
				Clients = GameClients.Values.ToArray();
			}
			catch (IndexOutOfRangeException)
			{
				goto Update;
			}
		}

	    public static GameClient FindClient(uint UID)
		{
			return Clients.FirstOrDefault(Client => Client.Hero.UID == UID);
		}

	    public static GameClient FindClient(string Name)
		{
			return Clients.FirstOrDefault(Client => Client.Hero.Name == Name);
		}

	    public static Entity FindEntity(string Name)
		{
			Entity retn = null;
			foreach (var iClient in Clients)
				if (iClient.Hero.Name == Name)
					retn = iClient.Hero;
			return retn;
		}

	    public static Entity FindEntity(uint UID)
		{
			Entity retn = null;
			foreach (var iClient in Clients)
				if (iClient.Hero.UID == UID)
					retn = iClient.Hero;
			return retn;
		}

	    public static ConquerGuild FGuild(uint UID)
		{
			return Guilds.Values.FirstOrDefault(Guild => Guild.UID == UID);
		}

	    public static ConquerGuild FGuild(string Name)
		{
			return Guilds.Values.FirstOrDefault(Guild => Guild.Name == Name);
		}

	    public static void Restart()
		{
			SaveAll(true);
			var p = new Process();
			p.StartInfo.FileName = "ConquerEmulator";
			p.Start();
			Environment.Exit(1);
		}

	    public static void SaveAll(bool Shutdown)
		{
			if (Clients.Length > 0)
			{
				Console.WriteLine("Saving {0} Characters. . .", Clients.Length);

                foreach (var Client in Clients)
                {
                    if (Shutdown)
                        Client.Socket.Disconnect();
                    else
                        MySqlDatabase.SaveCharacter(Client);
                }
			}
			MySqlDatabase.SaveGuilds();
		}

	    public static void SpawnMonster(uint TypeId, ushort X, ushort Y, uint MapId)
        {
            var MS = MonsterStats[TypeId];
            var Monster = new Monster();
            Monster.Entity = new Entity(EntityFlag.Monster, Monster);

            var Spawn = new MonsterSpawn();
            Spawn.Type = MS.Id;
            Spawn.XStart = Spawn.XStop = X;
            Spawn.YStart = Spawn.YStop = Y;
            Spawn.MapID = MapId;
            Spawn.UniqueID = Spawn.ManualSpawnCounter++;
            Spawn.NumberToSpawn = 1;
            Monster.LoadMobStatus(Monster, MS, Spawn);

            GetMobDictionary:
            Dictionary<uint, Monster> Mobs;
            if (Monsters.TryGetValue(Monster.Entity.MapID, out Mobs))
                Mobs.Add(Monster.Entity.UID, Monster);
            else
            {
                Mobs = new Dictionary<uint, Monster>();
                Monsters.TryAdd(Monster.Entity.MapID, Mobs);
                goto GetMobDictionary;
            }
        }

	    public static KoInfo GetKo(string Name)
		{
			return KoTop100.FirstOrDefault(x => x.Name == Name);
		}

	    public static void UpdateKoBoard(GameClient Client)
		{
		    var info = Client.KoInfo;
            var startPosition = info.Position;
			if ((KoTop100.Count >= 100) && (info.KillCount <= KoTop100[99].KillCount))
				return;
			if (KoTop100.All(x => x.Name != info.Name))
				KoTop100.Add(info);

			KoTop100.Sort((y, x) => x.KillCount.CompareTo(y.KillCount));
			if (KoTop100.Count > 100)
			{
				KoTop100[100].Position = -1;
				KoTop100.RemoveAt(100);
			}

			for (var i = 0; i < KoTop100.Count; i++)
				KoTop100[i].Position = i;

            if (info.Position > startPosition)
            {
                Client.KoInfo.KillCount = Client.HeadKillCounter;
                GameClient.SpeakToAll(Color.White, ChatType.Talk, $"{info.Name} has killed {info.KillCount} monsters and ranks {info.Position} on the KoBoard.");
            }
		}

	    public static void IncXY(ConquerAngle Facing, ref ushort x, ref ushort y)
		{
			sbyte xi, yi;
			xi = yi = 0;
			switch (Facing)
			{
				case ConquerAngle.North: xi = -1; yi = -1; break;
				case ConquerAngle.South: xi = 1; yi = 1; break;
				case ConquerAngle.East: xi = 1; yi = -1; break;
				case ConquerAngle.West: xi = -1; yi = 1; break;
				case ConquerAngle.NorthWest: xi = -1; break;
				case ConquerAngle.SouthWest: yi = 1; break;
				case ConquerAngle.NorthEast: yi = -1; break;
				case ConquerAngle.SouthEast: xi = 1; break;
			}
			x = (ushort)(x + xi);
			y = (ushort)(y + yi);
		}

	    public static int GetBaseClass(byte job)
		{
			if (job >= 100)
				return job / 100 * 100;
			return job / 10 * 10;
		}

	    public static int GetDistance(ushort X, ushort Y, ushort X2, ushort Y2)
		{
			return Math.Max(Math.Abs(X - X2), Math.Abs(Y - Y2));
		}

	    public static int GetDistance(IBaseEntity Entity, IBaseEntity Entity2)
		{
			return Math.Max(Math.Abs(Entity.X - Entity2.X), Math.Abs(Entity.Y - Entity2.Y));
		}

	    public static ConquerAngle GetFacing(short angle)
		{
			var c_angle = (sbyte)(angle / 46 - 1);
			return c_angle == -1 ? ConquerAngle.South : (ConquerAngle)c_angle;
		}

	    public static short GetAngle(ushort X, ushort Y, ushort x2, ushort y2)
		{
			var r = Math.Atan2(y2 - Y, x2 - X);
			if (r < 0)
				r += Math.PI * 2;
			return (short)Math.Round(r * 180 / Math.PI);
		}

	    public static double GetE2DDistance(int X, int Y, int X2, int Y2)
        {
            var x = Math.Abs(X - X2);
            var y = Math.Abs(Y - Y2);
            return Math.Sqrt((x * x) + (y * y));
        }

	    public static ushort GetAttributePoints(ushort Level)
		{
			const byte PrimeLevel = 120;

			ushort ret = 0;
			for (var i = (short)(Level - PrimeLevel); i > 0; i--)
				ret = (ushort)(ret + i);
			return ret;
		}

	    public static long SecondsFromNow(DateTime end)
        {
            long remaining = 0;
            if (end < DateTime.Now)
                return remaining;
            var diff = end.Subtract(DateTime.Now);
            remaining += diff.Seconds;
            remaining += diff.Minutes * 60;
            remaining += diff.Hours * 3600;
            remaining += diff.Days * 86400;
            return remaining;
        }
	}
}