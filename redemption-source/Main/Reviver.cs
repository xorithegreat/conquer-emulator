﻿using System;
using System.Collections.Generic;
using ConquerEmulator.Attacking;
using ConquerEmulator.Client;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Main
{
	internal class ReviveMonster : Entity
	{
	    public static ConquerSpell Revive = new ConquerSpell(1100, 0);

	    public ReviveMonster(uint UID, uint MapID, ushort X, ushort Y) : base(EntityFlag.Reviver, null)
		{
			Sex = 910;
			Level = byte.MaxValue;
			Name = "Reviver";
			this.UID = UID;
			this.X = X;
			this.Y = Y;
			this.MapID = new Map(MapID);
		}

	    public bool ValidTarget(GameClient Client)
		{
			if (Client.Hero.Dead)
			{
				if (DateTime.Now.AddSeconds(7) >= Client.Hero.Stamps.Revive)
				{
					Client.Revive(Client, false);
					var Targets = new Dictionary<IBaseEntity, Damage>(1);
					Targets.Add(Client.Hero, new Damage(0, 0));
					Packets.ShowAttack(this, Targets, Revive, Client.Hero.X, Client.Hero.Y);
					return true;
				}
			}
			return false;
		}
	}
}