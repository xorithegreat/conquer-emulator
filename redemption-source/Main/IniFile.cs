﻿using System;

namespace ConquerEmulator.Main
{
	public unsafe class IniFile
	{
	    public const int Int32_Size = 15, Int16_Size = 9, Int8_Size = 6, Bool_Size = 6, Double_Size = 20, Int64_Size = 22, Float_Size = 10;

	    public static Func<string, int> ToInt32 = int.Parse;
	    public static Func<string, uint> ToUInt32 = uint.Parse;
	    public static Func<string, short> ToInt16 = short.Parse;
	    public static Func<string, ushort> ToUInt16 = ushort.Parse;
	    public static Func<string, sbyte> ToInt8 = sbyte.Parse;
	    public static Func<string, byte> ToUInt8 = byte.Parse;
	    public static Func<string, bool> ToBool = bool.Parse;
	    public static Func<string, double> ToDouble = double.Parse;
	    public static Func<string, long> ToInt64 = long.Parse;
	    public static Func<string, ulong> ToUInt64 = ulong.Parse;
	    public static Func<string, float> ToFloat = float.Parse;
	    public string FileName;

	    public IniFile(string _FileName)
		{
			FileName = _FileName;
		}

	    public IniFile()
		{
			FileName = null;
		}

	    public string ReadString(string Table, string FileName, string Section, string Key, string Default)
		{
			return ReadString(Section, Key, Default);
		}

	    public string ReadString(string Section, string Key, string Default, int Size)
		{
			char* lpBuffer = stackalloc char[Size];
			Native.GetPrivateProfileStringW(Section, Key, Default, lpBuffer, Size, FileName);
			return new string(lpBuffer).Trim('\0');
		}

	    public string ReadString(string Section, string Key, string Default)
		{
			return ReadString(Section, Key, Default, 255);
		}

	    public T ReadValue<T>(string Section, string Key, T Default, Func<string, T> callback, int BufferSize)
		{
			try
			{
				return callback.Invoke(ReadString(Section, Key, Default.ToString(), BufferSize));
			}
			catch
			{
				return Default;
			}
		}

	    public bool SectionExists(string Section)
		{
			char* temp = stackalloc char[Section.Length + 1];
			var r = Native.GetPrivateProfileSectionW(Section, temp, Section.Length + 1, FileName);
			return temp[0] != 0;
		}

	    public int ReadInt32(string Section, string Key, int Default)
		{
			return ReadValue(Section, Key, Default, ToInt32, Int32_Size);
		}

	    public ulong ReadUInt64(string Section, string Key, ulong Default)
		{
			return ReadValue(Section, Key, Default, ToUInt64, Int64_Size);
		}

	    public long ReadInt64(string Section, string Key, long Default)
		{
			return ReadValue(Section, Key, Default, ToInt64, Int64_Size);
		}

	    public double ReadDouble(string Section, string Key, double Default)
		{
			return ReadValue(Section, Key, Default, ToDouble, Double_Size);
		}

	    public uint ReadUInt32(string Section, string Key, uint Default)
		{
			return ReadValue(Section, Key, Default, ToUInt32, Int32_Size);
		}

	    public short ReadInt16(string Section, string Key, short Default)
		{
			return ReadValue(Section, Key, Default, ToInt16, Int16_Size);
		}

	    public ushort ReadUInt16(string Section, string Key, ushort Default)
		{
			return ReadValue(Section, Key, Default, ToUInt16, Int16_Size);
		}

	    public sbyte ReadSByte(string Section, string Key, sbyte Default)
		{
			return ReadValue(Section, Key, Default, ToInt8, Int8_Size);
		}

	    public byte ReadByte(string Section, string Key, byte Default)
		{
			return ReadValue(Section, Key, Default, ToUInt8, Int8_Size);
		}

	    public bool ReadBool(string Section, string Key, bool Default)
		{
			return ReadValue(Section, Key, Default, ToBool, Bool_Size);
		}

	    public float ReadFloat(string Section, string Key, float Default)
		{
			return ReadValue(Section, Key, Default, ToFloat, Float_Size);
		}

	    public void WriteString(string Section, string Key, string Value)
		{
			Native.WritePrivateProfileStringW(Section, Key, Value, FileName);
		}

	    public void Write<T>(string Section, string Key, T Value)
		{
			Native.WritePrivateProfileStringW(Section, Key, Value.ToString(), FileName);
		}
	}
}