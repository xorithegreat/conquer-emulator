﻿using System;
using System.Drawing;

namespace ConquerEmulator.Main.DMap
{
	public enum CompressBitmapColor : byte
	{
		Black = 0,
		Blue = 1,
		Green = 2,
		Red = 3,
		White = 4
	}

	public class CompressedBitmap
	{
	    private readonly CompressBitmapColor[,] Coordinates;
	    public readonly int MaxHeight;
	    public readonly int MaxWidth;

	    public CompressedBitmap(string FileName)
		{
			using (var bmp = new Bitmap(FileName))
			{
				MaxWidth = bmp.Width;
				MaxHeight = bmp.Height;
				Coordinates = new CompressBitmapColor[MaxWidth, MaxHeight];
				for (var x = 0; x < bmp.Width; x++)
					for (var y = 0; y < bmp.Height; y++)
						Coordinates[x, y] = Translate((uint)bmp.GetPixel(x, y).ToArgb());
			}
		}

	    public CompressBitmapColor this[int x, int y]
		{
			get
			{
				if ((x < MaxWidth) && (y < MaxHeight))
					return Coordinates[x, y];
				return 0;
			}
		}

	    public static CompressBitmapColor Translate(uint dwColor)
		{
			switch (dwColor)
			{
				case 0xFF000000:
					return CompressBitmapColor.Black;
				case 0xFFFFFFFF:
					return CompressBitmapColor.White;
				case 0xFFFF0000:
					return CompressBitmapColor.Red;
				case 0xFF00FF00:
					return CompressBitmapColor.Green;
				case 0xFF0000FF:
					return CompressBitmapColor.Blue;
			}
			throw new ArgumentException("dwColor");
		}
	}
}