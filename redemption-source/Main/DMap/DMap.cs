﻿using System;
using System.Collections.Generic;
using System.IO;
using ConquerEmulator.Database;

namespace ConquerEmulator.Main.DMap
{
	[Flags]
	public enum DMapCellFlag : ushort
	{
		Invalid = 0x01,
		MonsterOnTile = 0x02,
		ItemOnTile = 0x04
	}

	public class ConquerDMap
	{
	    public static Dictionary<uint, ConquerDMap> Maps = new Dictionary<uint, ConquerDMap>();
	    private readonly DMapCellFlag[,] BinaryData;

	    public Map MapID;
	    public int MaxX, MaxY;

	    public ConquerDMap(uint MapID, uint MapDoc)
		{
			this.MapID = new Map(MapID);
			this.MapID.MapDoc = MapDoc;

			using (var Reader = new BinaryReader(new FileStream(Environment.CurrentDirectory + @"\Database\Maps\" + MapDoc + ".JMap", FileMode.Open)))
			{
				MaxX = Reader.ReadInt32();
				MaxY = Reader.ReadInt32();
				BinaryData = new DMapCellFlag[MaxX, MaxY];

				for (var x = 0; x < MaxX; x++)
				{
					for (var y = 0; y < MaxY; y++)
					{
						if (Reader.ReadBoolean())
							BinaryData[x, y] |= DMapCellFlag.Invalid;
					}
				}
			}
		}

	    public static void Load()
		{
			var Start = Native.timeGetTime();
			var maps = MySqlDatabase.LoadMaps();

			foreach (var map in maps)
			{
				if (File.Exists(Environment.CurrentDirectory + @"\Database\Maps\" + map.Item2 + ".JMap"))
				{
					var DMap = new ConquerDMap(map.Item1, map.Item2);
					Maps.Add(DMap.MapID, DMap);
				}
			}
			Console.WriteLine("{0} Maps Loaded In {1} Miliseconds!", Maps.Count, Native.timeGetTime() - Start);
		}

	    public void SetInvalid(ushort X, ushort Y, bool Value)
		{
			if (Value)
				BinaryData[X, Y] |= DMapCellFlag.Invalid;
			else
				BinaryData[X, Y] &= ~DMapCellFlag.Invalid;
		}

	    public bool InvalidNoHandicap(ushort X, ushort Y)
		{
			if ((MaxX > X) && (MaxY > Y))
				return (BinaryData[X, Y] & DMapCellFlag.Invalid) == DMapCellFlag.Invalid;
			return true;
		}

	    public bool Invalid(ushort X, ushort Y)
		{
			if ((MaxX <= X) || (MaxY <= Y))
				return true;

			if ((BinaryData[X, Y] & DMapCellFlag.Invalid) == DMapCellFlag.Invalid)
			{
				var tryx = X - 2;
				var tryy = Y - 2;

				if ((tryx <= 0) || (tryy <= 0))
					return true;

				for (; tryx < X + 2; tryx++)
				{
					for (; tryy < Y + 2; tryy++)
					{
						if (tryx > MaxX)
							return true;
						if (tryy > MaxY)
							return true;
						if ((BinaryData[tryx, tryy] & DMapCellFlag.Invalid) != DMapCellFlag.Invalid)
							return false;
					}
				}
				return true;
			}
			return false;
		}

	    public void SetMonsterOnTile(ushort X, ushort Y, bool Value)
		{
			if (Value)
				BinaryData[X, Y] |= DMapCellFlag.MonsterOnTile;
			else
				BinaryData[X, Y] &= ~DMapCellFlag.MonsterOnTile;
		}

	    public bool MonsterOnTile(ushort X, ushort Y)
		{
			if ((MaxX > X) && (MaxY > Y))
				return (BinaryData[X, Y] & DMapCellFlag.MonsterOnTile) == DMapCellFlag.MonsterOnTile;
			return false;
		}

	    public void SetItemOnTile(ushort X, ushort Y, bool Value)
		{
			if (Value)
				BinaryData[X, Y] |= DMapCellFlag.ItemOnTile;
			else
				BinaryData[X, Y] &= ~DMapCellFlag.ItemOnTile;
		}

	    public bool ItemOnTile(ushort X, ushort Y)
		{
			if ((MaxX > X) && (MaxY > Y))
				return (BinaryData[X, Y] & DMapCellFlag.ItemOnTile) == DMapCellFlag.ItemOnTile;
			return false;
		}

	    public bool FindValidDropLocation(ref ushort X, ref ushort Y, ushort Limit)
		{
			short offset_x = 1;
			short offset_y = 1;
			while (true)
			{
				var tryx = (ushort)(X - offset_x);
				var tryy = (ushort)(Y - offset_y);
				for (; tryx < X + offset_x; tryx++)
				{
					for (; tryy < Y + offset_y; tryy++)
					{
						if (!ItemOnTile(tryx, tryy))
						{
							if (!InvalidNoHandicap(tryx, tryy))
							{
								X = tryx;
								Y = tryy;
								return true;
							}
						}
					}
				}
				if ((X + offset_x < MaxX) && (Y + offset_y < MaxY))
				{
					offset_x += 1;
					offset_y += 1;
					if ((offset_x >= X) || (offset_y >= Y))
						return false;
					if ((offset_x >= Limit) || (offset_y >= Limit))
						return false;
				}
			}
		}
	}
}