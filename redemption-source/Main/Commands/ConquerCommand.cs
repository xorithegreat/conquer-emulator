﻿using System;
using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Events;
using ConquerEmulator.Item;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Main.Commands
{
	internal class ConquerCommand
	{
	    public static bool Parse(GameClient Client, string Command)
		{
			try
			{
				if (Command[0] != '@')
					return false;
				var Commands = Command.Split(' ');
				Commands[0] = Commands[0].ToLower();
				#region Player Commands
				if (Client.Flag >= PlayerFlag.Normal)
				{
					#region Dead-Check
					if (Client.Hero.Dead && (Client.Flag == PlayerFlag.Normal))
					{
						Client.Speak(Color.Yellow, ChatType.Talk, "Please revive first.");
						return false;
					}
					#endregion
					switch (Commands[0].ToLower())
                    {
                        #region @Accept
                        case "@accept":
                            {
                                if (Dueling.RequestUID == Client.Team.Leader.Hero.UID && Client.Team.TeamDictionary.Count == 2)
                                    Dueling.CreateDuel(Client, 1005, 50, 50);
                                break;
                            }
                        #endregion
                        #region @Save
                        case "@save":
							MySqlDatabase.SaveCharacter(Client);
							MySqlDatabase.SaveGuilds();
							break;
						#endregion
						#region @Reallot
						case "@reallot":
							if (Client.Hero.Reborn >= 1)
							{
								FlatDatabase.GetLevelStats(Client);

								Client.StatPoints = (ushort)(Client.Vitality + Client.Strength + Client.Spirit + Client.Agility);
								Client.StatPoints += Kernel.GetAttributePoints(Client.Hero.Level);
								Client.Vitality = Client.Strength = Client.Agility = Client.Spirit = 0;

								Client.CalculateStatBonus();

								var big = new BigUpdatePacket(7);
								big.UID = Client.Hero.UID;
								big.AllStats(Client, 0);
								big.HitpointsAndMana(Client, 5);
								Client.Send(big);
							}
							break;
                        #endregion
						#region @Scroll <Town Value>
						case "@scroll":
							if (((Client.Hero.StatusFlag & StatusFlag.BlackName) == StatusFlag.BlackName) && (Client.Hero.MapID.ID == 6000))
							{
								Client.Speak(Color.Turquoise, ChatType.TopLeft, "You can't scroll out of jail.");
								return false;
							}
							switch (Commands[1].ToUpper())
							{
								case "TC": Client.Teleport(1002, 430, 378); break;
								case "PC": Client.Teleport(1011, 193, 266); break;
								case "AM": Client.Teleport(1020, 566, 565); break;
								case "DC": Client.Teleport(1000, 496, 650); break;
								case "BI": Client.Teleport(1015, 717, 577); break;
								case "MA": Client.Teleport(1036, 211, 196); break;
								case "JAIL": Client.Teleport(6000, 29, 72); break;
								case "ARENA": Client.Teleport(1005, 50, 50); break;
                                case "TOURNAMENT":
                                    {
                                        if (TournamentBase.Started && TournamentBase.AcceptingEntries)
                                            TournamentBase.Join(Client);
                                        if (TeamDeathMatch.Started && TeamDeathMatch.AcceptingEntries)
                                            TeamDeathMatch.Join(Client);
                                        break;
                                    }
                            }
							break;
						#endregion
						#region @Sucicide/@Emo/@die
						case "@suicide":
						case "@emo":
						case "@die":
							Client.Hero.Die(null);
							break;
						#endregion
						#region @str, @vit, @spi, @agi <Value>
						case "@str":
						case "@vit":
						case "@spi":
						case "@agi":
							{
								var nValue = ushort.Parse(Commands[1]);
								if (Client.StatPoints >= nValue)
								{
									Client.StatPoints -= nValue;
									if (Commands[0] == "@str")
										Client.Strength += nValue;
									else if (Commands[0] == "@agi")
										Client.Agility += nValue;
									else if (Commands[0] == "@vit")
										Client.Vitality += nValue;
									else if (Commands[0] == "@spi")
										Client.Spirit += nValue;

									Client.CalculateStatBonus();

									var big = new BigUpdatePacket(7);
									big.UID = Client.Hero.UID;
									big.AllStats(Client, 0);
									big.HitpointsAndMana(Client, 5);
									Client.Send(big);
									return true;
								}
								Client.Speak(Color.Red, ChatType.Talk, "You don't have enough stats!");
								break;
							}
						#endregion
						#region @Dc/@break/@quit
						case "@dc":
						case "@quit":
						case "@break":
							{
								Client.Socket.Disconnect();
								break;
							}
						#endregion
					}
				}
				#endregion
				#region GM/GameMasterCommands
				if (Client.Flag >= PlayerFlag.GameManager)
				{
					switch (Commands[0].ToLower())
					{
						#region @XP
						case "@xp":
							Client.XpSkillCounter = 100;
							//Client.Hero.StatusFlag |= StatusFlag.XPList;
							//Client.Hero.StatusFlag &= ~StatusFlag.Superman;
							//Client.Hero.StatusFlag &= ~StatusFlag.Cyclone;
							//Client.Hero.Stamps.CanSeeXPSpells = DateTime.Now.AddSeconds(20);
							//Client.XPSkillCounter = 0;

							//Client.Send(new StatTypePacket(Client.Hero.UID, (uint)Client.Hero.StatusFlag, StatIDs.RaiseFlag).Serialize());
							break;
						#endregion
						#region @Heal or @Life
						case "@heal":
						case "@life":
							var update = new BigUpdatePacket(2);
							update.UID = Client.Hero.UID;
							update.HitpointsAndMana(Client, 0);
							Client.Send(update);
							break;
						#endregion
						#region @Stam <Value>
						case "@stam":
							Client.Stamina = byte.Parse(Commands[1]);
							Client.Send(new StatTypePacket(Client.Hero.UID, Client.Stamina, StatIDs.Stamina).Serialize());
							break;
						#endregion
						#region @Dtele <MapID> <X> <Y> <Dynamic MapID>
						case "@dtele":
							Client.Teleport(ushort.Parse(Commands[1]), ushort.Parse(Commands[2]), ushort.Parse(Commands[3]), ushort.Parse(Commands[4]));
							break;
						#endregion
						#region @Stm <Message> (SpeakToMap)
						case "@stm":
							{
								GameClient.SpeakToMap(Color.White, ChatType.Center, Commands[1], Client.Hero.MapID, Client.Hero.MapID.DynamicID);
								break;
							}
						#endregion
						#region @Kick <Name>
						case "@kick":
							var pClient = Kernel.FindClient(Commands[1]);
							if (pClient == null)
							{
								Client.Speak(Color.Turquoise, ChatType.Center, "Invalid Character.");
								break;
							}
							pClient.Socket.Disconnect();
							break;
						#endregion
						#region @Say <Message>
						case "@say":
							var ret = "";
							for (ushort l = 1; l < Commands.Length; l++)
								ret += Commands[l] + " ";
							GameClient.SpeakToAll(Color.Turquoise, ChatType.Center, ret);
							break;
						#endregion
						#region @Effect <EffectName>
						case "@effect":
							var SendEffect = new StringPacket((byte)Commands[1].Length);
							SendEffect.ID = StringIds.Effect;
							SendEffect.UID = Client.Hero.UID;
							Client.SendScreen(Packets.StringPacket(SendEffect, Commands[1]), true);
							break;
                        #endregion
                        #region @Goto <Name>
                        case "@find":
						case "@goto":
							{
                                foreach (var User in Kernel.Clients)
                                {
                                    if (User.Hero.Name == Commands[1])
                                    {
                                        Client.Teleport(User.Hero.MapID.ID, User.Hero.X, User.Hero.Y);
                                        break;
                                    }
                                }
								break;
							}
						#endregion
						#region @Bring <Name>
						case "@bring":
							{
                                foreach (var User in Kernel.Clients)
                                {
                                    if (User.Hero.Name == Commands[1])
                                    {
                                        User.Teleport(Client.Hero.MapID.ID, Client.Hero.X, Client.Hero.Y);
                                        break;
                                    }
                                }
								break;
							}
						#endregion
						#region @MapID (Displays current MapID)
						case "@mapid":
							Client.Speak(Color.White, ChatType.Center, "Current Map ID is: " + Client.Hero.MapID);
							break;
						#endregion
						#region @tele/mm <mapid> <x coordinate> <y coordinate>
						case "@tele":
                        case "@mm":
							Client.Teleport(ushort.Parse(Commands[1]), ushort.Parse(Commands[2]), ushort.Parse(Commands[3]));
							break;
						#endregion
						#region @clear (Clears Inventory Items)
						case "@clear":
							{
								foreach (var item in Client.Inventory)
									Client.RemoveInventory(item);
								Client.Speak(Color.White, ChatType.Talk, "Your items have been cleared.");
								break;
							}
						#endregion
						#region @Revive
						case "@revive":
							Client.Revive(Client, true);
							break;
                        #endregion
                        #region @Rb <ClassID>
                        case "@rb":
                        case "@reborn":
                            if (Client.Hero.Reborn < 1)
                            {
                                byte Job = 0;
                                byte.TryParse(Commands[1], out Job);
                                if (Job != 0)
                                    StatusChange.Reborn(Client, Job);
                            }
                            else
                                Client.Speak(Color.Turquoise, ChatType.TopLeft, "You're already reborn!");
                            break;
                        #endregion
                        #region @Level <Value>
                        case "@level":
                            if (byte.Parse(Commands[1]) <= 140)
                                StatusChange.Level(Client, byte.Parse(Commands[1]), true);
                            break;
                        #endregion
                        #region @Job <JobID>
                        case "@job":
                            var jobID = byte.Parse(Commands[1]);
                            if (FlatDatabase.IsValidJob(Kernel.GetBaseClass(jobID)))
                                StatusChange.ChangeJob(Client, jobID, true);
                            else
                                Client.Speak(Color.White, ChatType.Talk, "Please select a valid Job ID.");
                            break;
                        #endregion
                        #region @Prof <Prof Id> <Prof Level>
                        case "@prof":
                            {
                                if (FlatDatabase.IsCorrectProf(ushort.Parse(Commands[1])))
                                {
                                    var Prof = new ConquerSpell();
                                    Prof.ID = ushort.Parse(Commands[1]);
                                    Prof.Experience = 0;
                                    Prof.Level = byte.Parse(Commands[2]);

                                    var LProf = LearnProfPacket.Create();
                                    LProf.ID = Prof.ID;
                                    LProf.Level = Prof.Level;
                                    LProf.Experience = (uint)Prof.Experience;

                                    if (Client.Proficiencies.ContainsKey(Prof.ID))
                                        Client.Proficiencies.Remove(Prof.ID);
                                    Client.Proficiencies.TryAdd(Prof.ID, Prof);
                                    Client.Send(LProf, LProf.Size);
                                }
                                else
                                    Client.Speak(Color.White, ChatType.Talk, "Please select a valid Proficiency ID.");
                            }
                            break;
                        #endregion
                        #region @Spell,@Skill <Skill Id> <Skill Level>
                        case "@spell":
                        case "@skill":
                            {
                                if (FlatDatabase.IsCorrectSpell(ushort.Parse(Commands[1]), byte.Parse(Commands[2])))
                                {
                                    var Skill = new ConquerSpell();
                                    Skill.ID = ushort.Parse(Commands[1]);
                                    Skill.Experience = 0;
                                    Skill.Level = byte.Parse(Commands[2]);

                                    var LSpell = LearnSpellPacket.Create();
                                    LSpell.ID = Skill.ID;
                                    LSpell.Experience = Skill.Experience;
                                    LSpell.Level = Skill.Level;

                                    if (Client.Spells.ContainsKey(Skill.ID))
                                        Client.Spells.Remove(Skill.ID);
                                    Client.Spells.TryAdd(Skill.ID, Skill);
                                    Client.Send(LSpell, LSpell.Size);
                                }
                                else
                                    Client.Speak(Color.White, ChatType.Talk, "Please select a valid Spell ID");
                            }
                            break;
                        #endregion
                        #region @Ground <EffectID>
                        case "@ground":
                            {
                                var GDrop = new ItemGroundPacket((uint)RandomGenerator.Generator.Next(), uint.Parse(Commands[1]), GroundIDs.GroundEffect, Client.Hero.X, Client.Hero.Y);
                                Client.SendScreen(GDrop.Serialize(), true);
                                break;
                            }
                        #endregion
                        #region @model
                        case "@model":
                            switch (Commands[1].ToLower())
                            {
                                case "sfemale":
                                    Client.Hero.Sex = 2001;
                                    break;
                                case "lfemale":
                                    Client.Hero.Sex = 2002;
                                    break;
                                case "smale":
                                    Client.Hero.Sex = 1003;
                                    break;
                                case "lmale":
                                    Client.Hero.Sex = 1004;
                                    break;
                            }
                            Client.SendScreen(new StatTypePacket(Client.Hero.UID, Client.Hero.Sex, StatIDs.Model).Serialize(), true);
                            break;
                        #endregion
                        #region @hairstyle
                        case "@hairstyle":
                            Client.Hero.Hairstyle = ushort.Parse(Commands[1]);
                            Client.SendScreen(new StatTypePacket(Client.Hero.UID, Client.Hero.Hairstyle, StatIDs.Hairstyle).Serialize(), true);
                            break;
                        #endregion+
                    }
                }
				#endregion
				#region PM/Programmers/Project Manager Commands
				if (Client.Flag >= PlayerFlag.ProjectManager)
				{
					switch (Commands[0].ToLower())
					{
						#region @Ban <Name>
						case "@ban":
							{
								var pClient = Kernel.FindClient(Commands[1]);
								if (pClient == null)
								{
									Client.Speak(Color.Turquoise, ChatType.Center, "Invalid Character.");
									break;
								}

								MySqlDatabase.BanCharacter(pClient);
								pClient.Socket.Disconnect();

								Client.Speak(Color.Red, ChatType.Center, string.Format("{0} has been banned.", pClient.Hero.Name));
								break;
							}
						#endregion
                    }
				}
                #endregion
                #region Admin Commands
                if (Client.Flag == PlayerFlag.ServerAdmin)
                {
                    switch (Commands[0].ToLower())
                    {
                        #region @Spawn <typeID>
                        case "@spawn":
                            {
                                Kernel.SpawnMonster(uint.Parse(Commands[1]), Client.Hero.X, Client.Hero.Y, Client.Hero.MapID);
                                break;
                            }
                        #endregion
                        #region @Cps <Value>
                        case "@cps":
                            Client.ConquerPoints = uint.Parse(Commands[1]);
                            Client.Send(new StatTypePacket(Client.Hero.UID, Client.ConquerPoints, StatIDs.ConquerPoints).Serialize());
                            break;
                        #endregion
                        #region @Weather <Weather type> <Density>
                        case "@weather":
                            var Weather = new WeatherPacket((MapEffectValue)byte.Parse(Commands[1]), MapEffectID.EnableWeather, byte.Parse(Commands[2]));
                            PacketKernel.SendGlobalPacket(PacketKernel.Serialize(Weather, Weather.Size));
                            break;
                        #endregion
                        #region @Ipban <Name>
                        case "@ipban":
                            {
                                lock (Kernel.BannedIPs)
                                {
                                    var iClient = Kernel.FindClient(Commands[1]);
                                    if (iClient != null)
                                    {
                                        Kernel.BannedIPs.Add(iClient.Socket.RemoteAddress);
                                        FlatDatabase.SaveToFile(iClient.Socket.RemoteAddress);
                                        iClient.Socket.Disconnect();
                                    }
                                }
                                break;
                            }
                        #endregion
                        #region @FFA <Creates a FFA Tournament>
                        case "@ffa":
                            {
                                if (!TournamentBase.Started)
                                    TournamentBase.CreateTournament(ushort.Parse(Commands[1]), ushort.Parse(Commands[2]), ushort.Parse(Commands[3]), "FFA");
                                break;
                            }
                        #endregion
                        #region @PTB <Creates a Pass The Bomb Tournament>
                        case "@ptb":
                        case "@passthebomb":
                            {
                                if (!TournamentBase.Started)
                                    TournamentBase.CreateTournament(ushort.Parse(Commands[1]), ushort.Parse(Commands[2]), ushort.Parse(Commands[3]), "PassTheBomb");
                                break;
                            }
                        #endregion
                        #region @TDM <Creates a TeamDeathMatch Tournament>
                        case "@tdm":
                        case "@teamdeathmatch":
                            {
                                if (!TeamDeathMatch.Started)
                                    TeamDeathMatch.CreateTDM(1767);
                                break;
                            }
                        #endregion
                        #region @MKT <Creates a Most Kills Tournament>
                        case "@mkt":
                        case "@mostkills":
                            {
                                if (!TournamentBase.Started)
                                    TournamentBase.CreateTournament(ushort.Parse(Commands[1]), ushort.Parse(Commands[2]), ushort.Parse(Commands[3]), "MostKills");
                                break;
                            }
                        #endregion
                        #region @Item <ID>
                        case "@itemid":
                            {
                                var ID = uint.Parse(Commands[1]);
                                var itemFound = MySqlDatabase.FindItem(ID);
                                if (itemFound)
                                {
                                    var item_stats = new StanderdItemStats(ID);
                                    IConquerItem item = new ConquerItem(true);
                                    item.UID = ConquerItem.NextItemUID;
                                    item.ID = ID;
                                    item.Durability = item_stats.Durability;
                                    item.MaxDurability = item_stats.Durability;
                                    item.SocketTwo = 0;
                                    Client.AddInventory(item);
                                }
                                else
                                    Client.Speak(Color.Red, ChatType.Talk, "Invalid item, The item doesn't exist in the database.");
                                break;
                            }
                        #endregion
                        #region @Item <name> <quality> <plus> <bless> <enchant> <soc1> <soc2>
                        case "@item":
                            {
                                byte Quality = 1;
                                switch (Commands[2].ToLower())
                                {
                                    case "normalv1": Quality = 1; break;
                                    case "normalv2": Quality = 2; break;
                                    case "normalv3":
                                    case "normal": Quality = 3; break;
                                    case "fixed": Quality = 4; break;
                                    case "refined": Quality = 6; break;
                                    case "unique": Quality = 7; break;
                                    case "elite": Quality = 8; break;
                                    case "super": Quality = 9; break;
                                }
                                uint ID;
                                if (Quality <= 2)
                                    ID = MySqlDatabase.GetItemId(Commands[1], false, 0);
                                else
                                    ID = MySqlDatabase.GetItemId(Commands[1], true, Quality);
                                if (ID != 0)
                                {
                                    var item_stats = new StanderdItemStats(ID);
                                    IConquerItem item = new ConquerItem(true);
                                    item.UID = ConquerItem.NextItemUID;
                                    item.ID = ID;
                                    item.Durability = item_stats.Durability;
                                    item.MaxDurability = item_stats.Durability;

                                    if (Commands.Length >= 4)
                                        item.Plus = byte.Parse(Commands[3]);
                                    else
                                        item.Plus = 0;
                                    if (Commands.Length >= 5)
                                        item.Bless = byte.Parse(Commands[4]);
                                    else
                                        item.Bless = 0;
                                    if (Commands.Length >= 6)
                                        item.Enchant = byte.Parse(Commands[5]);
                                    else
                                        item.Enchant = 0;
                                    if (Commands.Length >= 7)
                                        item.SocketOne = byte.Parse(Commands[6]);
                                    else
                                        item.SocketOne = 0;
                                    if (Commands.Length >= 8)
                                        item.SocketTwo = byte.Parse(Commands[7]);
                                    else
                                        item.SocketTwo = 0;
                                    Client.AddInventory(item);
                                }
                                else
                                    Client.Speak(Color.Red, ChatType.Talk, "Invalid item, The item doesn't exist in the database.");
                                break;
                            }
                        #endregion
                        #region @silvers/@Money <value>
                        case "@silvers":
                        case "@money":
                            Client.Silvers = uint.Parse(Commands[1]);
                            Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());
                            break;
                        #endregion
                        #region @Reload
                        case "@reload":
                            {
                                FlatDatabase.LoadRates();
                                Client.Speak(Color.Turquoise, ChatType.Talk, "Rates succesfully reloaded.");
                                break;
                            }
                         #endregion
                    }
                }
				#endregion
				return true;
			}
			catch (Exception E)
			{
				LogHandler.WriteLine(E);
				return false;
			}
		}
	}
}