﻿using System;
using System.Drawing;
using ConquerEmulator.Client;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Main.Commands
{
	internal class ConsoleCommand
	{
	    public static void Parse(string Command)
		{
			var Commands = Command.Split(' ');
			Commands[0] = Commands[0].ToLower();

			switch (Commands[0].ToLower())
			{
				#region Close Console
				case "":
					{
						Kernel.SaveAll(true);
						Environment.Exit(0);
						break;
					}
				#endregion
				#region /Restart
				case "/restart":
					Kernel.Restart();
					break;
				#endregion
				#region /Clear Clears the console
				case "/clear":
					Console.Clear();
					break;
				#endregion
				#region /Save
				case "/save":
					Kernel.SaveAll(false);
					break;
				#endregion
				#region /Say
				case "/say":
					var ret = "";
					for (ushort l = 1; l < Commands.Length; l++)
						ret += Commands[l] + " ";
					GameClient.SpeakToAll(Color.Turquoise, ChatType.Center, ret);
					break;
				#endregion
				#region /Kick <Name>
				case "/kick":
					var Client = Kernel.FindClient(Commands[1]);
					if (Client == null)
					{
						Console.WriteLine("Invalid Character.");
						break;
					}
					Client.Socket.Disconnect();
					break;
				#endregion
				#region /GC
				case "/gc":
					GC.Collect();
					Console.WriteLine("Garbage has been collected.");
					break;
				#endregion
				#region Default
				default:
					Console.WriteLine("Unrecoginized console command.");
					break;
					#endregion
			}
		}
	}
}