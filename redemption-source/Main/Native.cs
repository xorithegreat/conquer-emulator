﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using ConquerEmulator.MultiThreading;

namespace ConquerEmulator
{
	internal unsafe class Native
	{
	    #region Winmn
	    [DllImport("winmm.dll", EntryPoint = "timeGetTime", CallingConvention = CallingConvention.StdCall)]
		public static extern uint timeGetTime();
	    #endregion
	    #region Msvcrt
	    [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern void* memcpy(void* dest, void* src, int size);

	    public static void* memset(sbyte* dst, sbyte fill, int length)
		{
			for (var i = 0; i < length; i++)
				*(dst + i) = fill;
			return dst;
		}

	    #endregion
	    #region Kernel32
	    [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		public static extern int GetPrivateProfileSectionW(string Section, char* ReturnBuffer, int Size, string FileName);

	    [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		public static extern uint GetPrivateProfileIntW(string Section, string Key, int Default, string FileName);

	    [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		public static extern uint GetPrivateProfileStringW(string Section, string Key, string Default, char* ReturnedString, int Size, string FileName);

	    [DllImport("kernel32.dll", CharSet = CharSet.Ansi)]
		public static extern uint GetPrivateProfileStringA(string Section, string Key, void* Default, sbyte* ReturnedString, int Size, string FileName);

	    [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool WritePrivateProfileStringW(string Section, string Key, string Value, string FileName);
	    #endregion
	}

	public static unsafe class NativeExtended
	{
	    public static bool CheckBitFlag(this uint value, uint flag)
		{
			return (value & flag) == flag;
		}

	    public static void CopyTo(this string str, void* pDest)
		{
			var dest = (byte*)pDest;
			for (var i = 0; i < str.Length; i++)
			{
				dest[i] = (byte)str[i];
			}
		}

	    public static bool Remove<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dictionary, TKey key)
		{
			TValue ignored;
			return dictionary.TryRemove(key, out ignored);
		}

	    public static T GetRandomElement<T>(this IEnumerable<T> enumerable)
		{
			return enumerable.ElementAt(RandomGenerator.Generator.Next(enumerable.Count()));
		}

	    public static int GetHashCode32(string s)
		{
			fixed (char* str = s.ToCharArray())
			{
				var chPtr = str;
				var num = 0x15051505;
				var num2 = num;
				var numPtr = (int*)chPtr;
				for (var i = s.Length; i > 0; i -= 4)
				{
					num = ((num << 5) + num + (num >> 0x1b)) ^ numPtr[0];
					if (i <= 2)
						break;
					num2 = ((num2 << 5) + num2 + (num2 >> 0x1b)) ^ numPtr[1];
					numPtr += 2;
				}
				return num + num2 * 0x5d588b65;
			}
		}
	}

	public class TIME
	{
	    private uint time;

	    public TIME(bool local = true)
		{
			time = local ? Native.timeGetTime() : 0;
		}

	    public int ElapsedSinceTick => (int)(Native.timeGetTime() - time);

	    public int RemainingTime => (int)(time - Native.timeGetTime());

	    public bool IsReady(uint now)
		{
			return time < now;
		}

	    public static implicit operator bool(TIME t)
		{
			return t.time < Native.timeGetTime();
		}

	    public void Set(int amount)
		{
			time = Native.timeGetTime() + (uint)amount;
		}

	    public void Set(uint time, int amount)
		{
			this.time = time + (uint)amount;
		}

	    public bool ValidateFrame(uint now, int leeway)
		{
			if (time == 0)
			{
				time = now;
				return true;
			}
			var num = (int)(now - time);
			var flag = num > leeway;
			if (flag)
				time = now;
			return flag;
		}

	    public override int GetHashCode()
		{
			return (int)Native.timeGetTime();
		}
	}
}