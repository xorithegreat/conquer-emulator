﻿namespace ConquerEmulator.Main
{
	public struct Map
	{
		public uint ID, DynamicID;
		public uint MapDoc;

		public Map(uint ID)
		{
			this.ID = ID;
			DynamicID = 0;
			MapDoc = 0;
		}

		public Map(uint ID, uint MapDoc)
		{
			this.ID = ID;
			this.MapDoc = MapDoc;
			DynamicID = 0;
		}

		public static implicit operator uint(Map Map)
		{
			return Map.ID;
		}

		public void MakeDynamic()
		{
			DynamicID++;
		}

		public static bool operator ==(Map Map, Map Map2)
		{
			return (Map.ID == Map2.ID) && (Map.DynamicID == Map2.DynamicID);
		}

		public static bool operator !=(Map Map, Map Map2)
		{
			return !(Map == Map2);
		}

		public static bool operator ==(Map Map, ushort Map2)
		{
			return Map.ID == Map2;
		}

		public static bool operator !=(Map Map, ushort Map2)
		{
			return Map.ID != Map2;
		}

		public override bool Equals(object obj)
		{
			if (obj is Map)
				return this == (Map)obj;
			return ID == obj as uint?;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public override string ToString()
		{
			return ID.ToString();
		}
	}
}