﻿using ConquerEmulator.Client;

namespace ConquerEmulator.Main.Interfaces
{
	internal interface INpc
	{
	    uint UID { get; set; }
	    ushort X { get; set; }
	    ushort Y { get; set; }
	    uint Type { get; set; }
	    ushort Interaction { get; set; }
	    bool IsVendor { get; set; }
	    uint StatusFlag { get; set; }
	    Map MapID { get; set; }
	    ushort Avatar { get; set; }

	    void SendSpawn(GameClient Client);

	    void ConvertToVendor(string Name);

	    void ConvertToStandard();
	}
}