﻿using ConquerEmulator.Client;
using ConquerEmulator.Item;

namespace ConquerEmulator.Main.Interfaces
{
	public interface IConquerItem
	{
	    uint UID { get; set; }
	    uint ID { get; set; }
	    byte Plus { get; set; }
	    byte Bless { get; set; }
	    byte Enchant { get; set; }
	    byte Color { get; set; }
	    byte SocketOne { get; set; }
	    byte SocketTwo { get; set; }
	    ushort Durability { get; set; }
	    ushort MaxDurability { get; set; }
	    ItemPosition Position { get; set; }
	    ItemStatus Status { get; set; }
	    bool IsEquipment { get; }
	    byte Locked { get; set; }
	    bool Unlocking { get; set; }
	    ushort Arrows { get; set; }

	    void Send(GameClient Client);

	    void SendInventoryUpdate(GameClient Client);
	}
}