﻿using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Main.Interfaces
{
	public enum EntityFlag : sbyte
	{
		Player = 1,
		Monster = 2,
		Reviver = 3,
		Item = 4,
		Pet = 5,
		Pole = 6,
		Gate = 7,
		TrainingGround = 8
	}

	public interface IBaseEntity
	{
	    bool Dead { get; }
	    ushort Defence { get; }
	    sbyte Dodge { get; }
	    EntityFlag EntityFlag { get; }
	    uint MagicAttack { get; }
	    Map MapID { get; }
	    int Hitpoints { get; set; }
	    int MaxHitpoints { get; }
	    ushort MDefence { get; }
	    uint MinAttack { get; }
	    uint MaxAttack { get; }
	    object Owner { get; }
	    ushort PlusMDefence { get; }
	    uint UID { get; set; }
	    ushort X { get; }
	    ushort Y { get; }
	    ushort Level { get; }
	    StatusFlag StatusFlag { get; set; }
	}
}