﻿namespace ConquerEmulator.Main.Interfaces
{
	public interface IClassPacket
	{
	    void Deserialize(byte[] Bytes);

	    byte[] Serialize();
	}
}