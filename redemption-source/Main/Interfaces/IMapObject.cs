﻿using ConquerEmulator.Client;

namespace ConquerEmulator.Main.Interfaces
{
	public enum MapObjectType
	{
		Player = 1,
		Monster = 2,
		Pet = 3,
		Item = 4,
		Npc = 5,
		SOB = 6,
		Reviver = 7
	}

	public interface IMapObject
	{
	    ushort X { get; }
	    ushort Y { get; }
	    Map MapID { get; }
	    uint UID { get; }
	    object Owner { get; }
	    MapObjectType MapObjType { get; }

	    void SendSpawn(GameClient Client);

	    void SendSpawn(GameClient Client, bool IgnoreScreen);
	}
}