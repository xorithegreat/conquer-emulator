﻿using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Main.Interfaces
{
	public interface IAssociate
	{
	    string Name { get; set; }
	    uint UID { get; set; }
	    bool Online { get; set; }
	    AssociationID ID { get; set; }

	    byte[] GetBytes();
	}
}