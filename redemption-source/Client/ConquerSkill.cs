﻿using ConquerEmulator.Database;
using ConquerEmulator.Main;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Client
{
    public class ConquerSpell
    {
        private ushort m_MaxLevel;
        private byte m_Level;
        private ushort m_NeededLevel;
        private int m_NeededExperience;

        public ushort ID;
        public int Experience;
        public byte Level { get { return m_Level; } set { m_Level = value; UpdateSkillInformation(); } }
        public bool MaxLevel { get { return (m_MaxLevel <= Level); } }

        public ConquerSpell()
        {
        }

        public ConquerSpell(ushort ID, byte Level)
        {
            this.ID = ID;
            this.Level = Level;
        }

        private void UpdateSkillInformation()
        {
            IniFile rdr = new IniFile(FlatDatabase.Location + "\\Spells\\" + ID.ToString() + "[" + Level.ToString() + "].ini");
            m_NeededExperience = rdr.ReadInt32("SpellInformation", "Experience", 0);
            m_NeededLevel = rdr.ReadUInt16("SpellInformation", "ReqLevel", 0);
            if (m_NeededExperience != 0)
                m_MaxLevel = (ushort)(Level + 1);
        }

        public static void CalculateProfExp(GameClient Client, ushort profID, uint Amount)
        {
            if (Amount == 0)
                return;

            ConquerSpell prof;
            if (!Client.Proficiencies.TryGetValue(profID, out prof))
                return;
            if (prof.MaxLevel)
                return;

            prof.Experience += (int)Amount;

            var NeededExp = FlatDatabase.LoadProfExp((byte)(prof.Level + 1));
            while (prof.Experience >= NeededExp)
            {
                prof.Experience -= (int)NeededExp;
                prof.Level++;
                NeededExp = FlatDatabase.LoadProfExp((byte)(prof.Level + 1));
                if (prof.MaxLevel)
                {
                    prof.Experience = 0;
                    break;
                }
            }
            var LSpell = LearnProfPacket.Create();
            LSpell.ID = prof.ID;
            LSpell.Experience = (uint)prof.Experience;
            LSpell.Level = prof.Level;
            Client.Send(LSpell, LSpell.Size);
        }

        public static void CalculateSpellExp(GameClient Client, uint SkillID, uint Amount)
        {
            if (Amount == 0)
                return;

            ConquerSpell skill;
            if (Client.Spells.TryGetValue((ushort)SkillID, out skill))
            {
                if (skill.MaxLevel)
                    return;

                skill.Experience += (int)Amount;
                var Needed = FlatDatabase.LoadSpellExp(skill.ID, skill.Level);
                while (skill.Experience >= Needed)
                {
                    skill.Experience -= (int)Needed;
                    skill.Level++;
                    Needed = FlatDatabase.LoadSpellExp(skill.ID, skill.Level);
                    if (skill.MaxLevel)
                    {
                        skill.Experience = 0;
                        break;
                    }
                }
                var LSpell = LearnSpellPacket.Create();
                LSpell.ID = skill.ID;
                LSpell.Experience = skill.Experience;
                LSpell.Level = skill.Level;
                Client.Send(LSpell, LSpell.Size);
            }
        }

        public static void LearnProf(GameClient Client, ushort Id)
        {
            if (!Client.Proficiencies.ContainsKey(Id))
                return;

            var Prof = Client.Proficiencies[Id];
            if (Prof.Level < 20)
            {
                Prof.ID = Id;
                Prof.Experience = 0;
                Prof.Level++;

                var LProf = LearnProfPacket.Create();
                LProf.ID = Prof.ID;
                LProf.Level = Prof.Level;
                LProf.Experience = (uint)Prof.Experience;

                if (Client.Proficiencies.ContainsKey(Prof.ID))
                    Client.Proficiencies.Remove(Prof.ID);
                Client.Proficiencies.TryAdd(Prof.ID, Prof);
                Client.Send(LProf, LProf.Size);
            }
        }

        public static void LearnSpell(GameClient Client, ushort Id, byte level)
        {
            if (Client.Spells.ContainsKey(Id))
                return;

            var Skill = new ConquerSpell();
            Skill.ID = Id;
            Skill.Experience = 0;
            Skill.Level = level;

            var LSpell = LearnSpellPacket.Create();
            LSpell.ID = Skill.ID;
            LSpell.Experience = Skill.Experience;
            LSpell.Level = Skill.Level;

            Client.Spells.TryAdd(Skill.ID, Skill);
            Client.Send(LSpell, LSpell.Size);
        }

        public static void UnlearnSpell(GameClient Client, ushort Id)
        {
            if (!Client.Spells.ContainsKey(Id))
                return;

            Client.Spells.Remove(Id);
            Client.Send(DataPacket.DataUpdate(Client.Hero.UID, Id, 0, 0, 0, DataIDs.UnlearnSpell));
        }

        public static ushort[] GetRebornSpells(byte Job, byte RebornInto)
        {
            ushort[] Skills = null;
            Job /= 10;
            RebornInto /= 10;
            switch (Job)
            {
                case 1: // trojan
                    switch (RebornInto)
                    {
                        case 5: Skills = new ushort[] { 1110, 1190 }; break;
                        case 14: Skills = new ushort[] { 1110, 1190, 1270 }; break;
                        case 1: Skills = new ushort[] { 3050 }; break;
                        case 2: Skills = new ushort[] { 1110, 1190, 5100 }; break;
                    }
                    break;
                case 2: // warrior
                    switch (RebornInto)
                    {
                        case 4:
                        case 14: Skills = new ushort[] { 1020, 1040 }; break;
                        case 5:
                        case 1: Skills = new ushort[] { 1040, 1015, 1320 }; break;
                        case 2: Skills = new ushort[] { 3060 }; break;
                        case 13: Skills = new ushort[] { 1020, 1040, 1025 }; break;
                    }
                    break;
                case 4: // archer
                    switch (RebornInto)
                    {
                        case 4: Skills = new ushort[] { 5000 }; break;
                    }
                    break;
                case 5: // ninja
                    switch (RebornInto)
                    {
                        case 5: Skills = new ushort[] { 6000, 6001, 6002, 6003, 6010, 6011 }; break;
                        default: Skills = new ushort[] { 6001 }; break;
                    }
                    break;
                case 13: // water tao
                    switch (RebornInto)
                    {
                        case 13: Skills = new ushort[] { 3090 }; break;
                        case 2:
                        case 5:
                        case 1: Skills = new ushort[] { 1005, 1090, 1095, 1195, 1085 }; break;
                        case 14: Skills = new ushort[] { 1050, 1175, 1075, 1055 }; break;
                    }
                    break;
                case 14: // fire tao
                    switch (RebornInto)
                    {
                        case 14: Skills = new ushort[] { 3080 }; break;
                        case 13: Skills = new ushort[] { 1120 }; break;
                        case 1:
                        case 2:
                        case 5:
                        case 4: Skills = new ushort[] { 1000, 1001, 1005, 1195 }; break;
                    }
                    break;
            }
            return Skills;
        }
    }
}