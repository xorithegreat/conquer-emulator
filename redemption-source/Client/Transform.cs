﻿using System;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Client
{
	public class Transform
	{
	    private readonly GameClient Client;
	    public ushort Defence;
	    public sbyte Dodge;
	    public ushort ID;
	    public ushort MDefence;
	    public ushort SpellID;

	    public Transform(GameClient owner)
		{
			Client = owner;
		}

	    public void Start(ushort TransformID, ushort SpellID)
		{
			Defence = Client.Hero.Defence;
			MDefence = Client.Hero.MDefence;
			Dodge = Client.Hero.Dodge;
			ID = TransformID;
			this.SpellID = SpellID;
		}

	    public void Stop()
		{
			var HPModifier = (double)Client.Hero.Hitpoints / Client.Hero.MaxHitpoints;
			Client.Hero.Defence = Defence;
			Client.Hero.MDefence = MDefence;
			Client.Hero.Dodge = Dodge;
			Client.CalculateStatBonus();
			Client.Hero.Hitpoints = Math.Max(1, (int)(Client.Hero.MaxHitpoints * HPModifier));
			Client.Hero.OverlappingMesh = 0;
			ID = 0;
		}

	    public void SendUpdates()
		{
			var big = new BigUpdatePacket(3);
			big.UID = Client.Hero.UID;
			big.Append(0, StatIDs.Model, Client.Hero.Model);
			big.Append(1, StatIDs.MaxHitPoints, Client.Hero.MaxHitpoints);
			big.Append(2, StatIDs.Hitpoints, Client.Hero.Hitpoints);
			Client.SendScreen(big, true);
		}
	}
}