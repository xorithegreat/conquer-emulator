﻿using System;
using System.Collections.Generic;
using ConquerEmulator.Database;
using ConquerEmulator.Main;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Client
{
    public enum NobilityID : sbyte
    {

        Serf = 0x00,
        Knight = 0x01,
        Baron = 0x03,
        Earl = 0x05,
        Duke = 0x07,
        Prince = 0x09,
        King = 0x0C
    }

    public class NobilityInfo
    {
        public uint Donation;
        public ushort Model;
        public string playerName;
        public uint UID;
    }

    internal class ConquerNobility
    {
        public static int PageCount, ListCount;
        public static NetStringPacker Page1 = new NetStringPacker();
        public static NetStringPacker Page2 = new NetStringPacker();
        public static NetStringPacker Page3 = new NetStringPacker();
        public static NetStringPacker Page4 = new NetStringPacker();
        public static NetStringPacker Page5 = new NetStringPacker();
        public static IList<NobilityInfo> AllRanks;
        public static List<uint> RankingList = new List<uint>();

        public static NetStringPacker GetPage(int PageNo)
		{
			switch (PageNo)
			{
				case 0: return Page1;
				case 1: return Page2;
				case 2: return Page3;
				case 3: return Page4;
				case 4: return Page5;
				default: return Page5;
			}
		}

        public static void UpdatePlayers()
        {
            foreach (var Client in Kernel.Clients)
            {
                if (Client.LoggedIn)//Check if level 70?
                    Client.Send(NobilityPacket.UpdateIcon(Client));
            }
        }

        public static void UpdateNobility()
        {
	        AllRanks = MySqlDatabase.GetNobilityPages();

			PageCount = (int)Math.Ceiling((double)AllRanks.Count / 10);// +1;

			if (AllRanks.Count == 0 || AllRanks == null)
				return;

			RankingList.Clear();

			if (PageCount >= 1)
			{
				Page1 = new NetStringPacker();
				if (AllRanks.Count >= 10)
					ListCount = 10;
				else
					ListCount = AllRanks.Count;

				//UID 0 0 Name Donation Medal Rank
				for (var i = 0; i < ListCount; i++)
				{
					RankingList.Add(AllRanks[i].UID);

					if (i >= 0 || i <= 2)
						Page1.AddString(AllRanks[i].UID + " 1 " + AllRanks[i].Model + AllRanks[i].playerName + " " + AllRanks[i].Donation + " " + NobilityMedal(i, AllRanks[i].Donation) + " " + i);
					else
						Page1.AddString(AllRanks[i].UID + " 0 0 " + AllRanks[i].playerName + " " + AllRanks[i].Donation + " " + NobilityMedal(i, AllRanks[i].Donation) + " " + i);
				}
			}

			if (PageCount >= 2)
			{
				Page2 = new NetStringPacker();
				if (AllRanks.Count >= 20)
					ListCount = 10;
				else
					ListCount = AllRanks.Count - 10;

				for (var i = 0; i < ListCount; i++)
				{
					Page2.AddString(AllRanks[i + 10].UID + " 0 0 " + AllRanks[i + 10].playerName + " " + AllRanks[i + 10].Donation + " " + NobilityMedal(i + 10, AllRanks[i + 10].Donation) + " " + i + 10);
					RankingList.Add(AllRanks[i].UID);
				}
			}

			if (PageCount >= 3)
			{
				Page3 = new NetStringPacker();

				if (AllRanks.Count >= 30)
					ListCount = 10;
				else
					ListCount = AllRanks.Count - 20;

				for (var i = 0; i < ListCount; i++)
				{
					Page3.AddString(AllRanks[i + 20].UID + " 0 0 " + AllRanks[i + 20].playerName + " " + AllRanks[i + 20].Donation + " " + NobilityMedal(i + 20, AllRanks[i + 20].Donation) + " " + i + 20);
					RankingList.Add(AllRanks[i].UID);
				}
			}

			if (PageCount >= 4)
			{
				Page4 = new NetStringPacker();

				if (AllRanks.Count >= 40)
					ListCount = 10;
				else
					ListCount = AllRanks.Count - 30;

				for (var i = 0; i < ListCount; i++)
				{
					Page4.AddString(AllRanks[i + 30].UID + " 0 0 " + AllRanks[i + 30].playerName + " " + AllRanks[i + 30].Donation + " " + NobilityMedal(i + 30, AllRanks[i + 30].Donation) + " " + i + 30);
					RankingList.Add(AllRanks[i].UID);
				}
			}

			if (PageCount >= 5)
			{
				Page5 = new NetStringPacker();
				if (AllRanks.Count >= 50)
					ListCount = 10;
				else
					ListCount = AllRanks.Count - 40;

				for (var i = 0; i < ListCount; i++)
				{
					Page5.AddString(AllRanks[i + 40].UID + " 0 0 " + AllRanks[i + 40].playerName + " " + AllRanks[i + 40].Donation + " " + NobilityMedal(i + 40, AllRanks[i + 40].Donation) + " " + i + 40);
					RankingList.Add(AllRanks[i].UID);
				}
			}
		}

        public static sbyte NobilityMedal(int rank, uint donation)
		{
			if (rank >= 0 && rank <= 2)
				return (sbyte)NobilityID.King;
			if (rank >= 3 && rank <= 14)
				return (sbyte)NobilityID.Prince;
			if (rank >= 15 && rank <= 49)
				return (sbyte)NobilityID.Duke;
			if (donation >= 200000000)
				return (sbyte)NobilityID.Earl;
			if (donation >= 100000000)
				return (sbyte)NobilityID.Baron;
			if (donation >= 30000000)
				return (sbyte)NobilityID.Knight;
			return (sbyte)NobilityID.Serf;
		}
    }
}
