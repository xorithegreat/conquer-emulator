﻿using System.Collections.Generic;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Client
{
    public class Trade
    {
        public bool Aggree = false;
        public GameClient Client;
        public uint CPs = 0;

        public Dictionary<uint, IConquerItem> Items = new Dictionary<uint, IConquerItem>();
        public uint Money = 0;

        public uint Partner = 0;
        public bool Trading = false;

        public Trade(object Sender)
        {
            Client = Sender as GameClient;
        }

        public void TradeItems(GameClient Recipiant)
        {
            if (Recipiant == null)
                return;

            foreach (var Item in Items.Values)
            {
                if (Recipiant.AddInventory(Item))
                {
                    Client.RemoveInventory(Item);
                }
            }
        }

        public void ExchangeMoney(GameClient Recipiant)
        {
            if (Recipiant == null)
                return;

            Recipiant.Silvers += Money;
            Recipiant.Send(new StatTypePacket(Recipiant.Hero.UID, Recipiant.Silvers, StatIDs.Money).Serialize());
            Client.Silvers -= Money;
            Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());

            Recipiant.ConquerPoints += CPs;
            Recipiant.Send(new StatTypePacket(Recipiant.Hero.UID, Recipiant.ConquerPoints, StatIDs.ConquerPoints).Serialize());
            Client.ConquerPoints -= CPs;
            Client.Send(new StatTypePacket(Client.Hero.UID, Client.ConquerPoints, StatIDs.ConquerPoints).Serialize());
        }
    }
}