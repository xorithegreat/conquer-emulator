﻿using System.Drawing;
using ConquerEmulator.Database;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Client
{
	internal class StatusChange
	{
	    public static void Level(GameClient Client, byte Level, bool Show)
		{
			if (Client.Hero.Level < Level)
			{
				Client.Hero.Level = Level;
				if ((Client.Hero.Reborn == 0) && (Client.Hero.Level < 121))
					FlatDatabase.GetLevelStats(Client);
				else
					Client.StatPoints += 3;
			}
			else
			{
				Client.Hero.Level = Level;
				FlatDatabase.GetLevelStats(Client);
			}

			Client.CalculateStatBonus();
			Client.Hero.Hitpoints = Client.Hero.MaxHitpoints;
			Client.Mana = Client.MaxMana;

			var update = new BigUpdatePacket(8);
			update.UID = Client.Hero.UID;
			update.Append(0, StatIDs.Level, Client.Hero.Level);
			update.HitpointsAndMana(Client, 1);
			update.AllStats(Client, 3);
			Client.Send(update);

			if (Show)
				Client.SendScreen(new DataPacket(Client.Hero.UID, DataIDs.Leveled).Serialize(), true);

            //Nobility
            if (Client.Hero.Level >= 70)
                Client.Send(NobilityPacket.UpdateIcon(Client));
        }

	    public static void ChangeJob(GameClient Client, byte Job, bool Show)
		{
			Client.Job = Job;
			FlatDatabase.GetLevelStats(Client);

			Client.CalculateStatBonus();
			Client.Hero.Hitpoints = Client.Hero.MaxHitpoints;
			Client.Mana = Client.MaxMana;

			var update = new BigUpdatePacket(8);
			update.UID = Client.Hero.UID;
			update.Append(0, StatIDs.Job, Client.Job);
			update.HitpointsAndMana(Client, 1);
			update.AllStats(Client, 3);
			Client.Send(update);

			if (Show)
			{
				var SPacket = new StringPacket(10);
				SPacket.UID = Client.Hero.UID;
				SPacket.ID = StringIds.Effect;
				Client.SendScreen(Packets.StringPacket(SPacket, "colorstar5"), true);
			}
		}

	    public static void Reborn(GameClient Client, byte Job)
		{
			#region Spell Reset
			foreach (var spell in Client.Spells.Values)
				ConquerSpell.UnlearnSpell(Client, spell.ID);
			Client.Spells.Clear();
			#endregion
			#region Reborn Skills
			var RBSpells = ConquerSpell.GetRebornSpells(Client.Job, Job);
            if (RBSpells != null)
            {
                foreach (var spellid in RBSpells)
                    ConquerSpell.LearnSpell(Client, spellid, 0);
            }
			#endregion
			#region Prof Reset
			foreach (var Prof in Client.Proficiencies.Values)
			{
				Prof.Level = 0;
				Prof.Experience = 0;

				var LProf = LearnProfPacket.Create();
				LProf.ID = Prof.ID;
				LProf.Level = 0;
				LProf.Experience = 0;
				Client.Send(LProf, LProf.Size);

				Client.Proficiencies.Remove(Prof.ID);
				Client.Proficiencies.TryAdd(Prof.ID, Prof);
			}
			#endregion
			Client.StatPoints = 30;
			Client.StatPoints += Kernel.GetAttributePoints((byte)Client.Hero.Level);

			Client.Hero.Reborn++;
			Client.Hero.Level = 15;
			Client.Job = Job;

			FlatDatabase.GetLevelStats(Client);
			#region GearDowngrade
			foreach (var Eqitem in Client.Equipment)
			{
				var Item = (ConquerItem)Eqitem.Value;
				if (Item != null)
				{
					FlatDatabase.UnloadItemStats(Client, Item);
					ConquerItem.DowngradeItem(Item);
					FlatDatabase.LoadItemStats(Client, Item);
					Item.Send(Client);
				}
			}
			Client.Unequip(Client, (ushort)ItemPosition.Left);
			#endregion
			#region Packet Sending
			var update = new BigUpdatePacket(8);
			update.UID = Client.Hero.UID;
			update.Append(0, StatIDs.Job, Client.Job);
			update.Append(1, StatIDs.Level, Client.Hero.Level);
			update.HitpointsAndMana(Client, 2);
			update.AllStats(Client, 4);
			Client.Send(update);

			var SPacket = new StringPacket(7);
			SPacket.UID = Client.Hero.UID;
			SPacket.ID = StringIds.Effect;
			Client.Send(Packets.StringPacket(SPacket, "LuckyGuy"));

			Client.SendScreen(Client.Hero.SpawnPacket, true);
			#endregion
			GameClient.SpeakToAll(Color.Pink, ChatType.Center, "Congratulations " + Client.Hero.Name + " just got reborn.");
		}
	}
}