﻿namespace ConquerEmulator.Client
{
	public class KoInfo
	{
		public uint KillCount;
		public string Name;

		public int Position { get; set; }
	}
}