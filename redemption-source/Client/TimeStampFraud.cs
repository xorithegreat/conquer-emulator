﻿using ConquerEmulator.Main;

namespace ConquerEmulator.Client
{
	public class TimestampFraud
	{
	    public const int Jump = 0, Walk = 1;
	    private const int Count = 2;
	    private static readonly TIME fraudTimer;
	    private readonly bool firstRound;
	    private readonly int roundsSpeed;
	    private readonly TIME roundsTimer;
	    private readonly TIME[] tslog;

	    private int challenges;
	    private GameClient client;

	    private uint previous;
	    private uint pvalid;
	    private int rounds;

	    static TimestampFraud()
		{
			fraudTimer = new TIME();
#if !DISABLED
			new Task(() =>
			{
				uint time = Native.timeGetTime();
				if (fraudTimer.IsReady(time))
				{
					Process(time);
				}
			});
#endif
		}

	    public TimestampFraud(GameClient owner)
		{
			roundsTimer = new TIME();
			roundsSpeed = 20000;
			firstRound = true;

			client = owner;
			tslog = new TIME[Count];
			for (var i = 0; i < tslog.Length; i++)
				tslog[i] = new TIME(false);
		}

	    public TIME this[int n] => tslog[n];

	    private static void Process(uint time)
		{
			foreach (var client in Kernel.Clients)
			{
				var fraud = client.TimeFraud;
				if (client.TimeFraud.firstRound)
				{
					if (fraud.roundsTimer.ElapsedSinceTick >= 45000) // 45s no response
						client.Kick("TimeStampFraud::Process() -- no response");
				}
				else if (fraud.roundsTimer)
				{
					if (fraud.rounds++ > 2)
						client.Kick("TimestampFraud::Process() -- failure to comply");
					fraud.roundsTimer.Set(fraud.roundsSpeed);
					fraud.challenges++;
					//LogHandler.WriteLine("Rounds challenge applied, rounds: {0}", fraud.rounds);
				}
			}
			fraudTimer.Set(1000);
		}

	    public bool Validate(uint clientTimestamp, string error)
		{
#if !DISABLED
			if (client.TimeFraud.previous == 0)
				return true;
			else if (clientTimestamp < pvalid)
			{
				client.Kick("TimestampFraud::Validate() -- curr < prev: " + error);
				return false;
			}
			else if (clientTimestamp > (client.TimeFraud.previous + roundsSpeed + 2500))
			{
				var delta = clientTimestamp - (client.TimeFraud.previous + roundsSpeed + 2500);
				client.Kick("TimestampFraud::Validate() -- failed to validate: " + error + " delta: " + delta);
				return false;
			}
			pvalid = clientTimestamp;
#endif
			return true;
		}

	    public void Ping(uint ts)
		{
#if !DISABLED
			rounds = Math.Max(rounds - 1, 0);
			if (previous != 0)
			{
				uint delta = (uint)(ts - previous);
				if (delta > 30000 || delta < 9000)
				{
					client.Kick("TimeStampFraud::TickComply() -- too large or small, delta:" + delta);
					return;
				}
				if (firstRound)
				{
					roundsSpeed = (int)delta;
					firstRound = false;
					//LogHandler.WriteLine("First rounds acknowledge, speed: " + delta);
				}
				else
				{
					var checkSpeed = Math.Abs(delta - roundsSpeed);
					if (checkSpeed > 25000) // allow 25s difference, user may be downloading something, etc.
					{
						client.Kick("TimeStampFraud::TickComply() -- too slow or too fast comply, delta: " + delta + ", speed: " + roundsSpeed + ", rounds:" + challenges);
						return;
					}
					roundsSpeed = (int)delta;
					//LogHandler.WriteLine("{0} complied with tick, ts: {1}, delta: {2}, checkSpeed: {3}", client.Hero.Name, ts, delta, checkSpeed);
				}
			}
			previous = ts;
#endif
		}
	}
}