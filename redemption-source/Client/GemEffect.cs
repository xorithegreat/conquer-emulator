﻿using System.Collections.Generic;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Client
{
	internal class GemEffect
	{
	    public static readonly Dictionary<byte, string> BLESS_EFFECTS = new Dictionary<byte, string>
	    {
            { 1, "Aegis1" },
            { 3, "Aegis2" },
            { 5, "Aegis3" },
            { 7, "Aegis4" }
        };

	    private static readonly Dictionary<byte, string> GEM_EFFECTS = new Dictionary<byte, string>
	    {
            { GemsConst.PhoenixGem, "phoenix" },
            { GemsConst.DragonGem, "goldendragon" },
            { GemsConst.FuryGem, "fastflash" },
            { GemsConst.RainbowGem, "rainbow" },
            { GemsConst.KylinGem, "goldenkylin" },
            { GemsConst.VioletGem, "purpleray" },
            { GemsConst.MoonGem, "moon" },
            { GemsConst.TortoiseGem, "recovery" }
        };

	    public static void BlessEffects(GameClient Client, IConquerItem item)
        {
            string itemEffect;
            BLESS_EFFECTS.TryGetValue(item.Bless, out itemEffect);
            if (!string.IsNullOrEmpty(itemEffect))
            {
                var SPacket = new StringPacket((byte)itemEffect.Length)
                {
                    UID = Client.Hero.UID,
                    ID = StringIds.Effect
                };
                Client.SendScreen(Packets.StringPacket(SPacket, itemEffect), true);
            }
        }

	    public static void GemEffects(GameClient Client)
		{
			var gem = Client.Gems[RandomGenerator.Generator.Next(0, Client.Gems.Length)];
            if (gem > 0)
            {
                var effect = GEM_EFFECTS[(byte)gem];
                if (!string.IsNullOrEmpty(effect))
                {
                    var SPacket = new StringPacket((byte)effect.Length);
                    SPacket.UID = Client.Hero.UID;
                    SPacket.ID = StringIds.Effect;
                    Client.SendScreen(Packets.StringPacket(SPacket, effect), true);
                }
            }
		}
	}
}