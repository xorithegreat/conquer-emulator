﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Client
{
	public class ClientVendor
	{
		private readonly GameClient Owner;
		private INpc m_VendorNpc;
		private ConcurrentDictionary<uint, VendingItem> VendingItems;

		public ClientVendor(GameClient Client)
		{
			Owner = Client;
		}

		public uint ShopID => m_VendorNpc.UID;
		public bool IsVending { get; private set; }

		public VendingItem[] Items
		{
			get
			{
				var r = new VendingItem[VendingItems.Count];
				VendingItems.Values.CopyTo(r, 0);
				return r;
			}
		}

		public bool StartVending()
		{
			if (IsVending)
				return false;

			Dictionary<uint, INpc> npcs;

			if (Kernel.Npcs.TryGetValue(Owner.Hero.MapID, out npcs))
			{
				var vendx = (ushort)(Owner.Hero.X - 2);
				var vendy = Owner.Hero.Y;
				foreach (var npc in npcs.Values)
				{
					if ((npc.X == vendx) && (npc.Y == vendy) && !npc.IsVendor)
					{
						m_VendorNpc = npc;
						m_VendorNpc.ConvertToVendor(Owner.Hero.Name);
					    Owner.SendScreen(((NpcSpawnPacket)m_VendorNpc).Packet, true);
						VendingItems = new ConcurrentDictionary<uint, VendingItem>();
						IsVending = true;
						break;
					}
				}
			}
			return IsVending;
		}

		public void StopVending()
		{
			if (IsVending)
			{
				m_VendorNpc.ConvertToStandard();
			    Owner.SendScreen(((NpcSpawnPacket)m_VendorNpc).Packet, true);
				m_VendorNpc = null;
				VendingItems = null;
				IsVending = false;
			}
		}

		public void AddItem(IConquerItem item, uint Cost, bool PurchaseWithGold)
		{
			var vItem = new VendingItem();
			vItem.FromItem(item, PurchaseWithGold);
			vItem.Price = Cost;
			vItem.ShopID = ShopID;

			VendingItems.TryAdd(vItem.UID, vItem);
		}

		public bool SelectItem(uint UID, out VendingItem Item)
		{
			return VendingItems.TryGetValue(UID, out Item);
		}

		public void RemoveItem(uint UID)
		{
			VendingItems.Remove(UID);
		}

		public static GameClient FindVendorClient(uint shopID)
		{
			foreach (var iClient in Kernel.Clients)
			{
				if (iClient == null)
					continue;
				if (!iClient.Vendor.IsVending)
					continue;

				if (iClient.Vendor.ShopID == shopID)
					return iClient;
			}
			return null;
		}
	}
}