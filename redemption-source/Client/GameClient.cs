﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using ConquerEmulator.Crypto;
using ConquerEmulator.Database;
using ConquerEmulator.Events;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.DMap;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Mob;
using ConquerEmulator.Network;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Client
{
    //TO-DO seperate enums
    public enum PkMode : byte
	{
		Kill = 0x00,
		Peace = 0x01,
		Team = 0x02,
		Capture = 0x03
	}

	public enum MerchantTypes : byte
	{
		Asking = 1,
		No = 0,
		Yes = 255
	}

	public enum PlayerFlag : sbyte
	{
		Normal = 0,
		GameManager = 1,
		ProjectManager = 2,
		ServerAdmin = 3
	}

	public enum LoginAction : sbyte
	{
		Normal = 1,
		Create = 2,
		Banned = 3
	}

	public class GameClient
	{
		private readonly byte[] encryptionKey;
		private readonly ServerKeyExchange exchange;

		private readonly ConcurrentDictionary<uint, IConquerItem> m_Inventory;
		private readonly byte[] serverSeal;

		public uint _ConquerPoints;

		private uint _Silvers;
		public byte[] AAMagic;
		public IBaseEntity AATarget;
		public AttackType AaType;
		public ushort AAX, AAY;

        public IBaseEntity PetTarget;

        public string Account, Password;

		public ushort Agility;
		public sbyte AttackRange;

		// Auto Attacking
		public bool AutoAttaking;
		public uint BaseMagicAttack;
		public uint BaseMaxAttack;

		// Standard Item Stats
		public uint BaseMinAttack;
		public sbyte BlessPercent;
		public GameCryptography Crypter;
	    public SpellCrypto SpellCrypto;

		public bool DialogAgreed;
		public int DragonballCount;

		public ConcurrentDictionary<ushort, IConquerItem> Equipment;
		public bool ExchangeComplete;

		public int Experience;
		public uint ExperienceMultiply = 0;
		public PlayerFlag Flag = PlayerFlag.Normal;
		public ConcurrentDictionary<uint, IAssociate> Friends, Enemies;
        public double[] Gems;
        public ConquerGuild Guild;

		public uint GuildDonation;
		public uint HeadKillCounter;
		public Entity Hero;

		public int HouseLevel = 0;

		public IConquerItem[] Inventory;
		public ushort ItemHP;
		public ushort ItemMP;

		public byte Job;
		public KoInfo KoInfo;
		public uint LastMap;

		public uint LastNpc = 0;

		public bool LoggedIn = false;
		public LoginAction LoginAction = LoginAction.Normal;
		public byte LottoTries;

		private ConquerDMap m_CurrentDMap;
		public ushort Mana;
		public ushort MaxMana;

		public MerchantTypes Merchant;

        //Met Bank
        public int MeteorCount;
		public PlayerMiner Miner;

		public uint PendingFriendUID;

		public MonsterPet Pet;
		public bool PickedBox;

		public PkMode PkMode;

		public uint PrayUID = 0;
		public int PrizeNPC;
		public Screen Screen;
		public SystemSocket Socket;
		public ConcurrentDictionary<ushort, ConquerSpell> Spells, Proficiencies;
		public ushort Spirit;
		public string Spouse;
		public int SpouseHouseLevel = 0;
		public uint SpouseUID = 0;
		public byte Stamina;
		public byte MaxStamina => (byte)(Hero.Stamps.HeavenBless > DateTime.Now ? 150 : 100);
		private ushort StatHP;
		public ushort StatPoints;

		public ushort Strength;
		public ConquerTeam Team;

		public CompressBitmapColor TileColor;
		public Trade Trade;
		public Transform Transform;
		public ClientVendor Vendor;
		public uint VirtuePoints = 0;
		public ushort Vitality;
		public Warehouse Warehouse;

		public byte XpSkillCounter;

	    public ushort DisCityKills;

        // Tournament variables
        public bool HasBomb;
        public sbyte YourTdmKills;
        public uint Wager;
        public bool InTournament;
        public bool PkAllowed => TournamentBase.PkEnabled || TeamDeathMatch.PkEnabled || Dueling.PkEnabled;

        //MonsterHunter Quest
	    public bool MonsterHunter;
        public ushort MHKillCount;
        public ushort MHAttempts;
        public ushort MonsterType;

	    public bool FloorEffect;

        public GameClient(SystemSocket _Socket)
		{
			Socket = _Socket;
			Hero = new Entity(EntityFlag.Player, this);

			Warehouse = new Warehouse();
			Screen = new Screen(this);

			m_Inventory = new ConcurrentDictionary<uint, IConquerItem>();
			Inventory = new IConquerItem[0];
			Equipment = new ConcurrentDictionary<ushort, IConquerItem>();

			Spells = new ConcurrentDictionary<ushort, ConquerSpell>();
			Proficiencies = new ConcurrentDictionary<ushort, ConquerSpell>();

			Gems = new double[GemsConst.MaxGems];

			Trade = new Trade(this);
			Team = new ConquerTeam();
			Guild = new ConquerGuild();
			Vendor = new ClientVendor(this);
			Friends = new ConcurrentDictionary<uint, IAssociate>();
			Enemies = new ConcurrentDictionary<uint, IAssociate>();

			Transform = new Transform(this);
			Miner = new PlayerMiner(this);
			TimeFraud = new TimestampFraud(this);

			KoInfo = Kernel.GetKo(Hero.Name) ?? new KoInfo
				                                    {
					                                    Position = -1,
					                                    Name = Hero.Name,
					                                    KillCount = HeadKillCounter
				                                    };

			encryptionKey = Encoding.ASCII.GetBytes("DR654dt34trg4UI6");
			serverSeal = Encoding.ASCII.GetBytes("TQServer");
			exchange = new ServerKeyExchange();
			Crypter = new GameCryptography(encryptionKey);
            SpellCrypto = new SpellCrypto();
		}

		public TimestampFraud TimeFraud { get; private set; }
		public uint ConquerPoints
		{
			get { return _ConquerPoints; }
			set
			{
				_ConquerPoints = value;
				_ConquerPoints = Math.Min(_ConquerPoints, DataStructures.MaxCurrency);
			}
		}
		public uint Silvers
		{
			get { return _Silvers; }
			set
			{
				_Silvers = value;
				_Silvers = Math.Min(_Silvers, DataStructures.MaxCurrency);
			}
		}
		public ConquerDMap CurrentDMap
		{
			get
			{
				if (m_CurrentDMap == null)
					ConquerDMap.Maps.TryGetValue(Hero.MapID.ID, out m_CurrentDMap);
				return m_CurrentDMap;
			}
		}
		public bool IsBowman
		{
			get
			{
				var weaponR = GetEquipmentItem(ItemPosition.Right);
				return weaponR != null && ConquerItem.CheckBow(weaponR.ID);
			}
		}
		//Nobility
		public NobilityInfo NobleInfo;
		public unsafe uint NobleDonation
		{
			get { return NobleInfo.Donation; }
			set
			{
				int oldRank = NobleRank;
				NobleInfo.Donation = value;
				MySqlDatabase.UpdateNobility(NobleInfo);
				ConquerNobility.UpdateNobility();

				fixed (byte* ptr = Hero.SpawnPacket)
				{
					*(sbyte*)(ptr + 84) = NobleMedal;
					*(int*)(ptr + 85) = NobleRank;
				}

				if (oldRank != NobleRank)
					ConquerNobility.UpdatePlayers();
			}
		}
		public int NobleRank
		{
			get
			{
			    if (NobleInfo == null)
			        return -1;
				if (NobleDonation == 0)
					return -1;
				if (MySqlDatabase.GetNobilityRank(NobleInfo.Donation) > 50)
					return -1;
				return ConquerNobility.RankingList.IndexOf(Hero.UID);
			}
		}
		public sbyte NobleMedal => ConquerNobility.NobilityMedal(NobleRank, NobleDonation);

        public unsafe void Send(byte[] Packet)
        {
            if (Socket.Connected)
            {
                try
                {
                    var Copy = new byte[Packet.Length];
                    fixed (byte* src = Packet, dest = Copy)
                    {
#if !DISABLED
						ushort type = *((ushort*)(src + 2));
						Console.WriteLine("type: " + type + " Size: " +Packet.Length);
#endif
                        Native.memcpy(dest, src, Packet.Length);
                    }

                    if (ExchangeComplete)
                        Buffer.BlockCopy(serverSeal, 0, Copy, Copy.Length - 8, 8);
                    lock (Crypter)
                    {
                        Crypter.Encrypt(Copy);
                    }

                    Socket.Send(Copy);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

		public void Send(object Packet, ushort Size)
		{
            if (Socket.Connected)
            {
                try
                {
                    var CopyPacket = PacketKernel.Serialize(Packet, Size);
#if !DISABLED
					ushort type = (ushort)BitConverter.ToUInt32(CopyPacket, 2);
					Console.WriteLine("type: " + type + " Size: " + CopyPacket.Length);
#endif
                    if (ExchangeComplete)
                        Buffer.BlockCopy(serverSeal, 0, CopyPacket, CopyPacket.Length - 8, 8);
                    lock (Crypter)
                    {
                        Crypter.Encrypt(CopyPacket);
                    }

                    Socket.Send(CopyPacket);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
		}

		public void Kick(string reason)
		{
			Console.WriteLine(Hero.Name + " kicked => " + reason);
			Socket.Disconnect();
		}

        public int DisKillRequirement()
        {
            var job = Kernel.GetBaseClass(Job);
            switch (job)
            {
                case 10:
                    return 800;
                case 25:
                    return 900;
                case 40:
                    return 1300;
                case 100:
                    if (Job >= 130 && Job <= 135)
                        return 600;
                    return 1000;
            }
            return 0;
        }

        //Offline TG
        public ulong OfflineTrainingTime;
        public void GainExpBall(double _time = 600)
        {
            if (Hero.Level >= 130)
                return;

            double upLevelTime = FlatDatabase.LoadUpLevelTime((byte)Hero.Level);
            double neededExperience = FlatDatabase.LoadLevelExp((byte)Hero.Level);

            var timeRemaining = upLevelTime * (neededExperience - (double)Experience) / neededExperience;

            if (_time >= timeRemaining)
            {
                Experience = 0;
                while (Math.Abs(upLevelTime) > 0 && _time >= timeRemaining)
                {
                    StatusChange.Level(this, (byte)(Hero.Level + 1), false);
                    _time -= timeRemaining;
                    timeRemaining = FlatDatabase.LoadUpLevelTime((byte)Hero.Level);
                    neededExperience = FlatDatabase.LoadLevelExp((byte)Hero.Level);
                }
                //Send level once
                SendScreen(new DataPacket(Hero.UID, DataIDs.Leveled).Serialize(), true);
            }

            if (Math.Abs(upLevelTime) > 0 && _time > 0)
                Experience += (int)(neededExperience / timeRemaining * _time);

            Send(new StatTypePacket(Hero.UID, (uint)Experience, StatIDs.Experience).Serialize());
        }
		public void SendOtgResults()
		{
			var Time = (uint)Math.Min(OfflineTrainingTime, DateTime.Now.Subtract(Hero.Stamps.OfflineTG).TotalMinutes);
			uint userExperience = 0;
			ushort userLevel = 0;

			if (Hero.Level <= 129)
			{
				userExperience = (uint)Experience;
				userLevel = Hero.Level;

				double upLevelTime = FlatDatabase.LoadUpLevelTime((byte)userLevel);
				double neededExperience = FlatDatabase.LoadLevelExp((byte)userLevel);

				var timeRemaining = upLevelTime * (neededExperience - (double)Experience) / neededExperience;

				if (Time >= timeRemaining)
				{
					userExperience = 0;
					while (Math.Abs(upLevelTime) > 0 && Time >= timeRemaining)
					{
						userLevel++;

						Time -= (uint)timeRemaining;
						timeRemaining = FlatDatabase.LoadUpLevelTime((byte)userLevel);
						neededExperience = FlatDatabase.LoadLevelExp((byte)userLevel);
					}
				}

				if (Math.Abs(upLevelTime) > 0 && Time > 0)
					userExperience += (uint)(neededExperience / timeRemaining * Time);
			}

			var InfoPacket = OfflineTgInfoPacket.Create();
			InfoPacket.Experience = userExperience;
			InfoPacket.Level = userLevel;
			InfoPacket.UsedTime = (ushort)Time;
			InfoPacket.RemainingTime = (ushort)(OfflineTrainingTime - Time);
			Send(InfoPacket, InfoPacket.Size);
		}

        public bool TeleportChecks(ushort X, ushort Y, uint MapID, out ConquerDMap newDMap)
        {
            if (Vendor.IsVending)
                Vendor.StopVending();
            if (Miner.Mining)
                Miner.Stop();
            if (Hero.MapID.ID != MapID)
                Pet?.Kill();
            if (AutoAttaking)
            {
                AATarget = null;
                AutoAttaking = false;
            }
            FloorEffect = false;

            if (InTournament)
            {
                if (TeamDeathMatch.Started && TeamDeathMatch.Teams.ContainsKey(this))
                    TeamDeathMatch.Leave(this);
                if (TournamentBase.Started && TournamentBase.Entries.Contains(this))
                    TournamentBase.Leave(this);
                if (Dueling.Started)
                {
                    if (Hero.MapID.ID == Dueling.Map.ID && this.Hero.MapID.DynamicID == Dueling.Map.DynamicID)
                        Dueling.ForfeitDuel(this);
                }
            }

            if (MapID != Hero.MapID.ID)
            {
                ConquerDMap.Maps.TryGetValue(MapID, out newDMap);
                Pet?.Kill();
            }
            else
            {
                newDMap = CurrentDMap;
            }
            if (newDMap != null)
            {
                if (newDMap.Invalid(X, Y))
                    return false;
            }
            return true;
        }

        public void Teleport(uint MapID, ushort X, ushort Y, uint DynamicID = 0)
        {
            ConquerDMap newDMap;
            if (!TeleportChecks(X, Y, MapID, out newDMap))
                return;

            m_CurrentDMap = newDMap;

            var Map = new Map(MapID, newDMap.MapID.MapDoc)
            {
                DynamicID = DynamicID
            };

            Hero.Action = ConquerAction.None;
            LastMap = Hero.MapID.ID;

            if (MapID != Hero.MapID)
                SendScreen(new DataPacket(Hero.UID, DataIDs.RemoveEntity).Serialize(), false);
            else
            {
                if (Kernel.GetDistance(X, Y, Hero.X, Hero.Y) >= DataStructures.ViewDistance)
                    SendScreen(new DataPacket(Hero.UID, DataIDs.RemoveEntity).Serialize(), false);
            }

            Screen.Cleanup();

            Hero.MapID = Map;
            Hero.X = X;
            Hero.Y = Y;
            Hero.LastX = X;
            Hero.LastY = Y;

            Send(DataPacket.DataUpdate(Hero.UID, Hero.MapID.MapDoc, 0, X, Y, DataIDs.ChangeMap));
            SendScreen(Hero.SpawnPacket, false);
        }

		public void PullBack(string Reason)
		{
			Speak(Color.Red, ChatType.Center, Reason);
			Teleport(Hero.MapID.ID, Hero.X, Hero.Y, Hero.MapID.DynamicID);

			Pet?.Reattach();
		}

		public void Revive(GameClient Client, bool reviveHere)
		{
			if (!Client.Hero.Dead)
				return;
			Client.Hero.Dead = false;

			Client.Hero.StatusFlag &= ~StatusFlag.Dead;
			Client.Hero.StatusFlag &= ~StatusFlag.Ghost;
			if (Client.Hero.Flashing)
				Client.Hero.Flashing = false;

			Client.Hero.Hitpoints = Client.Hero.MaxHitpoints;
			Client.Stamina = Client.MaxStamina;
			#region Packet Sending
			var big = new BigUpdatePacket(4);
			big.UID = Hero.UID;
			big.Append(0, StatIDs.Stamina, Stamina);
			big.Append(1, StatIDs.Hitpoints, Hero.Hitpoints);
			big.Append(2, StatIDs.Model, Hero.Model);
			big.Append(3, StatIDs.RaiseFlag, (ulong)Hero.StatusFlag);
			SendScreen(big, true);
			#endregion

			if (!reviveHere)
			{
				var res = new MapSettings(Hero.MapID.ID).RevivePoint;
				Teleport(res[0], (ushort)res[1], (ushort)res[2]);
			}

			Client.Hero.Stamps.Protection = DateTime.Now.AddSeconds(5.0);
		}

		public void AddBlessing(byte Days)
		{
            Hero.StatusFlag |= StatusFlag.Blessing;
            Hero.Stamps.HeavenBless = DateTime.Now.AddDays(Days);

            var SPacket = new StringPacket(5);
            SPacket.UID = Hero.UID;
            SPacket.ID = StringIds.Effect;
            Send(Packets.StringPacket(SPacket, "bless"));

            Send(new StatTypePacket(Hero.UID, (ulong)Hero.StatusFlag, StatIDs.RaiseFlag).Serialize());
            Send(new StatTypePacket(Hero.UID, (uint)Kernel.SecondsFromNow(Hero.Stamps.HeavenBless), StatIDs.HeavenBlessing).Serialize());
			Send(new StatTypePacket(Hero.UID, 0, StatIDs.OnlineTraining).Serialize());
		}

		public void AddEnemy(uint UID, string Name)
		{
			if (Enemies.ContainsKey(UID))
				return;

			IAssociate e = AssociatePacket.Create();
			e.ID = AssociationID.AddEnemy;
			e.Online = true;
			e.Name = Name;
			e.UID = UID;

			MySqlDatabase.AddEnemy(this, UID, Name);

			Enemies.TryAdd(e.UID, e);
			Send(e.GetBytes());
		}

		public void AddFriend(uint UID, string Name)
		{
			if (Friends.ContainsKey(UID))
				return;

			IAssociate e = AssociatePacket.Create();
			e.ID = AssociationID.AddFriend;
			e.Online = true;
			e.Name = Name;
			e.UID = UID;

			MySqlDatabase.AddFriend(this, UID, Name);

			Friends.TryAdd(e.UID, e);
			Send(e.GetBytes());
		}

		public void Speak(Color Color, ChatType ChatType, string Message)
		{
			var Notify = new MessageInfo(Message, "", "SYSTEM", Color, ChatType);
			Notify.Message = Message;
			Notify.dwParam = Hero.UID;
			Notify.To = Hero.Name;
			Send(Packets.Message(Notify));
		}

		public static void SpeakToMap(Color Color, ChatType ChatType, string Message, uint MapID, uint DynamicID)
		{
			var Map = new MessageInfo(Message, "", "SYSTEM", Color, ChatType);
			Map.Message = Message;
			foreach (var Client in Kernel.Clients)
			{
				if ((Client != null) && (Client.Hero.MapID.ID == MapID) && (Client.Hero.MapID.DynamicID == DynamicID))
				{
					Map.dwParam = Client.Hero.UID;
					Map.To = Client.Hero.Name;
					Client.Send(Packets.Message(Map));
				}
			}
		}

		public static void SpeakToAll(Color Color, ChatType ChatType, string Message)
		{
			var Notify = new MessageInfo(Message, "", "SYSTEM", Color, ChatType);
			Notify.Message = Message;
			foreach (var Client in Kernel.Clients)
			{
				Notify.dwParam = Client.Hero.UID;
				Notify.To = Client.Hero.Name;
				Client.Send(Packets.Message(Notify));
			}
		}

		public static void SpeakToAll(GameClient Client, Color Color, ChatType ChatType, string Message)
		{
			var Notify = new MessageInfo(Message, "", Client.Hero.Name, Color, ChatType);
			Notify.Message = Message;
			foreach (var Cli in Kernel.Clients)
			{
				Notify.dwParam = Cli.Hero.UID;
				Notify.To = Cli.Hero.Name;
				Cli.Send(Packets.Message(Notify));
			}
		}

		public void SendScreen(byte[] Msg, bool SendSelf)
		{
			foreach (var obj in Screen.Objects)
			{
				if (obj.MapObjType == MapObjectType.Player)
				{
					var gameClient = obj.Owner as GameClient;
					gameClient?.Send(Msg);
				}
			}
			if (SendSelf)
				Send(Msg);
		}

		public void SendScreen(IClassPacket CMsg, bool SendSelf)
		{
			SendScreen(CMsg.Serialize(), SendSelf);
		}

		public void CalculateStatBonus()
		{
			byte ManaBoost = 5;
			const byte HitpointBoost = 24;
			var JobID = (sbyte)(Job / 10);
			if ((JobID == 13) || (JobID == 14))
				ManaBoost += (byte)(5 * (Job - JobID * 10));
			StatHP = (ushort)(Strength * 3 + Agility * 3 + Spirit * 3 + Vitality * HitpointBoost + 1);
			MaxMana = (ushort)(Spirit * ManaBoost + ItemMP);
			Mana = Math.Min(Mana, MaxMana);

			switch (Job)
			{
				case 11: Hero.MaxHitpoints = (int)(StatHP * 1.05F); break;
				case 12: Hero.MaxHitpoints = (int)(StatHP * 1.08F); break;
				case 13: Hero.MaxHitpoints = (int)(StatHP * 1.10F); break;
				case 14: Hero.MaxHitpoints = (int)(StatHP * 1.12F); break;
				case 15: Hero.MaxHitpoints = (int)(StatHP * 1.15F); break;
				default: Hero.MaxHitpoints = StatHP; break;
			}
			Hero.MaxHitpoints += ItemHP;
			Hero.Hitpoints = Math.Min(Hero.Hitpoints, Hero.MaxHitpoints);

            // calculate attack
            Hero.MaxAttack = (uint)((Strength + BaseMaxAttack) * (1 + Gems[GemsConst.DragonGem]));
            Hero.MinAttack = (uint)((Strength + BaseMinAttack) * (1 + Gems[GemsConst.DragonGem]));
            Hero.MagicAttack = (uint)(BaseMagicAttack * (1 + Gems[GemsConst.PhoenixGem]));
        }

		public void LoadEquipment()
		{
			foreach (var equip in Equipment)
				Equip(equip.Value, equip.Key, this);
		}

		public void LoadSpells()
		{
			foreach (var spell in Spells)
			{
				var LSpell = LearnSpellPacket.Create();
				LSpell.ID = spell.Value.ID;
				LSpell.Level = spell.Value.Level;
				LSpell.Experience = spell.Value.Experience;
				Send(LSpell, LSpell.Size);
			}
		}

		public void LoadProficiencies()
		{
			foreach (var prof in Proficiencies)
			{
				var LProf = LearnProfPacket.Create();
				LProf.ID = prof.Value.ID;
				LProf.Level = prof.Value.Level;
				LProf.Experience = (uint)prof.Value.Experience;
				Send(LProf, LProf.Size);
			}
		}

		public bool IsFullSuper()
		{
			IConquerItem RHandWeapon = null;
			for (var pos = ConquerItem.FirstSlot; pos < ConquerItem.LastSlot; pos++)
			{
				if ((pos == ItemPosition.Bottle) || (pos == ItemPosition.Garment))
					continue;

				var Item = GetEquipmentItem(pos);
				if (pos == ItemPosition.Right)
					RHandWeapon = Item;

				if (RHandWeapon != null)
				{
					if ((pos == ItemPosition.Left) && ConquerItem.IsTwoHander(RHandWeapon.ID))
					{
						if (ConquerItem.GetQuality(RHandWeapon.ID) == 9)
							continue;
					}
				}
				if (Item == null)
					return false;

				if (ConquerItem.GetQuality(Item.ID) != 9)
					return false;
			}
			return true;
		}

		public bool ValidateEquipment(IConquerItem Item)
		{
			#region ItemType Checks
			switch (Item.Position)
			{
				case ItemPosition.Left:
					{
						if (ConquerItem.IsItemType(Item.ID, ItemType.Backsword))
							return false;
						if (ConquerItem.IsTwoHander(Item.ID))
							return false;
						var Right = GetEquipmentItem(ItemPosition.Right);
						if (Right != null)
							if (ConquerItem.IsTwoHander(Item.ID))
								return false;
						break;
					}
				case ItemPosition.Right:
					{
						if (ConquerItem.IsTwoHander(Item.ID))
						{
							var Left = GetEquipmentItem(ItemPosition.Left);
							if ((Left != null) && !ConquerItem.IsItemType(Left.ID, ItemType.Arrow))
								return false;
						}
						break;
					}
				case ItemPosition.Headgear:
					{
						var small = ConquerItem.GetSmallItemType(Item.ID);
						if ((small > 14) || (small < 11))
							return false;
						break;
					}
				case ItemPosition.Necklace:
					{
						if (ConquerItem.GetSmallItemType(Item.ID) != 12)
							return false;
						break;
					}
				case ItemPosition.Armor:
					{
						if (ConquerItem.GetSmallItemType(Item.ID) != 13)
							return false;
						break;
					}
				case ItemPosition.Ring:
					{
						if (ConquerItem.GetSmallItemType(Item.ID) != 15)
							return false;
						break;
					}
				case ItemPosition.Boots:
					{
						if (ConquerItem.GetSmallItemType(Item.ID) != 16)
							return false;
						break;
					}
			}
			#endregion
			#region Specific Item Checks
			var std = new StanderdItemStats(Item.ID);
			int Req;
			if ((Req = std.ReqProfLvl) != 0)
				if (Proficiencies.ContainsKey(ConquerItem.GetItemType(Item.ID)))
				{
					if (Req > std.ReqProfLvl)
						return false;
				}
				else
				{
					return false;
				}
			if (Hero.Level < std.ReqLvl)
				return false;
			if (Strength < std.ReqStr)
				return false;
			if (Agility < std.ReqAgi)
				return false;
			if ((Req = std.ReqJob) != 0)
			{
				var Req2 = Req / 10;
				if ((Req2 == 1) || (Req2 == 2) || (Req2 == 4) || (Req2 == 5))
				{
					Req2 = Req % 10;
					Req = Job % 10;
					if (!((Req <= 5) && (Req >= Req2)))
						return false;
				}
			}
			if ((Item.ID >= 137310) && (Item.ID <= 137910))
				if (Flag == 0)
					return false;
			#endregion
			return true;
		}

		public IConquerItem GetEquipmentItem(ItemPosition Position)
		{
			IConquerItem item;
			Equipment.TryGetValue((ushort)Position, out item);
			return item;
		}

		private void CreateInventoryInstance()
		{
			var temp = new IConquerItem[m_Inventory.Count];
			m_Inventory.Values.CopyTo(temp, 0);
			Inventory = temp;
		}

		public bool AddInventory(IConquerItem Item, ItemStatus status = ItemStatus.AddOrUpdate)
		{
			if (m_Inventory.Count < 40)
			{
				Item.Position = (ushort)ItemPosition.Inventory;

				if (m_Inventory.ContainsKey(Item.UID))
				{
					Item.Send(this);
					return true;
				}
				m_Inventory.TryAdd(Item.UID, Item);
				Item.Send(this);
				Item.Status = status;
				CreateInventoryInstance();
				return true;
			}
			return false;
		}

		public void AddInventory(uint ID, byte Plus = 0, byte Soc1 = 0, byte Soc2 = 0)
		{
			if (m_Inventory.Count < 40)
			{
				IConquerItem item = new ConquerItem(true);
				item.UID = ConquerItem.NextItemUID;
				item.ID = ID;
				item.Plus = Plus;
				item.SocketOne = Soc1;
				item.SocketTwo = Soc2;
				AddInventory(item);
			}
		}

		public void AddItems(uint ID, byte amount)
		{
			for (byte i = 0; i < amount; i++)
				AddInventory(ID);
		}

		public bool RemoveInventory(uint ID)
		{
			foreach (var Item in Inventory.Where(Item => Item.ID == ID))
			{
				RemoveInventory(Item);
				return true;
			}
			return false;
		}

		public void RemoveInventory(IConquerItem item, ItemStatus status = ItemStatus.Deleted)
		{
			if (status == ItemStatus.Deleted)
				MySqlDatabase.DeleteItem(item.UID);
			m_Inventory.Remove(item.UID);
			var Remove = new ItemUsagePacket(item.UID, 0, ItemUsageID.RemoveInventory);
			Send(Remove, Remove.Size);
			CreateInventoryInstance();
		}

		public sbyte CountInventory(uint ItemID)
		{
			sbyte Count = 0;
			foreach (var item in Inventory.Where(item => item.ID == ItemID))
				Count++;
			return Count;
		}

		public bool InventoryContains(uint ID)
		{
			return Inventory.Any(Item => Item.ID == ID);
		}
        public bool CheckMultiples(uint[] items)
        {
            return items.All(item => m_Inventory.ContainsKey(item));
        }

        public IConquerItem GetInventoryItem(uint UID)
		{
			IConquerItem item;
			m_Inventory.TryGetValue(UID, out item);
			return item;
		}

		public bool TryExpendItemOfId(uint itemid, int amountRequired)
		{
			IEnumerable<IConquerItem> items = Inventory.Where(x => x.ID == itemid).ToList();
			if (items.Sum(x => x.ID) >= amountRequired)
			{
				for (byte i = 0; i < amountRequired; i++)
					RemoveInventory(items.ElementAt(i));

				return true;
			}
			return false;
		}

		public bool TryExpendMeteors(int amountRequired)
		{
			IEnumerable<IConquerItem> meteorScrolls = Inventory.Where(x => x.ID == 720027).ToList();
			var scrollCount = (int)meteorScrolls.Sum(x => x.ID);

			IEnumerable<IConquerItem> meteors = Inventory.Where(x => x.ID == 1088001).ToList();
			var metCount = (int)meteors.Sum(x => x.ID);

			if (metCount >= amountRequired)
				return TryExpendItemOfId(1088001, amountRequired);
			if (metCount + scrollCount * 10 >= amountRequired)
			{
				while ((amountRequired > 10) && meteorScrolls.Any())
				{
					var scroll = meteorScrolls.First();
					RemoveInventory(scroll);
					amountRequired -= 10;
				}

				if (meteors.Sum(x => x.ID) < amountRequired)
				{
					OpenScroll(meteorScrolls.First());
					meteors = Inventory.Where(x => x.ID == 1088001);
				}
				foreach (var met in meteors.Take(amountRequired))
				{
					RemoveInventory(met);
					amountRequired--;
				}

				return true;
			}
			return false;
		}

		public bool OpenScroll(IConquerItem item, bool Meteor = true)
		{
			if (item == null)
				return false;
			if ((item.ID == 720027) || ((item.ID == 720028) && (Inventory.Length <= 30)))
			{
				RemoveInventory(item);

				for (var i = 0; i < 10; i++)
				{
					IConquerItem scroll = new ConquerItem(true);
					scroll.UID = ConquerItem.NextItemUID;
					scroll.ID = 1088001;

                    if (!Meteor)
                    {
                        scroll = new ConquerItem(true)
                        {
                            UID = ConquerItem.NextItemUID,
                            ID = 1088000
                        };
                    }
					AddInventory(scroll);
				}
				return true;
			}
			return false;
		}

        public bool HasItemType(uint[] ItemClass, byte AmountNeeded)
        {
            var hasList = new List<uint>();
            foreach (var item in Inventory)
            {
                if (item != null)
                {
                    if (ItemClass.Any(element => item.ID == element))
                        hasList.Add(item.ID);

                    if (hasList.Count == AmountNeeded)
                    {
                        foreach (var o in hasList)
                            RemoveInventory(o);

                        return true;
                    }
                }
            }
            return false;
        }

        public void PerformLottery()
		{
			var prize = Kernel.LotteryItems.GetRandomElement();

			IConquerItem item = new ConquerItem(true);
			item.ID = prize.ID;
			item.UID = ConquerItem.NextItemUID;
			item.Plus = prize.Plus;
			item.SocketOne = prize.SocketOne;
			item.SocketTwo = prize.SocketTwo;

			var item_stats = new StanderdItemStats(prize.ID);
			item.Durability = item_stats.Durability;
			item.MaxDurability = item_stats.Durability;
			AddInventory(item);

			PickedBox = true;
		}

		public void SendStatMessage()
		{
			var Msg = new MessageInfo(" Attack: {0}~{1} MagicAttack: {2} Defence: {3} MDefence: {4} Dodge: {5}", Color.White, ChatType.Center);
			Msg.Message = string.Format(Msg.Message, Hero.MinAttack, Hero.MaxAttack, Hero.MagicAttack, Hero.Defence, Hero.MDefence + Hero.PlusMDefence, Hero.Dodge);
			Send(Msg.Serialize());
		}

		public unsafe bool Unequip(GameClient Client, ushort EquipSlot)
		{
			IConquerItem Removed;
            if (Equipment.TryGetValue(EquipSlot, out Removed))
            {
                if (AddInventory(Removed))
                {
                    Equipment.Remove(EquipSlot);
                    var UnEquipPack = new ItemUsagePacket(Removed.UID, EquipSlot, ItemUsageID.UnequipItem);
                    Send(UnEquipPack.Serialize());
                    #region SpawnPacket
                    sbyte Position = -1;
                    switch (EquipSlot)
                    {
                        case 9: Position = 28; break;
                        case 1: Position = 24; break;
                        case 3: Position = 32; break;
                        case 4: Position = 36; break;
                        case 5: Position = 40; break;
                    }
                    if (Position != -1)
                    {
                        fixed (byte* Ptr = Hero.SpawnPacket)
                        {
                            *(uint*)(Ptr + Position) = 0;
                        }
                    }
                    #endregion
                    ushort swap = 0;
                    swap = (ushort)Removed.Position;
                    Removed.Position = (ItemPosition)EquipSlot;
                    FlatDatabase.UnloadItemStats(this, Removed);
                    Removed.Position = (ItemPosition)swap;
                    Client.SendScreen(Hero.SpawnPacket, false);
                    return true;
                }
            }
			return false;
		}

		public unsafe bool Equip(IConquerItem Item, ushort EquipSlot, GameClient Client)
		{
			Item.Position = (ItemPosition)EquipSlot;
			Item.Status = ItemStatus.AddOrUpdate;
			Send(((IClassPacket)Item).Serialize());
			FlatDatabase.LoadItemStats(this, Item);
			#region SpawnPacket
			sbyte Position = -1;
			switch (Item.Position)
			{
				case ItemPosition.Garment: Position = 28; break;
				case ItemPosition.Headgear: Position = 24; break;
				case ItemPosition.Armor: Position = 32; break;
				case ItemPosition.Right:Position = 36;	break;
				case ItemPosition.Left: Position = 40; break;
			}
			if (Position != -1)
			{
				fixed (byte* Packet = Hero.SpawnPacket)
				{
					*(uint*)(Packet + Position) = Item.ID;
				}
			}
			Client.SendScreen(Hero.SpawnPacket, false);
			#endregion
			if (!Equipment.ContainsKey(EquipSlot))
			{
				Equipment.TryAdd(EquipSlot, Item);
				return true;
			}
			return false;
		}
		#region DHKeyExchange
		public void StartExchange()
		{
			Send(exchange.CreateServerKeyPacket());
		}

		public void CompleteExchange(byte[] buffer)
		{
			if (buffer.Length <= 36)
				return;

			lock (Crypter)
			{
				var publicKey = Encoding.ASCII.GetString(buffer, buffer[11] + 19, 128).Trim('\0');
				exchange.HandleClientKeyPacket(publicKey, ref Crypter);
				ExchangeComplete = true;
			}
		}
		#endregion
	}
}