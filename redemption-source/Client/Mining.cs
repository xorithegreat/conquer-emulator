﻿using System;
using System.Drawing;
using System.Linq;
using ConquerEmulator.Database;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Mob;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Client
{
    public class PlayerMiner
	{
	    private readonly GameClient Owner;
	    private MineField Field;
	    private DateTime LastSwing;
	    public bool Mining;

	    public PlayerMiner(GameClient Client)
		{
			Owner = Client;
		}

	    public bool CanMine => Owner != null && DateTime.Now >= LastSwing.AddSeconds(3);

	    public void Stop()
		{
			Mining = false;
		}

	    public bool Start()
		{
			Mining = true;
			Field = new MineField(Owner.Hero.MapID.ID);
			return Field.ValidField;
		}

	    public bool SwingPickaxe()
		{
			var reqitem = Owner.Equipment.Values.FirstOrDefault(x => x.Position == ItemPosition.Right);
			if (reqitem == null)
			{
				Stop();
				return false;
			}
			if (!ConquerItem.IsItemType(reqitem.ID, ItemType.Pickaxe))
			{
				Stop();
				return false;
			}
			#region Ores
			var nRand = RandomGenerator.Generator.Next(10);
			foreach (var Ore in Field.Ores)
			{
				if (Ore.ID == 0)
					continue;

				var idOreType = Ore.GetRandom();
                var nChance = ReturnRate(Ore.ID);
				if (nRand < nChance)
				{
					var award = new ConquerItem(true);
					award.UID = ConquerItem.NextItemUID;
					award.ID = idOreType;
					if (Owner.AddInventory(award))
						Owner.Speak(Color.Turquoise, ChatType.TopLeft, "You\'ve Mined a ore.");
					break;
				}
			}
			#endregion
			#region Gems
			foreach (var nGemID in Field.FieldGems)
			{
				if (nGemID == 0)
					continue;

				var Slot = (int)((nGemID - 700000) / 10);
				for (byte j = 0; j < MineField.Gems[Slot].Length; j++)
				{
					if (MineField.Gems[Slot][j].Rate)
					{
						var award = new ConquerItem(true);
						award.UID = ConquerItem.NextItemUID;
						award.ID = MineField.Gems[Slot][j].ID;
						if (Owner.AddInventory(award))
							Owner.Speak(Color.Turquoise, ChatType.TopLeft, "You\'ve Mined a gem.");
						break;
					}
				}
			}
			#endregion
			Mining = true;
			LastSwing = DateTime.Now;

			Owner.SendScreen(new DataPacket(Owner.Hero.UID, DataIDs.Mining).Serialize(), true);
			return true;
		}

	    private int ReturnRate(uint OreId)
        {
            if (OreId >= 1072010 && OreId <= 1072019)
                return DataStructures.IronOre;
            if (OreId >= 1072020 && OreId <= 1072029)
                return DataStructures.CopperOre;
            if (OreId >= 1072040 && OreId <= 1072049)
                return DataStructures.SilverOre;
            if (OreId >= 1072050 && OreId <= 1072059)
                return DataStructures.GoldOre;
            return DataStructures.EuxeniteOre;
        }
	}

	public class MineField
	{
	    public static SpecialItemWatcher[][] Gems;
	    private readonly IniFile Mining = new IniFile(FlatDatabase.Location + "Mining.ini");
	    public uint[] FieldGems;

	    public Ore[] Ores;

	    static MineField()
		{
			Gems = new SpecialItemWatcher[GemsConst.MaxMineGems][];
			for (var i = 0; i < Gems.Length; i++)
			{
				Gems[i] = new SpecialItemWatcher[3];
				for (byte i2 = 0; i2 < Gems[i].Length; i2++)
				{
					var ID = (uint)(700000 + i * 10 + i2 + 1);
					int Rate;
                    if (ID == 2)
                        Rate = DataStructures.SuperGem;
                    else if (ID == 1)
                        Rate = DataStructures.RefinedGem;
					else
						Rate = DataStructures.NormalGem;
					Gems[i][i2] = new SpecialItemWatcher(ID, Rate);
				}
			}
		}

	    public MineField(uint MapID)
		{
			var strMap = MapID.ToString();
			Ores = new Ore[Mining.ReadSByte(strMap, "OreCount", 0)];
			for (sbyte i = 0; i < Ores.Length; i++)
			{
				Ores[i].ID = Mining.ReadUInt32(strMap, "Ore" + i, 0);
				Ores[i].MaxQuality = Mining.ReadSByte("Quality", Ores[i].ID.ToString(), 0);
			}
			FieldGems = new uint[Mining.ReadSByte(strMap, "GemCount", 0)];
			for (sbyte i = 0; i < Ores.Length; i++)
				FieldGems[i] = Mining.ReadUInt32(strMap, "Gem" + i, 0);
		}

	    public bool ValidField => Ores.Length > 0;

	    public struct Ore
		{
			public uint ID;
			public sbyte MaxQuality;

			public uint GetRandom()
			{
				var num = ID;
				if (MaxQuality > 0)
					num += (uint)RandomGenerator.Generator.Next(0, MaxQuality);
				return num;
			}
		}
	}
}