﻿using System;
using System.Linq;
using ConquerEmulator.Database;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Mob;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Client
{
	internal class Experience
	{
	    public static void ProcessLeveling(GameClient Attacker, IBaseEntity Opponent, uint Damage, ushort SpellID)
		{
			if (Opponent.EntityFlag == EntityFlag.Monster)
			{
				var Monster = (Monster)Opponent.Owner;
				if (Monster.IsGuard)
					return;
			}
			if (SpellID == 0)
			{
				Damage = (uint)(Damage * (1 + Attacker.Gems[GemsConst.VioletGem]));

				var rightHand = Attacker.Equipment.Values.FirstOrDefault(x => x.Position == ItemPosition.Right);
				if (rightHand != null)
					ConquerSpell.CalculateProfExp(Attacker, (ushort)(rightHand.ID / 1000), Damage);

				var leftHand = Attacker.Equipment.Values.FirstOrDefault(x => x.Position == ItemPosition.Left);
				if (leftHand != null)
					ConquerSpell.CalculateProfExp(Attacker, (ushort)(leftHand.ID / 1000), Damage / 2);
			}
			if (SpellID != 0)
				ConquerSpell.CalculateSpellExp(Attacker, SpellID, Damage);

			if (Attacker.Team.Active)
				Attacker.Team.CalculateExp(Opponent);

			CalculateExp(Attacker, Opponent.Level, Damage);
		}

	    public static void CalculateExp(GameClient Client, int OpponentLv, uint Amount)
		{
			if (Client.Hero.Level < 130)
			{
				double AddedExp = 0;
				if (OpponentLv + 4 < Client.Hero.Level)
					AddedExp = 0.1;
				if (OpponentLv + 4 >= Client.Hero.Level)
					AddedExp = 1;
				if (OpponentLv >= Client.Hero.Level)
					AddedExp = 1.1;
				if (OpponentLv - 4 > Client.Hero.Level)
					AddedExp = 1.3;

                if (Client.Hero.Stamps.HeavenBless > DateTime.Now)
                    Amount = (uint)(Amount * 1.2);
			    if (Client.Hero.Stamps.ExpPotion > DateTime.Now)
			        Amount = (uint)(Amount * 2.0);
				Amount = (uint)(Amount * (1 + Client.Gems[GemsConst.RainbowGem]));
				Amount = (uint)(Amount * AddedExp);
				Amount *= DataStructures.ExperienceRate;

				if (Client.Hero.MapID == 1039)
					Amount = (uint)Math.Round(Amount * 0.1); // * tgModifier

				Client.Experience += (int)Amount;

				var Needed = FlatDatabase.LoadLevelExp((byte)Client.Hero.Level);
				if ((int)(Client.Experience - Needed) >= 0)
				{
					Client.Experience = 0;
					StatusChange.Level(Client, (byte)(Client.Hero.Level + 1), true);
				}
				Client.Send(new StatTypePacket(Client.Hero.UID, (uint)Client.Experience, StatIDs.Experience).Serialize());
			}
		}
	}
}