﻿using System;

namespace ConquerEmulator.Client
{
	public class Stamps
	{
		public uint LastMessage, LastServiceMessage;
        public DateTime Revive = DateTime.Now,
                    Remove = DateTime.Now,
                    Attack = DateTime.Now,
                    Stamina = DateTime.Now,
                    Pointloss = DateTime.Now,
                    Flashing = DateTime.Now,
                    CycloneFinish = DateTime.Now,
                    SupermanFinish = DateTime.Now,
                    FlyFinish = DateTime.Now,
                    CanSeeXPSpells = DateTime.Now,
                    XPSpells = DateTime.Now,
                    Equip = DateTime.Now,
                    StigmaTimeUp = DateTime.Now,
                    GoldDrop = DateTime.Now,
                    BombTime = DateTime.Now,
                    CanShowCool = DateTime.Now,
                    TransformationTime = DateTime.Now,
                    BroadCast = DateTime.Now,
                    HeavenBless = DateTime.Now,
                    Protection = DateTime.Now,
                    LoginTime = DateTime.Now,
                    OfflineTG = DateTime.Now,
                    XPShield = DateTime.Now,
                    Invisibility = DateTime.Now,
                    MonsterHunter = DateTime.Now,
                    ExpPotion = DateTime.Now,
                    PLevelAssist = DateTime.Now;

        public static bool Check(DateTime Stamp)
		{
			return Stamp <= DateTime.Now;
		}
	}
}