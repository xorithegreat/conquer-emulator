﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ConquerEmulator.Database;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Network.Packets.Structures;

namespace ConquerEmulator.Client
{
	public class ConquerTeam
	{
	    public bool Active;
	    public bool CanJoin = true;
	    public bool IsLeader;
	    public bool ItemPickup = false;
	    public GameClient Leader;
	    public bool MoneyPickup = true;
	    private bool newbExp;

	    public Dictionary<uint, GameClient> TeamDictionary = new Dictionary<uint, GameClient>();

	    public GameClient[] Teammates
		{
			get
			{
				var array = new GameClient[TeamDictionary.Count];
				TeamDictionary.Values.CopyTo(array, 0);
				return array;
			}
		}

	    public void Add(GameClient Teammate)
		{
			TeamDictionary.Add(Teammate.Hero.UID, Teammate);
		}

	    public void CalculateExp(IBaseEntity mob)
		{
			if (NewbieInTeam(mob.Level))
				newbExp = true;

			foreach (var Member in Leader.Team.Teammates)
			{
				if ((Member == null) || Member.Team.IsLeader)
					continue;

				if (Kernel.GetDistance(Leader.Hero, Member.Hero) <= 32)
				{
					if (Member.Hero.MapID.ID != Leader.Hero.MapID.ID)
						continue;
					if (Member.Hero.Level >= 130)
						continue;

					ReceiveTeamExperience(Member, mob);

					var Needed = FlatDatabase.LoadLevelExp((byte)Member.Hero.Level);
					if ((int)(Member.Experience - Needed) >= 0)
					{
						if (Leader.Hero.Level >= 70)
							Leader.Team.AddVirtuePoints(Member);

						StatusChange.Level(Member, (byte)(Member.Hero.Level + 1), true);
					}
				}
			}
		}

	    private void ReceiveTeamExperience(GameClient Client, IBaseEntity mob)
		{
			int exp;
			var leveldiff = Client.Hero.Level - mob.Level;
			if (leveldiff <= -20)
				exp = Client.Hero.Level * 30;
			else
			{
				exp = mob.MaxHitpoints / 20;

				//bonus for being lower level
				if (leveldiff <= -10)
					exp = (int)(exp * 1.3);
				else if (leveldiff <= -5)
					exp = (int)(exp * 1.2);

				//bonus for having a noob in team
				if (newbExp)
					exp *= 2;

				//bonus for spouse killing monster
				if (Leader.Spouse == Client.Spouse)
					exp *= 2;
			}
			//bonus for water wizards  
			if ((Client.Job >= 133) && (Client.Job <= 135))
				exp *= 2;

			Experience.CalculateExp(Client, mob.Level, (uint)exp);
		}

	    public bool NewbieInTeam(ushort MonsterLevel)
		{
			return Leader.Team.Teammates.Any(Member => Member.Hero.Level - MonsterLevel < 20);
		}

	    public void AddVirtuePoints(GameClient Member)
		{
			if ((Leader == null) || (Member == null))
				return;

			var Add = (uint)(Member.Hero.Level * 17 / 13 * 12 / 2 + Member.Hero.Level * 3);
			Leader.VirtuePoints += Add;
			TeamMessage(Member, $"{Leader.Hero.Name} has gained {Add} virtuepoint(s).");
		}

	    public void TeamMessage(GameClient Client, string Message)
		{
			foreach (var Member in Leader.Team.Teammates)
				Member?.Speak(Color.Yellow, ChatType.Team, Message);
		}
	}
}