﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using ConquerEmulator.Database;
using ConquerEmulator.Main;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Client
{
	public enum GuildPosition : byte
	{
		BranchManager = 80,
		DeputyLeader = 90,
		DeputyManager = 70,
		InternManager = 60,
		Leader = 100,
		Member = 50,
		None = 0
	}

	public class ConquerGuild
	{
		public static ushort LastGuildUID;
		public List<string> Allies = new List<string>();
		public List<string> Enemies = new List<string>();
		public uint Fund, WarWins;
		public sbyte HasPrize;
		public sbyte HoldingPole;
	    public sbyte DeputyCount;

		public ConcurrentDictionary<uint, string> Members = new ConcurrentDictionary<uint, string>();
		public string Name, Leader, Bulletin;

		public ushort UID;

		public static void Create(ConquerGuild Guild, string members, string allies, string enemies)
		{
			var Members = members.Split('-');
			foreach (var Member in Members)
			{
				var Content = Member.Split('~');
				Guild.Members.TryAdd(uint.Parse(Content[1]), Member);
			}
			var Allies = allies.Split('-');
			foreach (var Allie in Allies)
				Guild.Allies.Add(Allie);
			var Enemies = enemies.Split('-');
			foreach (var Enemy in Enemies)
				Guild.Enemies.Add(Enemy);
		}

		public static void Create(string name, GameClient Client)
		{
			if (Kernel.FGuild(name) != null)
			{
                Client.Speak(Color.Red, ChatType.Talk, "That guild name is already taken.");
			    return;
			}

			var CGuild = new ConquerGuild
			{
				Name = name,
				Leader = Client.Hero.Name,
				Bulletin = "",
				UID = NextGuildUid(),
				Fund = 1000000,
				WarWins = 0,
				HoldingPole = 0,
				HasPrize = 0,
				Members = new ConcurrentDictionary<uint, string>(),
				Allies = new List<string>(),
				Enemies = new List<string>()
			};

			Client.Hero.GuildID = CGuild.UID;
			Client.GuildDonation = 1000000;
			Client.Hero.GuildRank = GuildPosition.Leader;
			CGuild.Members.TryAdd((uint)Client.Hero.GuildRank, ToString(Client));
			if (!Kernel.Guilds.ContainsKey(Client.Hero.GuildID))
				Kernel.Guilds.TryAdd(Client.Hero.GuildID, CGuild);

			GameClient.SpeakToAll(Color.White, ChatType.Talk, $"{Client.Hero.Name} has set up {CGuild.Name} successfuly!");

			MySqlDatabase.CreateGuild(CGuild);

			Client.Guild = CGuild;
			#region Packet Sending
			Client.Send(Packets.GuildInfo(CGuild, Client));
			var SPacket = new StringPacket((byte)CGuild.Name.Length);
			SPacket.ID = StringIds.GuildName;
			SPacket.UID = CGuild.UID;
			Client.SendScreen(Packets.StringPacket(SPacket, CGuild.Name), true);

			Client.SendScreen(Client.Hero.SpawnPacket, false);
			#endregion
		}

        public static void DisbandGuild(GameClient Client)
        {
            var Guild = Client.Guild;

            var GPacket = GuildPacket.Create();
            GPacket.UID = Client.Hero.GuildID;
            GPacket.ID = GuildIds.Disband;
            Client.Send(GPacket, GPacket.Size);

            foreach (var pClient in Kernel.Clients)
            {
                if (pClient.Guild.UID == Guild.UID)
                {
                    pClient.Guild.MemberQuits(pClient, false);
                    pClient.Send(Packets.GuildInfo(null, pClient));
                    pClient.SendScreen(pClient.Hero.SpawnPacket, false);
                }
            }

            Client.GuildDonation = 0;
            Client.Hero.GuildID = 0;
            Client.Hero.GuildRank = GuildPosition.None;

            Client.Send(Packets.GuildInfo(Client.Guild, Client));
            Client.Guild = null;
            Client.SendScreen(Client.Hero.SpawnPacket, false);

            MySqlDatabase.DeleteGuild(Guild);
            Kernel.Guilds.Remove(Guild.UID);
        }

		public void NewMember(GameClient Client)
		{
			SendGuildMsg(Client, $"{Client.Hero.Name} has joined the guild.");
			Client.GuildDonation = 0;
			Members.TryAdd(Client.Hero.UID, ToString(Client));
			MySqlDatabase.SaveGuild(this);
		}

		public void MemberQuits(GameClient pClient, bool Kicked)
		{
            if (pClient.Hero.GuildRank == GuildPosition.DeputyLeader)
            {
                pClient.Guild.DeputyCount--;
            }
            pClient.Guild = null;
            pClient.Hero.GuildID = 0;
            pClient.GuildDonation = 0;
            pClient.Hero.GuildRank = GuildPosition.None;
            Members.Remove(pClient.Hero.UID);
		}

        public void SendGuildMsg(GameClient Client, string Message)
        {
            foreach (var member in Members.Values)
            {
                var Content = member.Split('~');
                var Member = Kernel.FindClient(uint.Parse(Content[1]));
                if (Member != null && Member.Hero.UID != Client.Hero.UID)
                     Member.Speak(Color.White, ChatType.Guild, Message);
            }
        }

		public void ListMembers(GameClient Client)
		{
			var Return = new string[Members.Count];
			ushort i = 0;
			foreach (var member in Members.Values)
			{
				var Content = member.Split('~');
				var Current = Content[0] + " " + Content[2] + " ";
				var Member = Kernel.FindClient(uint.Parse(Content[1]));
				if (Member != null)
				{
					Current += "1";
					Return[i] = Current;
				}
				else
				{
					Current += "0";
					Return[i] = Current;
				}
				i++;
			}
			var SPacket = new StringPacket((byte)Return.Length);
			SPacket.UID = 11;
			SPacket.ID = StringIds.GuildList;
			Client.Send(Packets.StringPacket(SPacket, Return));
		}

		/// <summary>
		///     Member String Format: Name/UID/Level/KillCount
		/// </summary>
		/// <param name="Client"></param>
		/// <returns></returns>
		public static string ToString(GameClient Client)
		{
			return Client.Hero.Name + "~" + Client.Hero.UID + "~" + Client.Hero.Level + "~" + Client.GuildDonation + "~" + (byte)Client.Hero.GuildRank;
		}

		public static ushort NextGuildUid()
		{
			LastGuildUID++;
			return LastGuildUID;
		}
	}
}