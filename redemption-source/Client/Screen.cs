﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using ConquerEmulator.Events;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.Mob;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;

namespace ConquerEmulator.Client
{
    public class Screen
    {
        private readonly GameClient Client;
        private readonly ConcurrentDictionary<uint, IMapObject> ScreenDictionary;

        public Screen(GameClient _Client)
        {
            Client = _Client;
            ScreenDictionary = new ConcurrentDictionary<uint, IMapObject>();
            Objects = new IMapObject[0];
        }

        public IMapObject[] Objects { get; private set; }

        public bool Add(IMapObject Base)
        {
            if (!ScreenDictionary.ContainsKey(Base.UID))
            {
                ScreenDictionary.TryAdd(Base.UID, Base);
                Objects = new IMapObject[ScreenDictionary.Count];
                ScreenDictionary.Values.CopyTo(Objects, 0);
                return true;
            }
            return false;
        }

        public void Remove(uint ID)
        {
            lock (ScreenDictionary)
            {
                if (ScreenDictionary.Remove(ID))
                {
                    var tmp_Screen = new IMapObject[ScreenDictionary.Count];
                    ScreenDictionary.Values.CopyTo(tmp_Screen, 0);
                    Objects = tmp_Screen;
                }
            }
        }

        public void Cleanup()
        {
            foreach (var Base in Objects)
            {
                var remove = false;
                var Distance = (sbyte)Kernel.GetDistance(Client.Hero.X, Client.Hero.Y, Base.X, Base.Y);
                if (Base.MapObjType == MapObjectType.Monster)
                {
                    var baseEntity = Base as IBaseEntity;
                    if (baseEntity != null)
                        remove = baseEntity.Dead || (Distance >= DataStructures.ViewDistance);
                }
                if (Base.MapObjType == MapObjectType.Item)
                {
                    var item = Base as ConquerItem;
                    if (item != null)
                        remove = (Distance >= DataStructures.ViewDistance) || (Base.MapID.ID != Client.Hero.MapID.ID) || DateTime.Now > item.DroppedTime.AddSeconds(60);
                }
                else if (Base.MapObjType == MapObjectType.Player)
                {
                    if (remove = ((Distance >= DataStructures.ViewDistance) || (Base.MapID.ID != Client.Hero.MapID.ID)))
                    {
                        var pPlayer = Base.Owner as GameClient;
                        pPlayer?.Screen.ScreenDictionary.Remove(Client.Hero.UID);
                    }
                }
                else
                    remove = Distance >= DataStructures.ViewDistance;

                if (remove)
                    Remove(Base.UID);
            }
        }

        public void FullWipe()
        {
            ScreenDictionary.Clear();
            Objects = new IMapObject[0];
        }

        public void Reload(bool Clear, ConquerCallback Callback)
        {
            if (Clear)
                FullWipe();
            else
                Cleanup();

            #region Players
            foreach (var pClient in Kernel.Clients)
            {
                if (pClient.Hero.UID != Client.Hero.UID)
                {
                    if ((pClient.Hero.MapID.ID == Client.Hero.MapID.ID) && (pClient.Hero.MapID.DynamicID == Client.Hero.MapID.DynamicID))
                    {
                        if (Kernel.GetDistance(pClient.Hero.X, pClient.Hero.Y, Client.Hero.X, Client.Hero.Y) <= DataStructures.ViewDistance)
                        {
                            #region Guild Name
                            if (pClient.Guild != null)
                            {
                                var GName = new StringPacket((byte)pClient.Guild.Name.Length);
                                GName.ID = StringIds.GuildName;
                                GName.UID = pClient.Guild.UID;
                                Client.Send(Packets.StringPacket(GName, pClient.Guild.Name));
                            }
                            #endregion
                            #region Pet
                            if (pClient.Pet != null)
                            {
                                if (!pClient.Pet.Dead)
                                    pClient.Pet.SendSpawn(Client);
                            }
                            #endregion
                            pClient.Hero.SendSpawn(Client);
                            if (Callback != null)
                                Callback.Invoke(Client.Hero, pClient.Hero);
                        }
                    }
                }
            }
            #endregion
            #region NPCs
            Dictionary<uint, INpc> Npcs;
            if (Kernel.Npcs.TryGetValue(Client.Hero.MapID, out Npcs))
            {
                foreach (var npc in Npcs.Values)
                {
                    if (npc.MapID.ID == Client.Hero.MapID.ID && npc.MapID.DynamicID == Client.Hero.MapID.DynamicID)
                    {
                        if (Kernel.GetDistance(npc.X, npc.Y, Client.Hero.X, Client.Hero.Y) <= DataStructures.ViewDistance)
                            npc.SendSpawn(Client);
                    }
                }
            }
            #endregion
            #region Monsters
            Dictionary<uint, Monster> Mobs;
            if (Kernel.Monsters.TryGetValue(Client.Hero.MapID, out Mobs))
            {
                foreach (var Mob in Mobs.Values)
                {
                    if (Client.Hero.MapID.DynamicID == Mob.Entity.MapID.DynamicID)
                    {
                        if ((Kernel.GetDistance(Mob.Entity.X, Mob.Entity.Y, Client.Hero.X, Client.Hero.Y) <= DataStructures.ViewDistance) && !Mob.Entity.Dead)
                            Mob.Entity.SendSpawn(Client);
                    }
                }
            }
            foreach (var KVP in Kernel.Revivers)
            {
                var Reviver = KVP.Value;
                if (Reviver.MapID.ID == Client.Hero.MapID.ID && Reviver.MapID.DynamicID == Client.Hero.MapID.DynamicID)
                {
                    if (Kernel.GetDistance(Reviver.X, Reviver.X, Client.Hero.X, Client.Hero.X) <= DataStructures.ViewDistance)
                    {
                        Reviver.SendSpawn(Client);
                        if (Reviver.ValidTarget(Client))
                            break;
                    }
                }
            }
            #endregion
            #region SOB
            if (Client.Hero.MapID.ID == 1038)
            {
                foreach (var GuildMob in GuildWar.GuildMobs.Values)
                {
                    if (GuildMob.MapID.DynamicID == Client.Hero.MapID.DynamicID)
                    {
                        if (Kernel.GetDistance(GuildMob.X, GuildMob.Y, Client.Hero.X, Client.Hero.Y) <= DataStructures.ViewDistance)
                            GuildMob.SendSpawn(Client);
                    }
                }
            }
            if (Client.Hero.MapID.ID == 1039)
            {
                foreach (var TrainingMob in Kernel.TrainingMobs.Values)
                {
                    if (TrainingMob.MapID.DynamicID == Client.Hero.MapID.DynamicID)
                    {
                        if (Kernel.GetDistance(TrainingMob.X, TrainingMob.Y, Client.Hero.X, Client.Hero.Y) <= DataStructures.ViewDistance)
                            TrainingMob.SendSpawn(Client);
                    }
                }
            }
            #endregion
            #region Items
            foreach (var Item in Kernel.GroundItems.Values)
            {
                if (Item.MapID.ID == Client.Hero.MapID.ID && Item.MapID.DynamicID == Client.Hero.MapID.DynamicID)
                {
                    if (Kernel.GetDistance(Item.X, Item.Y, Client.Hero.X, Client.Hero.Y) <= DataStructures.ViewDistance)
                        Item.SendSpawn(Client);
                }
            }
            #endregion
            Objects = new IMapObject[ScreenDictionary.Count];
            ScreenDictionary.Values.CopyTo(Objects, 0);
        }

        public IMapObject FindObject(uint UID)
        {
            IMapObject res;
            ScreenDictionary.TryGetValue(UID, out res);
            return res;
        }
    }
}