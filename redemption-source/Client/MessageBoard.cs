﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConquerEmulator.Client
{
	public enum ActionType
	{
		None = 0,
		Del = 1,
		GetList = 2,
		List = 3,
		GetWords = 4
	}

	public class MessageBoard
	{
	    private const int TitleSize = 44;
	    private const int ListSize = 10;

	    private readonly List<MessageInfo> mMessages = new List<MessageInfo>();

	    public void Add(string aAuthor, string aWords)
		{
			var message = new MessageInfo();
			message.Author = aAuthor;
			message.Words = aWords;
			message.Date = DateTime.Now.ToString("yyyyMMddHHmmss");

			lock (mMessages)
			{
				mMessages.Add(message);
			}
		}

	    public void Delete(MessageInfo aMessage)
		{
			lock (mMessages)
			{
				if (mMessages.Contains(aMessage))
					mMessages.Remove(aMessage);
			}
		}

	    public string[] GetList(ushort aIndex)
		{
			string[] list;

			lock (mMessages)
			{
				if (mMessages.Count == 0)
					return null;

				if (aIndex / 8 * ListSize > mMessages.Count)
					return null;

				var start = mMessages.Count - (aIndex / 8 * ListSize + 1);

				if (start < ListSize)
					list = new string[(start + 1) * 3];
				else
					list = new string[ListSize * 3];

				var end = start - list.Length / 3;

				var x = 0;
				for (var i = start; i > end; i--)
				{
					var message = mMessages[i];
					list[x + 0] = message.Author;
					if (message.Words.Length > TitleSize)
						list[x + 1] = message.Words.Remove(TitleSize, message.Words.Length - TitleSize);
					else
						list[x + 1] = message.Words;
					list[x + 2] = message.Date;
					x += 3;
				}
			}

			return list;
		}

	    public string GetWords(string aAuthor)
		{
			lock (mMessages)
			{
				foreach (var message in mMessages.Where(message => message.Author == aAuthor))
					return message.Words;
			}

			return "";
		}

	    public MessageInfo GetMsgInfoByAuthor(string aAuthor)
		{
			lock (mMessages)
			{
				foreach (var message in mMessages.Where(message => message.Author == aAuthor))
					return message;
			}

			return new MessageInfo();
		}

	    public struct MessageInfo
		{
			public string Author;
			public string Words;
			public string Date;
		}
	}
}