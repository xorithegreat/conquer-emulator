﻿using System.Threading;

namespace ConquerEmulator.MultiThreading
{
	public class ThreadSafeCounter
	{
	    private int _counter;
	    private object _syncRoot;
	    public int CounterMaxValue;
	    public int CounterStartValue;

	    public ThreadSafeCounter(int start, int end)
		{
			_counter = start;
			CounterStartValue = start;
			CounterMaxValue = end;
		}

	    public ThreadSafeCounter()
		{
			CounterStartValue = 0;
			CounterMaxValue = int.MaxValue;
		}

	    public object SyncRoot
		{
			get
			{
				if (_syncRoot == null)
					Interlocked.CompareExchange(ref _syncRoot, new object(), null);
				return _syncRoot;
			}
		}

	    public int Counter
		{
			get
			{
				lock (SyncRoot)
				{
					_counter++;
					if (_counter > CounterMaxValue)
						_counter = CounterStartValue;
					return _counter;
				}
			}
		}
	}
}