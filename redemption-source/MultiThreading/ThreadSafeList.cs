﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;

namespace ConquerEmulator.MultiThreading
{
	public class ThreadSafeList<T> : IList<T>, INotifyCollectionChanged
	{
	    private readonly IList<T> _list;
	    private readonly ReaderWriterLockSlim _listLock = Locks.GetLockInstance(LockRecursionPolicy.NoRecursion);

	    public ThreadSafeList()
		{
			_list = new List<T>();
		}

	    public ThreadSafeList(int capacity)
		{
			_list = new List<T>(capacity);
		}

	    public ThreadSafeList(IEnumerable<T> collection)
		{
			_list = new List<T>(collection);
		}

	    public ICollection<T> Values
		{
			get
			{
				using (new ReadOnlyLock(_listLock))
				{
					return new List<T>(_list);
				}
			}
		}

	    public virtual IEnumerator<T> GetEnumerator()
		{
			return Values.GetEnumerator();
		}

	    IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

	    public virtual void Add(T item)
		{
			using (new WriteLock(_listLock))
			{
				_list.Add(item);
			}

			OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
		}

	    public virtual void Clear()
		{
			using (new WriteLock(_listLock))
			{
				_list.Clear();
			}

			OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
		}

	    public virtual bool Contains(T item)
		{
			using (new ReadOnlyLock(_listLock))
			{
				return _list.Contains(item);
			}
		}

	    public void CopyTo(T[] array, int arrayIndex)
		{
			using (new ReadOnlyLock(_listLock))
			{
				_list.CopyTo(array, arrayIndex);
			}
		}

	    public virtual bool Remove(T item)
		{
			var index = IndexOf(item);
			if (index < 0)
				return false;

			RemoveAt(index);
			return true;
		}

	    public int Count
		{
			get
			{
				using (new ReadOnlyLock(_listLock))
				{
					return _list.Count;
				}
			}
		}

	    public bool IsReadOnly
		{
			get
			{
				using (new ReadOnlyLock(_listLock))
				{
					return _list.IsReadOnly;
				}
			}
		}

	    public int IndexOf(T item)
		{
			using (new ReadOnlyLock(_listLock))
			{
				return _list.IndexOf(item);
			}
		}

	    public void Insert(int index, T item)
		{
			using (new WriteLock(_listLock))
			{
				_list.Insert(index, item);
			}

			OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
		}

	    public void RemoveAt(int index)
		{
			var item = this[index];

			using (new WriteLock(_listLock))
			{
				_list.RemoveAt(index);
			}

			OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
		}

	    public virtual T this[int index]
		{
			get
			{
				using (new ReadOnlyLock(_listLock))
				{
					return _list[index];
				}
			}
			set
			{
				var oldItem = _list[index];

				using (new WriteLock(_listLock))
				{
					_list[index] = value;
				}

				OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, value, oldItem));
			}
		}

	    event NotifyCollectionChangedEventHandler INotifyCollectionChanged.CollectionChanged
		{
			add => CollectionChanged += value;
	        remove => CollectionChanged -= value;
	    }

	    private event NotifyCollectionChangedEventHandler CollectionChanged;

	    protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
		{
			var handler = CollectionChanged;
			handler?.Invoke(this, e);
		}
	}
}