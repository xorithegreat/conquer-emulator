﻿using System.Collections.Concurrent;
using ConquerEmulator.Client;

namespace ConquerEmulator.Npc.Quests
{
    public class DisCity
    {
        public static bool Started;
        public static byte LeftKills, RightKills, Syrens;
        public static sbyte Stage2Count, Stage3Count;
        public static ConcurrentDictionary<uint, GameClient> LeftFlank = new ConcurrentDictionary<uint, GameClient>();
        public static ConcurrentDictionary<uint, GameClient> RightFlank = new ConcurrentDictionary<uint, GameClient>();

        public static void Clear()
        {
            Started = false;
            Syrens = 0;
            LeftKills = 0;
            RightKills = 0;
            Stage2Count = 0;
            Stage3Count = 0;
            LeftFlank.Clear();
            RightFlank.Clear();
        }
    }
}
