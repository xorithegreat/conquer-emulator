﻿using System;
using ConquerEmulator.Client;

namespace ConquerEmulator.Npc.Quests
{
    public class MonsterHunter
    {
        public static bool Verify(GameClient Client)
        {
            if (Client.MonsterHunter)
                return false;
            if (Client.Hero.Stamps.MonsterHunter < DateTime.Now)
            {
                AddToCounter(Client, true);
                return true;
            }
            return Client.MHAttempts < 3;
        }

        public static void AddToCounter(GameClient Client, bool ResetAttempts)
        {
            if (!ResetAttempts)
            {
                Client.MonsterHunter = false;
                Client.MHKillCount = 0;
                if (Client.MHAttempts != 3)
                    Client.MHAttempts++;
                Client.Hero.Stamps.MonsterHunter = DateTime.Now.AddHours(24);
            }
            else
                Client.MHAttempts = 0;
        }
    }
}
