﻿using System;
using System.Drawing;
using System.Linq;
using ConquerEmulator.Client;
using ConquerEmulator.Database;
using ConquerEmulator.Events;
using ConquerEmulator.Item;
using ConquerEmulator.Main;
using ConquerEmulator.Main.Interfaces;
using ConquerEmulator.MultiThreading;
using ConquerEmulator.Network.Packets.Structures;
using ConquerEmulator.Network.PacketStructures;
using ConquerEmulator.Npc.Quests;
using ConquerEmulator.PacketHandling;

namespace ConquerEmulator.Npc
{
	internal unsafe class NpcDialog
	{
	    public static void HandleNpcDialog(GameClient _client, INpc npc, ushort Option, byte[] Packet)
        {
            try
            {
                Client = _client;
                #region Guild Kick Member
                if (Packet[11] == 102)
                {
                    PacketThread.GuildKickPlayer(Client, Packet);
                    return;
                }
                #endregion
                var InputText = ReadString(Packet);
                switch (npc.UID)
                {
                    #region AncientDevil
                    #region Entrance NPC
                    case 300010:
                        {
                            Say("The Ancient Devil was sealed in this Island. The seal's power is very weak now.");
                            Say("The devil will awake soon. Can you help us?");
                            Link("How can I help you?", 1);
                            Finish();
                        }
                        if (Option == 1)
                        {
                            Say("First, get 5 amulets. Each amulet is protected by a Guard of different professions.");
                            Say(" Only if you are of the same profession, can you challenge the Guard. So you had better ask a friend for help.");
                            Say(" After you gather the 5 Amulets, click on the yellow marks on the ground to bring out the devil and its guards.");
                            Say(" Enable PK mode to kill them. Will you help us?");
                            Link("Yes. I shall try.", 2);
                            Link("Let me think it over.", 255);
                            Finish();
                        }
                        if (Option == 2)
                        {
                            Client.Teleport(1052, 191, 232);
                        }
                        break;
                    #endregion
                    #region Trojan Guard
                    case 300012:
                        {
                            if (Option == 0 && Client.Job >= 10 && Client.Job <= 15)
                            {
                                Say("To obtain the Trojan Amulet, you must enable PK mode to kill the Trojan Devil and give me the Trojan Cert. Are you ready?");
                                Link("Yeah.", 1);
                                Link("i have the Trojan Cert", 2);
                                Link("Not yet.", 255);
                                Finish();
                            }
                        }
                        if (Option == 1)
                        {
                            if (Client.Job >= 10 && Client.Job <= 15)
                            {
                                Client.Teleport(1052, 71, 172);
                            }
                            else
                            {
                                Say("Sorry, only a Trojan can challenge this test.");
                                Link("I see.", 255);
                                Finish();
                            }
                        }
                        if (Option == 2)
                        {
                            if (Client.TryExpendItemOfId(710017, 1))
                            {
                                Client.AddInventory(710012);
                            }
                            else
                            {
                                Say("Are you trying to make me look like a fool you dont have the Trojan Cert");
                                Link("No Sorry", 255);
                                Finish();
                            }
                        }
                        break;
                    #endregion
                    #region Fire Guard
                    case 300013:
                        {
                            if (Option == 0 && Client.Job >= 140 && Client.Job <= 145)
                            {
                                Say("To obtain the Fire Amulet, you must enable PK mode to kill the FireDevil and give me the Fire Cert. Are you ready?");
                                Link("Yeah.", 1);
                                Link("I have the Fire Cert", 2);
                                Link("Not yet.", 255);
                                Finish();
                            }
                        }
                        if (Option == 1)
                        {
                            if (Client.Job >= 140 && Client.Job <= 145)
                            {
                                Client.Teleport(1052, 160, 63);
                            }
                            else
                            {
                                Say("Sorry, only a Fire can challenge this test.");
                                Link("I see.", 255);
                                Finish();
                            }
                        }
                        if (Option == 2)
                        {
                            if (Client.TryExpendItemOfId(710018, 1))
                            {
                                Client.AddInventory(710013);
                            }
                            else
                            {
                                Say("Are you trying to make me look like a fool you dont have theFire Cert");
                                Link("No Sorry", 255);
                                Finish();
                            }
                        }
                        break;
                    #endregion
                    #region Water Guard
                    case 300014:
                        {
                            if (Option == 0 && Client.Job >= 130 && Client.Job <= 135)
                            {
                                Say("To obtain the Water Amulet, you must enable PK mode to kill the WaterDevil and give me the Water Cert. Are you ready?");
                                Link("Yeah.", 1);
                                Link("I have the Water Cert", 2);
                                Link("Not yet.", 255);
                                Finish();
                            }
                        }
                        if (Option == 1)
                        {
                            if (Client.Job >= 130 && Client.Job <= 135)
                            {
                                Client.Teleport(1052, 295, 163);
                            }
                            else
                            {
                                Say("Sorry, only a Water can challenge this test.");
                                Link("I see.", 255);
                                Finish();
                            }
                        }
                        if (Option == 2)
                        {
                            if (Client.TryExpendItemOfId(710019, 1))
                            {
                                Client.AddInventory(710014);
                            }
                            else
                            {
                                Say("Are you trying to make me look like a fool you dont have theWater Cert");
                                Link("No Sorry", 255);
                                Finish();
                            }
                        }
                        break;
                    #endregion
                    #region Archer Guard
                    case 300015:
                        {
                            if (Option == 0 && Client.Job >= 40 && Client.Job <= 45)
                            {
                                Say("Since you have passed the test and obtained the Archer Cert., I will give you the Archer Amulet.");
                                Link("Thanks.", 2);
                                Finish();
                            }
                            else
                            {
                                Say("To obtain the Archer Amulet, you must enable PK mode to kill the ArcherDevil and give me the Archer Cert. Are you ready?");
                                Link("Yeah.", 1);
                                Link("Not yet.", 255);
                                Finish();
                            }
                        }
                        if (Option == 1)
                        {
                            if (Client.Job >= 40 && Client.Job <= 45)
                            {
                                Client.Teleport(1052, 316, 313);
                            }
                            else
                            {
                                Say("Sorry, only a Archer can challenge this test.");
                                Link("I see.", 255);
                                Finish();
                            }
                        }
                        if (Option == 2)
                        {
                            if (Client.TryExpendItemOfId(710020, 1))
                            {
                                Client.AddInventory(710015);
                            }
                            else
                            {
                                Say("Are you trying to make me look like a fool you dont have theArcher Cert");
                                Link("No Sorry", 255);
                                Finish();
                            }
                        }
                        break;
                    #endregion
                    #region Warrior Guard
                    case 300011:
                        {
                            if (Option == 0 && Client.Job >= 20 && Client.Job <= 25)
                            {
                                Say("To obtain the Warrior Amulet, you must enable PK mode to kill the WarriorDevil and give me the Warrior Cert. Are you ready?");
                                Link("Yeah.", 1);
                                Link("I have the Warrior cert", 2);
                                Link("Not yet.", 255);
                                Finish();
                            }
                        }
                        if (Option == 1)
                        {
                            if (Client.Job >= 20 && Client.Job <= 25)
                            {
                                Client.Teleport(1052, 160, 291);
                            }
                            else
                            {
                                Say("Sorry, only a Warrior can challenge this test.");
                                Link("I see.", 255);
                                Finish();
                            }
                        }
                        if (Option == 2)
                        {
                            if (Client.TryExpendItemOfId(710016, 1))
                            {
                                Client.AddInventory(710011);
                            }
                            else
                            {
                                Say("Are you trying to make me look like a fool you dont have the Warrior Cert");
                                Link("No Sorry", 255);
                                Finish();
                            }
                        }
                        break;
                    #endregion
                    #endregion
                    #region SpaceMark 
                    case 10031: // [TwinCity]
                        {
                            var water = Client.Job >= 130 && Client.Job <= 135;
                            if (Option == 0)
                            {
                                if (water)
                                {
                                    Say($"Hello { Client.Hero.Name }, I am here to sell you a WindSpell for 100 silver, are you interested?");
                                    Link("Yes, I am.", 1);
                                    Link("No, I am not.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("Sorry, you are not a WaterTaoist, I can not give away our secrets to strangers.");
                                    Link("Aha, got it!", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (water && Client.Silvers >= 100)
                                {
                                    Client.Silvers -= 100;
                                    Client.AddInventory(1060025);
                                }
                                else
                                {
                                    Say("Sorry, you do not have enough silver.");
                                    Link("That is ok", 255);
                                    Finish();
                                }
                            }
                            break;
                        }
                    case 10032: // [TwinCity]
                        {
                            var water = Client.Job >= 130 && Client.Job <= 135;
                            if (Option == 0)
                            {
                                if (water)
                                {
                                    Say($"Hello { Client.Hero.Name }, I am here to sell you a WindSpell for 100 silver, are you interested?");
                                    Link("Yes, I am.", 1);
                                    Link("No, I am not.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("Sorry, you are not a WaterTaoist, I can not give away our secrets to strangers.");
                                    Link("Aha, got it!", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (water && Client.Silvers >= 100)
                                {
                                    Client.Silvers -= 100;
                                    Client.AddInventory(1060026);
                                }
                                else
                                {
                                    Say("Sorry, you do not have enough silver.");
                                    Link("That is ok", 255);
                                    Finish();
                                }
                            }
                            break;
                        }
                    case 10033: // [TwinCity]
                        {
                            var water = Client.Job >= 130 && Client.Job <= 135;
                            if (Option == 0)
                            {
                                if (water)
                                {
                                    Say($"Hello { Client.Hero.Name }, I am here to sell you a WindSpell for 100 silver, are you interested?");
                                    Link("Yes, I am.", 1);
                                    Link("No, I am not.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("Sorry, you are not a WaterTaoist, I can not give away our secrets to strangers.");
                                    Link("Aha, got it!", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (water && Client.Silvers >= 100)
                                {
                                    Client.Silvers -= 100;
                                    Client.AddInventory(1060027);
                                }
                                else
                                {
                                    Say("Sorry, you do not have enough silver.");
                                    Link("That is ok", 255);
                                    Finish();
                                }
                            }
                            break;
                        }
                    case 10040: // [DesertCity]
                        {
                            var water = Client.Job >= 130 && Client.Job <= 135;
                            if (Option == 0)
                            {
                                if (water)
                                {
                                    Say($"Hello { Client.Hero.Name }, I am here to sell you a WindSpell for 100 silver, are you interested?");
                                    Link("Yes, I am.", 1);
                                    Link("No, I am not.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("Sorry, you are not a WaterTaoist, I can not give away our secrets to strangers.");
                                    Link("Aha, got it!", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (water && Client.Silvers >= 100)
                                {
                                    Client.Silvers -= 100;
                                    Client.AddInventory(1060035);
                                }
                                else
                                {
                                    Say("Sorry, you do not have enough silver.");
                                    Link("That is ok", 255);
                                    Finish();
                                }
                            }
                            break;
                        }
                    case 10041: // [DesertCity]
                        {
                            var water = Client.Job >= 130 && Client.Job <= 135;
                            if (Option == 0)
                            {
                                if (water)
                                {
                                    Say($"Hello { Client.Hero.Name }, I am here to sell you a WindSpell for 100 silver, are you interested?");
                                    Link("Yes, I am.", 1);
                                    Link("No, I am not.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("Sorry, you are not a WaterTaoist, I can not give away our secrets to strangers.");
                                    Link("Aha, got it!", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (water && Client.Silvers >= 100)
                                {
                                    Client.Silvers -= 100;
                                    Client.AddInventory(1060034);
                                }
                                else
                                {
                                    Say("Sorry, you do not have enough silver.");
                                    Link("That is ok", 255);
                                    Finish();
                                }
                            }
                            break;
                        }
                    case 10037: // [ApeMountation]
                        {
                            var water = Client.Job >= 130 && Client.Job <= 135;
                            if (Option == 0)
                            {
                                if (water)
                                {
                                    Say($"Hello { Client.Hero.Name }, I am here to sell you a WindSpell for 100 silver, are you interested?");
                                    Link("Yes, I am.", 1);
                                    Link("No, I am not.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("Sorry, you are not a WaterTaoist, I can not give away our secrets to strangers.");
                                    Link("Aha, got it!", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (water && Client.Silvers >= 100)
                                {
                                    Client.Silvers -= 100;
                                    Client.AddInventory(1060031);
                                }
                                else
                                {
                                    Say("Sorry, you do not have enough silver.");
                                    Link("That is ok", 255);
                                    Finish();
                                }
                            }
                            break;
                        }
                    case 10038: // [ApeMountation]
                        {
                            var water = Client.Job >= 130 && Client.Job <= 135;
                            if (Option == 0)
                            {
                                if (water)
                                {
                                    Say($"Hello { Client.Hero.Name }, I am here to sell you a WindSpell for 100 silver, are you interested?");
                                    Link("Yes, I am.", 1);
                                    Link("No, I am not.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("Sorry, you are not a WaterTaoist, I can not give away our secrets to strangers.");
                                    Link("Aha, got it!", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (water && Client.Silvers >= 100)
                                {
                                    Client.Silvers -= 100;
                                    Client.AddInventory(1060032);
                                }
                                else
                                {
                                    Say("Sorry, you do not have enough silver.");
                                    Link("That is ok", 255);
                                    Finish();
                                }
                            }
                            break;
                        }
                    case 10039: // [ApeMountation]
                        {
                            var water = Client.Job >= 130 && Client.Job <= 135;
                            if (Option == 0)
                            {
                                if (water)
                                {
                                    Say($"Hello { Client.Hero.Name }, I am here to sell you a WindSpell for 100 silver, are you interested?");
                                    Link("Yes, I am.", 1);
                                    Link("No, I am not.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("Sorry, you are not a WaterTaoist, I can not give away our secrets to strangers.");
                                    Link("Aha, got it!", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (water && Client.Silvers >= 100)
                                {
                                    Client.Silvers -= 100;
                                    Client.AddInventory(1060033);
                                }
                                else
                                {
                                    Say("Sorry, you do not have enough silver.");
                                    Link("That is ok", 255);
                                    Finish();
                                }
                            }
                            break;
                        }
                    #endregion
                    #region SkyPass (Ape Mountain)
                    #region Daniel
                    case 30055://Daniel
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (Client.InventoryContains(721109))
                                        {
                                            Say("Congratulations! You have gained the prize token of Sky Pass. I will send you to claim the prize.");
                                            Link("Thanks", 1);
                                            Finish(npc);
                                            return;
                                        }
                                        Say("This is the way to Sky Pass. Are you here to challenge the Sky Pass?");
                                        Link("Please show me the way.", 2);
                                        Link("Just passing by.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        if (Client.InventoryContains(721109))
                                        {
                                            Client.Teleport(1040, 192, 250);
                                        }
                                        break;
                                    }
                                case 2:
                                    {
                                        Say("Don't rush into dangers! The Sky Pass was designed by my master, the God");
                                        Say("Cloud. He trapped horrific monsters in the Pass to test out the real hero.");
                                        Link("Can I have a try?", 3);
                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        Say("Sure! Everyone has a change to challenge it. If you pass 5 floors at one time,");
                                        Say("you'll see my great master.");
                                        Link("How does it work?", 4);
                                        Link("I got to go. Goodbye.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 4:
                                    {
                                        Say("The Pass consists of 5 floors, and each floor is guarded by the monsters of");
                                        Say("different level.");
                                        Link("How can I pass?", 5);
                                        Finish(npc);
                                        break;
                                    }
                                case 5:
                                    {
                                        Say("If lucky enough, you'll be directly sent to the next floor without doing anything;");
                                        Say("or you'll need to conquer the battle stage.");
                                        Link("Anything else I should know?", 6);
                                        Finish(npc);
                                        break;
                                    }
                                case 6:
                                    {
                                        Say("The higher level you are, the tougher stage with fiercer monsters you will be");
                                        Say("sent to, with a bigger chance to bypass the battle.");
                                        Link("I would like to try", 7);
                                        Link("I see. Thanks. Bye", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 7:
                                    {
                                        Client.Teleport(1040, 595, 383);
                                        break;
                                    }

                            }
                            break;
                        }
                    #endregion
                    #region StageGuard
                    case 30006:
                    case 30007:
                    case 30008:
                    case 30009:
                    case 30010:
                        if (Option == 0)
                        {
                            if (Client.InventoryContains(721100))
                            {
                                Say("Since you get the token, I will teleport you to the starting point to continue the challenge. Are you ready?");
                                Link("Yes. I am.", 1);
                                Link("Wait a moment.", 255);
                                Finish();
                            }
                            else
                            {
                                Say("Only once you fight in the stage and get a Pass Token, can I send you back to the challenge.");
                                Link("I see.", 255);
                                Finish();
                            }
                        }
                        if (Option == 1)
                        {
                            if (Client.TryExpendItemOfId(721100, 1))
                            {
                                Client.Teleport(1040, 595, 383);
                            }
                        }
                        break;
                    case 30016:
                    case 30017:
                    case 30018:
                    case 30019:
                    case 30020:
                        if (Option == 0)
                        {
                            if (Client.InventoryContains(721101))
                            {
                                Say("With this pass token, you may challenge the sky pass again. Would you like me to teleport you back?");
                                Link("Yes, please.", 1);
                                Link("Wait a moment.", 255);
                                Finish();
                            }
                            else
                            {
                                Say("Only once you fight in the stage and get a Pass Token, can I send you back to the challenge.");
                                Link("I see.", 255);
                                Finish();
                            }
                        }
                        if (Option == 1)
                        {
                            if (Client.TryExpendItemOfId(721101, 1))
                            {
                                Client.Teleport(1040, 544, 331);
                            }
                        }
                        break;
                    case 30026:
                    case 30027:
                    case 30028:
                    case 30029:
                    case 30030:
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721102))
                                {
                                    Say("With this pass token, you may challenge the sky pass again. Would you like me to teleport you back?");
                                    Link("Yes, please.", 1);
                                    Link("Wait a moment.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("Only once you fight in the stage and get a Pass Token, can I send you back to the challenge.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.TryExpendItemOfId(721102, 1))
                                {
                                    Client.Teleport(1040, 492, 281);
                                }
                            }
                            break;
                        }
                    case 30036:
                    case 30037:
                    case 30038:
                    case 30039:
                    case 30040:
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721103))
                                {
                                    Say("With this pass token, you may challenge the sky pass again. Would you like me to teleport you back?");
                                    Link("Yes, please.", 1);
                                    Link("Wait a moment.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("Only once you fight in the stage and get a Pass Token, can I send you back to the challenge.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.TryExpendItemOfId(721103, 1))
                                {
                                    Client.Teleport(1040, 436, 224);
                                }
                            }
                            break;
                        }
                    case 30046:
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721104))
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 6);
                                    if (rand == 1)
                                    {
                                        Say("Congrats! You have passed the sky pass. I am pleased to teleport you to claim the prize.");
                                        Link("Thanks.", 1);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.RemoveInventory(721104);
                                        Client.Teleport(1040, 394, 181);
                                        Say("I am so sorry that you will be sent back to challenge again.");
                                        Link("What bad luck", 255);
                                        Finish();
                                    }
                                }
                            }
                            else
                            {
                                Say("Keep up with the good job! I may send you to claim the prizes if you obtain the Pass Token.");
                                Link("I will try my best.", 255);
                                Finish();
                            }
                            if (Option == 1)
                            {
                                if (Client.TryExpendItemOfId(721104, 1))
                                {
                                    Client.AddInventory(721109);
                                    Client.Teleport(1040, 192, 250);
                                }
                            }
                            break;
                        }
                    case 30047:
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721105))
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 5);
                                    if (rand == 1)
                                    {
                                        Say("Congrats! You have passed the sky pass. I am pleased to teleport you to claim the prize.");
                                        Link("Thanks.", 1);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.RemoveInventory(721104);
                                        Client.Teleport(1040, 394, 181);
                                        Say("I am so sorry that you will be sent back to challenge again.");
                                        Link("What bad luck", 255);
                                        Finish();
                                    }
                                }
                            }
                            else
                            {
                                Say("Keep up with the good job! I may send you to claim the prizes if you obtain the Pass Token.");
                                Link("I will try my best.", 255);
                                Finish();
                            }
                            if (Option == 1)
                            {
                                if (Client.TryExpendItemOfId(721105, 1))
                                {
                                    Client.AddInventory(721109);
                                    Client.Teleport(1040, 192, 250);
                                }
                            }
                            break;
                        }
                    case 30048:
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721106))
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 4);
                                    if (rand == 1)
                                    {
                                        Say("Congrats! You have passed the sky pass. I am pleased to teleport you to claim the prize.");
                                        Link("Thanks.", 1);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.RemoveInventory(721106);
                                        Client.Teleport(1040, 394, 181);
                                        Say("I am so sorry that you will be sent back to challenge again.");
                                        Link("What bad luck", 255);
                                        Finish();
                                    }
                                }
                            }
                            else
                            {
                                Say("Keep up with the good job! I may send you to claim the prizes if you obtain the Pass Token.");
                                Link("I will try my best.", 255);
                                Finish();
                            }
                            if (Option == 1)
                            {
                                if (Client.TryExpendItemOfId(721106, 1))
                                {
                                    Client.AddInventory(721109);
                                    Client.Teleport(1040, 192, 250);
                                }
                            }
                            break;
                        }
                    case 30049:
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721107))
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 3);
                                    if (rand == 1)
                                    {
                                        Say("Congrats! You have passed the sky pass. I am pleased to teleport you to claim the prize.");
                                        Link("Thanks.", 1);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.RemoveInventory(721107);
                                        Client.Teleport(1040, 394, 181);
                                        Say("I am so sorry that you will be sent back to challenge again.");
                                        Link("What bad luck", 255);
                                        Finish();
                                    }
                                }
                            }
                            else
                            {
                                Say("Keep up with the good job! I may send you to claim the prizes if you obtain the Pass Token.");
                                Link("I will try my best.", 255);
                                Finish();
                            }
                            if (Option == 1)
                            {
                                if (Client.TryExpendItemOfId(721107, 1))
                                {
                                    Client.AddInventory(721109);
                                    Client.Teleport(1040, 192, 250);
                                }
                            }
                            break;
                        }
                    case 30050:
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721108))
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 2);
                                    if (rand == 1)
                                    {
                                        Say("Congrats! You have passed the sky pass. I am pleased to teleport you to claim the prize.");
                                        Link("Thanks.", 1);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.RemoveInventory(721108);
                                        Client.Teleport(1040, 394, 181);
                                        Say("I am so sorry that you will be sent back to challenge again.");
                                        Link("What bad luck", 255);
                                        Finish();
                                    }
                                }
                            }
                            else
                            {
                                Say("Keep up with the good job! I may send you to claim the prizes if you obtain the Pass Token.");
                                Link("I will try my best.", 255);
                                Finish();
                            }
                            if (Option == 1)
                            {
                                if (Client.TryExpendItemOfId(721108, 1))
                                {
                                    Client.AddInventory(721109);
                                    Client.Teleport(1040, 192, 250);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region GlumMonster [First Floor]
                    case 30001: // Under Level 91
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 91)
                                {
                                    Say("This is 1st pass. I may teleport you to the next pass directly, or you will be teleported to the battle stage. Are you ready?");
                                    Link("Yeah.", 1);
                                    Link("Let me think it over.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("This floor is for those who are under level 90.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level < 91)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 7);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 544, 331);
                                        Say("Congratulations! You have passed the first test. I am pleased to teleport you to the next pass.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 469, 488);
                                    }
                                }
                            }
                            break;
                        }
                    case 30002: // Between 80-100
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 80)
                                {
                                    Say("Sorry, you cannot challenge this floor if you are not between level 80 and 100.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                                else 
                                {
                                    if (Client.Hero.Level < 101)
                                    {
                                        Say("This is 1st pass. I may teleport you to the next pass directly, or you will be teleported to the battle stage. Are you ready?");
                                        Link("Yeah.", 1);
                                        Link("Let me think it over.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Say("Sorry, you cannot challenge this floor if you are not between level 80 and 100.");
                                        Link("I see.", 255);
                                        Finish();
                                    }
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level < 101)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 6);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 544, 331);
                                        Say("Congratulations! You have passed the first test. I am pleased to teleport you to the next pass.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 445, 512);
                                    }
                                }
                            }
                            break;
                        }
                    case 30003: // Between 90-110
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 90)
                                {
                                    Say("Sorry, you cannot challenge this floor if you are not between level 90 and 110.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                                else
                                {
                                    if (Client.Hero.Level < 111)
                                    {
                                        Say("This is the first floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                        Link("Yeah.", 1);
                                        Link("Let me think it over.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Say("If you want to challenge this pass, you must be between level 90 and 110.");
                                        Link("I see.", 255);
                                        Finish();
                                    }
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level >= 90 & Client.Hero.Level < 111)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 100);
                                    if (rand < 20)
                                    {
                                        Client.Teleport(1040, 544, 331);
                                        Say("Congratulations! You have passed the first test. I am pleased to teleport you to the next pass.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 421, 536);
                                    }
                                }
                            }
                            break;
                        }
                    case 30004: // Between 100-130
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 100)
                                {
                                    Say("If you want to challenge this pass, you must be above level 100.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("This is the first floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                    Link("Yeah.", 1);
                                    Link("Let me think it over.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level >= 100)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 100);
                                    if (rand < 25)
                                    {
                                        Client.Teleport(1040, 544, 331);
                                        Say("Congratulations! You have passed the first test. I am pleased to teleport you to the next pass.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 397, 560);
                                    }
                                }
                            }
                            break;
                        }
                    case 30005: // Between 110-130
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 110)
                                {
                                    Say("If you want to challenge this pass, you must be above level 110.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("This is the first floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                    Link("Yeah.", 1);
                                    Link("Let me think it over.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level >= 110)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 3);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 544, 331);
                                        Say("Congratulations! You have passed the first test. I am pleased to teleport you to the next pass.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 373, 584);
                                    }
                                }
                            }
                            break;
                        }
                    #endregion
                    #region GreenSnake [Second Floor]
                    case 30011: // Under Level 91
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 91)
                                {
                                    Say("This is the second floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                    Link("Yes. I am.", 1);
                                    Link("Wait a moment.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("This floor is for those who are under level 90.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level < 91)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 7);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 492, 281);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 421, 440);
                                    }
                                }
                            }
                            break;
                        }
                    case 30012: // Between 80 and 100
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 80)
                                {
                                    Say("If you want to challenge this floor, you must be between level 80 and 100.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    if (Client.Hero.Level < 101)
                                    {
                                        Say("This is the second floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                        Link("Yes. I am.", 1);
                                        Link("Wait a moment.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Say("If you want to challenge this floor, you must be between level 80 and 100.");
                                        Link("I see.", 255);
                                        Finish();
                                    }
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level >= 80 && Client.Hero.Level < 101)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 6);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 492, 281);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 397, 464);
                                    }
                                }
                            }
                            break;
                        }
                    case 30013: // Between 90 and 110
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 90)
                                {
                                    Say("If you want to challenge this floor, you must be between level 90 and 110.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    if (Client.Hero.Level < 111)
                                    {
                                        Say("This is the second floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                        Link("Yes. I am.", 1);
                                        Link("Wait a moment.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Say("This floor is for those who are between level 90 and 110.");
                                        Link("I see.", 255);
                                        Finish();
                                    }
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level >= 90 && Client.Hero.Level < 111)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 5);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 492, 281);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 373, 488);
                                    }
                                }
                            }
                            break;
                        }
                    case 30014: // 100-130
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 100)
                                {
                                    Say("Please come after your level is above 100.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("This is the second floor. I may teleport you to the next pass directly, or to the battle stage. Are you ready?");
                                    Link("Yes, I am", 1);
                                    Link("Wait a moment.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level > 100)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 4);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 492, 281);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 349, 512);
                                    }
                                }
                            }
                            break;
                        }
                    case 30015: // 110-130
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 110)
                                {
                                    Say("This floor is for those who are above level 110.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("This is the second floor. I may teleport you to the next pass directly, or to the battle stage. Are you ready?");
                                    Link("Yes, I am", 1);
                                    Link("Wait a moment.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level > 110)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 3);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 492, 281);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 325, 536);
                                    }
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Evilhawk [Third Floor]
                    case 30022: // Above L110
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level > 110)
                                {
                                    Say("This is the third floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                    Link("Yes. I am.", 1);
                                    Link("Wait a moment.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("This floor is for those who are above L110.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level > 110)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 3);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 436, 224);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 170, 158);
                                    }
                                }
                            }
                            break;
                        }
                    case 30024: // Between 80 and 100
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 80)
                                {
                                    Say("If you want to challenge this floor, you must be between level 80 and 100.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    if (Client.Hero.Level < 101)
                                    {
                                        Say("This is the third floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                        Link("Yes. I am.", 1);
                                        Link("Wait a moment.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Say("If you want to challenge this floor, you must be between level 80 and 100.");
                                        Link("I see.", 255);
                                        Finish();
                                    }
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level >= 80 && Client.Hero.Level < 101)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 6);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 436, 224);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 349, 416);
                                    }
                                }
                            }
                            break;
                        }
                    case 30025: // Under Level 91
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 91)
                                {
                                    Say("This is the third floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                    Link("Yes. I am.", 1);
                                    Link("Wait a moment.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("This floor is for those who are under level 90.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level < 91)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 7);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 436, 224);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 373, 224);
                                    }
                                }
                            }
                            break;
                        }
                    case 30023: // Between 90 and 110
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 90)
                                {
                                    Say("If you want to challenge this floor, you must be between level 90 and 110.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    if (Client.Hero.Level < 111)
                                    {
                                        Say("This is the third floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                        Link("Yes. I am.", 1);
                                        Link("Wait a moment.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Say("This floor is for those who are between level 90 and 110.");
                                        Link("I see.", 255);
                                        Finish();
                                    }
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level >= 90 && Client.Hero.Level < 111)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 100);
                                    if (rand < 20)
                                    {
                                        Client.Teleport(1040, 436, 224);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 335, 440);
                                    }
                                }
                            }
                            break;
                        }
                    case 30021: // 100-130
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 100)
                                {
                                    Say("Please come after your level is above 100.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("This is the Third floor. I may teleport you to the next pass directly, or to the battle stage. Are you ready?");
                                    Link("Yes, I am", 1);
                                    Link("Wait a moment.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level > 100)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 100);
                                    if (rand < 25)
                                    {
                                        Client.Teleport(1040, 436, 224);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 301, 464);
                                    }
                                }
                            }
                            break;
                        }
                    #endregion
                    #region MonsterGeneral [Fourth Floor]
                    case 30031: // Above L110
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level > 110)
                                {
                                    Say("This is the fourth floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                    Link("Yes. I am.", 1);
                                    Link("Wait a moment.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("This floor is for those who are above L110.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level > 110)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 3);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 394, 181);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 229, 440);
                                    }
                                }
                            }
                            break;
                        }
                    case 30032: // Between 80 and 100
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 80)
                                {
                                    Say("If you want to challenge this floor, you must be between level 80 and 100.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    if (Client.Hero.Level < 101)
                                    {
                                        Say("This is the fourth floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                        Link("Yes. I am.", 1);
                                        Link("Wait a moment.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Say("If you want to challenge this floor, you must be between level 80 and 100.");
                                        Link("I see.", 255);
                                        Finish();
                                    }
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level >= 80 && Client.Hero.Level < 101)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 6);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 394, 181);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 300, 367);
                                    }
                                }
                            }
                            break;
                        }
                    case 30033: // Under Level 91
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 91)
                                {
                                    Say("This is the fourth floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                    Link("Yes. I am.", 1);
                                    Link("Wait a moment.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("This floor is for those who are under level 90.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level < 91)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 7);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 394, 181);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 324, 343);
                                    }
                                }
                            }
                            break;
                        }
                    case 30034: // Between 90 and 110
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 90)
                                {
                                    Say("If you want to challenge this floor, you must be between level 90 and 110.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    if (Client.Hero.Level < 111)
                                    {
                                        Say("This is the fourth floor. I may teleport you to the next floor directly, or to the battle stage. Are you ready?");
                                        Link("Yes. I am.", 1);
                                        Link("Wait a moment.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Say("This floor is for those who are between level 90 and 110.");
                                        Link("I see.", 255);
                                        Finish();
                                    }
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level >= 90 && Client.Hero.Level < 111)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 100);
                                    if (rand < 20)
                                    {
                                        Client.Teleport(1040, 394, 181);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 276, 391);
                                    }
                                }
                            }
                            break;
                        }
                    case 30035: // 100-130
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 100)
                                {
                                    Say("Please come after your level is above 100.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("This is the fourth floor. I may teleport you to the next pass directly, or to the battle stage. Are you ready?");
                                    Link("Yes, I am", 1);
                                    Link("Wait a moment.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.Hero.Level > 100)
                                {
                                    var rand = RandomGenerator.Generator.Next(1, 4);
                                    if (rand == 1)
                                    {
                                        Client.Teleport(1040, 394, 181);
                                        Say("Congratulations! You passed! I will send you to the next floor.");
                                        Link("Thanks.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Client.Teleport(1040, 252, 415);
                                    }
                                }
                            }
                            break;
                        }
                    #endregion
                    #region TerminalGuard [Last Floor]
                    case 30043: // Above L110
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level > 110)
                                {
                                    Say("Welcome! This is the last floor. You are lucky to make it so far. It is tougher ahead. Do you want a try?");
                                    Link("Sure!", 1);
                                    Finish();
                                }
                                else
                                {
                                    Say("This floor is for those who are above L110.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                Say("But you have to fight in the stage for a Pass Token. The guard may send you to claim the prize or send you back to me.");
                                Link("Ok, let`s get started.", 2);
                                Link("Wait a moment.", 255);
                                Finish();
                            }
                            if (Option == 2)
                            {
                                if (Client.Hero.Level > 110)
                                    Client.Teleport(1040, 180, 391);
                            }
                            break;
                        }
                    case 30044: // Above L100
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level > 100)
                                {
                                    Say("Welcome! This is the last floor. You are lucky to make it so far. It is tougher ahead. Do you want a try?");
                                    Link("Sure!", 1);
                                    Finish();
                                }
                                else
                                {
                                    Say("This floor is for those who are above L100.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                Say("But you have to fight in the stage for a Pass Token. The guard may send you to claim the prize or send you back to me.");
                                Link("Ok, let`s get started.", 2);
                                Link("Wait a moment.", 255);
                                Finish();
                            }
                            if (Option == 2)
                            {
                                if (Client.Hero.Level > 100)
                                    Client.Teleport(1040, 204, 367);
                            }
                            break;
                        }
                    case 30045: // Between 90 and 110
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 90)
                                {
                                    Say("If you want to challenge this floor, you must be between level 90 and 110.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    if (Client.Hero.Level > 90 && Client.Hero.Level < 111)
                                    {
                                        Say("You have a good luck. This is the last pass. You must prove your ability before you get the big prize. Are you ready?");
                                        Link("Sure.", 1);
                                        Finish();
                                    }
                                    else
                                    {
                                        Say("This floor is for those who are between level 90 and 110.");
                                        Link("I see.", 255);
                                        Finish();
                                    }
                                }
                            }
                            if (Option == 1)
                            {
                                Say("But you have to fight in the stage for a Pass Token. The guard may send you to claim the prize or send you back to me.");
                                Link("Go ahead", 2);
                                Link("Wait a moment.", 255);
                                Finish();
                            }
                            if (Option == 2)
                            {
                                if (Client.Hero.Level >= 90 & Client.Hero.Level < 111)
                                {
                                    Client.Teleport(1040, 228, 343);
                                }
                            }
                            break;
                        }
                    case 30051: // Between 80 and 100
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 80)
                                {
                                    Say("If you want to challenge this floor, you must be between level 80 and 100.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    if (Client.Hero.Level < 101)
                                    {
                                        Say("You have a good luck. This is the last pass. You must prove your ability before you get the big prize. Are you ready?");
                                        Link("Sure.", 1);
                                        Finish();
                                    }
                                    else
                                    {
                                        Say("If you want to challenge this floor, you must be between level 80 and 100.");
                                        Link("I see.", 255);
                                        Finish();
                                    }
                                }
                            }
                            if (Option == 1)
                            {
                                Say("But you have to fight in the stage for a Pass Token. The guard may send you to claim the prize or send you back to me.");
                                Link("Go ahead", 2);
                                Link("Wait a moment.", 255);
                                Finish();
                            }
                            if (Option == 2)
                            {
                                if (Client.Hero.Level >= 80 && Client.Hero.Level < 101)
                                {
                                    Client.Teleport(1040, 252, 319);
                                }
                            }
                            break;
                        }
                    case 30052: // Under Level 91
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level < 91)
                                {
                                    Say("You have a good luck. This is the last pass. You must prove your ability before you get the big prize. Are you ready?");
                                    Link("Sure.", 1);
                                    Finish();
                                }
                                else
                                {
                                    Say("This floor is for those who are under level 90.");
                                    Link("I see.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                Say("But you have to fight in the stage for a Pass Token. The guard may send you to claim the prize or send you back to me.");
                                Link("Go ahead", 2);
                                Link("Wait a moment.", 255);
                                Finish();
                            }
                            if (Option == 2)
                            {
                                if (Client.Hero.Level < 91)
                                {
                                    Client.Teleport(1040, 277, 296);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region GodCloud
                    case 30054:
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721109))
                                {
                                    Say("Well done! You have passed the 5 tough floors in one time. You have showed your great competence and perseverance.");
                                    Link("Thanks.", 1);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.InventoryContains(721109))
                                {
                                    Say("Congratulations on passing my quest. You may now choose a reward. TreasureBox 1 will guarantee 5 meteors. TreasureBox 2 will give you 1, 4, 10 or 15 meteors. Please choose carefully!");
                                    Link("Treasure Box 1", 2);
                                    Link("Treasure Box 2", 3);
                                    Finish();
                                }
                            }
                            if (Option == 2)
                            {
                                if (Client.Inventory.Length <= 36)
                                {
                                    if (Client.TryExpendItemOfId(721109, 1))
                                    {
                                        Client.AddItems(1088001, 5);
                                        Client.Teleport(1002, 430, 380);
                                        Client.Speak(Color.Turquoise, ChatType.Talk, "Congratulations on completing the SkyPass quest. Your prize is in your pack!");
                                    }
                                }
                                else
                                {
                                    Say("Please make sure you have at least 4 inventory slots free.");
                                    Link("Okay.", 255);
                                    Finish();
                                }
                            }
                            if (Option == 3)
                            {
                                if (Client.Inventory.Length <= 25)
                                {
                                    if (Client.TryExpendItemOfId(721109, 1))
                                    {
                                        var rand = RandomGenerator.Generator.Next(1, 4);
                                        if (rand == 1)
                                        {
                                            Client.AddItems(1088001, 1);
                                        }
                                        if (rand == 2)
                                        {
                                            Client.AddItems(1088001, 4);
                                        }
                                        if (rand == 3)
                                        {
                                            Client.AddItems(1088001, 10);
                                        }
                                        if (rand == 4)
                                        {
                                            Client.AddItems(1088001, 15);
                                        }
                                        Client.Teleport(1002, 430, 380);
                                        Client.Speak(Color.Turquoise, ChatType.Talk, "Congratulations on completing the SkyPass quest. Your prize is in your pack!");
                                    }
                                }
                                else
                                {
                                    Say("Please make sure you have at least 15 inventory slots free.");
                                    Link("Okay.", 255);
                                    Finish();
                                }
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region House
                    #region Carpenter
                    case 30159:
                        if (Option == 0)
                        {
                            Say("For those wanting to build a house, I can exchange 10 Timber for a RosewoodVoucher, 10 RosewoodVouchers for ");
                            Say("a TimberVoucher, and 5 TimberVouchers for a HousePermit. How can I help you?");
                            Link("Rosewood Voucher.", 1);
                            Link("Timber Voucher.", 2);
                            Link("House Permit.", 3);
                            Link("No thanks.", 255);
                            Finish();
                        }
                        if (Option == 1)
                        {
                            if (Client.TryExpendItemOfId(721171, 10))
                            {
                                Client.AddInventory(7211721);
                                Say("There you go!");
                                Link("Thanks!", 255);
                                Finish();
                                return;
                            }
                            Say("You need 10 Timber to claim a RosewoodVoucher.");
                            Link("Sorry.", 255);
                            Finish();
                        }
                        if (Option == 2)
                        {
                            if (Client.TryExpendItemOfId(721172, 10))
                            {
                                Client.AddInventory(721173);
                                Say("There you go!");
                                Link("Thanks!", 255);
                                Finish();
                                return;
                            }
                            Say("You need 10 RosewoodVouchers to claim a TimberVoucher.");
                            Link("Sorry.", 255);
                            Finish();
                        }
                        if (Option == 3)
                        {
                            if (Client.TryExpendItemOfId(721173, 5))
                            {
                                Client.AddInventory(721170);
                                Say("There you go!");
                                Link("Thanks!", 255);
                                Finish();
                                return;
                            }
                            Say("You need 5 TimberVouchers to claim a HousePermit.");
                            Link("Sorry.", 255);
                            Finish();
                        }
                        break;
                    #endregion
                    #region Craftsman
                    case 30158:
                        {
                            if (Option == 0)
                            {
                                Say("Hey there, I can give you Timber for every 5 ores that you have. It can be any ore! How about it?");
                                Link("Take my ores!", 1);
                                Link("Not today.", 255);
                                Finish();
                            }
                            if (Option == 1)
                            {
                                uint[] oreTypes =
                                {
                                    1072031, //EuxeniteOre
                                    1072010, 1072011, 1072012, 1072013, 1072014, 1072015, 1072016, 1072017, 1072018, 1072019, //IronOre
                                    1072020, 1072021, 1072022, 1072023, 1072024, 1072025, 1072026, 1072027, 1072028, 1072029, //CopperOre 
                                    1072040, 1072041, 1072042, 1072043, 1072044, 1072045, 1072046, 1072047, 1072048, 1072049, //SilverOre
                                    1072050, 1072051, 1072052, 1072053, 1072054, 1072055, 1072056, 1072057, 1072058, 1072059 // GoldOre
                                }; 

                                if (Client.HasItemType(oreTypes, 5))
                                {
                                    Client.AddInventory(721171);
                                    Say("Here's your timber! Let me know when you would like to do business again.");
                                    Link("Thanks!", 255);
                                    Finish();
                                    return;
                                }
                                Say("Sorry, you don't have 5 ores.");
                                Link("Oh sorry.", 255);
                                Finish();
                            }
                            break;
                        }
                    #endregion
                    #region HouseAgent
                    case 30157:
                        {
                            if (Option == 0)
                            {
                                if (Client.HouseLevel != 0)
                                {
                                    Say("So I hope you're enjoying that new house!");
                                    Link("Sure am.", 255);
                                    Finish();
                                    return;
                                }
                                Say("If you bring me a HousePermit, I can sell you a house!");
                                Link("Here's a HousePermit!", 1);
                                Link("How do I get a HousePermit?", 2);
                                Finish();
                            }
                            if (Option == 1)
                            {
                                if (!Client.InventoryContains(721170))
                                {
                                    Say("Sorry, you don't have a HousePermit. I can't sell you a house.");
                                    Link("Oh okay.", 255);
                                    Finish();
                                }
                                Client.RemoveInventory(721170);
                                Client.HouseLevel = 1;
                                Say("Congraulations! You now have a house! You can talk to the HouseAdmin in the Market to visit whenever you want.");
                                Link("Thanks!", 255);
                                Finish();
                            }
                            break;
                        }
                    #endregion
                    #region HouseAdmin
                    case 30156:
                        {
                            if (Option == 0)
                            {
                                Say("Hello, I am the house admin. What would you like to do?");
                                Link("Buy a House.", 1);
                                Link("Go Home.", 2);
                                Link("Visit my spouses house.", 3);
                                Link("Nothing Thanks!.", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                if ((Client.CountInventory(721170) >= 1) && (Client.HouseLevel == 0))
                                {
                                    Client.RemoveInventory(721170);
                                    Client.HouseLevel = 1;
                                    Say($"Congratulations you have built a level {Client.HouseLevel} House!");
                                    Link("Awesome Thanks!", 255);
                                    Finish(npc);
                                }
                                else if ((Client.CountInventory(721174) >= 1) && (Client.HouseLevel == 1))
                                {
                                    Client.RemoveInventory(721174);
                                    Client.HouseLevel = 2;
                                    Say($"Congratulations you have built a level {Client.HouseLevel} House!");
                                    Link("Awesome Thanks!", 255);
                                    Finish(npc);
                                }
                                else if (Client.HouseLevel >= 2)
                                {
                                    Say("Your house has already been upgraded.");
                                    Link("I'm so rich...", 255);
                                    Finish(npc);
                                }
                                else
                                {
                                    Say("You do not have the required permits to build here!");
                                    Link("Darn", 255);
                                    Finish(npc);
                                }
                            }
                            else if (Option == 2)
                            {
                                if (Client.HouseLevel == 1)
                                {
                                    Client.Teleport(1098, 33, 41, Client.Hero.UID);
                                }
                                else if (Client.HouseLevel == 2)
                                {
                                    Client.Teleport(1099, 53, 83, Client.Hero.UID);
                                }
                                else
                                {
                                    Say("Get away you bum! You dont own a house!");
                                    Link("Could you spare some change?", 255);
                                    Finish(npc);
                                }
                            }
                            else if (Option == 3)
                            {
                                if (Client.SpouseHouseLevel == 1)
                                {
                                    Client.Teleport(1098, 33, 41, Client.SpouseUID);
                                }
                                else if (Client.SpouseHouseLevel == 2)
                                {
                                    Client.Teleport(1099, 53, 83, Client.SpouseUID);
                                }
                                else
                                {
                                    Say("Your spouse does not own a house!");
                                    Link("What a chump..", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region BI Socketer (Artisan Ou)
                    case 41:
                        {
                            if (Option == 0)
                            {
                                Say("A weapon without socket is just a weapon. But if you add sockets to your weapon, you can put gems in it. Want to socket a weapon?");
                                Link("Alright. I'm in.", 2);
                                Link("Just passing by", 255);
                                Finish(npc);
                            }
                            if (Option == 2)
                            {
                                var rightHand = Client.Equipment.Values.FirstOrDefault(x => x.Position == ItemPosition.Right);
                                if (rightHand != null)
                                {
                                    if (rightHand.SocketOne == GemsConst.NoSocket)
                                    {
                                        Say("You will need 1 DragonBall. Are you ready?");
                                        Link("Ready as ever.", 1);
                                        Link("No. Not yet.", 255);
                                        Finish(npc);
                                    }
                                    else if (rightHand.SocketTwo == GemsConst.NoSocket)
                                    {
                                        Say("You will need 5 DragonBalls. Are you ready?");
                                        Link("Ready as ever.", 5);
                                        Link("No. Not yet.", 255);
                                        Finish(npc);
                                    }
                                    else
                                    {
                                        Say("Your weapon already has two sockets.");
                                        Link("Oops.", 255);
                                        Finish(npc);
                                    }
                                }
                                else
                                {
                                    Say("You don't have a weapon equipped.");
                                    Link("Oops.", 255);
                                    Finish(npc);
                                }
                            }
                            if (Option == 1 || Option == 5)
                            {
                                var rightHand = Client.Equipment.Values.FirstOrDefault(x => x.Position == ItemPosition.Right);
                                if (rightHand != null)
                                {
                                    if (rightHand.ID < 410003 || rightHand.ID > 601339)
                                    {
                                        Say("That item does not appear to be a valid weapon. If you think this is an error, please report it on the forums.");
                                        Link("Sorry...", 255);
                                        Finish(npc);
                                    }
                                    else
                                    {
                                        if (Client.TryExpendItemOfId(1088000, Option))
                                        {
                                            FlatDatabase.UnloadItemStats(Client, rightHand);
                                            if (rightHand.SocketOne == GemsConst.NoSocket)
                                                rightHand.SocketOne = GemsConst.EmptySocket;
                                            else
                                                rightHand.SocketTwo = GemsConst.EmptySocket;
                                            FlatDatabase.LoadItemStats(Client, rightHand);
                                            rightHand.Send(Client);

                                            Say("Your Right Hand has succesfully been socketed.");
                                            Link("Cool. Thanks.", 255);
                                            Finish(npc);
                                        }
                                    }
                                }
                                else
                                {
                                    Say("You don't have a weapon equipped.");
                                    Link("Oops.", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region MeteorTear Quest
                    #region Milly
                    case 600000: //Milly
                        {
                            if (Option == 0)//721000
                            {
                                Say("Hello stranger. I haven't seen my sister for awhile and I got a letter for her would you like to deliver it for me please?");
                                Link("Yes, bet on me.", 1);
                                Link("Sorry, I'm busy.", 255);
                                Finish();
                            }
                            else if (Option == 1)
                            {
                                if (!Client.InventoryContains(721000) && !Client.InventoryContains(721001))
                                {
                                    if (Client.Inventory.Length < 40)
                                    {
                                        Client.AddInventory(721000);
                                        Say("Here! Take this letter and meet my sister Minner in Bird Island.");
                                        Link("Ok.", 255);
                                        Finish();
                                    }
                                    else
                                    {
                                        Say("I'm sorry but your inventory is full..");
                                        Link("Ah, I see.", 255);
                                        Finish();
                                    }
                                }
                                else
                                {
                                    Say("I already gave you the letter. Go see my sister in Bird Island.");
                                    Link("Ok ok.", 255);
                                    Finish();
                                }

                            }
                            break;
                        }
                    #endregion
                    #region Minner
                    case 600001: //Minner
                        {
                            if (Option == 0)//721000
                            {
                                if (!Client.InventoryContains(721000) && !Client.InventoryContains(721001) && !Client.InventoryContains(721002))
                                {
                                    Say("I'm so sad. I haven't seen my sister for a while and didn't get any notices from her. She should be in Phoenix Castle. Tell me if you see her.");
                                    Link("Sure I will.", 255);
                                    Finish();
                                }
                                else if (Client.InventoryContains(721000) && !Client.InventoryContains(721001) && !Client.InventoryContains(721002))
                                {
                                    Say("I'm so sad. I haven't seen my sister for a while and didn't get any notices from her.");
                                    Link("I got a letter from Milly.", 1);
                                    Link("Sorry, I'm busy.", 255);
                                    Finish();
                                }
                                else if (!Client.InventoryContains(721000) && Client.InventoryContains(721001) && !Client.InventoryContains(721002))
                                {
                                    Say("Go meet Joe in Desert city near Mystic Castle entrace.");
                                    Link("Yeah right.", 255);
                                    Finish();
                                }
                                else if (Client.InventoryContains(721002))
                                {
                                    Client.RemoveInventory(721002);
                                    Client.AddInventory(1088002);
                                    Say("Oh here you are! Thanks for delivering that to Joe. Take this Meteor Tear as reward for your courage!");
                                    Link("Thanks.", 255);
                                    Finish();
                                }
                            }
                            else if (Option == 1)
                            {
                                Say("Oh! Thank you but I miss my darling Joe. I'm sure he's in the Desert. I would appreciate if you gave him this Guardian Star. It means a lot for me.");
                                Link("Ok bet on me.", 2);
                                Link("Sorry, I'm busy.", 255);
                                Finish();
                            }
                            else if (Option == 2)
                            {
                                if (Client.InventoryContains(721000))
                                {
                                    Client.RemoveInventory(721000);
                                    Client.AddInventory(721001);
                                    Say("Thank you so much! Take this Guardian Star and meet Joe in the Desert near the Mystic Castle entrance.");
                                    Link("I'm on my way.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("Don't trick me you have no letter!");
                                    Link("I'm on my way.", 255);
                                    Finish();
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Joe
                    case 600002: //Joe
                        {
                            if (Option == 0)
                            {
                                if (!Client.InventoryContains(721001))
                                {
                                    Say("This desert is killing me it's so hot!");
                                    Link("Yes it is.", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say("I see you got the Guardian Star but I also want a meteor and an amrita...the sun is killing me!");
                                    Link("Ok here they are.", 1);
                                    Link("I don't have them.", 255);
                                    Finish();
                                }
                            }
                            else if (Option == 1)
                            {
                                if (Client.InventoryContains(1000030) && Client.InventoryContains(1088001) && Client.InventoryContains(721001))
                                {
                                    Client.RemoveInventory(1000030);
                                    Client.RemoveInventory(1088001);
                                    Client.RemoveInventory(721001);
                                    Client.AddInventory(721002);
                                    Say("Wow! I feel refreshed now! Here take this Sad Meteor and give it to Minner.");
                                    Link("Ok!", 255);
                                    Finish();
                                }
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region Dis City
                    #region SolarSaint
                    case 3215:
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(723088))
                                {
                                    if (Client.Hero.Level >= 110)
                                    {
                                        Say("I see you got the StarSword. Give it to me and I'll give you the reward!");
                                        Link("Here it is.", 11);
                                        Finish();
                                    }
                                    else
                                    {
                                        Say("You can't get the reward if your level is below 110!");
                                        Link("Oh, I see.", 255);
                                        Finish();
                                    }
                                }
                                else
                                {
                                    Say("Our ancestor exerted their utmost efforts and defeated the demons. Since");
                                    Say("then the world has been kept in peace forhundreds of years. Now the");
                                    Say("demons have come back and the world is getting into turbulence again.");
                                    Link("Could you tell me more?", 3);
                                    Link("I am here to drive away the evils", 1);
                                    Link("I'd better leave", 255);
                                    Finish();
                                }
                            }
                            else if (Option == 1)
                            {
                                if (!DisCity.Started)
                                {
                                    Say("I'm glad to see that you are such a knightly person. Please come next time.");
                                    Link("Ok.", 2);
                                    Finish();
                                }
                                else
                                {
                                    if (Client.Hero.Level >= 110)
                                    {
                                        Client.Teleport(4021, 291, 475);
                                    }
                                    else
                                    {
                                        Say("Sorry, you are too weak to fight these monsters, go get level 110 and talk to me later");
                                        Link("Okay, I will!", 255);
                                        Finish();
                                    }
                                }
                            }
                            else if (Option == 2)
                            {
                                Say("Find SolarSaint to take the quest from 17:00 to 17:05 every Mon and Wed, from 20:00 to 20:05 every Tues and Thur");
                                Link("Okay, I will!", 255);
                                Finish();
                            }
                            else if (Option == 3)
                            {
                                Say("The decisive battle between human and demon broke out here. The");
                                Say("ferocious battle lasted for seven days andnights. Countless heroes lost their");
                                Say("lives. And the justise won. Afterwards we can live in peace for hundreds of");
                                Say("years.");
                                Link("And then?", 4);
                                Link("Sounds great", 255);
                                Finish();
                            }
                            else if (Option == 4)
                            {
                                Say("UltimatePluto, leader of the demons sworn to take revenge in one thousand");
                                Say("years before he managed to run away. Toprevent the demons coming back, I");
                                Say("have been scouting their land cautiously. Yesterday I found something");
                                Say("unwanted.It seems that Ultimate Pluto has come round to endanger humas");
                                Say("again.");
                                Link("What should we do?", 5);
                                Link("Cool", 255);
                                Finish();
                            }
                            else if (Option == 5)
                            {
                                Say("Fortunately UltimatePluto is still unfledged. He must resort to a Battle");
                                Say("Formation for the moment. So I am about to organize an army too infiltrate his");
                                Say("land and destroy his formation before he becomes stronger.");
                                Link("I'll join in you", 1);
                                Link("I'd like to know more", 6);
                                Link("Sigh, I'm helpless.", 255);
                                Finish();
                            }
                            else if (Option == 6)
                            {
                                Say("You must kill UltimatePluto rapidly to get DarkHorn so that I can use it to");
                                Say("disable the formation. Before you can do that, you must break into HellGate,");
                                Say("enter the HellHall, and fight through the HellCloster. Countless ferocious");
                                Say("demonsare watching those strongholds. I'll give you some strategies on");
                                Say("breaking through those fortresses if you like.");
                                Link("I want to know HellGate.", 7);
                                Link("I want to know HellHall", 8);
                                Link("I want to know HellCloister", 9);
                                Link("I want to know BattleFormation", 10);
                                Link("Thanks. I know how to do", 255);
                                Finish();
                            }
                            else if (Option == 7)
                            {
                                Say("HellGate is shielded from poisonous fog, so you can't approach it. But");
                                Say("demons do not fear the gas. They may turninto SoulStones after they die. If");
                                Say("you get five stones for me, I'll help you break through the gate. To protect");
                                Say("the unrelated persons, I'll send the others back as soon as the first 60");
                                Say("persons pass through the gate");
                                Link("I'd like to know more!", 6);
                                Link("Thank you", 255);
                                Finish();
                            }
                            else if (Option == 8)
                            {
                                Say("HellHall is the very spot where the demons swear their oaths of allegiance to");
                                Say("UltimatePluto. Everybody must do his best to make a way out. Due to limited");
                                Say("time, I can lead only 30 persons to HellCloister");
                                Link("I'd like to know more!", 6);
                                Link("Thank You", 255);
                                Finish();
                            }
                            else if (Option == 9)
                            {
                                Say("You will be divided into two groups to attack from the left and the right flank of");
                                Say("HellCloister. Kill the Wraithas many as you can, because you can't reach");
                                Say("BattleFormation until the amount of Wraith is decreased to a certain level.");
                                Link("I'd like to know more!", 6);
                                Link("Thank You", 255);
                                Finish();
                            }
                            else if (Option == 10)
                            {
                                Say("BattleFormation is protected by Syrens. After they are killed out");
                                Say("UltimatePluto will appear. Make the besteffort to kill him, get his DarkHorn");
                                Say("and give it to me. I can disable the BattleFormation with it. Then I'll sendyou");
                                Say("back. But if you fails to do it, we have to retreat and wait for another opportunity");
                                Link("I'd like to know more!", 6);
                                Link("Thank You", 255);
                                Finish();
                            }
                            else if (Option == 11)
                            {
                                if (Client.Inventory.Length <= 39)
                                {
                                    if (Client.TryExpendItemOfId(723088, 1))
                                    {
                                        if (Client.Hero.Level < 130)
                                            Client.GainExpBall(300);

                                        Client.AddInventory(720028); // DBScroll

                                        Say("Here you are! Check your inventory!");
                                        Link("Thank you!", 255);
                                        Finish();
                                    }
                                }
                                else
                                {
                                    Say("Please make 5 or more empty spaces in your inventory in order to collect your reward!");
                                    Link("Ok.", 255);
                                    Finish();
                                }

                            }
                            break;
                        }
                    #endregion
                    #region Stage 1
                    case 3298:
                        {
                            if (Option == 0)
                            {
                                Say("With these SoulStones I can help you enter the HellGate shielded from poisonous fogs. Are you ready?");
                                Link("I'm ready", 1);
                                Link("Wait a moment", 255);
                                Finish();
                            }
                            if (Option == 1)
                            {
                                if (DisCity.Stage2Count < 50) // Stage2Count
                                {
                                    Client.Teleport(4022, 220, 340);
                                    if (Client.TryExpendItemOfId(723085, 5))// SoulStones
                                    {
                                        DisCity.Stage2Count++;
                                        GameClient.SpeakToAll(Client, Color.Yellow, ChatType.Center, $"{Client.Hero.Name} has just passed through the HellGate and entered HellHall, ranked {DisCity.Stage2Count} !");
                                    }
                                    else
                                    {
                                        foreach (var pClient in Kernel.Clients)
                                        {
                                            if (pClient.Hero.MapID.ID == 4021)
                                            {
                                                pClient.Teleport(1002, 400, 400); // TwinCity
                                                pClient.Speak(Color.Red, ChatType.Talk, "Sorry all 50 places have been taken. Please come back next week.");
                                            }
                                        }
                                        DisCity.Stage2Count = 0;
                                    }
                                }
                                else
                                {
                                    Say("Sorry you don't have 5 SoulStones");
                                    Link("Oh sorry, I will get them!", 255);
                                    Finish();
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Stage 2
                    case 3299:
                        {
                            if (Option == 0)
                            {
                                if (Client.DisCityKills < Client.DisKillRequirement())
                                {
                                    Say($"You'll pass if you kill {Client.DisKillRequirement()} monsters in HellHall. Hurry up you got {Client.DisKillRequirement() - Client.DisCityKills} monsters left to kill! Only 30 persons can get to HellCloister with me.");
                                    Link("I see", 255);
                                    Finish();
                                }
                                else
                                {
                                    Say($"You've killed the { Client.DisCityKills } monsters, and you can pass to the HellCloister. Hurry up! What flank do you want to go in?");
                                    Link("Left flank.", 1);
                                    Link("Right flank.", 2);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (DisCity.Stage3Count < 30)
                                {
                                    if (DisCity.LeftFlank.Count < 15)
                                    {
                                        Client.Teleport(4023, 297, 649);
                                        DisCity.Stage3Count++;
                                        DisCity.LeftFlank.TryAdd(Client.Hero.UID, Client);
                                        GameClient.SpeakToAll(Client, Color.Yellow, ChatType.Center, $"{Client.Hero.Name} has passed through the HellHall and has entered into the HellCloister, ranked  {DisCity.Stage3Count} !");
                                    }
                                    else
                                    {
                                        Say("I'm sorry but this flank is full.");
                                        Link("Oh damn...", 255);
                                        Finish();
                                    }
                                }
                                else
                                {
                                    foreach (var pClient in Kernel.Clients)
                                    {
                                        if (pClient.Hero.MapID.ID == 4021)
                                        {
                                            pClient.Teleport(1020, 566, 564);
                                            Say("Sorry but all the 30 places were taken. Come back next time.");
                                            Link("Oh damn...", 255);
                                            Finish();
                                        }
                                    }
                                    DisCity.Stage3Count = 0;
                                }
                            }
                            if (Option == 2)
                            {
                                if (DisCity.Stage3Count < 30)
                                {
                                    if (DisCity.RightFlank.Count < 15)
                                    {
                                        Client.Teleport(4023, 297, 649);
                                        DisCity.Stage3Count++;
                                        DisCity.RightFlank.TryAdd(Client.Hero.UID, Client);
                                        GameClient.SpeakToAll(Client, Color.Yellow, ChatType.Center, $"{Client.Hero.Name} has passed through the HellHall and has entered into the HellCloister, ranked  {DisCity.Stage3Count} !");
                                    }
                                    else
                                    {
                                        Say("I'm sorry but this flank is full.");
                                        Link("Oh damn...", 255);
                                        Finish();
                                    }
                                }
                                else
                                {
                                    foreach (var pClient in Kernel.Clients)
                                    {
                                        if (pClient.Hero.MapID.ID == 4021)
                                        {
                                            pClient.Teleport(1020, 566, 564);
                                            Say("Sorry but all the 30 places were taken. Come back next time.");
                                            Link("Oh damn...", 255);
                                            Finish();
                                        }
                                    }
                                    DisCity.Stage3Count = 0;
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Stage4 Reward
                    case 3300:
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(790001))
                                {
                                    Say($"Hooray! Thanks to you, {Client.Hero.Name}, Ultimate Pluto has been defeated! Here take this Sword and talk to me in Ape City for the reward!");
                                    Link("Thanks!", 1);
                                    Finish();
                                }
                                else
                                {
                                    Say("Hurry up and kill Ultimate Pluto! Don't forget to bring the Dark Horn!");
                                    Link("Ok.!", 255);
                                    Finish();
                                }
                            }
                            if (Option == 1)
                            {
                                if (Client.TryExpendItemOfId(790001, 1))
                                {
                                    Client.AddInventory(723088);
                                    Say("Here you are!");
                                    Link("Thanks", 255);
                                    Finish();
                                    foreach (var pClient in Kernel.Clients)
                                    {
                                        if (pClient.Hero.MapID >= 4021 && pClient.Hero.MapID <= 4024)
                                            pClient.Teleport(1020, 566, 564);
                                    }
                                    GameClient.SpeakToAll(Client, Color.Yellow, ChatType.Center, $"{Client.Hero.Name} got the StarSword from killing Ultimate Pluto and Dis City has ended!");
                                }
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region MoonBox
                    case 600076: // FortuneTeller
                        {
                            if (Option == 0)
                            {
                                Say("Did you hear about the Palace Method?");
                                Link("Palace Method?", 1);
                                Link("Just passing by.", 255);
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                Say("I discovered a mystic Tactic a few days ago, I tried to work it out but I failed and almost died.");
                                Link("I want to try.", 2);
                                Link("Too dangerous.", 255);
                                Finish(npc);
                            }
                            if (Option == 2)
                            {
                                Say("It's very dangerous, are you sure?");
                                Say("If you enter speak to Magie, she will help you reach your first tactic.");
                                Link("Yes, I am. Thanks!", 3);
                                Link("Too dangerous.", 255);
                                Finish(npc);
                            }
                            if (Option == 3)
                            {
                                if (Client.Hero.Level >= 70)
                                    Client.Teleport(1042, 28, 33);
                                else
                                {
                                    Say("Sorry you don't meet the minimum requirement of level 70.");
                                    Link("Okay thanks.", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    case 6100: // Maggie
                        {
                            uint[] req = { 721010, 721011, 721012, 721013, 721014, 721015, 721071 };
                            if (Option == 0)
                            {
                                var cont = req.All(element => Client.CheckMultiples(req));
                                if (cont)
                                {
                                    Say("Wow. I can't believe you were able to obtain all six Tokens. I will use these Tokens to teleport you ");
                                    Say("to the Life Tactic where you can help some poor Spirit. When you teleport, the Tokens will be formed into a SoulJade.");
                                    Say("Give this SoulJade to a any of LonelySpirits to save the Spirit's soul. You will recieve a MoonBox for your kindness. Are you ready?");
                                    Link("Yes teleport me.", 3);
                                    Link("Not yet.", 255);
                                    Finish(npc);
                                }
                                else
                                {
                                    Say($"Hello {Client.Hero.Name}. My name is Maggie. I tried to conquer these Tactics many years ago and I was killed.");
                                    Say("I choose to gaurd the entrance in the afterlife to stop others from following in my footsteps. If you choose to ");
                                    Say("ignore my warning, I will send you in. I cannot control what Tactic I'll send you to unless you have all Tokens.");
                                    Link("Tokens?", 1);
                                    Link("This is too dangerous, I must leave.", 255);
                                    Finish(npc);
                                }
                            }
                            if (Option == 1)
                            {
                                Say("There are six different Tokens you must gain from killing monsters. The monsters in each Tactic will drop a different ");
                                Say("token. Once you have all six, I can can channel them for you and teleport you to the life Tactic where you can help save ");
                                Say("some poor Spirits. I should warn you, there is one more Tactic you can be sent to called the Death Tactic. Like the name suggests, ");
                                Say("if you get sent their, you will die. Good luck adventurer.");
                                Link("Send me to a Tactic.", 2);
                                Link("This is too dangerous, I won't go.", 255);
                                Finish(npc);
                            }
                            if (Option == 2)
                            {
                                if (DataStructures.ChanceSuccess(5.0))
                                {
                                    //Death Tactic
                                    Client.Teleport(1049, 211, 156);
                                }
                                else
                                {
                                    var TacticMapIDs = new uint[] { 1043, 1044, 1045, 1046, 1047, 1048, 1049 };
                                    var TacticNum = RandomGenerator.Generator.Next(0, TacticMapIDs.Length);

                                    Client.Teleport(TacticMapIDs[TacticNum], 211, 156);
                                }
                            }
                            if (Option == 3)
                            {
                                foreach (var item in req)
                                {
                                    if (!Client.RemoveInventory(item))
                                        return;
                                }
                                Client.AddInventory(721072);
                                Client.Teleport(1050, 211, 156);
                            }
                            break;
                        }
                    case 6110: // PeaceTactic
                        {
                            if (Option == 0)
                            {
                                Say("You have entered the Peace Tactic. If you have not already, kill the monsters until you get a CommandToken. That is the ");
                                Say("only way you can leave this awful place. Good luck adventurer.");
                                Link("Thanks.", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    case 6111: // ChaosTactic
                        {
                            if (Option == 0)
                            {
                                Say("You have entered the Chaos Tactic. If you have not already, kill the monsters until you get a CommandToken. That is the ");
                                Say("only way you can leave this awful place. Good luck adventurer.");
                                Link("Thanks.", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    case 6112: // DesertedTactic
                        {
                            if (Option == 0)
                            {
                                Say("You have entered the Deserted Tactic. If you have not already, kill the monsters until you get a CommandToken. That is the ");
                                Say("only way you can leave this awful place. Good luck adventurer.");
                                Link("Thanks.", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    case 6113: // ProsperousTactic
                        {
                            if (Option == 0)
                            {
                                Say("You have entered the Prosperous Tactic. If you have not already, kill the monsters until you get a CommandToken. That is the ");
                                Say("only way you can leave this awful place. Good luck adventurer.");
                                Link("Thanks.", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    case 6114: // DisturbedTactic
                        {
                            if (Option == 0)
                            {
                                Say("You have entered the Disturbed Tactic. If you have not already, kill the monsters until you get a CommandToken. That is the ");
                                Say("only way you can leave this awful place. Good luck adventurer.");
                                Link("Thanks.", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    case 6115: // CalmedTactic
                        {
                            if (Option == 0)
                            {
                                Say("You have entered the Calmed Tactic. If you have not already, kill the monsters until you get a CommandToken. That is the ");
                                Say("only way you can leave this awful place. Good luck adventurer.");
                                Link("Thanks.", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    case 6116: // DeathTactic
                        {
                            if (Option == 0)
                            {
                                Say("I am so sorry to tell you that you are in the Death Tactic. There is no way to leave this place besides death.");
                                Link("WHAT!?!", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    case 6120: // Ghost[PeaceTactic]
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721010))
                                {
                                    Say("Since you have a token to this Tactic, I am able to harness it's power to send you out. Are you ready? ");
                                    Link("Yes, send me out.", 1);
                                    Link("Thanks.", 255);
                                }
                                else
                                {
                                    Say("I wish I could help you adventurer but I need the Token, the monsters are holding in order to send you out.");
                                    Link("I will get one.", 255);
                                }
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                if (Client.InventoryContains(721010))
                                    Client.Teleport(1042, 21, 25);
                            }
                            break;
                        }
                    case 6121: // Ghost[ChaosTactic]
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721011))
                                {
                                    Say("Since you have a token to this Tactic, I am able to harness it's power to send you out. Are you ready? ");
                                    Link("Yes, send me out.", 1);
                                    Link("Thanks.", 255);
                                }
                                else
                                {
                                    Say("I wish I could help you adventurer but I need the Token, the monsters are holding in order to send you out.");
                                    Link("I will get one.", 255);
                                }
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                if (Client.InventoryContains(721011))
                                    Client.Teleport(1042, 21, 25);
                            }
                            break;
                        }
                    case 6122: // Ghost[DesertedTactic]
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721012))
                                {
                                    Say("Since you have a token to this Tactic, I am able to harness it's power to send you out. Are you ready? ");
                                    Link("Yes, send me out.", 1);
                                    Link("Thanks.", 255);
                                }
                                else
                                {
                                    Say("I wish I could help you adventurer but I need the Token, the monsters are holding in order to send you out.");
                                    Link("I will get one.", 255);
                                }
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                if (Client.InventoryContains(721012))
                                    Client.Teleport(1042, 21, 25);
                            }
                            break;
                        }
                    case 6123: // Ghost[ProsperousTactic]
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721013))
                                {
                                    Say("Since you have a token to this Tactic, I am able to harness it's power to send you out. Are you ready? ");
                                    Link("Yes, send me out.", 1);
                                    Link("Thanks.", 255);
                                }
                                else
                                {
                                    Say("I wish I could help you adventurer but I need the Token, the monsters are holding in order to send you out.");
                                    Link("I will get one.", 255);
                                }
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                if (Client.InventoryContains(721013))
                                    Client.Teleport(1042, 21, 25);
                            }
                            break;
                        }
                    case 6124: // Ghost[DisturbedTactic]
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721014))
                                {
                                    Say("Since you have a token to this Tactic, I am able to harness it's power to send you out. Are you ready? ");
                                    Link("Yes, send me out.", 1);
                                    Link("Thanks.", 255);
                                }
                                else
                                {
                                    Say("I wish I could help you adventurer but I need the Token, the monsters are holding in order to send you out.");
                                    Link("I will get one.", 255);
                                }
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                if (Client.InventoryContains(721014))
                                    Client.Teleport(1042, 21, 25);
                            }
                            break;
                        }
                    case 6125: // Ghost[CalmedTactic]
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721015))
                                {
                                    Say("Since you have a token to this Tactic, I am able to harness it's power to send you out. Are you ready? ");
                                    Link("Yes, send me out.", 1);
                                    Link("Thanks.", 255);
                                }
                                else
                                {
                                    Say("I wish I could help you adventurer but I need the Token, the monsters are holding in order to send you out.");
                                    Link("I will get one.", 255);
                                }
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                if (Client.InventoryContains(721015))
                                    Client.Teleport(1042, 21, 25);
                            }
                            break;
                        }
                    case 6126: // Ghost[DeathTactic]
                        {
                            if (Option == 0)
                            {
                                Say("I'm sorry but the only way out of this tactic is to die like I have.");
                                Link("WHAT?!?", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    case 7660: // LonelyGhost
                    case 7661:
                    case 7662:
                    case 7663:
                    case 7664:
                    case 7665:
                        {
                            if (Option == 0)
                            {
                                if (Client.InventoryContains(721072))
                                {
                                    Say("Can it be? Is that... YES!!! it's a SoulJade. I thought I would never see one of those. Please free me from this prison");
                                    Say("so I can join my family. I will give you this MoonBox I acquired before I died. Will you help me?");
                                    Link("Yes, here the SoulJade", 1);
                                    Link("Sorry I cannot help you yet.", 255);
                                }
                                else
                                {
                                    Say("You cannot help us without a SoulJade. Please leave here until you get one.");
                                    Link("I will get one.", 255);
                                }
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                if (Client.InventoryContains(721072))
                                {
                                    Client.Teleport(1042, 21, 25);
                                    Client.RemoveInventory(721072); // SoulJade
                                    Client.AddInventory(721020); // MoonBox
                                }
                                else
                                {
                                    Say("You cannot help us without a SoulJade. Please leave here until you get one.");
                                    Link("I will get one.", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region OTGLeave
                    case 6536:
                        Client.SendOtgResults();
                        break;
                    #endregion
                    #region Lady Luck(MA->Lotto)
                    case 923:
                        {
                            if (Option == 0)
                            {
                                Say("Would you like to enter the Lottery and have a chance to win some wonderfull items?");
                                Say("It will cost you 27 CPs.");
                                Link("Yes", 1);
                                Link("Never mind", 255);
                                Finish(npc);
                                break;
                            }
                            if (Option == 1)
                            {
                                if (Client.LottoTries < 10)
                                {
                                    if (Client.Hero.Level >= 70)
                                    {
                                        if (Client.ConquerPoints >= 27)
                                        {
                                            Client.ConquerPoints -= 27;
                                            Client.Teleport(700, 39, 49);
                                            Client.LottoTries++;
                                        }
                                        else
                                        {
                                            Say("You do not have 27 CPs.");
                                            Link("Damn, sorry", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You're not L70.");
                                        Link("Damn, sorry,", 255);
                                        Finish(npc);
                                    }
                                }
                                else
                                {
                                    Say("You've already been to Lottery 10 times today");
                                    Link("Damn, sorry,", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Lady Luck(Lotto->MA)
                    case 924:
                        {
                            if (Option == 0)
                            {
                                Say("Would you like to leave lottery?");
                                Link("Yes", 1);
                                Link("Just passing by...", 255);
                                Finish(npc);
                                break;
                            }
                            if (Option == 1)
                            {
                                if (Client.PickedBox)
                                {
                                    Client.Teleport(1036, 211, 196);
                                    Client.PickedBox = false;
                                }
                                else
                                {
                                    Say("Are you sure you want to leave? You haven't picked a box yet.");
                                    Link("Yes", 2);
                                    Link("No", 255);
                                    Finish(npc);
                                    break;
                                }
                            }
                            if (Option == 2)
                            {
                                Client.Teleport(1036, 211, 196);
                                Client.PickedBox = false;
                            }
                            break;
                        }
                    #endregion
                    #region LuckyBox
                    case 925:
                    case 926:
                    case 927:
                    case 928:
                    case 929:
                    case 930:
                    case 931:
                    case 932:
                    case 933:
                    case 934:
                    case 935:
                    case 936:
                    case 937:
                    case 938:
                    case 939:
                    case 940:
                    case 942:
                    case 943:
                    case 944:
                    case 945:
                        {
                            if (Option == 0)
                            {
                                Say("Do you wish to pick this box?");
                                Link("Yes", 1);
                                Link("No", 255);
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                if (!Client.PickedBox)
                                {
                                    Client.PerformLottery();
                                }
                                else
                                {
                                    Say("You have already picked a box...");
                                    Link("Ok, sorry", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Market Upgrade
                    case 10062:
                        {
                            if (Option == 0)
                            {
                                Say("Hi. What i do is upgrade your equipment for meteors. How about it?");
                                Link("Upgrade quality", 1);
                                Link("Upgrade level", 2);
                                Link("Just passing by...", 255);
                                Finish(npc);
                                Client.DialogAgreed = false;
                            }
                            else if ((Option == 1) || (Option == 2))
                            {
                                Say("Choose the equipment you want to upgrade.");
                                Link("Headgear", (byte)(Option * 100 + 1));
                                Link("Necklace/Bag", (byte)(Option * 100 + 2));
                                Link("Armor", (byte)(Option * 100 + 3));
                                Link("Weapon", (byte)(Option * 100 + 4));
                                Link("Shield", (byte)(Option * 100 + 5));
                                Link("Ring", (byte)(Option * 100 + 6));
                                Link("Boots", (byte)(Option * 100 + 8));

                                Finish(npc);
                            }
                            else if ((Option >= 101) && (Option <= 111))
                            {
                                var Slot = (byte)(Option - 100);
                                var Equipment = Client.Equipment.Values.FirstOrDefault(x => x.Position == (ItemPosition)Slot);
                                if ((Equipment == null) || ConquerItem.IsItemType(Equipment.ID, ItemType.Arrow))
                                    goto notEquipped;

                                var standerd = new StanderdItemStats(Equipment.ID);
                                var quality = (byte)(Equipment.ID % 10);
                                var ItemLevel = (byte)standerd.ReqLvl;
                                if ((quality != 9) && (quality >= 3))
                                {
                                    byte DBReq = 2;

                                    if (quality < 5)
                                        quality = 5;

                                    switch (quality)
                                    {
                                        case 6:
                                            DBReq++;
                                            break;
                                        case 7:
                                            DBReq += 2;
                                            break;
                                        case 8:
                                            DBReq += 5;
                                            break;
                                    }

                                    DBReq = (byte)(DBReq + ItemLevel / 30);
                                    if (!Client.DialogAgreed)
                                    {
                                        Say("You need " + DBReq + " Dragonballs to upgrade.");
                                        Link("Upgrade it.", (byte)Option);
                                        Link("Forget it.", 255);

                                        Finish(npc);
                                        Client.DialogAgreed = true;
                                    }
                                    else
                                    {
                                        Client.DialogAgreed = false;
                                        if (Client.TryExpendItemOfId(1088000, DBReq))
                                        {
                                            FlatDatabase.UnloadItemStats(Client, Equipment);

                                            quality++;
                                            Equipment.ID = Equipment.ID / 10 * 10 + quality;

                                            FlatDatabase.LoadItemStats(Client, Equipment);
                                            Equipment.Send(Client);

                                            Say("Here you are. It's done.");
                                            Link("Thanks.", 255);

                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You don't have enough Dragonballs.");
                                            Link("I see.", 255);

                                            Finish(npc);
                                        }
                                    }
                                }
                                else
                                {
                                    Say("You cannot upgrade an item's quality which is already at maximum.");
                                    Link("I see", 255);
                                    Finish(npc);
                                }
                            }
                            else if ((Option >= 201) && (Option <= 208))
                            {
                                var Slot = (byte)(Option - 200);
                                var Equipment = Client.Equipment.Values.FirstOrDefault(x => x.Position == (ItemPosition)Slot);
                                if ((Equipment == null) || ConquerItem.IsItemType(Equipment.ID, ItemType.Arrow))
                                    goto notEquipped;

                                var quality = (byte)(Equipment.ID % 10);
                                var NewID = ConquerItem.TryGetUpgradeId(Equipment.ID);

                                if (NewID != 0)
                                {
                                    var nextLevelStats = new StanderdItemStats(NewID);
                                    var NewLevel = (byte)nextLevelStats.ReqLvl;

                                    var standerd = new StanderdItemStats(Equipment.ID);
                                    var PrevLevel = (byte)standerd.ReqLvl;

                                    if (Client.Hero.Level >= NewLevel)
                                    {
                                        if ((NewLevel != 0) && (NewLevel <= 123) && (PrevLevel < 120))
                                        {
                                            var dmetsRequired = (double)PrevLevel / 130 * 3;

                                            switch (quality)
                                            {
                                                case 7:
                                                    dmetsRequired *= 2;
                                                    break;
                                                case 8:
                                                    dmetsRequired *= 4;
                                                    break;
                                                case 9:
                                                    dmetsRequired *= 10;
                                                    break;
                                            }
                                            if (NewLevel < 67)
                                                dmetsRequired /= 2;
                                            dmetsRequired++;

                                            var metsRequired = (byte)dmetsRequired;
                                            if (!Client.DialogAgreed)
                                            {
                                                Say("You need " + metsRequired + " Meteors to upgrade.");
                                                Link("Upgrade it.", (byte)Option);
                                                Link("Forget it.", 255);

                                                Finish(npc);
                                                Client.DialogAgreed = true;
                                            }
                                            else
                                            {
                                                Client.DialogAgreed = false;
                                                if (Client.TryExpendMeteors(metsRequired))
                                                {
                                                    FlatDatabase.UnloadItemStats(Client, Equipment);
                                                    Equipment.ID = NewID;
                                                    FlatDatabase.LoadItemStats(Client, Equipment);
                                                    Equipment.Send(Client);

                                                    Say("Here you are. It's done.");
                                                    Link("Thanks.", 255);

                                                    Finish(npc);
                                                }
                                                else
                                                {
                                                    Say("You don't have enough Meteors.");
                                                    Link("No way! Are you really sure?", 255);

                                                    Finish(npc);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Say("I'm afraid i can't help you with that. I am not experienced enough with equipment that high level.");
                                            Link("You old geezer!", 255);
                                            Finish(npc);
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Market High Upgrade
                    case 7050:
                        {
                            if (Option == 0)
                            {
                                Say("As you know Magic Artisan sucks at upgrading high level gear. So i'm the one in charge of upgrading the ones he cannot.");
                                Link("Great! Just what i needed.", 10);
                                Link("Just passing by.", 255);
                                Finish(npc);
                                Client.DialogAgreed = false;
                            }
                            else if (Option == 10)
                            {
                                Say("Choose the equipment you want to upgrade.");
                                Link("Headgear", 1);
                                Link("Necklace/Bag", 2);
                                Link("Armor", 3);
                                Link("Weapon", 4);
                                Link("Shield", 5);
                                Link("Ring/Bracelet", 6);
                                Link("Boots", 8);
                                Link("Just passing by...", 255);
                                Finish(npc);
                            }
                            else if ((Option >= 1) && (Option <= 8))
                            {
                                var Slot = (byte)Option;
                                var Equipment = Client.Equipment.Values.FirstOrDefault(x => x.Position == (ItemPosition)Slot);
                                if ((Equipment == null) || ConquerItem.IsItemType(Equipment.ID, ItemType.Arrow))
                                    goto notEquipped;

                                var NewID = ConquerItem.TryGetUpgradeId(Equipment.ID);
                                if (NewID != 0)
                                {
                                    var nextLevelStats = new StanderdItemStats(NewID);
                                    var NewLevel = (byte)nextLevelStats.ReqLvl;

                                    if (Client.Hero.Level >= NewLevel)
                                    {
                                        if (!Client.DialogAgreed)
                                        {
                                            Say("You need a DragonBall. Are you ready?");
                                            Link("Upgrade it.", (byte)Option);
                                            Link("Forget it.", 255);

                                            Finish(npc);
                                            Client.DialogAgreed = true;
                                        }
                                        else
                                        {
                                            Client.DialogAgreed = false;
                                            if (Client.TryExpendItemOfId(1088000, 1))
                                            {
                                                FlatDatabase.UnloadItemStats(Client, Equipment);
                                                Equipment.ID = NewID;
                                                FlatDatabase.LoadItemStats(Client, Equipment);
                                                Equipment.Send(Client);

                                                Say("Here you are. It's done.");
                                                Link("Thanks.", 255);

                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("You don't have a DragonBall.");
                                                Link("Ahh.", 255);

                                                Finish(npc);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Say("You aren't high level enough to wear the item after upgrading.");
                                        Link("Alright.", 255);

                                        Finish(npc);
                                    }
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Ethereal
                    case 35015:
                        if (Option == 0)
                        {
                            Say("Are you in need of increasing the bless attribute of your equipment? Well, you've come to the right place.");
                            Link("Yep. I want to bless my equips.", 1);
                            Link("Just passing by", 255);
                            Finish(npc);
                        }
                        else if (Option == 1)
                        {
                            Say("The equipment must have at leaste 1 bless attribute before you I can bless it any further.");
                            Say("-1 to -3 costs 1 Super TortoiseGem, -3 to -5 costs 3 and -5 to -7 costs 5 of them.");
                            Link("Bless my HeadGear", 11);
                            Link("Bless my Necklace", 12);
                            Link("Bless my Armor", 13);
                            Link("Bless my Right Weapon", 14);
                            Link("Bless my Shield / Left Weapon", 15);
                            Link("Bless my Ring", 16);
                            Link("Bless my Boots", 18);
                            Finish(npc);
                        }
                        else if ((Option >= 11) && (Option <= 18) && (Option != 17))
                        {
                            var Slot = (byte)(Option - 10);
                            if (!Client.Equipment.ContainsKey(Slot))
                            {
                                Say("You don't have that equipped.");
                                Link("My bad.", 255);
                                Finish(npc);
                            }
                            var Equipment = Client.Equipment[Slot];
                            if (Equipment.Bless >= 1)
                            {
                                if (Equipment.Bless < 7)
                                {
                                    byte GemsNeed = 1;
                                    if (Equipment.Bless >= 5)
                                        GemsNeed = 5;
                                    else if (Equipment.Bless >= 3)
                                        GemsNeed = 3;
                                    if (Client.CountInventory(700073) >= GemsNeed)
                                    {
                                        for (byte i = 0; i < GemsNeed; i++)
                                            Client.RemoveInventory(700073);

                                        FlatDatabase.UnloadItemStats(Client, Equipment);
                                        Equipment.Bless++;
                                        FlatDatabase.LoadItemStats(Client, Equipment);
                                        Equipment.Send(Client);
                                    }
                                }
                                else
                                {
                                    Say("The equipment cannot be blessed anymore.");
                                    Link("Damn.", 255);
                                    Finish(npc);
                                }
                            }
                            else
                            {
                                Say("That equipment isn't blessed yet.");
                                Link("Oh yeah.", 255);
                                Finish(npc);
                            }
                        }
                        break;
                    #endregion
                    #region Composition
                    case 35016:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Hi, what would you like to do today?");
                                        Link("Compose", 1);
                                        Link("Enchant", 2);
                                        Link("No, thanks.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        Client.Send(new DataPacket(Client.Hero.UID, (ushort)GUINpcType.Compose, 0, 0, 0, DataIDs.GuiNpcInterface).Serialize());
                                        break;
                                    }
                                case 2:
                                    {
                                        Client.Send(new DataPacket(Client.Hero.UID, 1091, 0, 0, 0, (DataIDs)116).Serialize());
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Warehouse(s)
                    case 8: //TwinCity
                    case 10027: //Bird Island
                    case 10011: //DC
                    case 10012: //PC
                    case 10028: //AC
                    case 44: //MA
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (Client.Warehouse.PasswordTries >= 3)
                                        {
                                            Say("You have entered an incorrect password three times.");
                                            Say("Please relogin to try again.");
                                            Link("Ok.", 255);
                                            Finish(npc);
                                        }
                                        else if (Client.Warehouse.Password == "")
                                        {
                                            Client.Speak(Color.Red, ChatType.TopLeft, "You do not have a warehouse password. You may set one at WhsGuardian in Twin City (409, 392).");
                                            Client.Send(new DataPacket(Client.Hero.UID, (ushort)GUINpcType.Warehouse, 0, 0, 0, DataIDs.GuiNpcInterface).Serialize());
                                        }
                                        else
                                        {
                                            Say("Please input your warehouse password.");
                                            Input("Password:", 1);
                                            Link("Let me think.", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 1:
                                    {
                                        if (Client.Warehouse.Password == InputText)
                                        {
                                            Client.Warehouse.Validated = true;
                                            Client.Send(new DataPacket(Client.Hero.UID, (ushort)GUINpcType.Warehouse, 0, 0, 0, DataIDs.GuiNpcInterface).Serialize());
                                        }
                                        else
                                        {
                                            Say("The password you entered was incorrect.");
                                            Link("Let me try again.", 0);
                                            Link("Ok.", 255);
                                            Finish(npc);
                                            Client.Warehouse.PasswordTries++;
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region WhsGuardian
                    case 1061:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Hello! What can I do for you?");
                                        if (Client.Warehouse.Password == "")
                                        {
                                            Link("Put a password in my Warehouse.", 1);
                                        }
                                        else
                                        {
                                            Link("Remove Password.", 4);
                                            Link("Change Password.", 6);
                                        }
                                        Link("Let me think.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        Client.Warehouse.TmpPass = "";
                                        Say("Please put your password. Min characters: 4, and Max characters: 10. Just numbers is permitted.");
                                        Input("", 2);
                                        Link("Let me think.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Client.Warehouse.TmpPass = InputText;
                                        Say("Please input your Password again.");
                                        Input("", 3);
                                        Link("Cancel it.", 255);

                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        if (Client.Warehouse.TmpPass == InputText)
                                        {
                                            if ((Client.Warehouse.TmpPass.Length >= 4) && (Client.Warehouse.TmpPass.Length <= 10))
                                            {
                                                if (FlatDatabase.ValidWhPass(Client.Warehouse.TmpPass))
                                                {
                                                    Client.Warehouse.Password = Client.Warehouse.TmpPass;
                                                    Say("Done! Now your Warehouse is protected.");
                                                    Link("Thanks!", 255);
                                                    Finish(npc);
                                                }
                                                else
                                                {
                                                    Say("Only numbers are permitted.");
                                                    Link("Sorry", 255);
                                                    Finish(npc);
                                                }
                                            }
                                            else
                                            {
                                                Say("Min characters: 4, and max characters: 10");
                                                Link("Sorry!", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else
                                        {
                                            Say("The Passwords are not the same. Try again.");
                                            Input("", 3);
                                            Link("Cancel it.", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 4:
                                    {
                                        Say("To remove the Password, please put it here for your secuirty.");
                                        Input("", 5);
                                        Link("Let me think.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 5:
                                    {
                                        if (Client.Warehouse.Password == InputText)
                                        {
                                            Client.Warehouse.Password = "";
                                            Say("Done! Now you are without password!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Wrong! are you tryin to hack me?");
                                            Link("No, Sorry!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 6:
                                    {
                                        Say("To change your password please put the old first. Remeber just numbers is permited!");
                                        Input("", 7);
                                        Finish(npc);
                                        break;
                                    }
                                case 7:
                                    {
                                        if (Client.Warehouse.Password == InputText)
                                        {
                                            Say("What do you want the new Password to be?");
                                            Input("", 2);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Wrong! are you tryin to hack me?");
                                            Link("No, Sorry!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region ProfiencyGod
                    case 941:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("I can increase the proficiency of a weapon by 1 level, using a DragonBall.\n Do you want to proceed?");
                                        Link("Club", 48);
                                        Link("Sword", 42);
                                        Link("Blade", 41);
                                        Link("Hook", 43);
                                        Link("Whip", 44);
                                        Link("Axe", 45);
                                        Link("Next Page", 1);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        Say("I can increase the proficiency of a weapon by 1 level, using a DragonBall.\n Do you want an increase?");
                                        Link("Dagger", 49);
                                        Link("Glaive", 51);
                                        Link("Poleaxe", 53);
                                        Link("LongHammer", 54);
                                        Link("Spear", 56);
                                        Link("Bow", 50);
                                        Link("Previous Page", 0);
                                        Link("Next Page", 2);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Say("I can increase the proficiency of a weapon by 1 level, using an exp ball.\n Do you want an increase?");
                                        Link("Backsword", 100);
                                        Link("Scepter", 101);
                                        Link("Katana", 102);
                                        Link("Halbert", 58);
                                        Link("Wand", 103);
                                        Link("First Page", 0);
                                        Link("Second Page", 1);
                                        Finish(npc);
                                        break;
                                    }
                                case 41:
                                case 42:
                                case 43:
                                case 44:
                                case 45:
                                case 46:
                                case 47:
                                case 48:
                                case 49:
                                case 50:
                                case 51:
                                case 52:
                                case 53:
                                case 54:
                                case 55:
                                case 56:
                                case 57:
                                case 58:
                                    {
                                        if (Client.InventoryContains(1088002))
                                        {
                                            Client.RemoveInventory(1088002);
                                            var Prof = Option * 10;
                                            ConquerSpell.LearnProf(Client, (ushort)Prof);
                                        }
                                        else
                                        {
                                            Say("You don't have a db.");
                                            Link("Sorry.", 255);
                                        }
                                        break;
                                    }
                                case 100:
                                case 101:
                                case 102:
                                case 103:
                                    {
                                        if (Option == 100)
                                            Option = 421;
                                        if (Option == 101)
                                            Option = 481;
                                        if (Option == 102)
                                            Option = 601;
                                        if (Option == 103)
                                            Option = 561;

                                        if (Client.InventoryContains(1088002))
                                        {
                                            Client.RemoveInventory(1088002);
                                            ConquerSpell.LearnProf(Client, Option);
                                        }
                                        else
                                        {
                                            Say("You don't have a db.");
                                            Link("Sorry.", 255);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Custom Event's
                    #region GeneralWinner/Tournament Hoster
                    case 145:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Face(20);
                                        Say("I am the Tournament General. Would you like to join a tournament?");
                                        Link("Check Tournament", 1);
                                        Link("Just passing by...", 255);
                                        Finish();
                                        break;
                                    }
                                case 1:
                                    {
                                        if (TournamentBase.AcceptingEntries || TeamDeathMatch.AcceptingEntries)
                                        {
                                            Face(20);
                                            Say("A tournament has started! You may join now.");
                                            Link("Join Tournament", 2);
                                            Finish();
                                        }
                                        else
                                        {
                                            Face(20);
                                            Say("Sorry, there aren't any tournaments accepting players.");
                                            Say("I'll try again next time.");
                                            Link("Damn!", 255);
                                            Finish();
                                        }
                                        break;
                                    }
                                case 2:
                                    {
                                        if (TournamentBase.AcceptingEntries)
                                        {
                                            TournamentBase.Join(Client);
                                        }
                                        else if (TeamDeathMatch.AcceptingEntries)
                                        {
                                            TeamDeathMatch.Join(Client);
                                        }
                                    }
                                    break;
                            }
                            return;
                        }
                    #endregion
                    #region PrizeNPC
                    case 47:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (Client.PrizeNPC == 0)
                                        {
                                            Say("Sorry! You currently do not have any prizes waiting for you!");
                                            Link("Okay.", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            if (Client.PrizeNPC == 1)
                                                Say("I have 2 Dragonballs waiting for you!");
                                            else if (Client.PrizeNPC == 2)
                                                Say("I have 5 Dragonballs waiting for you!");
                                            Link("Retrieve Now", 1);
                                            Link("No now", 255);
                                            Finish(npc);
                                        }

                                        break;
                                    }
                                case 1:
                                    {
                                        if (Client.PrizeNPC == 1)
                                        {
                                            if (Client.Inventory.Length < 38)
                                            {
                                                Client.AddItems(1088000, 2);
                                                MySqlDatabase.SavePrize(Client);
                                            }
                                            else
                                            {
                                                goto case 2;
                                            }
                                        }
                                        else if (Client.PrizeNPC == 2)
                                        {
                                            if (Client.Inventory.Length < 35)
                                            {
                                                Client.AddItems(1088000, 5);
                                                MySqlDatabase.SavePrize(Client);
                                            }
                                            else
                                            {
                                                goto case 2;
                                            }
                                        }

                                        Say("Here is your prize! Enjoy!");
                                        Link("Thanks.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Say("Uh oh. You don't have enough room in your inventory!");
                                        Link("I'll fix that!", 255);
                                        Finish(npc);
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region Barber
                    case 10002:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Hi, I can offer you a variety of hairstyles.");
                                        Say("Would you like to make a payment of 500 silvers to change your hairstyle?");
                                        Link("Yes, Nostalgic Styles", 1);
                                        Link("No thanks, Just passing by.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        Link("Nostalgic 1", 11);
                                        Link("Nostalgic 2", 12);
                                        Link("Nostalgic 3", 13);
                                        Link("Nostalgic 4", 14);
                                        Link("Nostalgic 5", 15);
                                        Link("Nostalgic 6", 16);
                                        Link("Nostalgic 7", 17);
                                        Link("Back", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                    {
                                        if (Client.Silvers >= 500)
                                        {
                                            Money(-500);
                                            Client.Hero.Hairstyle = (ushort)(Math.Floor((decimal)(Client.Hero.Hairstyle / 100)) * 100 + Option);
                                            Client.SendScreen(new StatTypePacket(Client.Hero.UID, Client.Hero.Hairstyle, StatIDs.Hairstyle).Serialize(), true);

                                            Say("There you go, Your 'Nostalgic' hairstyle as requested");
                                            Link("Thank You", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region LoveStone
                    case 390:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("The fate brings lovers together. I hope that all can get married and live happily with their lover.");
                                        Say("What can I do for you?");
                                        Link("I want to get married", 1);
                                        Link("Nothing special", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        if ((Client.Spouse == "None") || (Client.Spouse == ""))
                                        {
                                            Say("Marriage is the promise forever. Are you sure that you want to spend the rest of your life with your lover?");
                                            Link("I want to get married.", 2);
                                            Link("I prefer being single.", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry. You are already married and I do not allow multiple marriages.");
                                            Link("Okay", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 2:
                                    {
                                        Say("Are you ready to propose?");
                                        Link("Yeah. I am ready.", 3);
                                        Link("Let me think it over.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        Client.Send(new DataPacket(Client.Hero.UID, 1067, 0, 0, 0, (DataIDs)116).Serialize());
                                        Say("Please click on your lover to propose");
                                        Link("OK.", 255);
                                        Finish(npc);
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Starlit
                    case 600055:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("One star in the sky represents a person. When a star falls,");
                                        Say("one person will dissppear. Then I will make up a new one");
                                        Link("You are great", 255);
                                        Link("Can you give me a star?", 1);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        Say("Every star represents a person on the Earth. How can I give it to you?");
                                        Say("If you have nothing special, please do not disturb me");
                                        Link("I am leaving.", 255);
                                        Link("What are you busy with", 2);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Say("Couples rely on each other as their stars in the sky do.");
                                        Say("When they do not get along well with each other, I shall...");
                                        Link("What will you do?", 3);
                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        Say("I will make their starrs move apart until they can no longer shine in each other. Then they will break up finally.");
                                        Link("Why are you so cruel?", 4);
                                        Link("I see.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 4:
                                    {
                                        Say("I do not take them apart unless they have suffered a lot");
                                        Say("from their marriage and decide to end their relationship");
                                        Link("Can you help me divorce?", 5);
                                        Link("I see", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 5:
                                    {
                                        Say("If your marriage left you nothing but regret and pain, then");
                                        Say("bring a meteor and a meteor tear and I can help you out");
                                        Link("Why are they needed?", 6);
                                        Link("I love my spouse", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 6:
                                    {
                                        Say("A star stays in the sky, then the person can revive. If it");
                                        Say("falls down as a meteor, he/she cannot, and I will get it back");
                                        Link("What is the Meteor Tear for?", 7);
                                        Finish(npc);
                                        break;
                                    }
                                case 7:
                                    {
                                        Say("The true love will move a meteor to tears, thus Meteor Tear is formed");
                                        Say("I do not take the Meteor Tear for myself.");
                                        Link("For whom then?", 8);
                                        Finish(npc);
                                        break;
                                    }
                                case 8:
                                    {
                                        Say("I shall give it to your spouse as comfort. I hope he/she");
                                        Say("will know the world is still full of love, and live happily");
                                        Link("I see", 255);
                                        Link("Get divorced", 9);
                                        Finish(npc);
                                        break;
                                    }
                                case 9:
                                    {
                                        Say("The lovers should care for each other. Do not draw a");
                                        Say("conclusion in haste. You had better think it over");
                                        Link("I have made up my mind.", 10);
                                        Link("Let me think it over", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 10:
                                    {
                                        Say("Are you sure you want to leave your lover?");
                                        Link("Yeah.", 11);
                                        Link("I changed my mind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 11:
                                    {
                                        Say("Since you have made up your mind, I shall help you and take");
                                        Say("away your Meteor Tear and Meteor.");
                                        Link("Thanks", 12);
                                        Link("I changed my mind.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 12:
                                    {
                                        if (Client.InventoryContains(1088001) && Client.InventoryContains(1088002))
                                        {
                                            Client.RemoveInventory(1088001);
                                            Client.RemoveInventory(1088002);
                                            MySqlDatabase.RemoveSpouse(Client);

                                            var SpousePacket = new StringPacket();
                                            SpousePacket.ID = StringIds.Spouse;
                                            SpousePacket.UID = Client.Hero.UID;

                                            Client.Send(Packets.StringPacket(SpousePacket, "None"));

                                            var Spouse = Kernel.FindClient(Client.Spouse);
                                            if (Spouse != null)
                                            {
                                                SpousePacket.ID = StringIds.Spouse;
                                                SpousePacket.UID = Spouse.Hero.UID;
                                                Spouse.Send(Packets.StringPacket(SpousePacket, "None"));
                                                Spouse.Spouse = "";
                                            }
                                            Client.Spouse = "";

                                            Say("You divorced your spouse. I hope you can think it over before");
                                            Say("you get married next time. Do not come for divorce any more");
                                            Link("Thank you", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry, you do not have a meteor and meteor tear.");
                                            Link("I see", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region PK Arena
                    case 10021:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Hello I can teleport you to the PK Arena for 50 silvers.");
                                        Link("Yes, please.", 1);
                                        Link("No, thanks.", 255);
                                        Link("I want to duel.", 2);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        if (Client.Silvers >= 50)
                                        {
                                            Client.Teleport(1005, 50, 50);
                                        }
                                        else
                                        {
                                            Say("Sorry, you do not have 50 Silvers.");
                                            Link("Damn.", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 2:
                                    {
                                        Say("You have to create a team in order to duel.");
                                        Say("Please input the number of Conquer Point's you want to wager.");
                                        Input("", 3);
                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        if (Client.ConquerPoints >= uint.Parse(InputText))
                                        {
                                            if (Client.Team.Active)
                                            {
                                                foreach (var client in Client.Team.Teammates)
                                                {
                                                    if (client != Client)
                                                    {
                                                        if (client.ConquerPoints < uint.Parse(InputText))
                                                        {
                                                            Say("Your opponent does not have enough CPs.");
                                                            Link("Damn.", 255);
                                                            Finish(npc);
                                                            return;
                                                        }
                                                    }
                                                }
                                                if (Client.Team.IsLeader && Client.Team.TeamDictionary.Count == 2)
                                                {
                                                    Dueling.Wager = ushort.Parse(InputText);
                                                    Dueling.RequestDuel(Client);
                                                }
                                                else
                                                {
                                                    Say("Either you are not the team leader or do not have sufficient players in your team.");
                                                    Link("Damn.", 255);
                                                    Finish(npc);
                                                }
                                            }
                                            else
                                            {
                                                Say("Please, create a team first consisting of 2 members.");
                                                Link("Damn.", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else
                                        {
                                            Say("You do not have enough CPs.");
                                            Link("Damn.", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Equipment dying
                    case 10063:
                        if (Option == 0)
                        {
                            Say("Hey! Do you want to dye your gear? For a price of one Meteor, you can do just that. So how about it?");
                            Link("Thats why I'm here. Lets do it.", 1);
                            Link("No, not right now.", 255);
                            Finish(npc);
                        }
                        else if (Option == 1)
                        {
                            if (Client.InventoryContains(1088001))
                            {
                                Client.RemoveInventory(1088001);
                                Client.Teleport(1008, 22, 26);
                            }
                            else
                            {
                                Say("You don't have a Meteor, so scram!");
                                Link("Oww... just don't hit me. :(", 255);
                                Finish(npc);
                            }
                        }
                        break;
                    case 10064:
                        if (Option == 0)
                        {
                            Say("So.. you're here to dye your gear, are you? Which one of your equipment do you want your color changed then?");
                            Link("Headgear", 1);
                            Link("Armor", 3);
                            Link("Shield", 5);
                            Link("Let me think.", 255);
                            Finish(npc);
                        }
                        else if (Option == 1 || Option == 3 || Option == 5)
                        {
                            if (Client.Equipment.Values.Any(x => x.Position == (ItemPosition)Option && (x.ID / 1000 == 900 || Option != 5)))
                            {
                                Say("And what color do you want it dyed in?");
                                Link("Orange", (byte)(Option * 10 + 3));
                                Link("Light blue", (byte)(Option * 10 + 4));
                                Link("Red", (byte)(Option * 10 + 5));
                                Link("Blue", (byte)(Option * 10 + 6));
                                Link("Yellow", (byte)(Option * 10 + 7));
                                Link("Purple", (byte)(Option * 10 + 8));
                                Link("White", (byte)(Option * 10 + 9));
                                Link("Let me think.", 255);
                                Finish(npc);
                            }
                            else
                                goto notEquipped;
                        }
                        else
                        {
                            var position = (byte)(Option / 10);
                            var color = (byte)(Option % 10);

                            if ((position == 1 || position == 3 || position == 5) && color > 2)
                            {
                                var equipment = Client.Equipment.Values.FirstOrDefault(x => x.Position == (ItemPosition)position);

                                if (equipment != null)
                                {
                                    equipment.Color = color;
                                    equipment.SendInventoryUpdate(Client);
                                }

                                Say("Here you go. It is done.");
                                Link("Thanks, man.", 255);
                                Finish(npc);
                            }
                        }
                        break;
                    #endregion                    
                    #region Conductresses
                    #region BI - Conductress
                    case 10056:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Say("I am the Bird Island Conductress. I can teleport you for a price of 100 silvers.");
                                            Link("Twin City", 1);
                                            Link("Market", 2);
                                            Link("Nevermind!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry. I cannot teleport you anywhere since you do not have 100 silvers!");
                                            Link("I'll fix that", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 1: //Twin City
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1015, 1010, 710);
                                            Money(-100);
                                        }
                                        break;
                                    }
                                case 2: //Market
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1036, 212, 195);
                                            Money(-100);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region TC - Conductress
                    case 10050:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Say("Where are going? I can teleport you for the price of 100 silvers!");
                                            Link("Phoenix Castle", 1);
                                            Link("Ape Mountain", 2);
                                            Link("Mine Cave", 3);
                                            Link("Desert City", 4);
                                            Link("Bird Island", 5);
                                            Link("Market", 6);
                                            Link("Just passing by...", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry. I cannot teleport you anywhere since you do not have 100 silvers!");
                                            Link("I'll fix that", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 1: //phoenix castle
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1002, 958, 555);
                                            Money(-100);
                                        }
                                        break;
                                    }
                                case 2: //ape mountain
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1002, 555, 957);
                                            Money(-100);
                                        }
                                        break;
                                    }
                                case 3: //mine cave
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1002, 053, 399);
                                            Money(-100);
                                        }
                                        break;
                                    }
                                case 4: //desert city
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1002, 069, 473);
                                            Money(-100);
                                        }
                                        break;
                                    }
                                case 5: //bird island
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1002, 232, 190);
                                            Money(-100);
                                        }
                                        break;
                                    }
                                case 6: //Market
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1036, 212, 195);
                                            Money(-100);
                                        }
                                        break;
                                    }
                            }

                            return;
                        }
                    #endregion
                    #region DC - Conductress
                    case 10051:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Say("I am the Desert City Conductress. I can teleport you for a price of 100 silvers.");
                                            Link("Twin City", 1);
                                            Link("Market", 2);
                                            Link("Mystic Castle", 3);
                                            Link("Nevermind!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry. I cannot teleport you anywhere since you do not have 100 silvers!");
                                            Link("I'll fix that", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 1: //Twin City
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1000, 970, 666);
                                            Money(-100);
                                        }
                                        break;
                                    }
                                case 2: //Market
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1036, 212, 195);
                                            Money(-100);
                                        }
                                        break;
                                    }
                                case 3: //Mystic Castle
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1000, 80, 320);
                                            Money(-100);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region PC - Conductress
                    case 10052:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Say("I am the Phoenix Castle Conductress. I can teleport you for a price of 100 silvers.");
                                            Link("Twin City", 1);
                                            Link("Market", 2);
                                            Link("Nevermind!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry. I cannot teleport you anywhere since you do not have 100 silvers!");
                                            Link("I'll fix that", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 1: //Twin City
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1011, 11, 376);
                                            Money(-100);
                                        }
                                        break;
                                    }
                                case 2: //Market
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1036, 212, 195);
                                            Money(-100);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region AC - Conductress
                    case 10053:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Say("I am the Ape City Conductress. I can teleport you for a price of 100 silvers.");
                                            Link("Twin City", 1);
                                            Link("Market", 2);
                                            Link("Nevermind!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry. I cannot teleport you anywhere since you do not have 100 silvers!");
                                            Link("I'll fix that", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 1: //Twin City
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1020, 381, 20);
                                            Money(-100);
                                        }
                                        break;
                                    }
                                case 2: //Market
                                    {
                                        if (Client.Silvers >= 100)
                                        {
                                            Teleport(1036, 212, 195);
                                            Money(-100);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region TC -> DC Conductress
                    case 10054:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Hello, I can show you the way to Desert City. Would you like to know it?");
                                        Link("Yes, please show me it.", 1);
                                        Link("I'll take a look around here.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        Say("Be careful. You dont know what danger is in Desert City.");
                                        Link("Okay.", 255);
                                        Finish(npc);
                                        Teleport(1000, 965, 664);
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region Mark. NpcDialogler
                    case 45:
                        {
                            switch (Option)
                            {
                                case 0:
                                    Say("Hello I can teleport you outside of the market for free! Do you want to leave?");
                                    Link("Why would you be so kind to do so?", 1);
                                    Link("No, Thank you anyway.", 255);
                                    Finish(npc);
                                    break;
                                case 1:
                                    switch (Client.LastMap)
                                    {
                                        case 1002:
                                            Teleport(1002, 430, 378);
                                            break;
                                        case 1015:
                                            Teleport(1015, 717, 577);
                                            break;
                                        case 1000:
                                            Teleport(1000, 496, 650);
                                            break;
                                        case 1011:
                                            Teleport(1011, 193, 266);
                                            break;
                                        case 1020:
                                            Teleport(1020, 566, 565);
                                            break;
                                        default:
                                            Say("Something went wrong. I've sent you to Twin City.");
                                            Link("Oh okay.", 255);
                                            Finish(npc);
                                            Teleport(1002, 430, 378);
                                            break;
                                    }
                                    break;
                            }
                            break;
                        }
                    #endregion
                    #region SurgeonMiracle
                    case 3381:
                        {
                            switch (Option)
                            {
                                case 0:
                                    Say("Do you want to have your size changed so that you can become much more adorable? Now here is a precious chance for you.");
                                    Link("I want to change my size.", 1);
                                    Link("I don't want to change", 255);
                                    Finish(npc);
                                    break;
                                case 1:
                                    Say("If you pay me one dragonball, I can have your size changed. You will become more attractive and start a fresh life. By the way, to avoid some unexpected things, make sure you are not in any disguise form.");
                                    Link("Here is a dragonball", 2);
                                    Link("I have no dragonball", 255);
                                    Finish(npc);
                                    break;
                                case 2:
                                    if (Client.InventoryContains(1088000))
                                    {
                                        Client.RemoveInventory(1088000);
                                        if (Client.Hero.Sex == 2001)
                                        {
                                            Client.Hero.Sex = 2002;
                                        }
                                        else if (Client.Hero.Sex == 2002)
                                        {
                                            Client.Hero.Sex = 2001;
                                        }
                                        else if (Client.Hero.Sex == 1003)
                                        {
                                            Client.Hero.Sex = 1004;
                                        }
                                        else if (Client.Hero.Sex == 1004)
                                        {
                                            Client.Hero.Sex = 1003;
                                        }
                                        else
                                        {
                                            Say("Sorry, something has gone terribly wrong with the procedure. Please try again.");
                                            Link("Okay...", 255);
                                            Finish(npc);
                                            Client.AddInventory(1088000);
                                            return;
                                        }
                                        Client.SendScreen(new StatTypePacket(Client.Hero.UID, Client.Hero.Sex, StatIDs.Model).Serialize(), true);
                                        Say("Congratulations! The procedure was a success!");
                                        Link("Thank you.", 255);
                                        Finish(npc);
                                    }
                                    else
                                    {
                                        Say("Sorry, there is no dragonball in your inventory. I can't have your size changed.");
                                        Link("I see.", 255);
                                        Finish(npc);
                                    }
                                    break;
                            }
                            break;
                        }
                    #endregion
                    #region Jail
                    #region CaptiainLi - outside of Jail
                    case 43:
                        {
                            if (Option == 0)
                            {
                                Say("Do you want to visit the jail?");
                                Link("Yes.", 1);
                                Link("Forget it.", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                Teleport(6000, 28, 71);
                            }
                            break;
                        }
                    #endregion
                    #region Warden - inside of Jail
                    case 42:
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.PkPoints > 99)
                                {
                                    Say("You are a killer! But I can't keep you here...");
                                    Link("Let Me Be Free", 1);
                                }
                                else
                                {
                                    Say("Do you want to leave now?");
                                    Link("Yeah.", 1);
                                }
                                Link("Just passing by.", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                Teleport(1002, 516, 356);
                            }
                            break;
                        }
                    #endregion
                    #region Warden - ID 140
                    case 140:
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.PkPoints > 99)
                                {
                                    Say("You are a killer! But I can't keep you here...");
                                    Link("Let Me Be Free", 1);
                                }
                                else
                                {
                                    Say("Do you want to leave now?");
                                    Link("Yeah.", 1);
                                }
                                Link("Just passing by.", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                Teleport(1002, 516, 356);
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region GeneralQing
                    case 30084:
                        {
                            if (Option == 0)
                            {
                                Say("I have sent my soldiers out to fetch the monthly provisions with my army token, but I have received no news since they left.");
                                Link("They must have troubles", 255);
                                Link("All will be fine", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    #endregion
                    #region Guild Map NpcDialogs
                    #region GuildWar Entrance
                    case 380:
                        {
                            if (Option == 0)
                            {
                                Say("Hello, I can send you to the guild area for free. Would you like to go?");
                                Link("Yes, please send me there.", 1);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                Teleport(1038, 349, 340);
                            }
                            break;
                        }
                    #endregion
                    #region GuildGateKeeper
                    case 7000:
                        {
                            switch (Option)
                            {
                                case 0:
                                    Say("Do you want to leave the guild area?");
                                    Link("Yes please.", 1);
                                    Link("I'd like to stay.", 255);
                                    Finish(npc);
                                    break;
                                case 1:
                                    Teleport(1002, 430, 378);
                                    break;
                            }
                            break;
                        }
                    #endregion

                    #region Conducter 1 GW Map
                    case 159751:
                        {
                            if (Option == 0)
                            {
                                Say("Do you want to go to Advanced Zone (canyon map)?");
                                Link("Yes, Please", 1);
                                Link("No thanks", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                if (Client.Silvers >= 1000)
                                {
                                    Client.Silvers -= 1000;
                                    Client.Teleport(1217, 534, 558);
                                }
                                else
                                {
                                    Say("You don't have 1000 silvers.");
                                    Link("Oh, I see.", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Conducter 2 GW Map
                    case 159750:
                        {
                            if (Option == 0)
                            {
                                Say("Do you want to go to Stone City?");
                                Link("Yes, Please", 1);
                                Link("No Thanks", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                if (Client.Silvers >= 1000)
                                {
                                    Client.Silvers -= 1000;
                                    Client.Teleport(1213, 450, 270);
                                }
                                else
                                {
                                    Say("You don't have 1000 silvers.");
                                    Link("Oh, I see.", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Conducter 3 GW Map
                    case 159749:
                        {
                            if (Option == 0)
                            {
                                Say("Do you want to go to Mystic Castle");
                                Link("Yes, Please", 1);
                                Link("No Thanks", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                if (Client.Silvers >= 1000)
                                {
                                    Client.Silvers -= 1000;
                                    Client.Teleport(1001, 334, 322);
                                }
                                else
                                {
                                    Say("You don't have 1000 silvers.");
                                    Link("Oh, I see.", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Conducter 4 GW Map
                    case 159748:
                        {
                            if (Option == 0)
                            {
                                Say("Do you want to go to Adventure Zone (plains)?");
                                Link("Yes, Please", 1);
                                Link("No Thanks", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                if (Client.Silvers >= 1000)
                                {
                                    Client.Silvers -= 1000;
                                    Client.Teleport(1216, 624, 753);
                                }
                                else
                                {
                                    Say("You don't have 1000 silvers.");
                                    Link("Oh, I see.", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion

                    #region Conducter1-4 NPC Return
                    case 159752://GC1
                    case 159753://GC2
                    case 159754://GC3
                    case 159755://GC4
                        {
                            switch (Option)
                            {
                                case 0:
                                    Say("Would you like to go back to the Guild Map?");
                                    Link("Yes", 1);
                                    Link("I'll stay here", 255);
                                    Finish(npc);
                                    break;
                                case 1:
                                    Teleport(1038, 348, 339);
                                    break;

                            }
                            break;
                        }
                    #endregion

                    #endregion
                    #region Guild Creation Guy
                    case 10003:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Hello. I am in charge of all the guilds, and I have the power to create and destroy them!");
                                        Link("Create Guild", 1);
                                        Link("Disband My Guild", 2);
                                        Link("Inquire a Guild", 3);
                                        Link("More options.", 17);
                                        Link("Just Passing By...", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 17:
                                    {
                                        Say("Hello. I am in charge of all the guilds, and I have the power to create and destroy them!");
                                        Link("Give Dep. Leader", 4);
                                        Link("Add Ally", 10);
                                        Link("Remove Ally", 11);
                                        Link("Add Enemy", 13);
                                        Link("Remove Enemy", 15);
                                        Link("Previous options..", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        if (Client.Hero.GuildID == 0)
                                        {
                                            Say("To create a guild, you must be at least level 90 and have 1,000,000 silvers. Are you sure you want to continue?");
                                            Link("Yes.", 5);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry. You are already a part of a guild. You need to leave your current guild before you create one");
                                            Link("Okay.", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 2:
                                    {
                                        if (Client.Hero.GuildID != 0)
                                        {
                                            if (Client.Guild.Leader == Client.Hero.Name)
                                            {
                                                Say("Are you sure you want to disband your guild? Anyone associated with, all silvers, and any Option you currently have will be gone.");
                                                Link("Yes", 6);
                                                Link("No.", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("Sorry. You cannot disband your guild as you are not the Guild Leader!");
                                                Link("Darn!", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else
                                        {
                                            Say("You can't disband a non-existant Guild!");
                                            Link("Okay.", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 3:
                                    {
                                        Say("Please type in the guild name you'd like to inquire about below.");
                                        Input("", 8);
                                        Finish(npc);
                                        break;
                                    }
                                case 4:
                                    {
                                        Say("Please type in the user you'd like to make a Deputy Leader. This user must be online.");
                                        Input("", 9);
                                        Finish(npc);
                                        break;
                                    }
                                case 5:
                                    {
                                        if ((Client.Hero.Level >= 90) && (Client.Silvers >= 100000))
                                        {
                                            Say("Guilds are a powerful way for a group to stay connected & fight for a common belief. Usually, strong and powerful guilds have meaningful names. Choose your name wisely.");
                                            Input("", 7);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            if (Client.Hero.Level < 90)
                                            {
                                                Say("Sorry. You are below level 90 and cannot create a guild.");
                                                Link("Damn!", 255);
                                                Finish(npc);
                                            }
                                            if (Client.Silvers < 1000000)
                                            {
                                                Say("Sorry. You have less than 1,000,000 silvers and therefore cannot create a guild.");
                                                Link("Damn!", 255);
                                                Finish(npc);
                                            }
                                        }
                                        break;
                                    }
                                case 6:
                                    {
                                        if (Client.Guild == null)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You do not currently have a guild.");
                                            return;
                                        }
                                        if (Client.Guild.Leader != Client.Hero.Name)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You are not the guild leader.");
                                            return;
                                        }

                                        ConquerGuild.DisbandGuild(Client);

                                        Say("Your Guild has been disbanded.");
                                        Link("Thank you!", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 7:
                                    {
                                        if ((Client.Hero.Level >= 90) && (Client.Silvers >= 100000))
                                        {
                                            if (InputText.Length < 2 || InputText.Length >= 16)
                                            {
                                                Client.Speak(Color.Red, ChatType.Talk, "Invalid guild name.");
                                                return;
                                            }
                                            if (!FlatDatabase.ValidName(InputText, false))
                                            {
                                                Client.Speak(Color.Red, ChatType.Talk, "Invalid guild name.");
                                                return;
                                            }
                                            Money(-1000000);
                                            ConquerGuild.Create(InputText, Client);
                                        }
                                        break;
                                    }
                                case 8:
                                    {
                                        var Guild = Kernel.FGuild(InputText);
                                        if (Guild != null)
                                        {
                                            Say($"Guild Name: {Guild.Name}; Guild Leader: {Guild.Leader}; Members: {Guild.Members.Count}; Guild Fund: {Guild.Fund}");
                                            Link("Okay.", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry. The guild you checked does not exist. Please try again.");
                                            Link("Try Again", 3);
                                            Link("No Thanks", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 9:
                                    {
                                        if (Client.Guild == null)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You do not currently have a guild.");
                                            return;
                                        }
                                        if (Client.Guild.Leader != Client.Hero.Name)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You are not the guild leader.");
                                            return;
                                        }
                                        if (Client.Guild.DeputyCount >= 6)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "Your Deputy positions are full.");
                                            return;
                                        }

                                        foreach (var DE in Client.Guild.Members)
                                        {
                                            var Content = DE.Value.Split('~');
                                            var Member = Kernel.FindClient(uint.Parse(Content[1]));

                                            if (Member == null)
                                            {
                                                Client.Speak(Color.Yellow, ChatType.Talk, "That member is currently offline.");
                                                return;
                                            }
                                            if (Member.Hero.Name == InputText)
                                            {
                                                if (Member.Hero.GuildRank != GuildPosition.DeputyLeader)
                                                {
                                                    Client.Guild.DeputyCount++;
                                                    Member.Hero.GuildRank = GuildPosition.DeputyLeader;
                                                    Member.SendScreen(Member.Hero.SpawnPacket, false);
                                                    Member.Send(Packets.GuildInfo(Member.Guild, Member));
                                                    Client.Speak(Color.Yellow, ChatType.Guild, $"{InputText} has been promoted to DeputyLeader.");
                                                }
                                            }
                                        }
                                        break;
                                    }
                                case 10:
                                    {
                                        if (Client.Guild == null)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You do not currently have a guild.");
                                            return;
                                        }
                                        if (Client.Guild.Leader != Client.Hero.Name)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You are not the guild leader.");
                                            return;
                                        }
                                        if (Client.Team == null)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You are not in a team.");
                                            return;
                                        }
                                        if (Client.Team.TeamDictionary.Count > 2)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You and the other Guild Leader need to be alone in the team.");
                                            return;
                                        }
                                        foreach (var member in Client.Team.TeamDictionary.Values)
                                        {
                                            if (member != Client)
                                            {
                                                if (member.Guild.Leader != member.Hero.Name)
                                                {
                                                    Client.Speak(Color.Yellow, ChatType.Talk, "They are not the leader of there guild.");
                                                    return;
                                                }
                                                if (member.Guild.Enemies.Contains(Client.Guild.Name))
                                                {
                                                    member.Speak(Color.Yellow, ChatType.Talk, "Please remove this guild as an enemy first.");
                                                    return;
                                                }
                                                if (Client.Guild.Enemies.Contains(member.Guild.Name))
                                                {
                                                    Client.Speak(Color.Yellow, ChatType.Talk, "Please remove this guild as an enemy first.");
                                                    return;
                                                }

                                                var GPacket = GuildPacket.Create();
                                                GPacket.UID = member.Hero.GuildID;
                                                GPacket.ID = GuildIds.Allied;
                                                Client.Send(GPacket, GPacket.Size);

                                                GPacket.UID = Client.Hero.GuildID;
                                                member.Send(GPacket, GPacket.Size);

                                                if (!Client.Guild.Allies.Contains(member.Guild.Name))
                                                    Client.Guild.Allies.Add(member.Guild.Name);
                                                if (member.Guild.Allies.Contains(member.Guild.Name))
                                                    member.Guild.Allies.Add(Client.Guild.Name);
                                            }
                                        }
                                        break;
                                    }
                                case 11:
                                    {
                                        Say("Please type in the guild you'd like to remove as an ally.");
                                        Input("", 12);
                                        Finish(npc);
                                        break;
                                    }
                                case 12:
                                    {
                                        if (Client.Guild == null)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You do not currently have a guild.");
                                            return;
                                        }
                                        if (Client.Guild.Leader != Client.Hero.Name)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You are not the guild leader.");
                                            return;
                                        }
                                        if (!Client.Guild.Allies.Contains(InputText))
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You're not allied to this guild.");
                                            return;
                                        }
                                        var guild = Kernel.Guilds.Values.FirstOrDefault(x => x.Name == InputText);
                                        if (guild != null)
                                        {
                                            var GPacket = GuildPacket.Create();
                                            GPacket.UID = guild.UID;
                                            GPacket.ID = GuildIds.Neutral;
                                            Client.Send(GPacket, GPacket.Size);

                                            Client.Guild.Allies.Remove(InputText);
                                        }
                                        break;
                                    }
                                case 13:
                                    {
                                        Say("Please type in the guild you'd like to add as an enemy.");
                                        Input("", 14);
                                        Finish(npc);
                                        break;
                                    }
                                case 14:
                                    {
                                        if (Client.Guild == null)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You do not currently have a guild.");
                                            return;
                                        }
                                        if (Client.Guild.Leader != Client.Hero.Name)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You are not the guild leader.");
                                            return;
                                        }
                                        if (Client.Guild.Allies.Contains(InputText))
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "Please remove them as an ally first.");
                                            return;
                                        }
                                        if (Client.Guild.Enemies.Contains(InputText))
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "This guild is already enemied.");
                                            return;
                                        }
                                        var guild = Kernel.Guilds.Values.FirstOrDefault(x => x.Name == InputText);
                                        if (guild != null)
                                        {
                                            var GPacket = GuildPacket.Create();
                                            GPacket.UID = guild.UID;
                                            GPacket.ID = GuildIds.Enemied;
                                            Client.Send(GPacket, GPacket.Size);

                                            Client.Guild.Enemies.Add(guild.Name);
                                        }
                                        break;
                                    }
                                case 15:
                                    {
                                        Say("Please type in the guild you'd like to remove as an enemy.");
                                        Input("", 16);
                                        Finish(npc);
                                        break;
                                    }
                                case 16:
                                    {
                                        if (Client.Guild == null)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You do not currently have a guild.");
                                            return;
                                        }
                                        if (Client.Guild.Leader != Client.Hero.Name)
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "You are not the guild leader.");
                                            return;
                                        }
                                        if (!Client.Guild.Enemies.Contains(InputText))
                                        {
                                            Client.Speak(Color.Yellow, ChatType.Talk, "This guild isn't enemied.");
                                            return;
                                        }
                                        var guild = Kernel.Guilds.Values.FirstOrDefault(x => x.Name == InputText);
                                        if (guild != null)
                                        {
                                            var GPacket = GuildPacket.Create();
                                            GPacket.UID = guild.UID;
                                            GPacket.ID = GuildIds.Neutral;
                                            Client.Send(GPacket, GPacket.Size);

                                            Client.Guild.Enemies.Remove(guild.Name);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Rebirth NPC
                    case 300500:
                        {
                            if (Option == 0)
                            {
                                Say("I devote all my life to the research of eternity, finally I know the arcanum \n of rebirth of life.");
                                Link("I would like to know it.", 1);
                                Link("I was just passing by.", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                Say("If you want to rebirth, you should reach a certain level, get the highest occupation");
                                Say("title and get a CelestialStone. After the rebirth you can distribute your attribute more freely.");
                                Say("And you can learn more powerfull skills.");
                                Link("What is CelestialStone?", 10);
                                Link("More powerful Skills?", 11);
                                Link("Let me think it over.", 255);
                                Link("Allot attribute freely.", 12);
                                Link("I would like to reborn.", 13);
                                Finish(npc);
                            }
                            else if (Option == 10)
                            {
                                Say("CelestialStone Syncretizes seven gems in the world, and I will pave the way for rebirth.");
                                Link("I See.", 255);
                                Finish(npc);
                            }
                            else if (Option == 11)
                            {
                                Say("The Skills before the rebirth will disappear. But some only disappear for a while,");
                                Say("and it will come back when the levelreaches half of the preexistance;");
                                Say("you can learn a new skill if you remain the same occupation as preexistance.");
                                Link("I See.", 255);
                                Finish(npc);
                            }
                            else if (Option == 12)
                            {
                                Say("After the rebirth you start at level 15, and get some bonus points.  You can");
                                Say("distribute every ability point gained in level up.");
                                Link("I See.", 255);
                                Finish(npc);
                            }

                            else if (Option == 13)
                            {
                                Say("There are two ways to get reborn.");
                                Link("Blessed Rebirth", 31);
                                Link("Normal Rebirth", 32);
                                Finish(npc);
                            }

                            else if ((Option == 31) || (Option == 32))
                            {
                                Say("While going through the rebirth you can change your class if you'd like.");
                                Say("What class would you like to reborn to?");
                                Link("Trojan.", 101);
                                Link("Warrior.", 102);
                                Link("Archer.", 104);
                                Link("Ninja.", 105);
                                Link("WaterTaoist.", 113);
                                Link("FireTaoist.", 114);
                                Finish(npc);
                            }
                            if ((Client.Hero.Reborn == 0) && (Client.Hero.Level >= (byte)(Kernel.GetBaseClass(Client.Job) == 100 ? 110 : 120)))
                            {
                                if (((Option >= 101) && (Option <= 105)) || (Option == 113) || (Option == 114))
                                {
                                    if (Client.TryExpendItemOfId(721259, 1))
                                    {
                                        var NewJob = (byte)((Option - 100) * 10);
                                        if ((Option >= 101) && (Option <= 105))
                                            NewJob++;
                                        else
                                            NewJob += 2;
                                        StatusChange.Reborn(Client, NewJob);
                                    }
                                    else
                                    {
                                        Say("You lack the required CelestialStone.");
                                        Link("Sorry.", 255);
                                        Finish(npc);
                                    }
                                }
                            }
                            else
                            {
                                Say("You don't meet the requirments.");
                                Link("Oh really..", 255);
                                Finish(npc);
                            }
                        }
                        break;
                    #endregion
                    #region Promotion Center
                    #region WarriorGod Promo
                    case 10001:
                        {
                            if ((Client.Job >= 20) && (Client.Job <= 25))
                            {
                                if (Option == 0)
                                {
                                    Say("What can I do for you, mighty warrior?\n");
                                    Link("Get promoted.", 1);
                                    Link("Learn skills.", 2);
                                    Link("Nothing.", 255);
                                    Finish(npc);
                                }
                                else if (Option == 1)
                                {
                                    if (Client.Job == 20)
                                    {
                                        if (Client.Hero.Level >= 15)
                                        {
                                            Client.Job += 1;
                                            Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                            Say("Congrats! You have been promoted!");
                                            Link("Thanks!!!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You must be at least level 15, sorry.");
                                            Link("Okay, thanks.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 21)
                                    {
                                        if (Client.Hero.Level >= 40)
                                        {
                                            Client.Job += 1;
                                            Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                            Say("Congrats! You have been promoted!");
                                            Link("Thanks!!!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You must be at least level 40, sorry.");
                                            Link("Okay, thanks.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 22)
                                    {
                                        if (Client.Hero.Level >= 70)
                                        {
                                            if (Client.InventoryContains(1080001))
                                            {
                                                Client.RemoveInventory(1080001);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Thanks!!!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                ErrorMsg(npc, "I'm sorry but you need an emerald to promote. Go get it.");
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 70, sorry.");
                                            Link("Okay, thanks.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 23)
                                    {
                                        if (Client.Hero.Level >= 100)
                                        {
                                            if (Client.InventoryContains(1088001))
                                            {
                                                Client.RemoveInventory(1088001);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Thanks!!!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                ErrorMsg(npc, "I'm sorry but you need a Meteor to promote. Go get one!");
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 100, sorry.");
                                            Link("Okay, thanks.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 24)
                                    {
                                        if (Client.Hero.Level >= 110)
                                        {
                                            if (Client.InventoryContains(721020))
                                            {
                                                Client.RemoveInventory(721020);
                                                Client.AddInventory(1088000);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Thanks!!!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                ErrorMsg(npc, "I'm sorry but you need an moonbox to promote. Go get it.");
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 110, sorry.");
                                            Link("Okay, thanks.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You need not be promoted anymore!");
                                        Link("Okay, thanks.", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 2)
                                {
                                    Say("What skill would you like to learn, warrior?");
                                    Link("Accuracy[Level 3]", 3);
                                    Link("Shield[Level 3]", 4);
                                    Link("Superman[Level 3]", 5);
                                    Link("Roar[Level 3]", 6);
                                    Link("FlyingMoon[Level 40]", 7);
                                    Link("Dash[Level 63]", 8);
                                    Finish(npc);
                                }
                                else if (Option == 3)
                                {
                                    if (Client.Hero.Level >= 3)
                                    {
                                        ConquerSpell.LearnSpell(Client, 1015, 0);
                                        Say("Congrats! You have learned this skill.");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                    else
                                    {
                                        Say("You are not yet high enough level for this skill.");
                                        Link("Okay, sorry.", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 4)
                                {
                                    if (Client.Hero.Level >= 3)
                                    {
                                        ConquerSpell.LearnSpell(Client, 1020, 0);
                                        Say("Congrats! You have learned this skill.");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                    else
                                    {
                                        Say("You are not yet high enough level for this skill.");
                                        Link("Okay, sorry.", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 5)
                                {
                                    if (Client.Hero.Level >= 3)
                                    {
                                        ConquerSpell.LearnSpell(Client, 1025, 0);
                                        Say("Congrats! You have learned this skill.");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                    else
                                    {
                                        Say("You are not yet high enough level for this skill.");
                                        Link("Okay, sorry.", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 6)
                                {
                                    ConquerSpell.LearnSpell(Client, 1040, 0);
                                    Say("Congrats! You have learned this skill.");
                                    Link("Thanks!", 255);
                                    Finish(npc);
                                }
                                else
                                {
                                    Say("You are not yet high enough level for this skill.");
                                    Link("Okay, sorry.", 255);
                                    Finish(npc);
                                }
                            }
                            else if (Option == 7)
                            {
                                if (Client.Hero.Level >= 40)
                                {
                                    ConquerSpell.LearnSpell(Client, 1320, 0);
                                    Say("Congrats! You have learned this skill.");
                                    Link("Thanks!", 255);
                                    Finish(npc);
                                }
                                else
                                {
                                    Say("You are not yet high enough level for this skill.");
                                    Link("Okay, sorry.", 255);
                                    Finish(npc);
                                }
                            }
                            else if (Option == 8)
                            {
                                if (Client.Hero.Level >= 63)
                                {
                                    ConquerSpell.LearnSpell(Client, 1051, 0);
                                    Say("Congrats! You have learned this skill.");
                                    Link("Thanks!", 255);
                                    Finish(npc);
                                }
                                else
                                {
                                    Say("You are not yet high enough level for this skill.");
                                    Link("Okay, sorry.", 255);
                                    Finish(npc);
                                }
                            }
                            else
                            {
                                Say("Please see your trainer NPC, I can only train warriors! Get lost.");
                                Link("Okay, sorry.", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    #endregion
                    #region ArcherGod
                    case 400:
                        {
                            if ((Client.Job >= 40) && (Client.Job <= 45))
                            {
                                if (Option == 0)
                                {
                                    Say("What can I do for you, speedy archer?\n");
                                    Link("Get promoted.", 1);
                                    Link("Learn skills.", 2);
                                    Link("Nothing.", 255);
                                    Finish(npc);
                                }
                                else if (Option == 1)
                                {
                                    if (Client.Job == 40)
                                    {
                                        if (Client.Hero.Level >= 15)
                                        {
                                            Client.Job += 1;
                                            Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                            Say("Congrats! You have been promoted!");
                                            Link("Yay!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You must be at least level 15, sorry.");
                                            Link("Damn.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 41)
                                    {
                                        if (Client.Hero.Level >= 40)
                                        {
                                            if (Client.InventoryContains(1072031))
                                            {
                                                Client.RemoveInventory(1072031);
                                                Client.RemoveInventory(1072031);
                                                Client.RemoveInventory(1072031);
                                                Client.RemoveInventory(1072031);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Yay!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("Sorry. You need to have 4 Euxenite Ores to be promoted!");
                                                Link("Damn!", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 40, sorry.");
                                            Link("Damn", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 42)
                                    {
                                        if (Client.Hero.Level >= 70)
                                        {
                                            if (Client.InventoryContains(1080001))
                                            {
                                                Client.RemoveInventory(1080001);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Yay!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                ErrorMsg(npc, "I'm sorry but you need an emerald to be promoted.");
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 70, sorry.");
                                            Link("Damn", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 43)
                                    {
                                        if (Client.Hero.Level >= 100)
                                        {
                                            if (Client.InventoryContains(1088001))
                                            {
                                                Client.RemoveInventory(1088001);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Yay!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                ErrorMsg(npc, "I'm sorry but you need a meteor to be promoted.");
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 100, sorry.");
                                            Link("Okay, thanks.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 44)
                                    {
                                        if (Client.Hero.Level >= 120)
                                        {
                                            if (Client.InventoryContains(721020))
                                            {
                                                Client.RemoveInventory(721020);
                                                Client.AddInventory(1088000);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Thanks!!!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                ErrorMsg(npc, "I'm sorry but you need a moonbox to be promoted.");
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 110, sorry.");
                                            Link("Damn", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You are already promoted to the highest ranking for your level!");
                                        Link("Yay!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 2)
                                {
                                    Say("What skill would you like to learn, archer?");
                                    Link("Elementary Fly [Level 15]", 3);
                                    Link("Scatter [Level 23]", 4);
                                    Link("Rapid Fire [Level 46]", 5);
                                    Link("Arrow Rain [Level 70]", 6);
                                    //Link("Improved Fly [Level 70]", 7);
                                    Link("Intensify [Level 71]", 7);
                                    Link("Advanced Fly [Level 100]", 8);
                                    Finish(npc);
                                }
                                else if (Option == 3)
                                {
                                    if (Client.Hero.Level >= 15) //Elementary Fly (XP)
                                    {
                                        ConquerSpell.LearnSpell(Client, 8002, 0);
                                        Say("Congrats! You have learned this skill.");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                    else
                                    {
                                        Say("You are not yet high enough level for this skill!");
                                        Link("Okay, sorry.", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 4) //Scatter
                                {
                                    if (Client.Hero.Level >= 23)
                                    {
                                        ConquerSpell.LearnSpell(Client, 8001, 0);
                                        Say("Congrats! You have learned this skill.");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                    else
                                    {
                                        Say("You are not yet high enough level for this skill!");
                                        Link("Okay, sorry.", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 5) //Rapid Fire
                                {
                                    if (Client.Hero.Level >= 46)
                                    {
                                        ConquerSpell.LearnSpell(Client, 8000, 0);
                                        Say("Congrats! You have learned this skill.");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                    else
                                    {
                                        Say("You are not yet high enough level for this skill.");
                                        Link("Okay, sorry.", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 6) //Arrow Rain
                                {
                                    if (Client.Hero.Level >= 70)
                                    {
                                        ConquerSpell.LearnSpell(Client, 8030, 0);
                                        Say("Congrats! You have learned this skill.");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                    else
                                    {
                                        Say("You are not yet high enough level for this skill.");
                                        Link("Okay, sorry.", 255);
                                        Finish(npc);
                                    }
                                }
                            }
                            else if (Option == 7) //Intensify
                            {
                                if (Client.Hero.Level >= 71)
                                {
                                    ConquerSpell.LearnSpell(Client, 9000, 0);
                                    Say("Congrats! You have learned this skill.");
                                    Link("Thanks!", 255);
                                    Finish(npc);
                                }
                                else
                                {
                                    Say("You are not yet high enough level for this skill.");
                                    Link("Okay, sorry.", 255);
                                    Finish(npc);
                                }
                            }
                            else if (Option == 8) //Advanced Fly
                            {
                                if (Client.Hero.Level >= 100)
                                {
                                    ConquerSpell.LearnSpell(Client, 8003, 0);
                                    Say("Congrats! You have learned this skill.");
                                    Link("Thanks!", 255);
                                    Finish(npc);
                                }
                                else
                                {
                                    Say("You are not yet high enough level for this skill.");
                                    Link("Okay, sorry.", 255);
                                    Finish(npc);
                                }
                            }
                            else
                            {
                                Say("Sorry. I can not teach you, as you are not an archer!");
                                Link("Okay, sorry.", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    #endregion
                    #region TrojanStar
                    case 10022:
                        {
                            if ((Client.Job >= 10) && (Client.Job <= 15))
                            {
                                if (Option == 0)
                                {
                                    Say("What can I do for you, powerful trojan?\n");
                                    Link("Get promoted.", 1);
                                    Link("Learn skills.", 2);
                                    Link("Nothing.", 255);
                                    Finish(npc);
                                }
                                else if (Option == 1)
                                {
                                    if (Client.Job == 10)
                                    {
                                        if (Client.Hero.Level >= 15)
                                        {
                                            Client.Job += 1;
                                            Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                            Say("Congrats! You have been promoted!");
                                            Link("Yay!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You must be at least level 15, sorry.");
                                            Link("Damn.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 11)
                                    {
                                        if (Client.Hero.Level >= 40)
                                        {
                                            Client.Job += 1;
                                            Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                            Say("Congrats! You have been promoted!");
                                            Link("Yay!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You must be at least level 40, sorry.");
                                            Link("Damn", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 12)
                                    {
                                        if (Client.Hero.Level >= 70)
                                        {
                                            if (Client.InventoryContains(1080001))
                                            {
                                                Client.RemoveInventory(1080001);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Yay!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                ErrorMsg(npc, "I'm sorry but you need an emerald to be promoted.");
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 70, sorry.");
                                            Link("Damn", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 13)
                                    {
                                        if (Client.Hero.Level >= 100)
                                        {
                                            if (Client.InventoryContains(1088001))
                                            {
                                                Client.RemoveInventory(1088001);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Yay!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                ErrorMsg(npc, "I'm sorry but you need a meteor to be promoted.");
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 100, sorry.");
                                            Link("Okay, thanks.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 14)
                                    {
                                        if (Client.Hero.Level >= 120)
                                        {
                                            if (Client.InventoryContains(721020))
                                            {
                                                Client.RemoveInventory(721020);
                                                Client.AddInventory(1088000);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Thanks!!!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                ErrorMsg(npc, "I'm sorry but you need a moonbox to be promoted.");
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 120, sorry.");
                                            Link("Damn", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You are already promoted to the highest ranking for your level!");
                                        Link("Yay!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 2)
                                {
                                    Say("What skill would you like to learn, archer?");
                                    Link("Hercules [Level 40]", 3);
                                    Link("Spirit Healing [Level 40]", 4);
                                    Link("Accuracy [Level 1]", 5);
                                    Link("Cyclone [Level 1]", 6);
                                    Link("Robot [Level 45]", 7);
                                    Finish(npc);
                                }
                                else if (Option == 3)
                                {
                                    if (Client.Hero.Level >= 40) //Hercules (XP)
                                    {
                                        ConquerSpell.LearnSpell(Client, 1115, 0);
                                        Say("Congrats! You have learned this skill.");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                    else
                                    {
                                        Say("You are not yet high enough level for this skill!");
                                        Link("Okay, sorry.", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 4) //Spirit Healing
                                {
                                    if (Client.Hero.Level >= 40)
                                    {
                                        ConquerSpell.LearnSpell(Client, 1190, 0);
                                        Say("Congrats! You have learned this skill.");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                    else
                                    {
                                        Say("You are not yet high enough level for this skill!");
                                        Link("Okay, sorry.", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 5) //Accuracy
                                {
                                    ConquerSpell.LearnSpell(Client, 1015, 0);
                                    Say("Congrats! You have learned this skill.");
                                    Link("Thanks!", 255);
                                    Finish(npc);
                                }
                                else if (Option == 6) //Cyclone
                                {
                                    ConquerSpell.LearnSpell(Client, 1110, 0);
                                    Say("Congrats! You have learned this skill.");
                                    Link("Thanks!", 255);
                                    Finish(npc);
                                }
                            }
                            else if (Option == 7) //Robot
                            {
                                if (Client.Hero.Level >= 45)
                                {
                                    ConquerSpell.LearnSpell(Client, 1270, 0);
                                    Say("Congrats! You have learned this skill.");
                                    Say("Please note: Transformation is currently not implemented; this skill is for future use");
                                    Link("Thanks!", 255);
                                    Finish(npc);
                                }
                                else
                                {
                                    Say("You are not yet high enough level for this skill.");
                                    Link("Okay, sorry.", 255);
                                    Finish(npc);
                                }
                            }
                            else
                            {
                                Say("Sorry. I can not teach you, as you are not a trojan!");
                                Link("Okay, sorry.", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    #endregion
                    #region TaoistMoon
                    case 10000:
                        {
                            if (Kernel.GetBaseClass(Client.Job) == 100)
                            {
                                if (Option == 0)
                                {
                                    Say("Most of the Taoists are little concerned with anything outside of the pursuit of advanced spiritual powers.");
                                    Say("You are gifted in harnessing your inner power, but remember, the roots of successlie in thoroughness and attention to detail.");
                                    Link("I would like to be Promoted.", 1);
                                    Link("Learn Basic Spells.", 2);
                                    Link("Learn Advanced Spells", 3);
                                    Link("Thanks for the Advice!.", 255);
                                    Finish(npc);
                                }
                                else if (Option == 1)
                                {
                                    if (Client.Job == 100)
                                    {
                                        if (Client.Hero.Level >= 15)
                                        {
                                            Client.Job += 1;
                                            Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                            Say("Congrats! You have been promoted!");
                                            Link("Yay!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You must be at least level 15, sorry.");
                                            Link("Damn.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 101)
                                    {
                                        if (Client.Hero.Level >= 40)
                                        {
                                            Say("You have reached a very important time in your life!");
                                            Say("What type of Tao will you become?");
                                            Link("Water Taoist.", 132);
                                            Link("Fire Taoist.", 142);
                                            Link("I am not ready yet!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You must be at least level 40, sorry.");
                                            Link("Damn", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 132)
                                    {
                                        if (Client.Hero.Level >= 70)
                                        {
                                            if (Client.InventoryContains(1080001))
                                            {
                                                Client.RemoveInventory(1080001);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Yay!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("You need an Emerald to Promote!");
                                                Link("Sorry", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 70, sorry.");
                                            Link("Damn.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 133)
                                    {
                                        if (Client.Hero.Level >= 100)
                                        {
                                            if (Client.InventoryContains(1088001))
                                            {
                                                Client.RemoveInventory(1088001);
                                                Client.AddInventory(700031);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Yay!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("You need a Meteor to Promote!");
                                                Link("Sorry", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 100, sorry.");
                                            Link("Damn.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 134)
                                    {
                                        if (Client.Hero.Level >= 110)
                                        {
                                            if (Client.InventoryContains(721020))
                                            {
                                                Client.RemoveInventory(721020);
                                                Client.AddInventory(1088000);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Yay!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("You need a MoonBox to Promote!");
                                                Link("Sorry", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 110, sorry.");
                                            Link("Damn.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 142)
                                    {
                                        if (Client.Hero.Level >= 70)
                                        {
                                            if (Client.InventoryContains(1080001))
                                            {
                                                Client.RemoveInventory(1080001);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Yay!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("You need an Emerald to Promote!");
                                                Link("Sorry", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 70, sorry.");
                                            Link("Damn.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 143)
                                    {
                                        if (Client.Hero.Level >= 100)
                                        {
                                            if (Client.InventoryContains(1088001))
                                            {
                                                Client.RemoveInventory(1088001);
                                                Client.AddInventory(700031);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Yay!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("You need a Meteor to Promote!");
                                                Link("Sorry", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 100, sorry.");
                                            Link("Damn.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if (Client.Job == 144)
                                    {
                                        if (Client.Hero.Level >= 110)
                                        {
                                            if (Client.InventoryContains(721020))
                                            {
                                                Client.RemoveInventory(721020);
                                                Client.AddInventory(1088000);
                                                Client.Job += 1;
                                                Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                                Say("Congrats! You have been promoted!");
                                                Link("Yay!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("You need a MoonBox to Promote!");
                                                Link("Sorry", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else
                                        {
                                            Say("You must be at least level 110, sorry.");
                                            Link("Damn.", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else if ((Client.Job == 135) || (Client.Job == 145))
                                    {
                                        Say("You are already the highest rank for your class!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 2)
                                {
                                    Say("What spell would you like to learn?");
                                    Link("Basic Spells", 4);
                                    Link("Xp Skills", 5);
                                    Link("Fire", 6);
                                    Link("Meditation", 7);
                                    Link("I'm not ready, thanks.", 255);
                                    Finish(npc);
                                }
                                else if (Option == 3)
                                {
                                    if ((Client.Job >= 132) && (Client.Job <= 135))
                                    {
                                        Say("What spell would you like to learn?");
                                        Link("HealingRain", 8);
                                        Link("Star of Accuracy", 9);
                                        Link("Magic Shield", 10);
                                        Link("Stigma", 11);
                                        Link("More Skills", 12);
                                        Link("I'm not ready, thanks.", 255);
                                        Finish(npc);
                                    }
                                    else if ((Client.Job >= 142) && (Client.Job <= 145))
                                    {
                                        Say("What spell would you like to learn?");
                                        Link("Fire Ring", 18);
                                        Link("Fire Meteor", 19);
                                        Link("Fire Circle", 20);
                                        Link("Tornado", 21);
                                        Link("I'm not ready, thanks.", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 4)
                                {
                                    if (!Client.Spells.ContainsKey(1000))
                                        ConquerSpell.LearnSpell(Client, 1000, 0); // Thunder
                                    if (!Client.Spells.ContainsKey(1005))
                                        ConquerSpell.LearnSpell(Client, 1005, 0); // Cure
                                    Say("Congrats! You have learned Thunder and Cure.");
                                    Link("Thanks!", 255);
                                    Finish(npc);
                                }
                                else if (Option == 5)
                                {
                                    if (Client.Hero.Level >= 70)
                                    {
                                        ConquerSpell.LearnSpell(Client, 1010, 0); // Lightning
                                        ConquerSpell.LearnSpell(Client, 1125, 0); // Volcano
                                        ConquerSpell.LearnSpell(Client, 5001, 0); // Speed Lightning
                                        Say("Congrats! You have learned Xp Skills.");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                    else if (Client.Hero.Level >= 40)
                                    {
                                        ConquerSpell.LearnSpell(Client, 1010, 0); // Lightning
                                        ConquerSpell.LearnSpell(Client, 1125, 0); // Volcano
                                        if ((Client.Job >= 132) && (Client.Job <= 135))
                                            ConquerSpell.LearnSpell(Client, 1050, 0); // Revive
                                        Say("Congrats! You have learned Xp Skills.");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                    else if (Client.Hero.Level >= 15)
                                    {
                                        ConquerSpell.LearnSpell(Client, 1010, 0); // Lightning
                                        Say("Congrats! You have learned Xp Skills.");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 6)
                                {
                                    if (!Client.Spells.ContainsKey(1001))
                                    {
                                        if (Client.Spells[1000].Level == 4)
                                        {
                                            if (Client.Hero.Level >= 40)
                                            {
                                                ConquerSpell.LearnSpell(Client, 1001, 0); // Fire
                                                Say("Congrats! You have learned Fire!");
                                                Link("Thanks!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("You are not level 40 yet!");
                                                Link("Thanks!", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else
                                        {
                                            Say("You need to train your Thunder to level 4!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned Fire!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 7)
                                {
                                    if (!Client.Spells.ContainsKey(1195))
                                    {
                                        if (Client.Hero.Level >= 44)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1195, 0); // Meditation
                                            Say("Congrats! You have learned Meditation!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 44 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 8)
                                {
                                    if (!Client.Spells.ContainsKey(1055))
                                    {
                                        if (Client.Hero.Level >= 40)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1055, 0); // Healing Rain
                                            Say("Congrats! You have learned Healing Rain!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 40 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 9)
                                {
                                    if (!Client.Spells.ContainsKey(1085))
                                    {
                                        if (Client.Hero.Level >= 45)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1085, 0); // Star of Accuracy
                                            Say("Congrats! You have learned Star of Accuracy!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 45 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 10)
                                {
                                    if (!Client.Spells.ContainsKey(1090))
                                    {
                                        if (Client.Hero.Level >= 50)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1090, 0); // Magic Shield
                                            Say("Congrats! You have learned Magic Shield!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 50 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 11)
                                {
                                    if (!Client.Spells.ContainsKey(1095))
                                    {
                                        if (Client.Hero.Level >= 55)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1095, 0); // Stigma
                                            Say("Congrats! You have learned Stigma!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 55 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 12)
                                {
                                    Say("What spell would you like to learn?");
                                    Link("Invisibility", 13);
                                    Link("Pray", 14);
                                    Link("Advanced Cure", 15);
                                    Link("Nectar", 16);
                                    Link("Water Elf", 17);
                                    Link("Previous Spells", 3);
                                    Link("I'm not ready, thanks.", 255);
                                    Finish(npc);
                                }
                                else if (Option == 13)
                                {
                                    if (!Client.Spells.ContainsKey(1075))
                                    {
                                        if (Client.Hero.Level >= 60)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1075, 0); // Invisibility
                                            Say("Congrats! You have learned Invisibility!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 60 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 14)
                                {
                                    if (!Client.Spells.ContainsKey(1100))
                                    {
                                        if (Client.Hero.Level >= 70)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1100, 0); // Pray
                                            Say("Congrats! You have learned Pray!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 70 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 15)
                                {
                                    if (!Client.Spells.ContainsKey(1175))
                                    {
                                        if (Client.Hero.Level >= 81)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1175, 0); // Advanced Cure
                                            Say("Congrats! You have learned Advanced Cure!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 81 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 16)
                                {
                                    if (!Client.Spells.ContainsKey(1170))
                                    {
                                        if (Client.Hero.Level >= 94)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1170, 0); // Nectar
                                            Say("Congrats! You have learned Nectar!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 94 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 17)
                                {
                                    if (!Client.Spells.ContainsKey(1280))
                                    {
                                        if (Client.Hero.Level >= 44)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1280, 0); // Water Elf
                                            Say("Congrats! You have learned Water Elf!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 44 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 18)
                                {
                                    if (!Client.Spells.ContainsKey(1150))
                                    {
                                        if (Client.Hero.Level >= 55)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1150, 0); // Fire Ring
                                            Say("Congrats! You have learned Fire Ring!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 55 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 19)
                                {
                                    if (!Client.Spells.ContainsKey(1180))
                                    {
                                        if (Client.Hero.Level >= 52)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1180, 0); // Fire Meteor
                                            Say("Congrats! You have learned Fire Meteor!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 52 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 20)
                                {
                                    if (!Client.Spells.ContainsKey(1120))
                                    {
                                        if (Client.Hero.Level >= 65)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1120, 0); // Fire Circle
                                            Say("Congrats! You have learned FireCircle!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You are not level 65 yet!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 21)
                                {
                                    if (!Client.Spells.ContainsKey(1002))
                                    {
                                        if (Client.Spells[1001].Level >= 3)
                                        {
                                            if (Client.Hero.Level >= 80)
                                            {
                                                ConquerSpell.LearnSpell(Client, 1002, 0); // Tornado
                                                Say("Congrats! You have learned Tornado!");
                                                Link("Thanks!", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("You are not level 80 yet!");
                                                Link("Thanks!", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else
                                        {
                                            Say("You need to continue to train your Fire spell!");
                                            Link("Thanks!", 255);
                                            Finish(npc);
                                        }
                                    }
                                    else
                                    {
                                        Say("You have already learned this spell!");
                                        Link("Thanks!", 255);
                                        Finish(npc);
                                    }
                                }
                                else if (Option == 132)
                                {
                                    Client.Job += 31;
                                    Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                    Say("Congrats! You have been promoted to a Water Tao!!");
                                    Link("Thanks!!!", 255);
                                    Finish(npc);
                                }
                                else if (Option == 142)
                                {
                                    Client.Job += 41;
                                    Client.Send(new StatTypePacket(Client.Hero.UID, Client.Job, StatIDs.Job).Serialize());
                                    Say("Congrats! You have been promoted to a Fire Tao!!");
                                    Link("Thanks!!!", 255);
                                    Finish(npc);
                                }
                            }
                            else
                            {
                                Say("Sorry, I can not teach you, as you are not a Taoist!");
                                Link("Okay, sorry.", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region Birth Village
                    #region Guide
                    case 1060:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Welcome to the wonderful world of Conquer! Press CTRL and left click to jump around.");
                                        Say("Be careful though! Don't fall off the rocks! Make your way through our village to learn more.");
                                        Link("I want to leave now", 1);
                                        Link("Okay, thanks!", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        Teleport(1002, 430, 380);
                                        if (Client.Job == 100)
                                        {
                                            ConquerSpell.LearnSpell(Client, 1000, 0);
                                            ConquerSpell.LearnSpell(Client, 1005, 0);
                                        }
                                        Say("This is Twin City. It's a dangerous world. I've given you some starter items.");
                                        Link("Thank you", 255);
                                        Finish(npc);
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region KnowItAll
                    case 10010:
                        {
                            if (Option == 0)
                            {
                                Say("Hello! This path leads you to our main city; Twin City! ");
                                Say("If you're ready I can send you there.");
                                Link("I'd like to go!", 1);
                                Link("I'd rather stay", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                Teleport(1002, 430, 380);
                                if (Client.Job == 100)
                                {
                                    ConquerSpell.LearnSpell(Client, 1000, 0);
                                    ConquerSpell.LearnSpell(Client, 1005, 0);
                                }
                                Say("This is Twin City. It's a dangerous world. I've given you some starter items.");
                                Link("Thank you", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    #endregion
                    #region Mr.Nosy
                    case 10009:
                        {
                            if (Option == 0)
                            {
                                Say("Hey, I will let you in on a little secret! Become the best!");
                                Link("Sweet!", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    #endregion
                    #region TaoistStar
                    case 10055:
                        {
                            if (Option == 0)
                            {
                                if (Client.Job == 100)
                                {
                                    Say("Hello, would you like me to teach you thunder & cure?");
                                    Link("Yes Please!", 1);
                                    Link("I don't want to learn!", 255);
                                    Finish(npc);
                                }
                                else if (Client.Job != 100)
                                {
                                    Say("Sorry. I cannot teach skills to adventurers not studying to be a Taoist!");
                                    Link("Okay.", 255);
                                    Finish(npc);
                                }
                            }
                            else if (Option == 1)
                            {
                                ConquerSpell.LearnSpell(Client, 1000, 0);
                                ConquerSpell.LearnSpell(Client, 1005, 0);
                                Say("Congratulations! You have learned Thunder & Cure!");
                                Link("Thank you!", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    #endregion
                    #region Blacksmith
                    case 10005:
                        {
                            if (Option == 0)
                            {
                                Say("Hello! I offer various items and weaponry in every main town!");
                                Link("Cool.", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    #endregion
                    #region Warehouseman
                    case 10006:
                        {
                            if (Option == 0)
                            {
                                Say("Hello there! I am the Warehouseman, and I am able to store your precious items! I am available in every town including in the market!");
                                Link("Cool.", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    #endregion
                    #region Armorer
                    case 10007:
                        {
                            if (Option == 0)
                            {
                                Say("Hello, Adventurer! I can help you get geared up with normal-valued armors!");
                                Say("I offer a large collection, and am always willing to help!");
                                Link("Cool!", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    #endregion
                    #region Pharmacist
                    case 10008:
                        {
                            if (Option == 0)
                            {
                                Say("I am the Pharmacist; I sell you health and mana potions of all varieties!");
                                Link("Okay", 255);
                                Finish(npc);
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region BoxerLi 
                    case 104839:
                        {
                            if (Option == 0)
                            {
                                Say("Hello I can help you train after you reach level 20, But I will charge you 1,000 gold. Would you like to go to the Training Grounds?");
                                Link("Yes Please, Here is the money", 1);
                                Link("No Thank you I cannot afford it.", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                if (Client.Silvers >= 1000)
                                {
                                    Teleport(1039, 219, 215);
                                    Money(-1000);
                                }
                                else
                                {
                                    Say("How dare you try to rip me off! Get lost, Or get my money!");
                                    Link("I'm sorry, I didn't realize.", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Millionaire Lee
                    case 5004:
                        {
                            if (Option == 0)
                            {
                                Say("I can help you pack and store those cumbersome Meteors and Dragonballs");
                                Link("Pack My Meteors", 1);
                                Link("Pack My Dragonballs", 2);
                                Link("Store My Meteors", 3);
                                Link("Store My Dragonballs", 5);
                                Link("[METEORS] I would like to make a withdrawl", 7);
                                Link("[DRAGONBALLS] I would like to make a withdrawl.", 9);
                                Link("I don't trust anyone will my balls!", 255);
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                if (Client.TryExpendItemOfId(1088001, 10))
                                    Client.AddInventory(720027);
                                else
                                {
                                    Say("You don't have enough metors to pack! You need at least 10!");
                                    Link("I'm sorry, I'll get more.", 255);
                                    Finish(npc);
                                }
                            }
                            else if (Option == 2)
                            {
                                if (Client.TryExpendItemOfId(1088000, 10))
                                    Client.AddInventory(720028);
                                else
                                {
                                    Say("You don't have enough Dragonballs to pack! You need at least 10!");
                                    Link("I'm sorry, I'll get more.", 255);
                                    Finish(npc);
                                }
                            }
                            else if (Option == 3)
                            {
                                Say($"You have {Client.MeteorCount} meteors in storage.");
                                Input("Input Amount:", 4);
                                Finish(npc);
                            }
                            else if (Option == 4)
                            {
                                if (Client.TryExpendItemOfId(1088001, byte.Parse(InputText)))
                                {
                                    Client.MeteorCount += byte.Parse(InputText);
                                    Say($"You now have {Client.MeteorCount} Meteors in storage.");
                                }
                                else
                                {
                                    Say("You don\'t have sufficient Meteors.");
                                }
                                Link("Ok.", 255);
                                Finish(npc);
                            }
                            else if (Option == 5)
                            {
                                Say($"You have {Client.DragonballCount} DragonBalls in storage.");
                                Input("Input Amount:", 6);
                                Finish(npc);
                            }
                            else if (Option == 6)
                            {
                                if (Client.TryExpendItemOfId(1088000, byte.Parse(InputText)))
                                {
                                    Client.DragonballCount += byte.Parse(InputText);
                                    Client.RemoveInventory(1088000);
                                    Say($"You now have {Client.DragonballCount} DragonBalls in storage.");
                                }
                                else
                                {
                                    Say("You don't have sufficient DragonBalls.");
                                }
                                Link("Ok.", 255);
                                Finish(npc);
                            }
                            else if (Option == 7)
                            {
                                Say($"You have {Client.MeteorCount} Meteors in storage.");
                                Input("Withdrawl amount:", 8);
                                Finish(npc);
                            }
                            else if (Option == 8)
                            {
                                if (Client.MeteorCount >= byte.Parse(InputText))
                                {
                                    if (Client.Inventory.Length + byte.Parse(InputText) > 40)
                                    {
                                        Say("You need to clear up some inventory space first!");
                                    }
                                    else
                                    {
                                        Client.MeteorCount -= byte.Parse(InputText);
                                        Client.AddItems(1088001, byte.Parse(InputText));
                                    }
                                    Say($"You have {Client.MeteorCount} Meteors remaining.");
                                }
                                else
                                {
                                    Say("You don't have sufficient Meteors.");
                                }
                                Link("Ok", 255);
                                Finish(npc);
                            }
                            else if (Option == 9)
                            {
                                Say($"You have {Client.DragonballCount} Dragonballs in storage.");
                                Input("Withdrawl amount", 10);
                                Finish(npc);
                            }
                            else if (Option == 10)
                            {
                                if (Client.DragonballCount >= int.Parse(InputText))
                                {
                                    if (Client.Inventory.Length + byte.Parse(InputText) > 40)
                                    {
                                        Say("You need to clear up some inventory space first!");
                                    }
                                    else
                                    {
                                        Client.DragonballCount -= byte.Parse(InputText);
                                        Client.AddItems(1088000, byte.Parse(InputText));
                                    }
                                    Say($"You have {Client.DragonballCount} DragonBalls remaining.");
                                }
                                else
                                {
                                    Say("You don't have sufficient dragonballs");
                                }
                                Link("Ok", 255);
                                Finish(npc);
                            }
                        }
                        break;
                    #endregion
                    #region Shelby
                    case 300000:
                        {
                            if (Option == 0)
                            {
                                if (Client.Hero.Level >= 70)
                                {
                                    Say("If you power level newbies, we will reward you with Meteors, Dragonballs, and Experience!");
                                    Link("Tell me more details.", 1);
                                    Link("Check my Virue points.", 2);
                                    Link("Claim Prize", 3);
                                    Link("I'm not interested.", 255);
                                    Finish(npc);
                                }
                                else
                                {
                                    Say("You need to train harder! Only players level 70 or above can gain Virtue points!");
                                    Link("I See.", 255);
                                    Finish(npc);
                                }
                            }
                            else if (Option == 1)
                            {
                                Say("If you are above level 70 and try to power level the newbies(at least 20 levels lower than yourself), you will be awarded with virtue points.");
                                Link("I see thanks.", 255);
                                Finish(npc);
                            }
                            else if (Option == 2)
                            {
                                Say($"Your virtue points are currently {Client.VirtuePoints}, please try to gain more");
                                Link("Thanks", 255);
                                Finish(npc);
                            }
                            else if (Option == 3)
                            {
                                Say("What prize would you like to claim?");
                                Link("Meteor", 4);
                                Link("Dragonball", 5);
                                Link("Experience", 6);
                                Link("Thanks", 255);
                                Finish(npc);
                            }
                            else if (Option == 4)
                            {
                                if (Client.VirtuePoints >= 5000)
                                {
                                    Client.VirtuePoints -= 5000;
                                    Client.AddInventory(1088001);
                                    Say($"There you go, you have {Client.VirtuePoints} virtue points left!");
                                    Say("Check your inventory for your Meteor.");
                                    Link("Thanks", 255);
                                    Finish(npc);
                                }
                                else
                                {
                                    Say("You have insufficient Virtue Points!");
                                    Link("Darn!", 255);
                                    Finish(npc);
                                }
                            }
                            else if (Option == 5)
                            {
                                if (Client.VirtuePoints >= 275000)
                                {
                                    Client.VirtuePoints -= 275000;
                                    Client.AddInventory(1088000);
                                    Say($"There you go, you have {Client.VirtuePoints} virtue points left!");
                                    Say("Check your inventory for your DragonBall.");
                                    Link("Thanks", 255);
                                    Finish(npc);
                                }
                                else
                                {
                                    Say("You have insufficient Virtue Points!");
                                    Link("Darn!", 255);
                                    Finish(npc);
                                }
                            }
                            else if (Option == 6)
                            {
                                if (Client.VirtuePoints >= 15000)
                                {
                                    Client.VirtuePoints -= 15000;
                                    Client.Experience += 2000000;
                                    Client.Send(new StatTypePacket(Client.Hero.UID, (uint)Client.Experience, StatIDs.Experience).Serialize());
                                    Say($"There you go, you have {Client.VirtuePoints} virtue points left!");
                                    Say("Check your inventory for your Exp.");
                                    Link("Thanks", 255);
                                    Finish(npc);
                                }
                                else
                                {
                                    Say("You have insufficient Virtue Points!");
                                    Link("Darn!", 255);
                                    Finish(npc);
                                }
                            }
                        }
                        break;
                    #endregion
                    #region Jeweler Sun
                    case 2065:
                        {
                            if (Option == 0)
                            {
                                Say("Wanna mix some gems together? Give me 15 normal gems and i'll mix them into refined one. Same goes for refineds to super.");
                                Link("I wanna mix normal gems into refined ones.", 1);
                                Link("I wanna mix refined gems into super ones.", 2);
                                Link("No.", 255);
                                Finish(npc);
                            }
                            else if ((Option == 1) || (Option == 2))
                            {
                                Say("Choose the gem you want to mix.");
                                Link("Phoenix Gem", (byte)(Option * 10 + 0));
                                Link("Dragon Gem", (byte)(Option * 10 + 1));
                                Link("Fury Gem", (byte)(Option * 10 + 2));
                                Link("Rainbow Gem", (byte)(Option * 10 + 3));
                                Link("Kylin Gem", (byte)(Option * 10 + 4));
                                Link("Violet Gem", (byte)(Option * 10 + 5));
                                Link("Moon Gem", (byte)(Option * 10 + 6));
                                Link("Next.", (byte)(100 * Option));

                                Finish(npc);
                            }
                            else if ((Option == 100) || (Option == 200))
                            {
                                Option = (byte)(Option / 100);

                                Say("Choose the gem you want to mix.");
                                Link("Tortoise Gem", (byte)(Option * 10 + 7));
                                Link("Thunder Gem", (byte)(Option * 10 + 8));
                                Link("Glory Gem", (byte)(Option * 10 + 9));

                                Finish(npc);
                            }
                            else if ((Option >= 10) && (Option <= 19))
                            {
                                uint ItemID = 0;
                                if (Option <= 17)
                                    ItemID = (uint)((Option - 10) * 10 + 700001);
                                else if (Option == 18)
                                    ItemID = 700101;
                                else
                                    ItemID = 700121;

                                if (Client.TryExpendItemOfId(ItemID, 15))
                                {
                                    Client.AddInventory(ItemID + 1);
                                }
                                else
                                {
                                    Say("You don't have enough gems.");
                                    Link("I see.", 255);
                                    Finish(npc);
                                }
                            }
                            else if ((Option >= 20) && (Option <= 29))
                            {
                                uint ItemID = 0;
                                if (Option <= 27)
                                    ItemID = (uint)((Option - 20) * 10 + 700002);
                                else if (Option == 28)
                                    ItemID = 700102;
                                else
                                    ItemID = 700122;

                                if (Client.TryExpendItemOfId(ItemID, 15))
                                {
                                    Client.AddInventory(ItemID + 1);
                                }
                                else
                                {
                                    Say("You don't have enough gems.");
                                    Link("I see.", 255);

                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Boxers
                    case 4000:
                    case 4001:
                    case 4002:
                    case 4003:
                    case 4004:
                        {
                            if (Option == 0)
                            {
                                Say("If you are above level 20 you can use the training group to level up without consuming any mana, stamina, or durability.");
                                Say("The cost is 1000 Gold, would you like to give it a try?");
                                Link("Yes please", 1);
                                Link("No Thanks...", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                if (Client.Silvers >= 1000)
                                {
                                    Money(-1000);
                                    Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());
                                    Teleport(1039, 216, 216);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region BoxerHuang
                    case 600075:
                        {
                            if (Option == 0)
                            {
                                Say("Hello, are you ready to leave the Training Grounds?");
                                Link("Yes, Thanks", 1);
                                Link("No, I'll stay", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                switch (Client.LastMap)
                                {
                                    case 1002:
                                        Teleport(1002, 430, 378);
                                        break;
                                    case 1015:
                                        Teleport(1015, 717, 565);
                                        break;
                                    case 1000:
                                        Teleport(1000, 481, 633);
                                        break;
                                    case 1011:
                                        Teleport(1011, 207, 258);
                                        break;
                                    case 1020:
                                        Teleport(1020, 556, 565);
                                        break;
                                    default:
                                        Say("Something went wrong. I've sent you to Twin City.");
                                        Link("Oh okay.", 255);
                                        Finish(npc);
                                        Teleport(1002, 430, 378);
                                        break;
                                }
                            }
                            break;
                        }
                    #endregion
                    #region Mine Entrance NPCs
                    #region TC Mine
                    case 4293:
                        {
                            switch (Option)
                            {
                                case 0:
                                    Say("Do you want to visit the mine?");
                                    Link("Yes", 1);
                                    Link("No", 255);
                                    Finish(npc);
                                    break;
                                case 1:
                                    Teleport(1028, 155, 95);
                                    Say("Here are the mines. Be careful! Dangerous monsters and unforgivable players may emerge");
                                    Say("to snatch your goodies!");
                                    Link("Thank you", 255);
                                    Finish(npc);
                                    break;

                            }
                            break;
                        }
                    #endregion
                    #region PC Mine
                    case 125:
                        {
                            if (Option == 0)
                            {
                                Say("Hello traveler. Want to go to Phoenix Castle mine? You have to be level 40 or over.");
                                Link("Yes!", 1);
                                Link("No thanks.", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                if (Client.Hero.Level >= 40)
                                {
                                    Client.Teleport(1025, 28, 70);
                                }
                                else
                                {
                                    Say("You are not level 40. Come back when you reach 40");
                                    Link("I see.", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region AC Mine
                    case 1008:
                        {
                            if (Option == 0)
                            {
                                Say("Hello traveler. Want to go to the Ape Mountain Mines? You have to be level 70 or over.");
                                Link("Yes!", 1);
                                Link("No thanks.", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                if (Client.Hero.Level >= 70)
                                {
                                    Client.Teleport(1026, 140, 104);
                                }
                                else
                                {
                                    Say("You are not level 70. Come back when you reach 70");
                                    Link("I see.", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region DC Mine
                    case 1006:
                        {
                            if (Option == 0)
                            {
                                Say("Hello traveler. Want to go to the Desert mines? You have to be level 70 or over.");
                                Link("Yes!", 1);
                                Link("No thanks.", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                if (Client.Hero.Level >= 70)
                                {
                                    Client.Teleport(1027, 140, 104);
                                }
                                else
                                {
                                    Say("You are not level 70. Come back when you reach 70");
                                    Link("I see.", 255);
                                    Finish(npc);
                                }
                            }
                            break;
                        }
                    #endregion
                    #region MZ Mine
                    case 19002:
                        {
                            if (Option == 0)
                            {
                                Say("Hello there! I can take you to the Adventure Zone Mines. Do you want to enter?");
                                Link("Yes!", 1);
                                Link("Just passing by...", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                Client.Teleport(1029, 30, 70);
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region Lab NPCs
                    #region Simon
                    case 1152:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Hello I can teleport you to Lab for 2,000 Virtue Point's.");
                                        Link("Let's go.", 1);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        if (Client.VirtuePoints >= 2000)
                                        {
                                            Client.VirtuePoints -= 2000;
                                            Client.Teleport(1351, 015, 127);
                                        }
                                        else
                                        {
                                            Say("You do not have enough Virtue points.");
                                            Link("Sorry.", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Lab2
                    case 1153:
                        {
                            if (Option == 0)
                            {
                                Say("Would you like to go to lab2? it will cost you a Skytoken.");
                                Link("Lab 2", 1);
                                Link("TwinCity", 2);
                                Link("Just passing by.", 255);
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                if (Client.InventoryContains(721537))
                                {
                                    Client.RemoveInventory(721537);
                                    Client.Teleport(1352, 029, 230);
                                }
                                else
                                {
                                    Say("You do not have a Sky Token Kill GoldGhosts for the token");
                                    Link("ok", 255);
                                    Finish(npc);
                                }
                            }
                            if (Option == 2)
                                Client.Teleport(1002, 431, 379);
                            break;
                        }
                    #endregion
                    #region Lab3
                    case 1154:
                        {
                            if (Option == 0)
                            {
                                Say("Would u like to go to lab3? It will cost you 1 Earth Token. Additionally I can also teleport you back to Twin City.");
                                Link("Lab 3", 1);
                                Link("TwinCity", 2);
                                Link("Just passing by.", 255);
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                if (Client.InventoryContains(721538))
                                {
                                    Client.RemoveInventory(721538);
                                    Client.Teleport(1353, 028, 270);
                                }
                                else
                                {
                                    Say("You don't have a Earth Token Kill AgilRats for the token");
                                    Link("I see.", 255);
                                    Finish(npc);
                                }
                            }
                            if (Option == 2)
                                Client.Teleport(1002, 431, 379);
                            break;
                        }
                    #endregion
                    #region Lab4
                    case 1155:
                        {
                            if (Option == 0)
                            {
                                Say("Would you like to go to Lab4 or go back to TwinCity?");
                                Link("Lab4", 1);
                                Link("TwinCity", 2);
                                Link("No thanks i think i'm gonna stay here awhile!", 255);
                                Finish(npc);
                            }
                            if (Option == 1)
                            {
                                if (Client.InventoryContains(721539))
                                {
                                    Client.RemoveInventory(721539);
                                    Client.Teleport(1354, 009, 290);
                                }
                                else
                                {
                                    Say("You do not have a SoulToken Kill FiendBats for the token");
                                    Link("I see.", 255);
                                    Finish(npc);
                                }
                            }
                            if (Option == 2)
                                Client.Teleport(1002, 431, 379);
                            break;
                        }
                    #endregion
                    #region Lab4 Exit
                    case 1156:
                        {
                            if (Option == 0)
                            {
                                Say("Would you like to go back to Twin City?");
                                Link("Yes", 1);
                                Link("No thank you", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                Client.Teleport(1002, 430, 378);
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region FurnitureNPC
                    case 30161:
                        {
                            if (Option == 0)
                            {
                                Say("Welcome to Twin City Furniture Store. Do you want to take a look at our store?");
                                Link("Sure.", 1);
                                Link("I am not interested.", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                Client.Teleport(1511, 51, 70);
                            }
                            break;
                        }
                    #endregion
                    #region MetZone
                    #region Entrance
                    case 300655: //MetZone
                        {
                            if (Option == 0)
                            {
                                Say("Would you like to go in Met Zone? Note that you can't use scrolls inside there.");
                                Link("Please teleport me there.", 1);
                                Link("No. Wait a moment.", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                Client.Teleport(1210, 1039, 717);
                            }
                            break;
                        }
                    #endregion
                    #region Exit
                    case 300653:
                        {
                            if (Option == 0)
                            {
                                Say("Want to get out of here?");
                                Link("Yes, please!", 1);
                                Link("I want to stay!", 255);
                                Finish(npc);
                            }
                            else if (Option == 1)
                            {
                                Client.Teleport(1002, 427, 379);
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region Monster Hunter Quest (every major town)
                    #region TCCaptain
                    case 2001://TCCaptain
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (MonsterHunter.Verify(Client))
                                        {
                                            if (Client.Hero.Level <= 25)
                                            {
                                                Say("Rampant monsters are everywhere! It's time to deal with these pests! I am");
                                                Say("offering rewards to anyone who pitches in and slays a few beasts!");
                                                Say("Anyone can earn such rewards up to 3 times every day. And that, my");
                                                Say("friend will you from novice to master in no time! Now select the monster");
                                                Say("you want to hunt.");
                                                Link("Slay Pheasant", 1);
                                                Link("Slay TurtleDove", 2);
                                                Link("Slay Robins", 3);
                                                Link("Slay Apparitions", 4);
                                                Link("Slay Poltergeists", 5);
                                                Link("Nevermind...", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 26 && Client.Hero.Level <= 45)
                                            {
                                                Say("Sorry! Looks like your help is needed in Phoenix Castle!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 249);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 46 && Client.Hero.Level <= 65)
                                            {
                                                Say("Sorry! Looks like your help is needed in Ape City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 250);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 66 && Client.Hero.Level <= 85)
                                            {
                                                Say("Sorry! Looks like your help is needed in Desert City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 251);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 86 && Client.Hero.Level <= 100)
                                            {
                                                Say("Sorry! Looks like your help is needed in Bird Island!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 252);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 101 && Client.Hero.Level <= 120)
                                            {
                                                Say("Sorry! Looks like your help is needed in Mystic Castle!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 253);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("Sorry! It appears you cannot participate anymore.");
                                                Link("Okay", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else if (Client.MonsterHunter && Client.MHKillCount <= 20)
                                        {
                                            Say("Keep on fighting the good fight! I'm rooting for you to slay those monsters!");
                                            Link("I lost my jar!", 246);
                                            Link("Just passing by...", 255);
                                            Finish(npc);

                                        }
                                        else if (Client.MonsterHunter && Client.MHKillCount >= 20)
                                        {
                                            Say("Congratulations! You have successfully killed at least 20 of the monsters!");
                                            Say("Would you like to claim your reward now?");
                                            Link("Yes!", 245);
                                            Link("No", 255);
                                            Finish(npc);

                                        }
                                        else if (Client.MHAttempts == 3)
                                        {
                                            Say("Sorry, you've reached your max attempts for today. Please try again tomorrow (or 24 hours from now)!");
                                            Link("Okay!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Um, something has gone wrong... would you like a reset?");
                                            Link("Sure", 246);
                                            Link("Just passing by...", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 1:
                                    {
                                        Client.MonsterType = 01;
                                        Say("Pheasant's are not a big threat, but they are destroying our crops.");
                                        Say("I need you to kill 20 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Client.MonsterType = 02;
                                        Say("Turtledove's are running amok across the New Plains!");
                                        Say("I need you to kill 20 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        Client.MonsterType = 03;
                                        Say("Robin's are running amok across the New Plains!");
                                        Say("I need you to kill 20 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 4:
                                    {
                                        Client.MonsterType = 04;
                                        Say("Apparition's are running amok across the New Plains!");
                                        Say("I need you to kill 20 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 5:
                                    {
                                        Client.MonsterType = 05;
                                        Say("Poltergeist's are running amok across the New Plains!");
                                        Say("I need you to kill 20 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 245:
                                    {
                                        if (Client.TryExpendItemOfId(750000, 1))
                                        {
                                            MonsterHunter.AddToCounter(Client, false);
                                            Client.AddInventory(723700);
                                            Say("I have rewarded you greatly! Enjoy this ExpBall!");
                                            Say("Right click on it to obtain valuable experience and level up!");
                                            Link("Thank you!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You need to give me your Cloud'sSaintJar in order to obtain the reward.");
                                            Link("Let's try again!", 0);
                                            Link("Oops!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 246:
                                    {
                                        Say("I can reset your quest stats for you. Be warned, you'll need to start over.");
                                        Say("Also this reset will count as an attempted try for this quest today .");
                                        Link("Do it.", 247);
                                        Link("I change my mind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 247:
                                    {
                                        MonsterHunter.AddToCounter(Client, false);
                                        Client.RemoveInventory(750000);
                                        Say("Alright. I have reset the quest for you. You may try again!");
                                        Link("Start Over", 0);
                                        Link("Thank you!", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 248:
                                    {
                                        if (Client.Inventory.Length < 40 && Client.MonsterHunter == false)
                                        {
                                            var Item = new ConquerItem(true);
                                            Item.UID = ConquerItem.NextItemUID;
                                            Item.ID = 750000;
                                            Item.CloudSaintID = Client.MonsterType;
                                            Item.CloudSaintKillsRequired = 20;
                                            Client.AddInventory(Item);
                                            Client.MonsterHunter = true;
                                            Say("Go out and slay those wretched beasts once and for all!");
                                            Link("Will do!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry, your inventory is full or you already have one! I can't give you this Cloud Saint's Jar.");
                                            Link("I lost my Jar", 246);
                                            Link("I'll fix that!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 249:
                                    {
                                        Teleport(1011, 229, 258);
                                        Say("Speak to PCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 250:
                                    {
                                        Teleport(1020, 569, 621);
                                        Say("Speak to ACCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 251:
                                    {
                                        Teleport(1000, 477, 633);
                                        Say("Speak to DCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 252:
                                    {
                                        Teleport(1015, 790, 569);
                                        Say("Speak to BICaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 253:
                                    {
                                        Teleport(1000, 082, 319);
                                        Say("Speak to MCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region PCCaptain
                    case 2002://PCCaptain
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (MonsterHunter.Verify(Client))
                                        {
                                            if (Client.Hero.Level <= 45)
                                            {
                                                Say("Rampant monsters are everywhere! It's time to deal with these pests! I am");
                                                Say("offering rewards to anyone who pitches in and slays a few beasts!");
                                                Say("Anyone can earn such rewards up to 3 times every day. And that, my");
                                                Say("friend will you from novice to master in no time! Now select the monster");
                                                Say("you want to hunt.");
                                                Link("Slay WingedSnake", 1);
                                                Link("Slay Bandit", 2);
                                                Link("Slay Ratling", 3);
                                                Link("Slay FireSpirit", 4);
                                                Link("Nevermind...", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 1 && Client.Hero.Level <= 25)
                                            {
                                                Say("Sorry! Looks like your help is needed in Twin City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 249);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 46 && Client.Hero.Level <= 65)
                                            {
                                                Say("Sorry! Looks like your help is needed in Ape City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 250);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 66 && Client.Hero.Level <= 85)
                                            {
                                                Say("Sorry! Looks like your help is needed in Desert City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 251);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 86 && Client.Hero.Level <= 100)
                                            {
                                                Say("Sorry! Looks like your help is needed in Bird Island!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 252);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 101 && Client.Hero.Level <= 120)
                                            {
                                                Say("Sorry! Looks like your help is needed in Mystic Castle!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 253);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("Sorry! It appears you cannot participate anymore.");
                                                Link("Okay", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else if (Client.MonsterHunter && Client.MHKillCount <= 50)
                                        {
                                            Say("Keep on fighting the good fight! I'm rooting for you to slay those monsters!");
                                            Link("I lost my jar!", 246);
                                            Link("Just passing by...", 255);
                                            Finish(npc);

                                        }
                                        else if (Client.MonsterHunter && Client.MHKillCount >= 50)
                                        {
                                            Say("Congratulations! You have successfully killed at least 50 of the monsters!");
                                            Say("Would you like to claim your reward now?");
                                            Link("Yes!", 245);
                                            Link("No", 255);
                                            Finish(npc);

                                        }
                                        else if (Client.MHAttempts == 3)
                                        {
                                            Say("Sorry, you've reached your max attempts for today. Please try again tomorrow (or 24 hours from now)!");
                                            Link("Okay!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Um, something has gone wrong... would you like a reset?");
                                            Link("Sure", 246);
                                            Link("Just passing by...", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 1:
                                    {
                                        Client.MonsterType = 06;
                                        Say("Turtledove's are not a big threat, but they are destroying our crops.");
                                        Say("I need you to kill 50 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Client.MonsterType = 07;
                                        Say("Robin's are running amok across the New Plains!");
                                        Say("I need you to kill 50 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        Client.MonsterType = 08;
                                        Say("Apparition's are running amok across the New Plains!");
                                        Say("I need you to kill 50 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 4:
                                    {
                                        Client.MonsterType = 09;
                                        Say("Poltergeist's are running amok across the New Plains!");
                                        Say("I need you to kill 50 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 245:
                                    {
                                        if (Client.TryExpendItemOfId(750000, 1))
                                        {
                                            MonsterHunter.AddToCounter(Client, false);
                                            Client.AddInventory(723700);
                                            Say("I have rewarded you greatly! Enjoy this ExpBall!");
                                            Say("Right click on it to obtain valuable experience and level up!");
                                            Link("Thank you!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You need to give me your Cloud'sSaintJar in order to obtain the reward.");
                                            Link("Let's try again!", 0);
                                            Link("Oops!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 246:
                                    {
                                        Say("I can reset your quest stats for you. Be warned, you'll need to start over.");
                                        Say("Also this reset will count as an attempted try for this quest today .");
                                        Link("Do it.", 247);
                                        Link("I change my mind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 247:
                                    {
                                        MonsterHunter.AddToCounter(Client, false);
                                        Client.RemoveInventory(750000);
                                        Client.MonsterHunter = false;
                                        Say("Alright. I have reset the quest for you. You may try again!");
                                        Link("Start Over", 0);
                                        Link("Thank you!", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 248:
                                    {
                                        if (Client.Inventory.Length < 40 && Client.MonsterHunter == false)
                                        {
                                            var Item = new ConquerItem(true);
                                            Item.UID = ConquerItem.NextItemUID;
                                            Item.ID = 750000;
                                            Item.CloudSaintID = Client.MonsterType;
                                            Item.CloudSaintKillsRequired = 50;
                                            Client.AddInventory(Item);
                                            Client.MonsterHunter = true;
                                            Say("Go out and slay those wretched beasts once and for all!");
                                            Link("Will do!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry, your inventory is full or you already have one! I can't give you this Cloud Saint's Jar.");
                                            Link("I lost my Jar", 246);
                                            Link("I'll fix that!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 249:
                                    {
                                        Teleport(1002, 439, 438);
                                        Say("Speak to TCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 250:
                                    {
                                        Teleport(1020, 569, 621);
                                        Say("Speak to ACCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 251:
                                    {
                                        Teleport(1000, 477, 633);
                                        Say("Speak to DCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 252:
                                    {
                                        Teleport(1015, 790, 569);
                                        Say("Speak to BICaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 253:
                                    {
                                        Teleport(1000, 082, 319);
                                        Say("Speak to MCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region ACCaptain
                    case 2003://ACCaptain
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (MonsterHunter.Verify(Client))
                                        {
                                            if (Client.Hero.Level <= 65)
                                            {
                                                Say("Rampant monsters are everywhere! It's time to deal with these pests! I am");
                                                Say("offering rewards to anyone who pitches in and slays a few beasts!");
                                                Say("Anyone can earn such rewards up to 3 times every day. And that, my");
                                                Say("friend will you from novice to master in no time! Now select the monster");
                                                Say("you want to hunt.");
                                                Link("Slay Macaque", 1);
                                                Link("Slay GiantApe", 2);
                                                Link("Slay ThunderApe", 3);
                                                Link("Slay Snakeman", 4);
                                                Link("Nevermind...", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 1 && Client.Hero.Level <= 25)
                                            {
                                                Say("Sorry! Looks like your help is needed in Twin City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 249);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 26 && Client.Hero.Level <= 46)
                                            {
                                                Say("Sorry! Looks like your help is needed in Phoenix Castle");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 250);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 66 && Client.Hero.Level <= 85)
                                            {
                                                Say("Sorry! Looks like your help is needed in Desert City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 251);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 86 && Client.Hero.Level <= 100)
                                            {
                                                Say("Sorry! Looks like your help is needed in Bird Island!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 252);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 101 && Client.Hero.Level <= 120)
                                            {
                                                Say("Sorry! Looks like your help is needed in Mystic Castle!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 253);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("Sorry! It appears you cannot participate anymore.");
                                                Link("Okay", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else if (Client.MonsterHunter && Client.MHKillCount <= 100)
                                        {
                                            Say("Keep on fighting the good fight! I'm rooting for you to slay those monsters!");
                                            Link("I lost my jar!", 246);
                                            Link("Just passing by...", 255);
                                            Finish(npc);

                                        }
                                        else if (Client.MonsterHunter && Client.MHKillCount >= 100)
                                        {
                                            Say("Congratulations! You have successfully killed at least 100 of the monsters!");
                                            Say("Would you like to claim your reward now?");
                                            Link("Yes!", 245);
                                            Link("No", 255);
                                            Finish(npc);

                                        }
                                        else if (Client.MHAttempts == 3)
                                        {
                                            Say("Sorry, you've reached your max attempts for today. Please try again tomorrow (or 24 hours from now)!");
                                            Link("Okay!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Um, something has gone wrong... would you like a reset?");
                                            Link("Sure", 246);
                                            Link("Just passing by...", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 1:
                                    {
                                        Client.MonsterType = 10;
                                        Say("Macaque's are running amok across our beautiful Love Canyon!");
                                        Say("I need you to kill 100 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Client.MonsterType = 11;
                                        Say("GiantApe's are running amok across our beautiful Love Canyon!");
                                        Say("I need you to kill 100 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        Client.MonsterType = 12;
                                        Say("ThunderApe's are running amok across our beautiful Love Canyon!");
                                        Say("I need you to kill 100 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 4:
                                    {
                                        Client.MonsterType = 13;
                                        Say("Snakeman's are running amok across our beautiful Love Canyon!");
                                        Say("I need you to kill 100 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 245:
                                    {
                                        if (Client.TryExpendItemOfId(750000, 1))
                                        {
                                            MonsterHunter.AddToCounter(Client, false);
                                            Client.AddInventory(723700);
                                            Say("I have rewarded you greatly! Enjoy this ExpBall!");
                                            Say("Right click on it to obtain valuable experience and level up!");
                                            Link("Thank you!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You need to give me your Cloud'sSaintJar in order to obtain the reward.");
                                            Link("Let's try again!", 0);
                                            Link("Oops!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 246:
                                    {
                                        Say("I can reset your quest stats for you. Be warned, you'll need to start over.");
                                        Say("Also this reset will count as an attempted try for this quest today .");
                                        Link("Do it.", 247);
                                        Link("I change my mind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 247:
                                    {
                                        MonsterHunter.AddToCounter(Client, false);
                                        Client.RemoveInventory(750000);
                                        Client.MonsterHunter = false;
                                        Say("Alright. I have reset the quest for you. You may try again!");
                                        Link("Start Over", 0);
                                        Link("Thank you!", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 248:
                                    {
                                        if (Client.Inventory.Length < 40 && Client.MonsterHunter == false)
                                        {
                                            var Item = new ConquerItem(true);
                                            Item.UID = ConquerItem.NextItemUID;
                                            Item.ID = 750000;
                                            Item.CloudSaintID = Client.MonsterType;
                                            Item.CloudSaintKillsRequired = 100;
                                            Client.AddInventory(Item);
                                            Client.MonsterHunter = true;
                                            Say("Go out and slay those wretched beasts once and for all!");
                                            Link("Will do!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry, your inventory is full or you already have one! I can't give you this Cloud Saint's Jar.");
                                            Link("I lost my Jar", 246);
                                            Link("I'll fix that!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 249:
                                    {
                                        Teleport(1002, 439, 438);
                                        Say("Speak to TCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 250:
                                    {
                                        Teleport(1011, 229, 258);
                                        Say("Speak to PCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 251:
                                    {
                                        Teleport(1000, 477, 633);
                                        Say("Speak to DCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 252:
                                    {
                                        Teleport(1015, 790, 569);
                                        Say("Speak to BICaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 253:
                                    {
                                        Teleport(1000, 082, 319);
                                        Say("Speak to MCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region DCCaptain
                    case 2004://DCCaptain
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (MonsterHunter.Verify(Client))
                                        {
                                            if (Client.Hero.Level <= 85)
                                            {
                                                Say("Rampant monsters are everywhere! It's time to deal with these pests! I am");
                                                Say("offering rewards to anyone who pitches in and slays a few beasts!");
                                                Say("Anyone can earn such rewards up to 3 times every day. And that, my");
                                                Say("friend will you from novice to master in no time! Now select the monster");
                                                Say("you want to hunt.");
                                                Link("Slay SandMonster", 1);
                                                Link("Slay HillMonster", 2);
                                                Link("Slay RockMonster", 3);
                                                Link("Slay BladeGhost", 4);
                                                Link("Nevermind...", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 1 && Client.Hero.Level <= 25)
                                            {
                                                Say("Sorry! Looks like your help is needed in Twin City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 249);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 26 && Client.Hero.Level <= 45)
                                            {
                                                Say("Sorry! Looks like your help is needed in Phoenix Castle");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 250);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 46 && Client.Hero.Level <= 65)
                                            {
                                                Say("Sorry! Looks like your help is needed in Ape City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 251);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 86 && Client.Hero.Level <= 100)
                                            {
                                                Say("Sorry! Looks like your help is needed in Bird Island!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 252);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 101 && Client.Hero.Level <= 120)
                                            {
                                                Say("Sorry! Looks like your help is needed in Mystic Castle!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 253);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("Sorry! It appears you cannot participate anymore.");
                                                Link("Okay", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else if (Client.MonsterHunter && Client.MHKillCount <= 150)
                                        {
                                            Say("Keep on fighting the good fight! I'm rooting for you to slay those monsters!");
                                            Link("I lost my jar!", 246);
                                            Link("Just passing by...", 255);
                                            Finish(npc);

                                        }
                                        else if (Client.MonsterHunter && Client.MHKillCount >= 150)
                                        {
                                            Say("Congratulations! You have successfully killed at least 150 of the monsters!");
                                            Say("Would you like to claim your reward now?");
                                            Link("Yes!", 245);
                                            Link("No", 255);
                                            Finish(npc);

                                        }
                                        else if (Client.MHAttempts == 3)
                                        {
                                            Say("Sorry, you've reached your max attempts for today. Please try again tomorrow (or 24 hours from now)!");
                                            Link("Okay!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Um, something has gone wrong... would you like a reset?");
                                            Link("Sure", 246);
                                            Link("Just passing by...", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 1:
                                    {
                                        Client.MonsterType = 14;
                                        Say("SandMonsters are running amok across our beautiful Desert!");
                                        Say("I need you to kill 200 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Client.MonsterType = 15;
                                        Say("HillMonsters are running amok across our beautiful Desert!");
                                        Say("I need you to kill 150 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        Client.MonsterType = 16;
                                        Say("RockMonsters are running amok across our beautiful Desert!");
                                        Say("I need you to kill 150 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 4:
                                    {
                                        Client.MonsterType = 17;
                                        Say("BladeGhost's are running amok across our beautiful Desert!");
                                        Say("I need you to kill 150 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 245:
                                    {
                                        if (Client.TryExpendItemOfId(750000, 1))
                                        {
                                            MonsterHunter.AddToCounter(Client, false);
                                            Client.AddInventory(723700);
                                            Say("I have rewarded you greatly! Enjoy this ExpBall!");
                                            Say("Right click on it to obtain valuable experience and level up!");
                                            Link("Thank you!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You need to give me your Cloud'sSaintJar in order to obtain the reward.");
                                            Link("Let's try again!", 0);
                                            Link("Oops!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 246:
                                    {
                                        Say("I can reset your quest stats for you. Be warned, you'll need to start over.");
                                        Say("Also this reset will count as an attempted try for this quest today .");
                                        Link("Do it.", 247);
                                        Link("I change my mind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 247:
                                    {
                                        MonsterHunter.AddToCounter(Client, false);
                                        Client.RemoveInventory(750000);
                                        Say("Alright. I have reset the quest for you. You may try again!");
                                        Link("Start Over", 0);
                                        Link("Thank you!", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 248:
                                    {
                                        if (Client.Inventory.Length < 40 && Client.MonsterHunter == false)
                                        {
                                            var Item = new ConquerItem(true);
                                            Item.UID = ConquerItem.NextItemUID;
                                            Item.ID = 750000;
                                            Item.CloudSaintID = Client.MonsterType;
                                            Item.CloudSaintKillsRequired = 150;
                                            Client.AddInventory(Item);
                                            Client.MonsterHunter = true;
                                            Say("Go out and slay those wretched beasts once and for all!");
                                            Link("Will do!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry, your inventory is full or you already have one! I can't give you this Cloud Saint's Jar.");
                                            Link("I lost my Jar", 246);
                                            Link("I'll fix that!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 249:
                                    {
                                        Teleport(1002, 439, 438);
                                        Say("Speak to TCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 250:
                                    {
                                        Teleport(1011, 229, 258);
                                        Say("Speak to PCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 251:
                                    {
                                        Teleport(1020, 569, 621);
                                        Say("Speak to ACCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 252:
                                    {
                                        Teleport(1015, 790, 569);
                                        Say("Speak to BICaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 253:
                                    {
                                        Teleport(1000, 082, 319);
                                        Say("Speak to MCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region BICaptain
                    case 2005://BICaptain
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (MonsterHunter.Verify(Client))
                                        {
                                            if (Client.Hero.Level <= 100)
                                            {
                                                Say("Rampant monsters are everywhere! It's time to deal with these pests! I am");
                                                Say("offering rewards to anyone who pitches in and slays a few beasts!");
                                                Say("Anyone can earn such rewards up to 3 times every day. And that, my");
                                                Say("friend will you from novice to master in no time! Now select the monster");
                                                Say("you want to hunt.");
                                                Link("Slay Birdman", 1);
                                                Link("Slay HawKing", 2);
                                                Link("Nevermind...", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 1 && Client.Hero.Level <= 25)
                                            {
                                                Say("Sorry! Looks like your help is needed in Twin City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 249);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 26 && Client.Hero.Level <= 45)
                                            {
                                                Say("Sorry! Looks like your help is needed in Phoenix Castle");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 250);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 46 && Client.Hero.Level <= 65)
                                            {
                                                Say("Sorry! Looks like your help is needed in Ape City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 251);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 66 && Client.Hero.Level <= 85)
                                            {
                                                Say("Sorry! Looks like your help is needed in Desert City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 252);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 101 && Client.Hero.Level <= 120)
                                            {
                                                Say("Sorry! Looks like your help is needed in Mystic Castle!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 253);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("Sorry! It appears you cannot participate anymore.");
                                                Link("Okay", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else if (Client.MonsterHunter && Client.MHKillCount <= 200)
                                        {
                                            Say("Keep on fighting the good fight! I'm rooting for you to slay those monsters!");
                                            Link("I lost my jar!", 246);
                                            Link("Just passing by...", 255);
                                            Finish(npc);

                                        }
                                        else if (Client.MonsterHunter && Client.MHKillCount >= 200)
                                        {
                                            Say("Congratulations! You have successfully killed at least 200 of the monsters!");
                                            Say("Would you like to claim your reward now?");
                                            Link("Yes!", 245);
                                            Link("No", 255);
                                            Finish(npc);

                                        }
                                        else if (Client.MHAttempts == 3)
                                        {
                                            Say("Sorry, you've reached your max attempts for today. Please try again tomorrow (or 24 hours from now)!");
                                            Link("Okay!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Um, something has gone wrong... would you like a reset?");
                                            Link("Sure", 246);
                                            Link("Just passing by...", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 1:
                                {
                                        Client.MonsterType = 18;
                                        Say("Birdman's are running amok across our beautiful Islands!");
                                        Say("I need you to kill 200 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Client.MonsterType = 19;
                                        Say("HawKing are running amok across our beautiful Islands!");
                                        Say("I need you to kill 200 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 245:
                                    {
                                        if (Client.TryExpendItemOfId(750000, 1))
                                        {
                                            MonsterHunter.AddToCounter(Client, false);
                                            Client.AddInventory(723700);
                                            Say("I have rewarded you greatly! Enjoy this ExpBall!");
                                            Say("Right click on it to obtain valuable experience and level up!");
                                            Link("Thank you!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You need to give me your Cloud'sSaintJar in order to obtain the reward.");
                                            Link("Let's try again!", 0);
                                            Link("Oops!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 246:
                                    {
                                        Say("I can reset your quest stats for you. Be warned, you'll need to start over.");
                                        Say("Also this reset will count as an attempted try for this quest today .");
                                        Link("Do it.", 247);
                                        Link("I change my mind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 247:
                                    {
                                        MonsterHunter.AddToCounter(Client, false);
                                        Client.RemoveInventory(750000);
                                        Client.MonsterHunter = false;
                                        Say("Alright. I have reset the quest for you. You may try again!");
                                        Link("Start Over", 0);
                                        Link("Thank you!", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 248:
                                    {
                                        if (Client.Inventory.Length < 40 && Client.MonsterHunter == false)
                                        {
                                            var Item = new ConquerItem(true);
                                            Item.UID = ConquerItem.NextItemUID;
                                            Item.ID = 750000;
                                            Item.CloudSaintID = Client.MonsterType;
                                            Item.CloudSaintKillsRequired = 200;
                                            Client.AddInventory(Item);
                                            Client.MonsterHunter = true;
                                            Say("Go out and slay those wretched beasts once and for all!");
                                            Link("Will do!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry, your inventory is full or you already have one! I can't give you this Cloud Saint's Jar.");
                                            Link("I lost my Jar", 246);
                                            Link("I'll fix that!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 249:
                                    {
                                        Teleport(1002, 439, 438);
                                        Say("Speak to TCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 250:
                                    {
                                        Teleport(1011, 229, 258);
                                        Say("Speak to PCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 251:
                                    {
                                        Teleport(1020, 569, 621);
                                        Say("Speak to ACCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 252:
                                    {
                                        Teleport(1000, 477, 633);
                                        Say("Speak to DCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 253:
                                    {
                                        Teleport(1000, 082, 319);
                                        Say("Speak to MCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region MCCaptain
                    case 2006://MCCaptain
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        if (MonsterHunter.Verify(Client))
                                        {
                                            if (Client.Hero.Level <= 120)
                                            {
                                                Say("Rampant monsters are everywhere! It's time to deal with these pests! I am");
                                                Say("offering rewards to anyone who pitches in and slays a few beasts!");
                                                Say("Anyone can earn such rewards up to 3 times every day. And that, my");
                                                Say("friend will you from novice to master in no time! Now select the monster");
                                                Say("you want to hunt.");
                                                Link("Slay Birdman", 1);
                                                Link("Slay HawKing", 2);
                                                Link("Nevermind...", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 1 && Client.Hero.Level <= 25)
                                            {
                                                Say("Sorry! Looks like your help is needed in Twin City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 249);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 26 && Client.Hero.Level <= 45)
                                            {
                                                Say("Sorry! Looks like your help is needed in Phoenix Castle");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 250);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 46 && Client.Hero.Level <= 65)
                                            {
                                                Say("Sorry! Looks like your help is needed in Ape City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 251);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 66 && Client.Hero.Level <= 85)
                                            {
                                                Say("Sorry! Looks like your help is needed in Desert City!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 252);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else if (Client.Hero.Level >= 86 && Client.Hero.Level <= 100)
                                            {
                                                Say("Sorry! Looks like your help is needed in Bird Island!");
                                                Say("Would you like to go there now?");
                                                Link("Yes please!", 253);
                                                Link("No thank you", 255);
                                                Finish(npc);
                                            }
                                            else
                                            {
                                                Say("Sorry! It appears you cannot participate anymore.");
                                                Link("Okay", 255);
                                                Finish(npc);
                                            }
                                        }
                                        else if (Client.MonsterHunter && Client.MHKillCount <= 250)
                                        {
                                            Say("Keep on fighting the good fight! I'm rooting for you to slay those monsters!");
                                            Link("I lost my jar!", 246);
                                            Link("Just passing by...", 255);
                                            Finish(npc);

                                        }
                                        else if (Client.MonsterHunter && Client.MHKillCount >= 250)
                                        {
                                            Say("Congratulations! You have successfully killed at least 250 of the monsters!");
                                            Say("Would you like to claim your reward now?");
                                            Link("Yes!", 245);
                                            Link("No", 255);
                                            Finish(npc);

                                        }
                                        else if (Client.MHAttempts == 3)
                                        {
                                            Say("Sorry, you've reached your max attempts for today. Please try again tomorrow (or 24 hours from now)!");
                                            Link("Okay!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Um, something has gone wrong... would you like a reset?");
                                            Link("Sure", 246);
                                            Link("Just passing by...", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 1:
                                    {
                                        Client.MonsterType = 20;
                                        Say("Tombats are running amok across our beautiful Castle!");
                                        Say("I need you to kill 250 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Client.MonsterType = 55;
                                        Say("BanditL97's are running amok across our beautiful Castle!");
                                        Say("I need you to kill 250 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        Client.MonsterType = 56;
                                        Say("BloodyBat's are running amok across our beautiful Castle!");
                                        Say("I need you to kill 250 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 4:
                                    {
                                        Client.MonsterType = 57;
                                        Say("BullMonster's are running amok across our beautiful Castle!");
                                        Say("I need you to kill 250 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 5:
                                    {
                                        Client.MonsterType = 58;
                                        Say("RedDevilL117's are running amok across our beautiful Castle!");
                                        Say("I need you to kill 300 of them. Take this Cloud Saint's Jar so you can store");
                                        Say("the monster souls inside.");
                                        Link("I accept the quest", 248);
                                        Link("Nevermind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 245:
                                    {
                                        if (Client.TryExpendItemOfId(750000, 1))
                                        {
                                            MonsterHunter.AddToCounter(Client, false);
                                            Client.AddInventory(723700);
                                            Say("I have rewarded you greatly! Enjoy this ExpBall!");
                                            Say("Right click on it to obtain valuable experience and level up!");
                                            Link("Thank you!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("You need to give me your Cloud'sSaintJar in order to obtain the reward.");
                                            Link("Let's try again!", 0);
                                            Link("Oops!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 246:
                                    {
                                        Say("I can reset your quest stats for you. Be warned, you'll need to start over.");
                                        Say("Also this reset will count as an attempted try for this quest today .");
                                        Link("Do it.", 247);
                                        Link("I change my mind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 247:
                                    {
                                        MonsterHunter.AddToCounter(Client, false);
                                        Client.RemoveInventory(750000);
                                        Client.MonsterHunter = false;
                                        Say("Alright. I have reset the quest for you. You may try again!");
                                        Link("Start Over", 0);
                                        Link("Thank you!", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 248:
                                    {
                                        if (Client.Inventory.Length < 40 && Client.MonsterHunter == false)
                                        {
                                            var Item = new ConquerItem(true);
                                            Item.UID = ConquerItem.NextItemUID;
                                            Item.ID = 750000;
                                            Item.CloudSaintID = Client.MonsterType;
                                            Item.CloudSaintKillsRequired = 250;
                                            Client.AddInventory(Item);
                                            Client.MonsterHunter = true;
                                            Say("Go out and slay those wretched beasts once and for all!");
                                            Link("Will do!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry, your inventory is full or you already have one! I can't give you this Cloud Saint's Jar.");
                                            Link("I lost my Jar", 246);
                                            Link("I'll fix that!", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 249:
                                    {
                                        Teleport(1002, 439, 438);
                                        Say("Speak to TCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 250:
                                    {
                                        Teleport(1011, 229, 258);
                                        Say("Speak to PCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 251:
                                    {
                                        Teleport(1020, 569, 621);
                                        Say("Speak to ACCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 252:
                                    {
                                        Teleport(1000, 477, 633);
                                        Say("Speak to DCCaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 253:
                                    {
                                        Teleport(1015, 790, 569);
                                        Say("Speak to BICaptain to continue your journey!");
                                        Link("Okay", 255);
                                        Finish(npc);
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region Bomb Siege Quest (Maple Forest / Ape Mountain)
                    #region Norbert
                    case 421:
                        switch (Option)
                        {
                            case 0:
                                {
                                    if ((Client.CountInventory(721262) >= 5) && (Client.CountInventory(721263) >= 1) && Client.Silvers >= 30000)
                                    {
                                        Say("Hey! You have the material I want. Do you come for the Bomb?");
                                        Link("Yeah.", 6);
                                        Link("Just passing by.", 255);
                                        Finish(npc);

                                    }
                                    else
                                    {
                                        Say("Saltpeter, Sulphur, charcoal, and now make a fire...");
                                        Link("What are you doing?", 1);
                                        Link("I'd better not bother you.", 255);
                                        Finish(npc);
                                    }
                                    break;
                                }
                            case 1:
                                {
                                    Say("I'm refining a bomb which is very powerful and dangerous.");
                                    Link("What's it used for?", 2);
                                    Link("Wow, I'd better leave here.", 255);
                                    Finish(npc);
                                    break;
                                }
                            case 2:
                                {
                                    Say("It's used to blow down the guild gate! I participated in a guild war and");
                                    Say("witnessed lots of people had lost their lives when attacking the gate. Since then, I");
                                    Say("have been working on inventing powerful bombs which can destroy the guild");
                                    Say("gate. Finally, I can make it!");
                                    Link("Can you make one for me?", 3);
                                    Link("Oh. I see.", 255);
                                    Finish(npc);
                                    break;
                                }
                            case 3:
                                {
                                    Say("No problem. But you know such bombs are powerful and dangerous.");
                                    Say("Whoever uses it to destroy a guild gate will be killed by the bomb, too!");
                                    Link("Thanks. I need it badly", 4);
                                    Link("Then forget it.", 255);
                                    Finish(npc);
                                    break;
                                }
                            case 4:
                                {
                                    Say("Good. You're going to need to collect 5 Saltpeter and a Sulphur.");
                                    Say("I also charge 30,000 Silvers as a fee.");
                                    Link("Where do I get the materials?", 5);
                                    Link("I give up.", 255);
                                    Finish(npc);
                                    break;
                                }
                            case 5:
                                {
                                    Say("You can get Saltpeter from my friend OldQuarrier in Ape City and get Sulphur");
                                    Say("from the Bandits nearby.");
                                    Link("Okay, I'll be back soon.", 255);
                                    Finish(npc);
                                    break;
                                }
                            case 6:
                                {
                                    if ((Client.CountInventory(721262) >= 5) && (Client.CountInventory(721263) >= 1) && Client.Silvers >= 30000)
                                    {
                                        Client.TryExpendItemOfId(721262, 5);
                                        Client.RemoveInventory(721263);
                                        Client.Silvers -= 30000;
                                        Client.AddInventory(721261);
                                        Say("You can take the Bomb away. Remember that you'll be killed once you use it");
                                        Say("to destroy the guild gate!");
                                        Link("I see.", 255);
                                        Finish(npc);
                                    }
                                    break;
                                }
                        }
                        break;
                    #endregion
                    #region OldQuarrier
                    case 422:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Let me guess. It's Norbert who asked you to come here. Am I right?");
                                        Link("Yeah, I need Saltpeter.", 3);
                                        Link("Nope.", 1);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        Say("Are you coming for Saltpeter? Since Norbert invented the bomb, I have been kept");
                                        Say("busy with supplying Saltpeter for him.");
                                        Link("What is the Bomb?", 2);
                                        Link("Sorry to bother you.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Say("It is powerfuly and used to bomb out a Guild Gate. If you're interested in it, you can");
                                        Say("visit Norbert for the details. He lives in a small village in Maple Forest.");
                                        Link("I see.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        if (Client.Inventory.Length < 40)
                                        {
                                            Client.AddInventory(721262);
                                            Say("My pleasure. Here you are.");
                                            Link("Thank you.", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry, your inventory is full!");
                                            Link("I'll fix that.", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #endregion
                    #region Snake Island Quest (Bird Island)
                    case 30115:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("What beautiful scenery! I feel so lucky to meet you here, so my problem of starvation can be solved. Come here, baby!");
                                        Link("Be off, or I'll kil you!", 1);
                                        Link("I'm gone, bye!", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        Say("Wait, I'm only joking! If you spare my life, I'll lead you to a marvellous place");
                                        Say("with treasures everywhere! You can take what you want if you are able to");
                                        Say("break through the Snake Array");
                                        Link("What is Snake Array?", 2);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Say("Don't kill me and I'll explain. It's a kind of battle array formed by 17 islands.");
                                        Say("Our boss watches the last one. Beat the boss down, and the treasure will be");
                                        Say("yours!");
                                        Link("Send me on the way", 3);
                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        Say("Um, I don't mean to question your ability. However, you may pay for you greed");
                                        Say("with your life. I've seen that a lot, before.");
                                        Link("I fear nothing.", 4);
                                        Link("I changed my mind", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 4:
                                    {
                                        Client.Teleport(1063, 449, 358);
                                        break;
                                    }

                            }
                            break;
                        }
                    case 30125:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Are you leaving the snake array?");
                                        Link("I had better come later", 1);
                                        Link("No. I never give up.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        Client.Teleport(1015, 717, 577);
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Guides (Twin City)
                    case 4102://TC Guide
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Hello! I am here to help you if you have questions about the server!");
                                        Link("Get VIP", 11);
                                        Link("Get Newbie Exp", 12);
                                        Link("Power Level Assist", 13);
                                        Link("Special Events", 14);
                                        Link("How to Vote", 15);
                                        Link("More Options", 1);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        Say("Hello! I am here to help you if you have questions about the server!");
                                        Link("Weapon Sockets", 16);
                                        Link("Item Sockets[Non-Weapon]", 17);
                                        Link("Adventure Zone", 18);
                                        Link("The Mines & Mining", 19);
                                        Link("Max level", 20);
                                        Link("More Options", 2);
                                        Finish(npc);
                                        break;
                                    }
                                case 2:
                                    {
                                        Say("Hello! I am here to help you if you have questions about the server!");
                                        Link("Getting Gear", 21);
                                        Link("Conquer Points", 22);
                                        Link("Available Commands", 23);
                                        Link("Plus/Enchant Items", 24);
                                        Link("Bless Items", 25);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 11://Get VIP
                                    {
                                        Say("There are multiple ways to get VIP! If you're under level 70 and non-reborn,");
                                        Say("you can enjoy 24 hours of free VIP. Other ways involve buying it from different");
                                        Say("players in the Market or purchasing it off our website! If you'd like to purchase");
                                        Say("it, please visit our website!");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 12://Newbie Experience
                                    {
                                        Say("If you're under level 70, non-reborn, and would like 2 hours of double");
                                        Say("experience, you can go to NoobAssistant (438, 377) in Twin City (center");
                                        Say(" of Twin City) to inquire about it!");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 13://Power Level Assist
                                    {
                                        if (Client.Hero.Level < 70)
                                        {
                                            Say("I can make an announcement for you that will be broadcast to others wanting");
                                            Say("to power level new players. Would you like me to make the announcement?");
                                            Link("Yes, please", 254);
                                            Link("No thank you.", 255);
                                            Link("Return", 0);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Power Level Assist is for new players under level 70 to be power leveled by");
                                            Say("higher leveled players looking to help the noobies. Because you're over");
                                            Say("level 70, this feature is not available to you.");
                                            Link("Thank you.", 255);
                                            Link("Return", 0);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 14://Special Events
                                    {
                                        Say("We have multiple Special Events for you to enjoy! They are: Pass The Bomb, Team Deathmatch, and Duel");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 15://How to Vote
                                    {
                                        Say("You may vote for our server every 12 hours. Upon login, if our system hasn't");
                                        Say("recorded a vote in 12 hours, we'll notify you with a pop-up to vote.");
                                        Say("Voting helps our server become better known on top sites and thus more");
                                        Say("players! We encourage you to vote for your favorite server! However, if you");
                                        Say("wish to disable these pop-ups, please speak to our Vote NPC in Twin City (435, 374).");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 16://Weapon Sockets
                                    {
                                        Say("Weapons have a chance to earn a socket when upgrading the level or quality");
                                        Say("in Twin City or Market. If you want to socket immediately, you can talk");
                                        Say("to ArtisanOu (756, 545) in Bird Island to insert your first or second");
                                        Say("socket. 1 Dragonball is needed for the first, and an additional 5 for the second.");
                                        Link("Thank you", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 17://Item Sockets[Non-weapon]
                                    {
                                        Say("Armor and other equipment have a chance to earn a socket when upgrading");
                                        Say("the level or quality in Twin City or Market. Sockets can be obtained");
                                        Say("from special events, through special prizes, or Moonbox.");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 18://Adventure Zone
                                    {
                                        Say("Accessing the Adventure Zone is a treachorous and dangerous path.");
                                        Say("I recommend you be a high level before making the trek to the Adventure");
                                        Say("Zone. In addition, you may want to go see one of the GuildConductresses");
                                        Say("to teleport you directly to certain points in Adventure Zone for a fee");
                                        Say("of 1000 silvers.");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 19://The Mines & Mining
                                    {
                                        Say("Mining is an integral part of the world of Conquer. With a pickaxe, any player");
                                        Say("can become a miner! Every major town has a Mine available to be mined,");
                                        Say("and each mine has a multitude of gems waiting to be uncovered. Be");
                                        Say("careful though - the mines are dangerous as they have monsters, or");
                                        Say("even other players out to get miners for their sweet, sweet gems!");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 20://Max Level
                                    {
                                        Say("The max level is currently 130.");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 21://Getting Gear
                                    {
                                        Say("The best players work hard to earn and maintain the best gears. You too, can");
                                        Say("carry high performance gears by working hard. Earning gears is obtained by");
                                        Say("Job promotions, hunting (monster drops), trading, and buying/selling in the");
                                        Say("Market. When it comes to the Market, the right amount of silvers or the");
                                        Say("right item another player wants is the key to success.");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 22://Conquer Points
                                    {
                                        Say("The server does not utilize this currency at this time, Therefore your CP amount will remain zero.");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 23://Available Commands
                                    {
                                        Say("We offer the following quick chat commands to our players:");
                                        Say("@accept - Tournament Acceptance");
                                        Say("@save - Save your character on demand (we also save your character automatically)");
                                        Say("@quit/@dc - Disconnect from the game server.");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 24://Plus/Enchant Items
                                    {
                                        Say("You can plus and enchant your items by going to the Market and speaking to");
                                        Say("WuxingOven (192, 213). Plus'ing an item brings with it more power, whereas");
                                        Say("Enchanting an item brings additional HP to your character.");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 25://Bless Items
                                    {
                                        Say("You can Bless items by speaking to Ethereal (198, 213) in the Market!");
                                        Say("There, you can add additional attributes to your gear, making it even stronger.");
                                        Link("Thank you.", 255);
                                        Link("Return", 0);
                                        Finish(npc);
                                        break;
                                    }
                                case 254://Power Level Assist Broadcast Message
                                    {
                                        if (Client.Hero.Level < 70 && Client.Hero.Stamps.PLevelAssist < DateTime.Now)
                                        {
                                            GameClient.SpeakToAll(Color.Red, ChatType.TopLeft, string.Format("{0} would like to be power leveled. Current level: {1}.", Client.Hero.Name, Client.Hero.Level));
                                            Client.Hero.Stamps.PLevelAssist = DateTime.Now.AddHours(1);
                                            Say("I have made an announcement for you in the top left.");
                                            Link("Thank you!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry, you can only make 1 announcement per hour.");
                                            Link("Okay...", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    case 4103://NoobAssistant
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Welcome to the server! I am here to give newcomers and other's a");
                                        Say("double experience potion if you are under level 70 (and not reborn)!");
                                        Say("Also, I'll give you VIP for a maximum of 24 hours if you're new too!");
                                        Link("Double Experience, please!", 1);
                                        Link("Free VIP, please!", 2);
                                        Link("Power Level Assist", 3);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        if (Client.Hero.Level < 70 && Client.Hero.Reborn == 0)
                                        {
                                            Client.Hero.Stamps.ExpPotion = DateTime.Now.AddHours(2);
                                            Client.Send(new StatTypePacket(Client.Hero.UID, (uint)Kernel.SecondsFromNow(Client.Hero.Stamps.ExpPotion), StatIDs.DoubleExpTime).Serialize());   //THIS IS NOT COMPLETE.
                                            Say("Here's your double experience for 2 hours!");
                                            Link("Thank you!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("The free double expereince potion is for players under level 70 (and not reborn)");
                                            Link("Thank you.", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 2:
                                    {
                                        //THIS IS NOT IMPLEMENTED (yet)
                                        Say("No VIP for you.");
                                        Link("You liar!", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 3:
                                    {
                                        if (Client.Hero.Level < 70)
                                        {
                                            Say("I can make an announcement for you that will be broadcast to others wanting");
                                            Say("to power level new players. Would you like me to make the announcement?");
                                            Link("Yes, please", 254);
                                            Link("No thank you.", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Power Level Assist is for new players under level 70 to be power leveled by");
                                            Say("higher leveled players looking to help the noobies. Because you're over");
                                            Say("level 70, this feature is not available to you.");
                                            Link("Thank you.", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                                case 254://Power Level Assist Broadcast Message
                                    {
                                        if (Client.Hero.Level < 70 && Client.Hero.Stamps.PLevelAssist < DateTime.Now)
                                        {
                                            GameClient.SpeakToAll(Color.Red, ChatType.TopLeft, string.Format("{0} would like to be power leveled. Current level: {1}.", Client.Hero.Name, Client.Hero.Level));
                                            Client.Hero.Stamps.PLevelAssist = DateTime.Now.AddHours(1);
                                            Say("I have made an announcement for you in the top left.");
                                            Link("Thank you!", 255);
                                            Finish(npc);
                                        }
                                        else
                                        {
                                            Say("Sorry, you can only make 1 announcement per hour.");
                                            Link("Okay...", 255);
                                            Finish(npc);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Default Speech
                    default:
                        {
                            switch (Option)
                            {
                                case 0:
                                    {
                                        Say("Sorry! I currently have nothing useful to say!");
                                        Link("NPC ID", 1);
                                        Link("Ok.", 255);
                                        Finish(npc);
                                        break;
                                    }
                                case 1:
                                    {
                                        Say("NPC ID: \"" + npc.UID + "\".");
                                        Link("Thanks.", 255);
                                        Finish(npc);
                                        break;
                                    }
                            }
                        }
                        break;
                        #endregion
                }
                return;
                notEquipped:
                {
                    Say("You don't have that item equipped.");
                    Link("Sorry.", 255);
                    Finish(npc);
                }
            }
            catch (Exception e)
            {
                LogHandler.WriteLine(e);
            }
        }

	    #region Dialog Functions/Variables
	    private static GameClient Client;

	    public static void Say(string Text)
		{
			if (Text.Length > 255)
			{
				Console.WriteLine("NPC of ID {0} has text that is to long");
				return;
			}
			Text = Text.Replace("~", " ") + "\n";
			var Dialog = new NpcPacket((byte)Text.Length);
			var Pac = PacketKernel.Serialize(Dialog, Dialog.Size);
			fixed (byte* Buffer = Pac)
			{
				PacketKernel.Encode(Buffer, Text, 14);
				var SNpc = NpcPacket.Alloc(Buffer, (byte)Text.Length);
				SNpc->ID = NpcType.Dialogue;
				Client.Send(Pac);
			}
		}

	    public static void Link(string Text, byte DialogLink)
		{
			if (Text.Length > 255)
			{
				Console.WriteLine("NPC of ID {0} has text that is to long");
				return;
			}
			Text = Text.Replace("~", " ") + "\n";
			var Dialog = new NpcPacket((byte)Text.Length);
			var Pac = PacketKernel.Serialize(Dialog, Dialog.Size);
			fixed (byte* Buffer = Pac)
			{
				PacketKernel.Encode(Buffer, Text, 14);
				var LNpc = NpcPacket.Alloc(Buffer, (byte)Text.Length);
				LNpc->ID = NpcType.Option;
				LNpc->OptionID = DialogLink;
				Client.Send(Pac);
			}
		}

	    public static void Input(string Text, byte DialogLink)
		{
			if (Text.Length > 255)
			{
				Console.WriteLine("NPC of ID {0} has text that is to long");
				return;
			}
			var Dialog = new NpcPacket((byte)Text.Length);
			var Pac = Dialog.Serialize();
			fixed (byte* Buffer = Pac)
			{
				PacketKernel.Encode(Buffer, Text, 14);
				var LNpc = NpcPacket.Alloc(Buffer, (byte)Text.Length);
				LNpc->ID = NpcType.Input;
				LNpc->OptionID = DialogLink;
				Client.Send(Pac);
			}
		}

	    public static void Face(ushort FaceID)
		{
			var Dialog = new NpcPacket(0);
			Dialog.Avatar = FaceID;
			Dialog.ID = NpcType.Avatar;
			Client.Send(Dialog.Serialize());
		}

	    public static void Finish(INpc npc)
		{
			Face(npc.Avatar);
			var Dialog = new NpcPacket(0);
			Dialog.ID = NpcType.Finish;
			Client.Send(Dialog.Serialize());
		}

	    //
	    // Seperate Finish function for overriding database loaded faces 
	    //
	    public static void Finish()
		{
			var Dialog = new NpcPacket(0);
			Dialog.ID = NpcType.Finish;
			Client.Send(Dialog.Serialize());
		}

	    public static void Teleport(uint MapID, ushort X, ushort Y)
		{
			Client.Teleport(MapID, X, Y);
		}

	    public static void Money(int Value)
		{
			Client.Silvers += (uint)Value;
			Client.Send(new StatTypePacket(Client.Hero.UID, Client.Silvers, StatIDs.Money).Serialize());
		}

	    public static void ErrorMsg(INpc npc, string text)
		{
			Say(text);
			Link("I See.", 255);
			Finish(npc);
		}

	    private static string ReadString(byte[] Data)
		{
			var str = PacketKernel.Decode(Data, 14, Data.Length - 3);
            var split = str.Split('\0');
            return split[0];
        }
	    #endregion
	}
}