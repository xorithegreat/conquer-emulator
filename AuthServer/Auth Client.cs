﻿using System;
using ConquerEmulator.Network;

namespace ConquerEmulator
{
    public enum LoginAction : sbyte
    {
        Normal = 1,
        Create = 2,
        Banned = 3
    }

    public class AuthClient
	{
		public string Account;
		public AuthCrypto Crypter;
		public uint Identifier = 0;
		public string Password;
	    public LoginAction LoginAction = LoginAction.Normal;

        public SystemSocket Socket;

		public AuthClient()
		{
		}

		public AuthClient(SystemSocket _Socket)
		{
			Socket = _Socket;
			Crypter = new AuthCrypto();
		}

		public unsafe void Send(void* Ptr)
		{
            if (Socket.Connected)
            {
                try
                {
                    lock (this)
                    {
                        ushort Size = *((ushort*)Ptr);
                        byte[] Chunk = new byte[Size];
                        Crypter.Encrypt((byte*)Ptr, Chunk, Size);

                        Socket.Send(Chunk);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
		}
	}
}