﻿using System;
using ConquerEmulator;
using ConquerEmulator.Database;
using ConquerEmulator.Network;

namespace ConquerEmulator
{
	public partial class Program
	{
		private static readonly AuthServer AServer = new AuthServer();

		private static void Main(string[] args)
		{
			Console.Title = "Conquer Emulator - Auth Server";

            Console.WriteLine(string.Format("{0," + ((Console.WindowWidth / 2) + (Settings.Credits.Length / 2)) + "}", Settings.Credits));
            
            FlatDatabase.Config = new IniFile(FlatDatabase.Location + "Config.ini");
			FlatDatabase.LoadConfig();
			Bruteforce.Init(10);
			FlatDatabase.CreateConnection();
			AServer.Start();

			Console.WriteLine("Auth Server started.");

			while (true)
			{
				if (string.IsNullOrEmpty(Console.ReadLine()))
					Environment.Exit(0);
			}
		}
	}
}