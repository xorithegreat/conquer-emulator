﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;

namespace ConquerEmulator.Network
{
	public class BruteforceEntry
	{
		public DateTime AddedTimeRemove;
		public string IPAddress;
		public int TimesTried;
		public DateTime Unbantime;
	}

	public static class Bruteforce
	{
		public static int MaxTrials;
		public static ConcurrentDictionary<string, BruteforceEntry> Entries = new ConcurrentDictionary<string, BruteforceEntry>();

		private static readonly ThreadStart ThreadStart = ThreadExecute;

		private static void ThreadExecute()
		{
			while (true)
			{
				var now = DateTime.Now;
                foreach (var be in Entries.Values.ToList())
                {
                    if (be.AddedTimeRemove <= now)
                        Entries.Remove(be.IPAddress);
                    else if ((be.Unbantime <= now) && (be.Unbantime.Day == DateTime.Now.Day))
                        Entries.Remove(be.IPAddress);
                }
				Thread.Sleep(1500);
			}
		}

		public static void Init(int WatchBeforeBan)
		{
			MaxTrials = WatchBeforeBan;
			new Thread(ThreadStart).Start();
		}

		public static void AddTry(string IPAddress)
		{
			BruteforceEntry be;
			if (!Entries.TryGetValue(IPAddress, out be))
			{
				be = new BruteforceEntry
					     {
						     IPAddress = IPAddress,
						     AddedTimeRemove = DateTime.Now.AddMinutes(5),
						     Unbantime = new DateTime()
					     };
				Entries.TryAdd(IPAddress, be);
			}
			else
			{
				be.TimesTried++;
				if (be.TimesTried >= MaxTrials)
					be.Unbantime = DateTime.Now.AddMinutes(15);
			}
		}

		public static bool IsBanned(string IPAddress)
		{
			var isBanned = false;
			BruteforceEntry be;
			if (Entries.TryGetValue(IPAddress, out be))
				isBanned = be.Unbantime.Day == DateTime.Now.Day;
			return isBanned;
		}
	}
}