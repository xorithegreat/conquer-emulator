﻿using System;
using ConquerEmulator.Database;
using ConquerEmulator.Network;
using ConquerEmulator.Packet_Structures;

namespace ConquerEmulator
{
	public partial class Program
	{
		private unsafe class AuthServer
		{
			private readonly ServerSocket aServer = new ServerSocket();

			public void Start()
			{
				aServer.OnConnect += AuthServer_OnClientConnect;
				aServer.OnReceive += AuthServer_OnClientReceive;

				aServer.Enable(FlatDatabase.APort, 100, 1024);
			}

			private void AuthServer_OnClientReceive(SystemSocket Sender, byte[] Arg)
			{
				var Client = Sender.Owner as AuthClient;
				if (Client == null)
					return;

				try
				{
					Client.Crypter.Decrypt(Arg, Arg, Arg.Length);
					fixed (byte* Buffer = Arg)
					{
						var Type = *(ushort*)(Buffer + 2);
						switch (Type)
						{
							case 0x43E:
								{
									var resp = AuthResponsePacket.Create();
									if (Arg.Length == 276)
									{
										Client.Account = new string((sbyte*)Buffer + 4);
										Client.Password = new string(PasswordCrypter.Decrypt((uint*)(Buffer + 132)));
										if (FlatDatabase.Authenticate(Client))
										{
                                            resp.IPAddress = FlatDatabase.ServerIP;
											resp.Key1 = Client.Identifier;
											resp.Key2 = NativeExtended.GetHashCode32(Client.Password);
											resp.Port = FlatDatabase.GPort;
                                            Console.WriteLine("{0}: {1}/{2} has connected.", DateTime.Now, Client.Account, Client.Socket.Socket.RemoteEndPoint);
                                        }
										else
										{
                                            resp.Key1 = 1;
                                            Client.Identifier = 0;
											Bruteforce.AddTry(Sender.RemoteAddress);
										}
										Client.Send(&resp);
									}
									else
									{
										Client.Socket.Disconnect();
									}
								}
								break;
						}
					}
				}
				catch (Exception e)
				{
					FlatDatabase.WriteLine(e);
					Sender.Disconnect();
				}
			}

			private void AuthServer_OnClientConnect(SystemSocket Sender, object Arg)
			{
				Sender.Owner = new AuthClient(Sender);
				if (Bruteforce.IsBanned(Sender.RemoteAddress))
					Sender.Disconnect();
			}
		}
	}
}