﻿using System;
using System.IO;
using MySql.Data.MySqlClient;

namespace ConquerEmulator.Database
{
	internal class FlatDatabase
	{
		public static string Location = Directory.GetCurrentDirectory() + "\\Database\\";
		public static IniFile Config;
		private static readonly object WriteSync = new object();

		public static void LoadConfig()
		{
            myAcc = Config.ReadString("Database", "Account", "root");
            myPass = Config.ReadString("Database", "Password", "7CX3o28V3G9499D");
            myIP = Config.ReadString("Database", "mysqlIP", "localhost");
            myDB = Config.ReadString("Database", "dbName", "conquer");
            myPort = Config.ReadUInt16("Database", "Port", 3306);
            ServerIP = Config.ReadString("Server", "IP", "107.172.221.147");
            GPort = Config.ReadUInt16("Server", "Game Port", 5816);
        }

		public static bool Authenticate(AuthClient Client)
		{
            using (var m_Conn = new MySqlConnection(ConnectionString))
            {
                m_Conn.Open();

                var Command = new MySqlCommand("SELECT * FROM accounts WHERE Name = @name", m_Conn);
                Command.Parameters.AddWithValue("@name", Client.Account);

                var DPass = "";
                using (var rdr = Command.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        Client.Identifier = Convert.ToUInt32(rdr["Id"].ToString());
                        DPass = rdr["Password"].ToString();
                        Client.LoginAction = (LoginAction)Convert.ToInt16(rdr["LoginAction"]);
                        rdr.Close();
                    }
                }

                if (Client.Password != DPass)
                    return false;
            }
			return true;
		}

		public static void WriteLine(Exception Exc)
		{
			try
			{
				lock (WriteSync)
				{
					var errorsDirectory = Location + "\\Errors\\";
					if (!Directory.Exists(errorsDirectory))
						Directory.CreateDirectory(errorsDirectory);

					var errorLog = errorsDirectory + $"Errors({DateTime.Now.Month}.{DateTime.Now.Day}).txt";
					using (var writer = new StreamWriter(errorLog, true))
					{
						writer.WriteLine("[{0}] \r\n{1}\r\n", DateTime.Now, Exc);
					}
				}
				Console.WriteLine(Exc.ToString());
			}
			catch
			{
			}
		}
		#region Connection
		public static string myAcc, myPass, myIP, myDB, ServerIP, ServerVersion;
		public static ushort myPort, APort, GPort;

		public static string ConnectionString;

		public static bool CreateConnection()
		{
			ConnectionString = "Server='" + myIP + "';Port='" + myPort + "';Database='" + myDB + "';Username='" + myAcc + "';Password='" + myPass + "';Pooling=true; Max Pool Size = 100; Min Pool Size = 25; SslMode=none";

			try
			{
				using (var m_Conn = new MySqlConnection(ConnectionString))
				{
					m_Conn.Open();
					m_Conn.Close();
					return true;
				}
			}
			catch (Exception e)
			{
				return false;
			}
		}
		#endregion
	}
}