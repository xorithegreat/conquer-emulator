﻿using System.Runtime.InteropServices;

namespace ConquerEmulator.Packet_Structures
{
	/// <summary>
	///     0x41F (Server->Client)
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	public unsafe struct AuthResponsePacket
	{
		public ushort Size;
		public ushort Type;
		public uint Key2;
		public uint Key1;
		private fixed sbyte szIPAddress [16];
		public uint Port;

		public string IPAddress
		{
			get
			{
				fixed (sbyte* bp = szIPAddress)
				{
					return new string(bp);
				}
			}
			set
			{
				var ip = value;
				fixed (sbyte* bp = szIPAddress)
				{
					for (var i = 0; i < ip.Length; i++)
						bp[i] = (sbyte)ip[i];
				}
			}
		}

		public static AuthResponsePacket Create()
		{
			var retn = new AuthResponsePacket();
			retn.Size = 0x20;
			retn.Type = 0x41F;
			return retn;
		}
	}
}