﻿using System.Collections.Concurrent;
using System.Runtime.InteropServices;

namespace ConquerEmulator
{
	public unsafe class Native
	{
		#region Msvcrt
		[DllImport("msvcrt.dll")]
		public static extern void* memcpy(void* dst, void* src, int length);
		#endregion
		#region Winmn
		[DllImport("winmm.dll", EntryPoint = "timeGetTime", CallingConvention = CallingConvention.StdCall)]
		public static extern uint timeGetTime();
		#endregion
		#region Kernel32
		[DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		public static extern uint GetPrivateProfileIntW(string Section, string Key, int Default, string FileName);

		[DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		public static extern uint GetPrivateProfileStringW(string Section, string Key, string Default, char* ReturnedString, int Size, string FileName);

		[DllImport("kernel32.dll", CharSet = CharSet.Ansi)]
		public static extern uint GetPrivateProfileStringA(string Section, string Key, void* Default, sbyte* ReturnedString, int Size, string FileName);

		[DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool GetPrivateProfileStructW(string Section, string Key, void* lpStruct, int StructSize, string FileName);

		[DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		public static extern int GetPrivateProfileSectionNamesW(char* ReturnBuffer, int Size, string FileName);

		[DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		public static extern int GetPrivateProfileSectionW(string Section, char* ReturnBuffer, int Size, string FileName);

		[DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool WritePrivateProfileStringW(string Section, string Key, string Value, string FileName);

		[DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool WritePrivateProfileStructW(string Section, string Key, void* lpStruct, int StructSize, string FileName);

		[DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool WritePrivateProfileSectionW(string Section, string String, string FileName);
		#endregion
	}

	public static class NativeExtended
	{
		public static bool Remove<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dictionary, TKey key)
		{
			TValue ignored;
			return dictionary.TryRemove(key, out ignored);
		}

		public static unsafe uint GetHashCode32(string s)
		{
			fixed (char* str = s.ToCharArray())
			{
				var chPtr = str;
				var num = 0x15051505;
				var num2 = num;
				var numPtr = (int*)chPtr;
				for (var i = s.Length; i > 0; i -= 4)
				{
					num = ((num << 5) + num + (num >> 0x1b)) ^ numPtr[0];
					if (i <= 2)
						break;
					num2 = ((num2 << 5) + num2 + (num2 >> 0x1b)) ^ numPtr[1];
					numPtr += 2;
				}
				return (uint) (num + num2 * 0x5d588b65);
			}
		}
	}
}