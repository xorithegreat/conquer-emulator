﻿using System;
using System.Threading;

namespace ConquerEmulator.Main
{
	public class RandomGenerator : Random
	{
		private object _syncRoot;

		public static RandomGenerator Generator { get; } = new RandomGenerator();

		public object SyncRoot
		{
			get
			{
				if (_syncRoot == null)
					Interlocked.CompareExchange(ref _syncRoot, new object(), null);
				return _syncRoot;
			}
		}

		public override int Next()
		{
			lock (SyncRoot)
			{
				return base.Next();
			}
		}

		public override int Next(int maxVal)
		{
			lock (SyncRoot)
			{
				return base.Next(maxVal);
			}
		}

		public override int Next(int minVal, int maxVal)
		{
			lock (SyncRoot)
			{
				return base.Next(minVal, maxVal);
			}
		}

		public override void NextBytes(byte[] buffer)
		{
			lock (SyncRoot)
			{
				base.NextBytes(buffer);
			}
		}

		public override double NextDouble()
		{
			lock (SyncRoot)
			{
				return base.NextDouble();
			}
		}
	}
}