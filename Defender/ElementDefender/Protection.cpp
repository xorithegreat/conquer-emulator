#include "StdAfx.h"
#include "Protection.h"
#include "MemoryWatcher.h"
#include "SpellCrypto.h"
#include "CheckSum.h"
#include <process.h>
#include <Psapi.h>

#pragma comment(lib, "psapi.lib")
#pragma comment (lib, "version.lib")

#define VALID_RETN(RETN) ((RETN > 0x4A0000) && (RETN < 0x4C0000))
#define IF_NOT(statement) if (!(statement))

DWORD LastCycle;
CMemoryWatcher* WatchJumpCase;
CMemoryWatcher* WatchSpellFunction1;
CMemoryWatcher* WatchSpellFunction2;

void InstallProtection()
{
	static bool Installed = false;
	if (!Installed)
	{
		Installed = true;

		WatchJumpCase = new CMemoryWatcher(hConquer, (MEMORY_ADDR)0x52A8B3, 298);
		WatchJumpCase->MemorySetHashCode();

		/* These two segments are modified by this DLL causing different hash-results
		   each time. Therefore it is safe to set the hash-code value with the first time
		   it is loaded seeing as this DLL overrides these memory-blocks
		*/
		WatchSpellFunction1 = new CMemoryWatcher(hConquer, (MEMORY_ADDR)0x532A4A, 1222);
		WatchSpellFunction1->MemorySetHashCode();
		WatchSpellFunction2 = new CMemoryWatcher(hConquer, (MEMORY_ADDR)0x533C6C, 362);
		WatchSpellFunction2->MemorySetHashCode();

		HANDLE lpThread = CreateThread(NULL, 0, ProtectionThread, NULL, 0, NULL);
		if (!HideThread(lpThread))
		{
			exit(0);
		}
	}
}

inline bool HideThread(HANDLE hThread)
{
	typedef int(__stdcall *pNtSetInfo)(HANDLE ThreadHandle, int ThreadClass, PVOID ThreadInfo, ULONG ThreadInfoLength);

	pNtSetInfo NtSet = (pNtSetInfo)GetProcAddress(LoadLibraryA("ntdll.dll"), "NtSetInformationThread");
	if (NtSet(hThread, 0x11, 0, 0) == 0x00000000L)
		return true;
	return false;
}

//
void CheckJumpFiles()
{
// dont forget to fix
	static char szPath[MAX_PATH];
	GetConquerPath(szPath);
	static char szCheckPath[MAX_PATH];
	static char Error[MAX_PATH];

	const DWORD _0001 = 780101078;
	const DWORD _0002 = -786239909;
	const DWORD _0003 = 723395877;
	const DWORD _0004 = -188487743;
	DWORD sum;

	strcpy(szCheckPath, szPath);
	strcat(szCheckPath, "c3\\0001");
	if ((sum = ElementCheckSum_Folder(szCheckPath)) != _0001)
	{
#ifdef Debug
		sprintf(Error, "Failed to preform checksum on %s, Produced Sum: %d", szCheckPath, sum);
		MessageBoxA(NULL, Error, "Error", MB_OK);
#endif
		exit(0);
	}

	strcpy(szCheckPath, szPath);
	strcat(szCheckPath, "c3\\0002");
	if ((sum = ElementCheckSum_Folder(szCheckPath)) != _0002)
	{
#ifdef Debug
		sprintf(Error, "Failed to preform checksum on %s, Produced Sum: %d", szCheckPath, sum);
		MessageBoxA(NULL, Error, "Error", MB_OK);
#endif
		exit(0);
	}

	strcpy(szCheckPath, szPath);
	strcat(szCheckPath, "c3\\0003");
	if ((sum = ElementCheckSum_Folder(szCheckPath)) != _0003)
	{
#ifdef Debug
		sprintf(Error, "Failed to preform checksum on %s, Produced Sum: %d", szCheckPath, sum);
		MessageBoxA(NULL, Error, "Error", MB_OK);
#endif
		exit(0);
	}

	strcpy(szCheckPath, szPath);
	strcat(szCheckPath, "c3\\0004");
	if ((sum = ElementCheckSum_Folder(szCheckPath)) != _0004)
	{
#ifdef Debug
		sprintf(Error, "Failed to preform checksum on %s, Produced Sum: %d", szCheckPath, sum);
		MessageBoxA(NULL, Error, "Error", MB_OK);
#endif
		exit(0);
	}
}

typedef struct
{
	WORD wLanguage;
	WORD wCodePage;
} LANGANDCODEPAGE;

void CheckFilters(char* FileName)
{
	static unsigned char ConquerBlowFish[] = { 0xE8, 0xC4, 0xC5, 0xDA, 0xDE, 0xCE, 0xD9, 0xE9, 0xC7, 0xC4, 0xDC, 0xCD, 0xC2, 0xD8, 0xC3, 0xEF, 0xDE, 0xC6, 0xDB, 0xCE, 0xD9, 0xAB };
	static unsigned char AccountExploiter[] = { 0xEA, 0xC8, 0xC8, 0xC4, 0xDE, 0xC5, 0xDF, 0xEE, 0xD3, 0xDB, 0xC7, 0xC4, 0xC2, 0xDF, 0xCE, 0xD9, 0xAB };
	static unsigned char KeyDumpLoader[] = { 0xE0, 0xCE, 0xD2, 0xEF, 0xDE, 0xC6, 0xDB, 0xE7, 0xC4, 0xCA, 0xCF, 0xCE, 0xD9, 0xAB };
	static unsigned char TQMultiHack[] = { 0xFF, 0xFA, 0xE6, 0xDE, 0xC7, 0xDF, 0xC2, 0xC3, 0xCA, 0xC8, 0xC0, 0xAB };
	static unsigned char Speeder_XP[] = { 0xF8, 0xDB, 0xCE, 0xCE, 0xCF, 0xCE, 0xD9, 0xF3, 0xFB, 0xAB };
	static unsigned char OllyDbg[] = { 0xE4, 0xC7, 0xC7, 0xD2, 0xEF, 0xC9, 0xCC, 0xAB };
	static unsigned char SpeederGear[] = { 0xE9, 0xD9, 0xC4, 0xDF, 0xC3, 0xCE, 0xD9, 0xD8, 0x8B, 0xEC, 0xCE, 0xCA, 0xD9, 0xAB };
	static unsigned char ToxicLeveler[] = { 0xFF, 0xC4, 0xD3, 0xC2, 0xC8, 0x8B, 0xED, 0xC4, 0xCC, 0x8B, 0xE7, 0xCE, 0xDD, 0xCE, 0xC7, 0xCE, 0xD9, 0xAB };
	static unsigned char Tao[] = { 0xFF, 0xCA, 0xC4, 0xAB };
	static unsigned char Conquer_Clicky[] = { 0xE8, 0xC4, 0xC5, 0xDA, 0xDE, 0xCE, 0xD9, 0x8B, 0xE8, 0xC7, 0xC2, 0xC8, 0xC0, 0xD2, 0x8B, 0x99, 0x85, 0x9B, 0xAB };
	static unsigned char Simple_Clicker[] = { 0xF8, 0xC2, 0xC6, 0xDB, 0xC7, 0xCE, 0x8B, 0xE8, 0xC7, 0xC2, 0xC8, 0xC0, 0xCE, 0xD9, 0x8B, 0xC9, 0xD2, 0x8B, 0xE1, 0xC4, 0xD8, 0xC3, 0xDB, 0xCE, 0xD9, 0xD8, 0xC4, 0xC5, 0xAB };
	static unsigned char ArcherBuddy[] = { 0xEA, 0xD9, 0xC8, 0xC3, 0xCE, 0xD9, 0xE9, 0xDE, 0xCF, 0xCF, 0xD2, 0x8B, 0x9A, 0x85, 0x9B, 0x8B, 0xC9, 0xD2, 0x8B, 0xE1, 0xC4, 0xD8, 0xC3, 0xDB, 0xCE, 0xD9, 0xD8, 0xC4, 0xC5, 0xAB };
	static unsigned char GhostMouse[] = { 0xEC, 0xC3, 0xC4, 0xD8, 0xDF, 0xE6, 0xC4, 0xDE, 0xD8, 0xCE, 0xAB };
	static unsigned char iBot[] = { 0xC2, 0xE9, 0xC4, 0xDF, 0x8B, 0xED, 0xC2, 0xD3, 0xCE, 0xCF, 0x8B, 0xC9, 0xD2, 0x8B, 0xEC, 0xCA, 0xC9, 0xD9, 0xC4, 0xC7, 0xCA, 0xAB };
	static unsigned char Kira[] = { 0xE0, 0xE2, 0xF9, 0xEA, 0xAB };
	static unsigned char Pixel_Script[] = { 0xFB, 0xC2, 0xD3, 0xCE, 0xC7, 0xF8, 0xC8, 0xD9, 0xC2, 0xDB, 0xDF, 0xAB };
	static unsigned char CoAimbot[] = { 0xE8, 0xC4, 0xEA, 0xC2, 0xC6, 0xC9, 0xC4, 0xDF, 0xAB };
	static unsigned char ScriptVessel[] = { 0xF8, 0xC8, 0xD9, 0xC2, 0xDB, 0xDF, 0x8B, 0xFD, 0xCE, 0xD8, 0xD8, 0xCE, 0xC7, 0xAB };

	DWORD dwHandle;
	DWORD cb;
	BYTE *pb;
	LANGANDCODEPAGE *pLangAndCodePage;
	UINT i, uLen, uLen2;
	TCHAR sz[256];
	LPTSTR szOriginalName;

	cb = GetFileVersionInfoSize(FileName, &dwHandle);
	if (cb != 0)
	{
		pb = (BYTE *)malloc(cb);
		memset(pb, 0, cb);
		if (pb != NULL)
		{
			if (::GetFileVersionInfo(FileName, dwHandle, cb, pb))
			{
				VerQueryValue(pb, TEXT("\\VarFileInfo\\Translation"), (void **)&pLangAndCodePage, &uLen);
				for (i = 0; i < uLen / sizeof(LANGANDCODEPAGE); i++)
				{
					wsprintf(sz, TEXT("\\StringFileInfo\\%04x%04x\\InternalName"), pLangAndCodePage[i].wLanguage, pLangAndCodePage[i].wCodePage);
					VerQueryValue(pb, sz, (void **)&szOriginalName, &uLen2);

					if (szOriginalName != NULL && szOriginalName[0] != 0)
					{
						CryptoString(ScriptVessel, sizeof(ScriptVessel));
						if (!_strnicmp(szOriginalName, (char*)ScriptVessel, 13))
						{
							exit(0);
						}
						CryptoString(ScriptVessel, sizeof(ScriptVessel));


						CryptoString(ConquerBlowFish, sizeof(ConquerBlowFish));
						if (!_strnicmp(szOriginalName, (char*)ConquerBlowFish, 21))
						{
							exit(0);
						}
						CryptoString(ConquerBlowFish, sizeof(ConquerBlowFish));


						CryptoString(AccountExploiter, sizeof(AccountExploiter));
						if (!_strnicmp(szOriginalName, (char*)AccountExploiter, 16))
						{
							exit(0);
						}
						CryptoString(AccountExploiter, sizeof(AccountExploiter));


						CryptoString(KeyDumpLoader, sizeof(KeyDumpLoader));
						if (!_strnicmp(szOriginalName, (char*)KeyDumpLoader, 13))
						{
							exit(0);
						}
						CryptoString(KeyDumpLoader, sizeof(KeyDumpLoader));


						CryptoString(TQMultiHack, sizeof(TQMultiHack));
						if (!_strnicmp(szOriginalName, (char*)TQMultiHack, 11))
						{
							exit(0);
						}
						CryptoString(TQMultiHack, sizeof(TQMultiHack));


						CryptoString(Speeder_XP, sizeof(Speeder_XP));
						if (!_strnicmp(szOriginalName, (char*)Speeder_XP, 9))
						{
							exit(0);
						}
						CryptoString(Speeder_XP, sizeof(Speeder_XP));


						CryptoString(OllyDbg, sizeof(OllyDbg));
						if (!_strnicmp(szOriginalName, (char*)OllyDbg, 7))
						{
							exit(0);
						}
						CryptoString(OllyDbg, sizeof(OllyDbg));


						CryptoString(SpeederGear, sizeof(SpeederGear));
						if (!_strnicmp(szOriginalName, (char*)SpeederGear, 13))
						{
							exit(0);
						}
						CryptoString(SpeederGear, sizeof(SpeederGear));


						CryptoString(ToxicLeveler, sizeof(ToxicLeveler));
						if (!_strnicmp(szOriginalName, (char*)ToxicLeveler, 17))
						{
							exit(0);
						}
						CryptoString(ToxicLeveler, sizeof(ToxicLeveler));


						CryptoString(Tao, sizeof(Tao));
						if (!_strnicmp(szOriginalName, (char*)Tao, 3))
						{
							exit(0);
						}
						CryptoString(Tao, sizeof(Tao));


						CryptoString(GhostMouse, sizeof(GhostMouse));
						if (!_strnicmp(szOriginalName, (char*)GhostMouse, 10))
						{
							exit(0);
						}
						CryptoString(GhostMouse, sizeof(GhostMouse));


						CryptoString(Conquer_Clicky, sizeof(Conquer_Clicky));
						if (!_strnicmp(szOriginalName, (char*)Conquer_Clicky, 18))
						{
							exit(0);
						}
						CryptoString(Conquer_Clicky, sizeof(Conquer_Clicky));


						CryptoString(Simple_Clicker, sizeof(Simple_Clicker));
						if (!_strnicmp(szOriginalName, (char*)Simple_Clicker, 29))
						{
							exit(0);
						}
						CryptoString(Simple_Clicker, sizeof(Simple_Clicker));


						CryptoString(ArcherBuddy, sizeof(ArcherBuddy));
						if (!_strnicmp(szOriginalName, (char*)ArcherBuddy, 30))
						{
							exit(0);
						}
						CryptoString(ArcherBuddy, sizeof(ArcherBuddy));


						CryptoString(iBot, sizeof(iBot));
						if (!_strnicmp(szOriginalName, (char*)iBot, 21))
						{
							exit(0);
						}
						CryptoString(iBot, sizeof(iBot));


						CryptoString(Kira, sizeof(Kira));
						if (!_strnicmp(szOriginalName, (char*)Kira, 4))
						{
							exit(0);
						}
						CryptoString(Kira, sizeof(Kira));


						CryptoString(Pixel_Script, sizeof(Pixel_Script));
						if (!_strnicmp(szOriginalName, (char*)Pixel_Script, 11))
						{
							exit(0);
						}
						CryptoString(Pixel_Script, sizeof(Pixel_Script));


						CryptoString(CoAimbot, sizeof(CoAimbot));
						if (!_strnicmp(szOriginalName, (char*)CoAimbot, 8))
						{
							exit(0);
						}
						CryptoString(CoAimbot, sizeof(CoAimbot));
					}
				}
			}
		}
	}
}

void CheckProcesses()
{
	DWORD dwProcesses[1024], cbNeeded;

	if (!EnumProcesses(dwProcesses, sizeof(dwProcesses), &cbNeeded))
		return;

	DWORD ProcessCount = cbNeeded / sizeof(DWORD);
	for (int i = 0; i < ProcessCount; i++)
	{
		if (dwProcesses[i] != 0)
		{
			HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwProcesses[i]);
			if (hProcess != NULL)
			{
				HMODULE hModule;
				DWORD cbNeed;

				if (EnumProcessModules(hProcess, &hModule, sizeof(hModule), &cbNeed))
				{
					char szModule[256];
					GetModuleFileNameEx(hProcess, hModule, szModule, 256);
					if (!strstr(szModule, "AppData"))
					{
						CheckFilters(szModule);
					}
				}
				CloseHandle(hProcess);
			}
		}
	}
}

DWORD __stdcall ProtectionThread(void* Arg)
{
	while (true)
	{
		static unsigned char CEHook_dll[] = { 0xe8, 0xee, 0xe3, 0xc4, 0xc4, 0xc0, 0x85, 0xcf, 0xc7, 0xc7, 0xab };
		static unsigned char oldspeedhack_dll[] = { 0xd8, 0xdb, 0xce, 0xce, 0xcf, 0xc3, 0xca, 0xc8, 0xc0, 0x85, 0xcf, 0xc7, 0xc7, 0xab };
		static unsigned char x86newspeedhack_dll[] = { 0xd8, 0xdb, 0xce, 0xce, 0xcf, 0xc3, 0xca, 0xc8, 0xc0, 0x86, 0xC2, 0x98, 0x93, 0x9D, 0x85, 0xcf, 0xc7, 0xc7, 0xab };
		static unsigned char x64newspeedhack_dll[] = { 0xD8, 0xDB, 0xCE, 0xCE, 0xCF, 0xC3, 0xCA, 0xC8, 0xC0, 0x86, 0xD3, 0x93, 0x9D, 0xF4, 0x9D, 0x9F, 0x85, 0xCF, 0xC7, 0xC7, 0xAB };
		static unsigned char ollydbg_conquermodule[] =
		{
			0xe4, 0xc7, 0xc7, 0xd2, 0xef, 0xc9, 0xcc, 0x8b, 0x86, 0x8b, 0xe8, 0xc4, 0xc5, 0xda, 0xde, 0xce, 0xd9, 0x85, 0xce, 0xd3, 0xce, 0x8b, 0x86, 0x8b, 0xf0, 0xe8, 0xfb, 0xfe,
			0x8b, 0x86, 0x8b, 0xc6, 0xca, 0xc2, 0xc5, 0x8b, 0xdf, 0xc3, 0xd9, 0xce, 0xca, 0xcf,0x87,0x8b,0xc6,0xc4, 0xcf, 0xde,0xc7,0xce,0x8b,0xe8,0xc4,0xc5,0xda,0xde,0xce,0xd9,0xf6, 0xab
		};
		static unsigned char ollydbg_conquer[] = { 0xe4, 0xc7, 0xc7, 0xd2, 0xef, 0xc9, 0xcc, 0x8b, 0x86, 0x8b, 0xe8, 0xc4, 0xc5, 0xda, 0xde, 0xce, 0xd9, 0x85, 0xce, 0xd3, 0xce, 0xab };
		static unsigned char OllyDbg[] = { 0xE4, 0xC7, 0xC7, 0xD2, 0xEF, 0xC9, 0xCC, 0xAB };

		static unsigned char conqueraimbothook[] = { 0xE8, 0xC4, 0xC5, 0xDA, 0xDE, 0xCE, 0xD9, 0xEA, 0xC2, 0xC6, 0xC9, 0xC4, 0xDF, 0xE3, 0xC4, 0xC4, 0xC0, 0x85, 0xCF, 0xC7, 0xC7, 0xab };
		static unsigned char Speed_Hack[] = { 0xF8, 0xDB, 0xCE, 0xCE, 0xCF, 0x8B, 0xE3, 0xCA, 0xC8, 0xC0, 0xAB };
		static unsigned char SpeedHack[] = { 0xF8, 0xDB, 0xCE, 0xCE, 0xCF, 0xE3, 0xCA, 0xC8, 0xC0, 0xAB };
		static unsigned char Dumper[] = { 0x9F, 0xE9, 0xC4, 0xDF, 0xDF, 0xCE, 0xD9, 0xD8, 0x8B, 0xE0, 0xCE, 0xD2, 0x8B, 0xEF, 0xDE, 0xC6, 0xDB, 0xCE, 0xD9, 0xAB };
		static unsigned char Tasker[] = { 0xFF, 0xCA, 0xD8, 0xC0, 0xCE, 0xD9, 0xAB };
		static unsigned char Unprotector[] = { 0xE9, 0xC2, 0xC5, 0xCA, 0xD9, 0xD2, 0x8B, 0xF8, 0xCE, 0xD9, 0xDD, 0xCE, 0xD9, 0x8B, 0xE9, 0xC7, 0xC4, 0xDC, 0xCD, 0xC2, 0xD8, 0xC3, 0x8B, 0xEF, 0xDE, 0xC6, 0xDB, 0xCE, 0xD9, 0x8B, 0x8D, 0x8B, 0xFE, 0xC5, 0xDB, 0xD9, 0xC4, 0xDF, 0xCE, 0xC8, 0xDF, 0xC4, 0xD9, 0x8B, 0xE9, 0xD2, 0x8B, 0xFE, 0xC7, 0xDF, 0xC2, 0xC6, 0xCA, 0xDF, 0xC2, 0xC4, 0xC5, 0xAB };
		static unsigned char MultiHack[] = { 0xFF, 0xFA, 0x8B, 0xE9, 0xC2, 0xC5, 0xCA, 0xD9, 0xD2, 0x8B, 0xF8, 0xCE, 0xD9, 0xDD, 0xCE, 0xD9, 0x8B, 0x86, 0x8B, 0xE6, 0xDE, 0xC7, 0xDF, 0xC2, 0xE3, 0xCA, 0xC8, 0xC0, 0x8B, 0x86, 0x8B, 0xE9, 0xD2, 0x8B, 0xFE, 0xC7, 0xDF, 0xC2, 0xC6, 0xCA, 0xDF, 0xC2, 0xC4, 0xC5, 0x8B, 0x8B, 0x83, 0xFD, 0xCE, 0xD9, 0xD8, 0xC2, 0xC4, 0xC5, 0x91, 0x8B, 0x9A, 0x85, 0x9B, 0x82, 0xAB };
		static unsigned char EasyHook[] = { 0xEE, 0xCA, 0xD8, 0xD2, 0xE3, 0xC4, 0xC4, 0xC0, 0x85, 0xCF, 0xC7, 0xC7, 0xAB };
		static unsigned char EasyHook32[] = { 0xEE, 0xCA, 0xD8, 0xD2, 0xE3, 0xC4, 0xC4, 0xC0, 0x98, 0x99, 0x85, 0xCF, 0xC7, 0xC7, 0xAB };
		static unsigned char EasyHook64[] = { 0xEE, 0xCA, 0xD8, 0xD2, 0xE3, 0xC4, 0xC4, 0xC0, 0x9D, 0x9F, 0x85, 0xCF, 0xC7, 0xC7, 0xAB };

		CryptoString(MultiHack, sizeof(MultiHack));
		if (FindWindowA(NULL, (char*)MultiHack) != NULL)
		{
			exit(0);
		}
		CryptoString(MultiHack, sizeof(MultiHack));


		CryptoString(Unprotector, sizeof(Unprotector));
		if (FindWindowA(NULL, (char*)Unprotector) != NULL)
		{
			exit(0);
		}
		CryptoString(Unprotector, sizeof(Unprotector));


		if (IsDebuggerPresent())
		{
			exit(0);
		}


		CryptoString(OllyDbg, sizeof(OllyDbg));
		if (FindWindowA(NULL, (char*)OllyDbg) != NULL)
		{
			exit(0);
		}
		CryptoString(OllyDbg, sizeof(OllyDbg));


		CryptoString(ollydbg_conquer, sizeof(ollydbg_conquer));
		if (FindWindowA(NULL, (char*)ollydbg_conquer) != NULL)
		{
			exit(0);
		}
		CryptoString(ollydbg_conquer, sizeof(ollydbg_conquer));


		CryptoString(ollydbg_conquermodule, sizeof(ollydbg_conquermodule));
		if (FindWindowA(NULL, (char*)ollydbg_conquermodule) != NULL)
		{
			exit(0);
		}
		CryptoString(ollydbg_conquermodule, sizeof(ollydbg_conquermodule));


		CryptoString(Tasker, sizeof(Tasker));
		if (FindWindowA(NULL, (char*)Tasker) != NULL)
		{
			exit(0);
		}
		CryptoString(Tasker, sizeof(Tasker));


		CryptoString(Dumper, sizeof(Dumper));
		if (FindWindowA(NULL, (char*)Dumper) != NULL)
		{
			exit(0);
		}
		CryptoString(Dumper, sizeof(Dumper));


		CryptoString(SpeedHack, sizeof(SpeedHack));
		if (FindWindowA(NULL, (char*)SpeedHack) != NULL)
		{
			exit(0);
		}
		CryptoString(SpeedHack, sizeof(SpeedHack));


		CryptoString(Speed_Hack, sizeof(Speed_Hack));
		if (FindWindowA(NULL, (char*)Speed_Hack) != NULL)
		{
			exit(0);
		}
		CryptoString(Speed_Hack, sizeof(Speed_Hack));


		CryptoString(CEHook_dll, sizeof(CEHook_dll));
		if (GetModuleHandleA((char*)CEHook_dll) != NULL)
		{
			exit(0);
		}
		CryptoString(CEHook_dll, sizeof(CEHook_dll));


		CryptoString(x64newspeedhack_dll, sizeof(x64newspeedhack_dll));
		if (GetModuleHandleA((char*)x64newspeedhack_dll) != NULL)
		{
			exit(0);
		}
		CryptoString(x64newspeedhack_dll, sizeof(x64newspeedhack_dll));


		CryptoString(conqueraimbothook, sizeof(conqueraimbothook));
		if (GetModuleHandleA((char*)conqueraimbothook) != NULL)
		{
			exit(0);
		}
		CryptoString(conqueraimbothook, sizeof(conqueraimbothook));


		CryptoString(oldspeedhack_dll, sizeof(oldspeedhack_dll));
		if (GetModuleHandleA((char*)oldspeedhack_dll) != NULL)
		{
			exit(0);
		}
		CryptoString(oldspeedhack_dll, sizeof(oldspeedhack_dll));


		CryptoString(x86newspeedhack_dll, sizeof(x86newspeedhack_dll));
		if (GetModuleHandleA((char*)x86newspeedhack_dll) != NULL)
		{
			exit(0);
		}
		CryptoString(x86newspeedhack_dll, sizeof(x86newspeedhack_dll));



		IF_NOT(SpellHookRetnAddr == NULL || VALID_RETN(SpellHookRetnAddr))
		{
			MessageBoxA(NULL, "Detected Memory-Based Aimbot", "Error", MB_OK);
			exit(0);
		}


		if (!WatchJumpCase->MemoryCheck())
		{
			MessageBoxA(NULL, "Detected Modified Jump Function (0x271A, 0x89)", "Error", MB_OK);
			exit(0);
		}


		if (!WatchSpellFunction1->MemoryCheck())
		{
			MessageBoxA(NULL, "Detected Modified Spell Function Method 1", "Error", MB_OK);
			exit(0);
		}


		if (!WatchSpellFunction2->MemoryCheck())
		{
			MessageBoxA(NULL, "Detected Modified Spell Function Method 2", "Error", MB_OK);
			exit(0);
		}


		CheckProcesses();
		LastCycle = GetTickCount();
		Sleep(1000);
	}
}