// stdafx.cpp : source file that includes just the standard includes
// ElementDefender.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

void CryptoString (unsigned char* str, int len)
{
	for (int i = 0; i < len; i++)
		str[i] ^= 0xAB;
}

void GetConquerPath (char* szPath)
{
	GetModuleFileNameA(NULL, szPath, MAX_PATH);
	int i = strlen(szPath);
	for (int j = i-1; j >= 0; j--)
	{
		if (szPath[j] == '\\')
		{
			szPath[j + 1] = NULL;
			break;
		}
	}
}

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file
