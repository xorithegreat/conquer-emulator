#pragma once
#ifndef __SWORDFISH_CRYPTO_H__
#define __SWORDFISH_CRYPTO_H__

class CSwordfish
{
private:
		unsigned int* Key;
        unsigned char* Dec_Ivec;
        unsigned char* Enc_Ivec;
        unsigned char nDec;
        unsigned char nEnc;

		void SFSmallCrypt(unsigned int& I, unsigned char* I2, unsigned int key);
		void SFBigCrypt(unsigned int& l, unsigned int& r);
public:
	CSwordfish(unsigned char* userkey, int userkeyLength);
	~CSwordfish(void);
	void SFEncrypt(unsigned char* Data, int DataLength);
	void SFDecrypt(unsigned char* Data, int DataLength);
};

#endif