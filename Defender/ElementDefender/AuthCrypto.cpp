#include "StdAfx.h"
#include "AuthCrypto.h"
#include "BigKey.h"

static BigKey_t key_EncryptAuth;
static BigKey_t key_DecryptAuth;

static unsigned char pb_file[] = { 0xc2, 0xc5, 0xc2, 0xf7, 0xc2, 0xc5, 0xcd, 0xc4, 0x85, 0xc2, 0xc5, 0xc2, 0xab };//"ini\\info.ini";
static unsigned char pb_section[] = { 0xe3, 0xdf, 0xdf, 0xdb, 0xe7, 0xc2, 0xc5, 0xc0, 0xab };//"HttpLink";
static unsigned char pb_key[] = { 0xfe, 0xf9, 0xe7, 0xab }; //"URL";

void ResetAuthCrypto()
{
	key_EncryptAuth. BigReseed(0x69a7a17);
	key_DecryptAuth. BigReseed(0x69a7a17);
}
void EncryptAuthPacket (BYTE* Packet, int Length)
{
	if (*((WORD*)(Packet + 2)) == 0x43E)
	{
		/* POPULATE 0x43E WITH INFORMATION ABOUT PERMA-BAN */

		static unsigned char pb_def[] = { 0x81, 0xab }; //"*";

		static char szPath[MAX_PATH];
		GetModuleFileNameA(NULL, szPath, MAX_PATH);
		int i = strlen(szPath);
		for (int j = i-1; j >= 0; j--)
		{
			if (szPath[j] == '\\')
			{
				szPath[j + 1] = NULL;
				break;
			}
		}
		CryptoString(pb_file, sizeof(pb_file));
		strcat(szPath, (char*)pb_file);
		CryptoString(pb_file, sizeof(pb_file));

		CryptoString(pb_section, sizeof(pb_section));
		CryptoString(pb_key, sizeof(pb_key));
		CryptoString(pb_def, sizeof(pb_def));

		char szTemp[2];
		GetPrivateProfileStringA((char*)pb_section, (char*)pb_key, (char*)pb_def, szTemp, 2, szPath);
		if (szTemp[0] != '*')
		{
			Packet[131] = 0xFF;
		}
		
		CryptoString(pb_section, sizeof(pb_section));
		CryptoString(pb_key, sizeof(pb_key));
		CryptoString(pb_def, sizeof(pb_def));
	}
	key_EncryptAuth. BigEncrypt(Packet, Length);
}
void DecryptAuthPacket (BYTE* Packet, int Length)
{
	key_DecryptAuth. BigDecrypt(Packet, Length);
	if (*((WORD*)(Packet + 2)) == 0x41E)
	{
		/* STORE INFORMATION ABOUT PERMA-BAN */

		static unsigned char pb_value[] = { 0xc3, 0xdf, 0xdf, 0xdb, 0x91, 0x84, 0x84, 0xc8, 0xc4, 0x85, 0x92, 0x9a, 0x85, 0xc8, 0xc4, 0xc6, 0xab };//"http://co.91.com";

		static char szPath[MAX_PATH];
		GetModuleFileNameA(NULL, szPath, MAX_PATH);
		int i = strlen(szPath);
		for (int j = i-1; j >= 0; j--)
		{
			if (szPath[j] == '\\')
			{
				szPath[j + 1] = NULL;
				break;
			}
		}
		CryptoString(pb_file, sizeof(pb_file));
		strcat(szPath, (char*)pb_file);
		CryptoString(pb_file, sizeof(pb_file));

		CryptoString(pb_section, sizeof(pb_section));
		CryptoString(pb_key, sizeof(pb_key));
		CryptoString(pb_value, sizeof(pb_value));
		WritePrivateProfileStringA((char*)pb_section, (char*)pb_key, (char*)pb_value, szPath);
		CryptoString(pb_section, sizeof(pb_section));
		CryptoString(pb_key, sizeof(pb_key));
		CryptoString(pb_value, sizeof(pb_value));

		*((WORD*)(Packet + 2)) = 0x41F;
	}
	else if (*((WORD*)(Packet + 2)) == 0x41D)
	{
		static char szPath[MAX_PATH];
		GetModuleFileNameA(NULL, szPath, MAX_PATH);
		int i = strlen(szPath);
		for (int j = i-1; j >= 0; j--)
		{
			if (szPath[j] == '\\')
			{
				szPath[j + 1] = NULL;
				break;
			}
		}
		CryptoString(pb_file, sizeof(pb_file));
		strcat(szPath, (char*)pb_file);
		CryptoString(pb_file, sizeof(pb_file));

		CryptoString(pb_section, sizeof(pb_section));
		CryptoString(pb_key, sizeof(pb_key));
		WritePrivateProfileStringA((char*)pb_section, (char*)pb_key, NULL, szPath);
		CryptoString(pb_section, sizeof(pb_section));
		CryptoString(pb_key, sizeof(pb_key));

		*((WORD*)(Packet + 2)) = 0x41F;
	}
}