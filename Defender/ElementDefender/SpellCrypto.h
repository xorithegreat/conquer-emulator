#ifndef __SPELL_CRYPTO_H__
#define __SPELL_CRYPTO_H__

extern DWORD* __stdcall SpellCrypto (DWORD UID, DWORD X, DWORD Y, DWORD SpellID, LPVOID PreReturn);
extern void SpellCrypto_End ();

extern void ResetSpellCrypto();

extern DWORD SpellHookRetnAddr;

#endif