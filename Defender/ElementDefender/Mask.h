// AuthCrypto.cpp
#define EncryptAuthPacket fdebb0123
#define DecryptAuthPacket f9234febc
#define ResetAuthCrypto f329485
#define ResetSpellCrypto f102211

// BigKey.h
#define BigKey_t ffee245
#define BigEncrypt f271aa7a
#define BigDecrypt fa7aa192
#define BigReseed f2852578

// CheckSum.h
#define ElementCheckSum ffee2472
#define ElementCheckSum_Folder ffee2473

// dllmain.cpp
#define AuthCrypto f968636
#define InstallAuthCrypto f21847
#define my_connect f2415412
#define my_exec f2412412
#define IsConquerExecutable f332562
#define InstallDebuggerProtection f298eba2
#define InstallSpellCrypto_v1 f62413369
#define InstallSpellCrypto_v2 f989202
#define InstallJumpProtection f213420
#define LoginHook f1273982
#define InstallLoginHook f133792
#define InstallDetours f0985532
#define UninstallDetours f241412

// FileSearch.cpp
#define SearchDirectory f19839e1ab

// ElementDefender.cpp
#define ResetAllEncryptions fb294e
#define DefenderInit f12989aeb

// Key.h
#define Key_t f3298532
#define GenerateKey fa7a2918
#define Reseed f2421889

// MemoryWatcher.cpp
#define CMemoryWatcher f257135
#define MemorySetHashCode f2198344
#define MemoryShowHashCode fabc123
#define MemoryCheck fab91eef

// Protection.cpp
#define ProtectionThread f98198ea
#define InstallProtection f98198eb
#define CheckProcesses f324923
#define HideThread f23j423
#define CheckFilters f386211
#define version_nmsp f398872
#define CheckJumpFiles f98198ad

// SpellCrypto.cpp
#define SpellCrypto f19839ef1
#define SpellCrypto_End f100abc21
#define ResetSpellCrypt fabde11f

// StdAfx.cpp
#define CryptoString f2352e21
#define GetConquerPath f198198e

// Swordfish.cpp
#define RollLeft f1984195
#define RollRight f329067980
#define CSwordfish f1297398
#define SFSmallCrypt f129849
#define SFBigCrypt f1938eab
#define SFEncrypt f109eab10
#define SFDecrypt f091abcf1