#include "StdAfx.h"
#include "SpellCrypto.h"
#include "SwordFish.h"

CSwordfish* fisherman = NULL;
DWORD SpellHookRetnAddr = NULL;
#define VALID_SPELLRETN(Addr) (((0x533CEB < Addr && Addr < 0x533CEB + 148)) || ((0x532D57 < Addr && Addr < 0x532D57 + 144)))

void ResetSpellCrypto()
{
	static unsigned char sfkey[] = { 0x75, 0x22, 0x09, 0xC9, 0x88, 0xD9, 0x99, 0x06 };
	if (fisherman != NULL)
		delete fisherman;
	fisherman = new CSwordfish(sfkey, sizeof(sfkey));
}

DWORD* __stdcall	SpellCrypto (DWORD UID, DWORD X, DWORD Y, DWORD SpellID, LPVOID PreReturn)
{
	static DWORD returndw[4];
	static DWORD check_ebp;

	__asm mov eax, dword ptr ss:[ebp+4]
	__asm mov check_ebp, eax

	if (VALID_SPELLRETN(check_ebp))
	{
		fisherman->SFEncrypt((PBYTE)&UID, sizeof(DWORD));
		fisherman->SFEncrypt((PBYTE)&X, sizeof(WORD));
		fisherman->SFEncrypt((PBYTE)&Y, sizeof(WORD));	
		fisherman->SFEncrypt((PBYTE)&SpellID, sizeof(WORD));
	}
	else
	{
		fisherman->SFEncrypt((PBYTE)&SpellID, sizeof(WORD));
		fisherman->SFEncrypt((PBYTE)&Y, sizeof(WORD));	
		fisherman->SFEncrypt((PBYTE)&UID, sizeof(DWORD));
		fisherman->SFEncrypt((PBYTE)&X, sizeof(WORD));
	}

	if (GetTickCount() - LastCycle > 10000)
	{
		exit(0);
	}

	returndw[0] = UID;
	returndw[1] = X;
	SpellHookRetnAddr = (DWORD)((DWORD)PreReturn + 1);
	returndw[2] = Y;
	returndw[3] = SpellID;
	return returndw;
}
__declspec(naked) void SpellCrypto_End () { }