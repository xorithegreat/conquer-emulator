#ifndef __BIG_KEY_H__
#define __BIG_KEY_H__

#include "Key.h"

struct BigKey_t
{
private:
	static const int key_capacity = sizeof(DWORD) * 4;
	BYTE key_bytes[key_capacity];
	Key_t key;
	int pos;
public:
	BigKey_t ()
	{
		BigReseed (0);
		pos = 0;
	}
	BigKey_t (DWORD initial_key)
	{
		BigReseed (initial_key);
	}
	__forceinline void BigReseed (DWORD initial_key)
	{
		key. Reseed(initial_key, initial_key ^ 0x12345, initial_key ^ 0xabcdef, initial_key & 0xFFFF);
		DWORD* dwkey = (DWORD*)key_bytes;
		dwkey[0] = key. GenerateKey();
		dwkey[1] = key. GenerateKey();
		dwkey[2] = key. GenerateKey();
		dwkey[3] = key. GenerateKey();
		pos = 0;		
	}
	__forceinline void BigEncrypt (BYTE* block, int block_size)
	{
		DWORD block_checksum = 0;
		for (int i = 0; i < block_size; i++)
		{
			block_checksum = (block_checksum << 4) ^ block[i];
			block[i] ^= key_bytes[pos++];
			if (pos == key_capacity)
			{
				BigReseed (block_checksum);
			}
		}
	}
	__forceinline void BigDecrypt (BYTE* block, int block_size)
	{
		DWORD block_checksum = 0;
		for (int i = 0; i < block_size; i++)
		{
			block[i] ^= key_bytes[pos++];
			block_checksum = (block_checksum << 4) ^ block[i];
			if (pos == key_capacity)
			{
				BigReseed (block_checksum);
			}
		}
	}
};
#endif