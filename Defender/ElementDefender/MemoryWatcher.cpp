#include "StdAfx.h"
#include "MemoryWatcher.h"
#include "CheckSum.h"

void CMemoryWatcher::MemorySetHashCode()
{
	BYTE* buffer = new BYTE[BlockSize];
	ReadProcessMemory(hProcess, Address, buffer, BlockSize, NULL);
	HashResult = ElementCheckSum(buffer, BlockSize);
	delete[] buffer;
}

void CMemoryWatcher::MemorySetHashCode(DWORD Result)
{
	HashResult = Result;
}

#ifndef Publish
void CMemoryWatcher::MemoryShowHashCode(char* szName)
{
	char Buffer[256];
	sprintf(Buffer, "The result hashcode for the block %s is %x", szName, HashResult);
	MessageBoxA(NULL, Buffer, "ShowHashCode", MB_OK);
}
#endif

BOOL CMemoryWatcher::MemoryCheck()
{
	BYTE* buffer = new BYTE[BlockSize];
	ReadProcessMemory(hProcess, Address, buffer, BlockSize, NULL);
	DWORD tempResult = ElementCheckSum(buffer, BlockSize);
	delete[] buffer;

	if (tempResult == HashResult)
		return TRUE;
	return FALSE;
}

CMemoryWatcher::CMemoryWatcher(HANDLE process, MEMORY_ADDR addr, int blockSize)
{
	hProcess = process;
	Address = addr;
	BlockSize = blockSize;
}

CMemoryWatcher::~CMemoryWatcher(void)
{
}
