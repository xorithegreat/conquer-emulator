// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "detours.h"
#include "ElementDefender.h"
//
#pragma comment(lib, "Shell32.lib")
HANDLE hConquer;
DWORD ConquerID;
//

static int (WINAPI *trampoline_connect)(SOCKET s, const sockaddr *name, int namelen) = connect;
static HINSTANCE(WINAPI *trampoline_exec)(HWND hwnd, LPCSTR lpOperation, LPCSTR lpFile, LPCSTR lpParameters, LPCSTR lpDirectory, int ShowCmd) = ShellExecuteA;

// The InstallAuthCrypto() function, will need the be changed (addresses)
// as newer patches come out, and these addresses change.
void __stdcall AuthCrypto (BYTE* Packet, int Length, BOOL Decrypt)
{
	if (Decrypt)
		DecryptAuthPacket (Packet, Length);
	else
		EncryptAuthPacket (Packet, Length);
}
void InstallAuthCrypto (DWORD ConquerID, HANDLE hConquer)
{
	// DllInject
	const int DllInject_Size = 256;
	BYTE DllInject[DllInject_Size];
	// Hook Addresses
	const LPVOID EncryptionHook = (LPVOID)0x005100DD;
	const LPVOID EncryptionHook_SendPush = (LPVOID)0x0050FA49;
	const LPVOID EncryptionHook_Patch = (LPVOID)0x0051012A;
	// Hook Sizes
	const int EncryptionHook_Size = 94;
	const int EncryptionHook_SendPush_Size = 2;

	DWORD Address = (DWORD)AuthCrypto;

	memset(DllInject, NOP, DllInject_Size);
	DllInject[3] = 0xFF; // PUSH DWORD PTR SS:[EBP+10] - Decrypt (BOOL)
	DllInject[4] = 0x75;
	DllInject[5] = 0x10;
	DllInject[6] = 0xFF; // PUSH DWORD PTR SS:[EBP+C] - Packet Length (DWORD)
	DllInject[7] = 0x75;
	DllInject[8] = 0x0C;
	DllInject[9] = 0x50; // PUSH EAX - Packet (BYTE*)
	DllInject[10] = 0xB8; // MOV EAX, Address
	DllInject[11] = (BYTE)(Address & 0xFF);
	DllInject[12] = (BYTE)((Address >> 8) & 0xFF);
	DllInject[13] = (BYTE)((Address >> 16) & 0xFF);
	DllInject[14] = (BYTE)((Address >> 24) & 0xFF);
	DllInject[15] = 0xFF; // CALL EAX
	DllInject[16] = 0xD0;
	WriteProcessMemory(hConquer, EncryptionHook, DllInject, EncryptionHook_Size, NULL);
		
	memset(DllInject, NOP, DllInject_Size);
	DllInject[0] = 0x6A; // PUSH 0 - FALSE, Parameter: 'Decrypt'
	DllInject[1] = 0x00;
	WriteProcessMemory(hConquer, EncryptionHook_SendPush, DllInject, EncryptionHook_SendPush_Size, NULL);
}
//
// This will never need to be modified.
void IsConquerExecutable (DWORD ConquerID)
{
#ifdef Publish
	HMODULE hmain = GetModuleHandle(NULL);
	char szName[MAX_PATH];
	GetModuleFileNameA(hmain, szName, MAX_PATH);
	const char* ConquerExe = "Conquer.exe";
	char* ptrName = (szName + strlen(szName)) - strlen(ConquerExe);
	if (strcmp(ptrName, ConquerExe) != 0)
	{
#ifdef ALERT_FAILURE
		MessageBoxA(NULL, "This application is not Conquer.exe", "Error", MB_OK);
#endif
		exit(0);
	}
#endif
}
//
// This will never need to be modified.
void InstallDebuggerProtection (DWORD ConquerID, HANDLE hConquer)
{
#ifdef Publish
	const int Patch_Size = 1;
	BYTE DllInject[Patch_Size];
	DWORD Address = (DWORD)exit;
	HMODULE Module = GetModuleHandleA("ntdll.dll");
	if (Module != NULL)
	{
		FARPROC PatchAddr = GetProcAddress(Module, "DbgBreakPoint");
		if (PatchAddr != NULL)
		{
			memset(DllInject, NOP, Patch_Size);
			WriteProcessMemory(hConquer, (LPVOID)PatchAddr, DllInject, Patch_Size, NULL);
		}
	}
#endif
}
//
// The InstallSpellCrypto_v1(), and InstallSpellCrypto_v2() function, 
// will need the be changed (addresses) as newer patches come out, and these addresses change.
void InstallSpellCrypto_v1 (DWORD ConquerID, HANDLE hConquer) 
{
	const int DllInject_Size = 256;
	BYTE DllInject[DllInject_Size];

	const int SpellPatch_Size = 144;
	const LPVOID SpellPatch = (LPVOID)0x00532D57;

	DWORD Address;

	memset(DllInject, NOP, DllInject_Size);
	DllInject[0] = 0x6A; // PUSH 0
	DllInject[1] = 0x00;
	DllInject[2] = 0xFF; //	PUSH DWORD PTR SS:[EBP-10 - Spell Level (TQ Encrypted)
	DllInject[3] = 0x75;
	DllInject[4] = 0xF0;
	DllInject[5] = 0x8B; // MOV EDI,ECX
	DllInject[6] = 0xF9;
	DllInject[7] = 0xC7; // MOV DWORD PTR SS:[EBP-4],9
	DllInject[8] = 0x45;
	DllInject[9] = 0xFC;
	DllInject[10] = 0x09; 
	DllInject[11] = 0x00;
	DllInject[12] = 0x00;
	DllInject[13] = 0x00;
	DllInject[14] = 0x8B; // MOV ECX,ESI
	DllInject[15] = 0xCE;
	DllInject[16] = 0x81; // XOR ECX,2B426E85
	DllInject[17] = 0xF1;
	DllInject[18] = 0x85;
	DllInject[19] = 0x6E;
	DllInject[20] = 0x42;
	DllInject[21] = 0x2B;
	DllInject[22] = 0x8B; // MOV EDX,ECX
	DllInject[23] = 0xD1;
	DllInject[24] = 0xC1; // SHR EDX,13
	DllInject[25] = 0xEA;
	DllInject[26] = 0x13;
	DllInject[27] = 0xC1; // SHL ECX,0D
	DllInject[28] = 0xE1;
	DllInject[29] = 0x0D;
	DllInject[30] = 0x09; // OR EDX,ECX
	DllInject[31] = 0xCA;
	DllInject[32] = 0x8B; // MOV ECX,DWORD PTR SS:[EBP+C]
	DllInject[33] = 0x4D;
	DllInject[34] = 0x0C;
	DllInject[35] = 0x89; // MOV DWORD PTR SS:[EBP+8],EDX
	DllInject[36] = 0x55; 
	DllInject[37] = 0x08;
	DllInject[38] = 0xFF; // PUSH DWORD PTR SS:[EBP+4] - Return Pointer
	DllInject[39] = 0x75;
	DllInject[40] = 0x04;
	DllInject[41] = 0x57; // PUSH EDI - Spell ID (Unencrypted)

	DllInject[47] = 0x8B; // MOV EDX,ECX
	DllInject[48] = 0xD1;
	DllInject[49] = 0x8B; // MOV ECX,DWORD PTR SS:[EBP+10]
	DllInject[50] = 0x4D;
	DllInject[51] = 0x10;
	DllInject[52] = 0x8B; // MOV ESI,ECX
	DllInject[53] = 0xF1;
	DllInject[54] = 0x8B; // MOV EAX,DWORD PTR SS:[EBP-14]
	DllInject[55] = 0x45;
	DllInject[56] = 0xEC;
	DllInject[57] = 0x8B; // MOV EAX,DWORD PTR DS:[EAX+60]
	DllInject[58] = 0x40;
	DllInject[59] = 0x60;

	Address = (DWORD)SpellCrypto;
	DllInject[66] = 0x59; // POP ECX (Move SpellID back into ECX)
	DllInject[67] = 0x51; // PUSH ECX - SpellID (Unecnrypted)
	DllInject[68] = 0x56; // PUSH ESI - Y (Unencrypted)
	DllInject[69] = 0x52; // PUSH EDX - X (Unencrypted)
	DllInject[70] = 0x50; // PUSH EAX - UID (Unencrypted, TQ doesn't encrypt this)
	DllInject[71] = 0xB8; // MOV EAX,Address		
	DllInject[72] = (BYTE)(Address & 0xFF);
    DllInject[73] = (BYTE)((Address >> 8) & 0xFF);
    DllInject[74] = (BYTE)((Address >> 16) & 0xFF);
    DllInject[75] = (BYTE)((Address >> 24) & 0xFF);
	DllInject[76] = 0xFF; // CALL EAX
	DllInject[77] = 0xD0;
	DllInject[78] = 0x8B; // MOV ESI, DWORD PTR DS:[EAX+8] - Encrypted Y
	DllInject[79] = 0x70;
	DllInject[80] = 0x08;
	DllInject[81] = 0x8B; // MOV EDX, DWORD PTR DS:[EAX+4] - Encrypted X
	DllInject[82] = 0x50;
	DllInject[83] = 0x04;
	DllInject[84] = 0x8B; // MOV ECX, DWORD PTR DS:[EAX+C] - Encrypted SpellID
	DllInject[85] = 0x48;
	DllInject[86] = 0x0C;
	DllInject[87] = 0x51; // PUSH ECX (this'll be used for the TQ function, SpellID)
	DllInject[88] = 0x8B; // MOV EAX, DWORD PTR DS:[EAX] - Encrypted UID
	DllInject[89] = 0x00;

	DllInject[100] = 0x8D; // LEA ECX,DWORD PTR SS:[EBP-438] 
	DllInject[101] = 0x8D;
	DllInject[102] = 0xC8;
	DllInject[103] = 0xFB; 
	DllInject[104] = 0xFF;
	DllInject[105] = 0xFF;

	WriteProcessMemory(hConquer, SpellPatch, DllInject, SpellPatch_Size, NULL);
}
void InstallSpellCrypto_v2 (DWORD ConquerID, HANDLE hConquer)
{
	const int DllInject_Size = 256;
	BYTE DllInject[DllInject_Size];

	const int SpellPatch_Size = 148;
	const LPVOID SpellPatch = (LPVOID)0x00533CEB;

	DWORD Address;
	memset(DllInject, NOP, DllInject_Size);

	DllInject[0] = 0xFF; // PUSH DWORD PTR SS:[EBP-34]
	DllInject[1] = 0x75;
	DllInject[2] = 0xCC;
	DllInject[3] = 0xC6; // MOV BYTE PTR SS:[EBP-4],29
	DllInject[4] = 0x45;
	DllInject[5] = 0xFC;
	DllInject[6] = 0x29;
	DllInject[7] = 0x8B; // MOV EDI,ECX
	DllInject[8] = 0xF9;
	DllInject[9] = 0x8B; // MOV ECX,DWORD PTR SS:[EBP+C] 
	DllInject[10] = 0x4D;
	DllInject[11] = 0x0C;
	DllInject[12] = 0x81; // ADD ECX,746F4AE6
	DllInject[13] = 0xC1;
	DllInject[14] = 0xE6;
	DllInject[15] = 0x4A;
	DllInject[16] = 0x6F;
	DllInject[17] = 0x74;
	DllInject[18] = 0x33; // XOR ECX,ESI
	DllInject[19] = 0xCE;
	DllInject[20] = 0x81; // XOR ECX,5F2D2463
	DllInject[21] = 0xF1;
	DllInject[22] = 0x63;
	DllInject[23] = 0x24;
	DllInject[24] = 0x2D;
	DllInject[25] = 0x5F;

	DllInject[27] = 0x8B; // MOV EDX,ECX
	DllInject[28] = 0xD1;
	DllInject[29] = 0xC1; // SHR EDX,13
	DllInject[30] = 0xEA;
	DllInject[31] = 0x13;
	DllInject[32] = 0xC1; // SHL ECX,0D
	DllInject[33] = 0xE1;
	DllInject[34] = 0x0D;
	DllInject[35] = 0x09; // OR EDX,ECX
	DllInject[36] = 0xCA;
	DllInject[37] = 0x8B; // MOV ECX,DWORD PTR SS:[EBP-4C] - X
	DllInject[38] = 0x4D;
	DllInject[39] = 0xB4;
	DllInject[40] = 0x89; // MOV DWORD PTR SS:[EBP+8],EDX
	DllInject[41] = 0x55;
	DllInject[42] = 0x08;
	DllInject[43] = 0x8B; // MOV EDX,ECX
	DllInject[44] = 0xD1;

	DllInject[45] = 0x8B; // MOV ESI,DWORD PTR SS:[EBP-50] - Y
	DllInject[46] = 0x75;
	DllInject[47] = 0xB0;

	DllInject[48] = 0x8B; // MOV EAX,DWORD PTR SS:[EBP-10]
	DllInject[49] = 0x45;
	DllInject[50] = 0xF0;

	DllInject[51] = 0x8B; // MOV EAX,DWORD PTR DS:[EAX+60]
	DllInject[52] = 0x40;
	DllInject[53] = 0x60;

	DllInject[54] = 0xFF; // PUSH DWORD PTR SS:[EBP+4] - Return Pointer
	DllInject[55] = 0x75;
	DllInject[56] = 0x04;
	DllInject[57] = 0x57; // PUSH EDI (SpellID)

	Address = (DWORD)SpellCrypto;
	DllInject[66] = 0x59; // POP ECX (Move SpellID back into ECX)
	DllInject[67] = 0x51; // PUSH ECX - SpellID (Unecnrypted)
	DllInject[68] = 0x56; // PUSH ESI - Y (Unencrypted)
	DllInject[69] = 0x52; // PUSH EDX - X (Unencrypted)
	DllInject[70] = 0x50; // PUSH EAX - UID (Unencrypted, TQ doesn't encrypt this)
	DllInject[71] = 0xB8; // MOV EAX,Address		
	DllInject[72] = (BYTE)(Address & 0xFF);
    DllInject[73] = (BYTE)((Address >> 8) & 0xFF);
    DllInject[74] = (BYTE)((Address >> 16) & 0xFF);
    DllInject[75] = (BYTE)((Address >> 24) & 0xFF);
	DllInject[76] = 0xFF; // CALL EAX
	DllInject[77] = 0xD0;
	DllInject[78] = 0x8B; // MOV ESI, DWORD PTR DS:[EAX+8] - Encrypted Y
	DllInject[79] = 0x70;
	DllInject[80] = 0x08;
	DllInject[81] = 0x8B; // MOV EDX, DWORD PTR DS:[EAX+4] - Encrypted X
	DllInject[82] = 0x50;
	DllInject[83] = 0x04;
	DllInject[84] = 0x8B; // MOV ECX, DWORD PTR DS:[EAX+C] - Encrypted SpellID
	DllInject[85] = 0x48;
	DllInject[86] = 0x0C;
	DllInject[87] = 0x51; // PUSH ECX (this'll be used for the TQ function, SpellID)
	DllInject[88] = 0x8B; // MOV EAX, DWORD PTR DS:[EAX] - Encrypted UID
	DllInject[89] = 0x00;	

	DllInject[100] = 0x8D; // LEA ECX,DWORD PTR SS:[EBP-438] 
	DllInject[101] = 0x8D;
	DllInject[102] = 0x5C;
	DllInject[103] = 0xFA; 
	DllInject[104] = 0xFF;
	DllInject[105] = 0xFF;

	WriteProcessMemory(hConquer, SpellPatch, DllInject, SpellPatch_Size, NULL);
}
//
// The InstallLoginHook() and LoginHook() function will need to be changed (addresses)
// as newer patches come out, and these addresses change.
void __stdcall LoginHook()
{
	LPVOID WATCH_EBX = (LPVOID)0x00650218;
	LPVOID ASM_EBX;
	__asm mov ASM_EBX,ebx

	if (ASM_EBX != WATCH_EBX)
	{
		ResetAllEncryptions();
	}
}
void InstallLoginHook (DWORD ConquerID, HANDLE hConquer)
{
	const int Patch_Size = 37;
	const LPVOID Patch_Addr = (LPVOID)0x0050F5B3;
	BYTE Patch[Patch_Size];
	memset(Patch, NOP, Patch_Size);

	DWORD Address = (DWORD)LoginHook;
	Patch[0] = 0xB8; // MOV EAX, Address
	Patch[1] = (BYTE)(Address & 0xFF);
	Patch[2] = (BYTE)((Address >> 8) & 0xFF);
	Patch[3] = (BYTE)((Address >> 16) & 0xFF);
	Patch[4] = (BYTE)((Address >> 24) & 0xFF);
	Patch[5] = 0xFF; // CALL EAX
	Patch[6] = 0xD0;
	Patch[7] = 0x33; // XOR EBX,EBX
	Patch[8] = 0xDB;
	Patch[9] = 0x89; // MOV DWORD PTR DS:[EDI+4],EBX
	Patch[10] = 0x5F; 
	Patch[11] = 0x04;
	Patch[12] = 0x89; // MOV DWORD PTR DS:[EDI],EBX
	Patch[13] = 0x1F;
	Patch[14] = 0x8D; // LEA EDI,DWORD PTR DS:[ESI+1221C]
	Patch[15] = 0xBE;
	Patch[16] = 0x1C;
	Patch[17] = 0x22;
	Patch[18] = 0x01;
	Patch[19] = 0x00;
	Patch[20] = 0x8D; // LEA ECX,DWORD PTR DS:[EDI+8]
	Patch[21] = 0x4F;
	Patch[22] = 0x08;
	Patch[14] = 0x8D; // LEA ECX,DWORD PTR DS:[ESI+12430]
	Patch[15] = 0x8E;
	Patch[16] = 0x30;
	Patch[17] = 0x24;
	Patch[18] = 0x01;
	Patch[19] = 0x00;
	Patch[20] = 0x89; // MOV DWORD PTR DS:[EDI+4],EBX
	Patch[21] = 0x5F;
	Patch[22] = 0x04; 
	Patch[23] = 0x89; // MOV DWORD PTR DS:[EDI],EBX
	Patch[24] = 0x1F;
	
	WriteProcessMemory(hConquer, Patch_Addr, Patch, Patch_Size, NULL);
}
//
// The InstallJumpProtection() function will need to be changed (addresses)
// as newer patches come out, and these addresses change.
void InstallJumpProtection()
{
#ifdef Publish
	const int Patch_Size = 2;
	const LPVOID Patch_Addr = (LPVOID)0x52A8B3;
	BYTE Patch[Patch_Size] = { 0xeb, 0x7a };

	WriteProcessMemory(hConquer, Patch_Addr, Patch, Patch_Size, NULL);
#endif
}
//
// Detoured Connect Function
//
int WINAPI my_connect(SOCKET s, const sockaddr *name, int namelen)
{
	sockaddr_in *sck = (sockaddr_in*)name;
	unsigned short dwPort = htons(sck->sin_port);
	if (dwPort >= 9958 && dwPort <= 9964)
	{
		dwPort = 9958;
	}
	else if (dwPort == 80)
	{
		return HOST_NOT_FOUND;
	}
	sck->sin_port = htons(dwPort);
	sck->sin_addr.s_addr = inet_addr(szServerIP);
	return trampoline_connect(s, (sockaddr*)sck, sizeof(sockaddr_in));
}
//
// Detoured ShellExecute Function
//
HINSTANCE WINAPI my_exec(HWND hwnd, LPCSTR lpOperation, LPCSTR lpFile, LPCSTR lpParameters, LPCSTR lpDirectory, int ShowCmd)
{
	if (strcmp(lpFile, "http://co.91.com/signout/") == 0)
		return (HINSTANCE)ERROR_FILE_NOT_FOUND;
	return trampoline_exec(hwnd, lpOperation, lpFile, lpParameters, lpDirectory, ShowCmd);
}
//
//This Function does not need modification
//	
void InstallDetours(HMODULE hModule)
{
	DisableThreadLibraryCalls(hModule);
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourAttach(&(PVOID &)trampoline_exec, my_exec);
	DetourAttach(&(PVOID &)trampoline_connect, my_connect);
	DetourTransactionCommit();
}
void UninstallDetours()
{
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourDetach(&(PVOID &)trampoline_exec, my_exec);
	DetourDetach(&(PVOID &)trampoline_connect, my_connect);
	DetourTransactionCommit();
}

//
//
// Main entry point (DLL)
BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		ConquerID = GetProcessId(GetCurrentProcess());
		hConquer = OpenProcess(PROCESS_VM_WRITE | PROCESS_VM_READ | PROCESS_VM_OPERATION, FALSE, ConquerID);
		if (hConquer == NULL)
		{
			MessageBoxA(NULL, "OpenProcess() error.", "Error", MB_OK);
		}
		else
		{
			InstallDebuggerProtection (ConquerID, hConquer);
			IsConquerExecutable (ConquerID);
			InstallDetours(hModule); // WORKING

			InstallAuthCrypto (ConquerID, hConquer);
			InstallSpellCrypto_v1 (ConquerID, hConquer);
			InstallSpellCrypto_v2 (ConquerID, hConquer);
			InstallLoginHook (ConquerID, hConquer);
			InstallJumpProtection();

			DefenderInit ();
		}
		break;

	case DLL_PROCESS_DETACH:
		{
			UninstallDetours();
			break;
		}
	}
	return TRUE;
}

