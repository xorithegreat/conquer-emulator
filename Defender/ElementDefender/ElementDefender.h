#ifndef __HYBRID_DEFENDER_H__
#define __HYBRID_DEFENDER_H__


#include "CheckSum.h"
#include "MemoryWatcher.h"
#include "Key.h"
#include "BigKey.h"
#include "Swordfish.h"
#include "AuthCrypto.h"
#include "SpellCrypto.h"
#include "Protection.h"

extern void DefenderInit ();
extern void ResetAllEncryptions ();

#endif