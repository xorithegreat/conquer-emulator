#ifndef __AUTH_CRYPTO_H__
#define __AUTH_CRYPTO_H__


extern void EncryptAuthPacket (BYTE* Packet, int Length);
extern void DecryptAuthPacket (BYTE* Packet, int Length);
extern void ResetAuthCrypto ();

#endif