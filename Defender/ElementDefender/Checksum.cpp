#include "StdAfx.h"
#include "FileSearch.h"
#include <vector>

DWORD ElementCheckSum (char* file)
{
	FILE* pfile = fopen(file, "rb");
	if (pfile == NULL)
	{
#ifdef ALERT_FAILURE
		MessageBoxA(NULL, "fopen(?, \"rb\") Failed.", "Error", MB_OK);
#endif
		exit(0);
	}
	DWORD numPtr[2];
	int num = 352654597;
	int num2 = num;
	while (fread(numPtr, sizeof(DWORD), 2, pfile) == 2)
	{
		num = (((num << 5) + num) + (num >> 27)) ^ numPtr[0];
		num2 = (((num2 << 5) + num2) + (num2 >> 27)) ^ numPtr[1];
    }
	fclose(pfile);
    return (num + (num2 * 1566083941));
}
DWORD ElementCheckSum (unsigned char* bytes, int length)
{
	int num = 352654597;
	int num2 = num;
	for (int i = 0; i < length; i += 4)
	{
		WORD* numPtr = (WORD*)(bytes + i);
		num = (((num << 5) + num) + (num >> 27)) ^ numPtr[0];
		num2 = (((num2 << 5) + num2) + (num2 >> 27)) ^ numPtr[1];
	}
	return (num + (num2 * 1566083941));
}
DWORD ElementCheckSum_Folder (char* path)
{
	int len = strlen(path)-1;
	if (path[len] == '\\')
		path[len] = NULL;
	std::vector<std::string>::iterator it;
	std::vector<std::string> dir;
	SearchDirectory(dir, path, "c3");

	DWORD sum = 0;
	for (it = dir.begin(); it < dir.end(); it++)
	{
		std::string str = *it;
		sum ^= ElementCheckSum((char*)str.c_str());
	}

	return sum;
}