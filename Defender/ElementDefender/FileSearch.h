#pragma once
#ifndef __FILESEARCH_H__
#define __FILESEARCH_H__
#include <iostream>
#include <vector>
#include <windows.h>

extern int SearchDirectory(std::vector<std::string> &refvecFiles,
                    const std::string        &refcstrRootDirectory,
                    const std::string        &refcstrExtension,
                    bool                     bSearchSubdirectories = true);
#endif