// ElementDefender.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "ElementDefender.h"

void DefenderInit ()
{
	static char szPath[MAX_PATH];
	static char Error[MAX_PATH];
	GetConquerPath(szPath);
	strcat(szPath, "Conquer.exe");

	CheckJumpFiles();
	
#ifdef Publish
	DWORD sum;
	if ((sum = ElementCheckSum(szPath)) != CONQUER_EXE_CHECKSUM)
	{
#ifdef ALERT_FAILURE
		sprintf(Error, "Failed to preform checksum on %s, Produced Sum: %d", szPath, sum);
		MessageBoxA(NULL, Error, "Error", MB_OK);`	
#endif
		exit(0);
	}
#endif
}

void ResetAllEncryptions ()
{
	ResetSpellCrypto();
	ResetAuthCrypto();
	InstallProtection();
}