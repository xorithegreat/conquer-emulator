#include "StdAfx.h"
#include "Swordfish.h"

		const int Dec_IvecLength = 8;
		const int Enc_IvecLength = 8;
		const int InitializeKeyLength = 18;
		const int KeyLength = InitializeKeyLength;
        unsigned int InitializeKey[] = 
		{
			0x243f6a66, 0x85a308c3, 0x13198a2f, 0x03707348,
			0xa4093825, 0x299f31e0, 0x082efa92, 0xec4e6c83,
			0x452821a6, 0x38d01327, 0xbe5466df, 0x34e90c5c,
			0xc0ac29e7, 0xc97c50ad, 0x3f84d5b7, 0xb5470918,
			0x9216d5d1, 0x8979fb1c
        };
		__forceinline int RollLeft(unsigned int Value, unsigned char Roll, unsigned char Size)
        {
            Roll = (unsigned char)(Roll & 0x1F);
            return (int)((Value << Roll) | (Value >> (Size - Roll)));
        }
        __forceinline int RollRight(unsigned int Value, unsigned char Roll, unsigned char Size)
        {
            Roll = (unsigned char)(Roll & 0x1F);
            return (int)((Value << (Size - Roll)) | (Value >> Roll));
        }

		void CSwordfish::SFSmallCrypt(unsigned int& I, unsigned char* I2, unsigned int key)
		{
			I = (I ^ key) ^ ((Key[I2[3] % KeyLength] << 8) | Key[I2[1] % KeyLength]);
		}
		void CSwordfish::SFBigCrypt(unsigned int& l, unsigned int& r)
        {
                l = (l ^ Key[0]);
                void* Ptr_R = &r;
				void* Ptr_L = &l;
                {
                    for (unsigned char i = 0; i < 16; i++)
                    {
                        if (i % 2 == 0)
                            SFSmallCrypt(r, (unsigned char*)Ptr_L, Key[i]);
                        else
                            SFSmallCrypt(l, (unsigned char*)Ptr_R, Key[i]);
                    }
                }
                r = (r ^ Key[17]);
                unsigned int swap = l;
                l = r;
                r = swap;
        }
//
		CSwordfish::CSwordfish(unsigned char* userkey, int userkeyLength)
        {
            Key = new unsigned int[InitializeKeyLength];
            memcpy(Key, InitializeKey, InitializeKeyLength * sizeof(int));
            Dec_Ivec = new unsigned char[Dec_IvecLength];
            Enc_Ivec = new unsigned char[Enc_IvecLength];
			memset(Dec_Ivec, 0, Dec_IvecLength * sizeof(char));
			memset(Enc_Ivec, 0, Enc_IvecLength * sizeof(char));
            nDec = 0;
            nEnc = 0;

            for (unsigned char i = 0; i < userkeyLength; i++)
            {
                if (i % 2 == 0)
                    Key[i] = (unsigned int)RollLeft(Key[i], i, 32);
                else
                    Key[i] = (unsigned int)RollRight(Key[i], i, 32);
                Key[i] ^= userkey[i];
            }
            unsigned int l = 0, r = 0;
            for (int j = 0; j < KeyLength-1; j++)
            {
                SFBigCrypt(l, r);
                Key[j] ^= l;
                Key[j + 1] ^= r;
            }
        }
		CSwordfish::~CSwordfish()
		{
			delete[] Key;
			delete[] Dec_Ivec;
			delete[] Enc_Ivec;
		}

		void CSwordfish::SFEncrypt(unsigned char* Data, int DataLength)
        {
            for (int i = 0; i < DataLength; i++)
            {
                if (nEnc == 0)
                {
                    void* ptr = Enc_Ivec;
                    {
                        unsigned int l = *((unsigned int*)ptr);
                        unsigned int r = *((unsigned int*)ptr + 1);
                        SFBigCrypt(l, r);
                        *((unsigned int*)ptr) = l;
                        *((unsigned int*)ptr + 1) = r;
                    }
                }
                Data[i] ^= Enc_Ivec[nEnc];
                nEnc = (unsigned char)((nEnc + 1) % Enc_IvecLength);
            }
        }
		void CSwordfish::SFDecrypt(unsigned char* Data, int DataLength)
        {
            for (int i = 0; i < DataLength; i++)
            {
                if (nDec == 0)
                {
                    void* ptr = Dec_Ivec;
                    {
                        unsigned int l = *((unsigned int*)ptr);
                        unsigned int r = *((unsigned int*)ptr + 1);
                        SFBigCrypt(l, r);
                        *((unsigned int*)ptr) = l;
                        *((unsigned int*)ptr + 1) = r;
                    }
                }
                Data[i] ^= Dec_Ivec[nDec];
                nDec = (unsigned char)((nDec + 1) % Dec_IvecLength);
            }
        }
//