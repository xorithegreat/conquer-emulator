#ifndef __CHECK_SUM_H__
#define __CHECK_SUM_H__

extern DWORD ElementCheckSum (char* file);
extern DWORD ElementCheckSum (unsigned char* bytes, int length);
extern DWORD ElementCheckSum_Folder (char* path);

#endif