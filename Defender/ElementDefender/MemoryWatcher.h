#pragma once
#ifndef __MEMORY_WATCHER_H__
#define __MEMORY_WATCHER_H__
typedef void* MEMORY_ADDR;

class CMemoryWatcher
{
private:
	MEMORY_ADDR Address;
	int BlockSize;
	HANDLE hProcess;
	DWORD HashResult;
public:
	CMemoryWatcher(HANDLE process, MEMORY_ADDR addr, int blockSize);
	void MemorySetHashCode(DWORD Result);
	void MemorySetHashCode();
	void MemoryShowHashCode(char* szName);
	BOOL MemoryCheck();
	~CMemoryWatcher(void);
};
#endif