#ifndef __KEY_H__
#define __KEY_H__
struct Key_t
{
private: 
	DWORD x, y, z, w;
public:
	Key_t (DWORD X, DWORD Y, DWORD Z, DWORD W)
	{
		Reseed(X, Y, Z, W);
	}
	Key_t ()
	{
		Reseed (0, 0, 0, 0);
	}
	__forceinline void Reseed (DWORD X, DWORD Y, DWORD Z, DWORD W)
	{
		x = X;
		y = Y;
		z = Z;
		w = W;
	}
	__forceinline DWORD GenerateKey ()
	{
		DWORD t = (x ^ (x << 11));
        x = y; y = z; z = w;
        w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w;
	}
};
#endif