#ifndef __PROTECTION_H__
#define __PROTECTION_H__

extern DWORD __stdcall ProtectionThread (void* Arg);
extern void InstallProtection();
extern void CheckJumpFiles();
extern inline bool HideThread(HANDLE hThread);

#endif
