// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include "targetver.h"
#include <windows.h>
#include <stdio.h>
#include "Mask.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "ws2_32.lib")

#define NOP 0x90
#define CONQUER_EXE_CHECKSUM 3572346583

#define Publish
//#define Debug

#ifndef Debug
#define ALERT_FAILURE
#endif

#ifdef Debug
#define szServerIP "25.72.96.193"
#endif
#ifdef Publish
#define szServerIP "50.81.123.4"
#endif

extern void CryptoString (unsigned char* str, int len);
extern void GetConquerPath (char* szBuffer);

extern HANDLE hConquer;
extern DWORD ConquerID;
extern DWORD LastCycle;